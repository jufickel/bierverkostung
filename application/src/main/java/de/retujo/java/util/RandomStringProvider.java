/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

import java.util.Random;

import javax.annotation.concurrent.Immutable;

/**
 * This class generates a random string which is composed of capital letters between 'A' and 'Z'. The length of the
 * string is determined by the constructor argument.
 *
 * @since 1.2.0
 */
@Immutable
public final class RandomStringProvider implements Provider<String> {

    /**
     * The default string length if no other was specified.
     */
    static final byte DEFAULT_LENGTH = 5;

    private static final char CAPITAL_A = 65;
    private static final char CAPITAL_Z = 90;

    private final int stringLength;

    private RandomStringProvider(final int theStringLength) {
        stringLength = theStringLength;
    }

    /**
     * Returns an instance of {@code RandomStringProvider} which generates random string with the default length.
     *
     * @return the instance.
     * @see #DEFAULT_LENGTH
     */
    public static RandomStringProvider getInstance() {
        return new RandomStringProvider(DEFAULT_LENGTH);
    }

    /**
     * Returns an instance of {@code RandomStringProvider}.
     *
     * @param stringLength the length of the random string the returned instance produces.
     * @return the instance.
     */
    public static RandomStringProvider getInstance(final int stringLength) {
        return new RandomStringProvider(stringLength);
    }

    @Override
    public String get() {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            sb.append((char) (CAPITAL_A + random.nextInt(CAPITAL_Z - CAPITAL_A)));
        }
        return sb.toString();
    }

}
