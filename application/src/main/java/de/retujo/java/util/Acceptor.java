/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

/**
 * This interface represents an operation which accepts a single input value as argument and returns no result. Thus
 * implementations of this interface are expected to work via side-effects.
 *
 * @param <T> the type of the accepted value.
 *
 * @since 1.0.0
 */
@FunctionalInterface
public interface Acceptor<T> {

    /**
     * Performs this operation with the specified input.
     *
     * @param value the input argument.
     */
    void accept(T value);

}
