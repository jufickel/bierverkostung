/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

/**
 * This interface represents a generic function which produces for a given input an appropriate output.
 *
 * @param <I> the type of the input to the function.
 * @param <O> the type of the output the function produces.
 *
 * @since 1.2.0
 */
public interface Function<I, O> {

    /**
     * Applies this function to the given input.
     *
     * @param input the input to processed by this function.
     * @return the appropriate output of this function for the provided {@code input}.
     */
    O apply(I input);

}
