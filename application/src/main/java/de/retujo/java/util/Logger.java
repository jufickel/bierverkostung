/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

import android.util.Log;

import java.text.MessageFormat;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.BuildConfig;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A logger which respects the globally set build config to decide whether to log or not.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
public final class Logger {

    private final LoggerImplementation loggerImplementation;

    private Logger(final LoggerImplementation theLoggerImplementation) {
        loggerImplementation = theLoggerImplementation;
    }

    /**
     * Returns a new instance of {@code Logger} with the simple name of the specified Class as its tag.
     *
     * @param klasse the Class of which the simple name is the tag of the returned logger.
     * @return the instance.
     * @throws NullPointerException if {@code klasse} is {@code null}.
     */
    public static Logger forSimpleClassName(final Class<?> klasse) {
        return forTag(isNotNull(klasse, "class").getSimpleName());
    }

    /**
     * Returns a new instance of {@code Logger} for the specified tag.
     *
     * @param tag the tag of the returned logger.
     * @return the instance.
     * @throws NullPointerException if {@code tag} is {@code null}.
     * @throws IllegalArgumentException if {@code tag} is empty.
     */
    public static Logger forTag(final String tag) {
        return new Logger(BuildConfig.DEBUG ? new FullyEnabled(argumentNotEmpty(tag, "tag")) : new Disabled());
    }

    /**
     * Logs a message at the INFO level.
     *
     * @param format the format string.
     * @param args the optional arguments.
     * @see MessageFormat
     */
    public void info(final String format, final Object ... args) {
        loggerImplementation.info(format, args);
    }

    /**
     * Logs a message at the DEBUG level.
     *
     * @param format the format string.
     * @param args the optional arguments.
     * @see MessageFormat
     */
    public void debug(final String format, final Object ... args) {
        loggerImplementation.debug(format, args);
    }

    /**
     * Logs a message at the WARN level.
     *
     * @param format the format string.
     * @param args the optional arguments.
     * @see MessageFormat
     */
    public void warn(final String format, final Object... args) {
        loggerImplementation.warn(format, args);
    }

    /**
     * Logs a message at the ERROR level.
     *
     * @param format the format string.
     * @param args the optional arguments.
     * @see MessageFormat
     */
    public void error(final String format, final Object ... args) {
        loggerImplementation.error(format, args);
    }

    /**
     * Logs a message at the ERROR level.
     *
     * @param t the cause of the error.
     * @param format the format string.
     * @param args the optional arguments.
     * @see MessageFormat
     */
    public void error(final Throwable t, final String format, final Object ... args) {
        loggerImplementation.error(t, format, args);
    }

    private interface LoggerImplementation {
        void info(String format, Object ... args);

        void debug(String format, Object ... args);

        void warn(String format, Object ... args);

        void error(String format, Object ... args);

        void error(Throwable t, String format, Object ... args);
    }

    @Immutable
    private static final class Disabled implements LoggerImplementation {
        @Override
        public void info(final String format, final Object... args) {
            // Nothing to do.
        }

        @Override
        public void debug(final String format, final Object... args) {
            // Nothing to do.
        }

        @Override
        public void warn(final String format, final Object... args) {
            // Nothing to do.
        }

        @Override
        public void error(final String format, final Object... args) {
            // Nothing to do.
        }

        @Override
        public void error(final Throwable t, final String format, final Object... args) {
            // Nothing to do.
        }
    }

    @AllNonnull
    @Immutable
    private static final class FullyEnabled implements LoggerImplementation {
        private final String tag;

        private FullyEnabled(final String theTag) {
            tag = theTag;
        }

        @Override
        public void info(final String format, final Object... args) {
            Log.i(tag, format(format, args));
        }

        private static String format(final String pattern, final Object ... args) {
            argumentNotEmpty(pattern, "message pattern");
            isNotNull(args, "message args");
            return MessageFormat.format(pattern, args);
        }

        @Override
        public void debug(final String format, final Object... args) {
            Log.d(tag, format(format, args));
        }

        @Override
        public void warn(final String format, final Object... args) {
            Log.w(tag, format(format, args));
        }

        @Override
        public void error(final String format, final Object... args) {
            Log.e(tag, format(format, args));
        }

        @Override
        public void error(final Throwable t, final String format, final Object... args) {
            Log.e(tag, format(format, args), checkThrowable(t));
        }

        private static Throwable checkThrowable(final Throwable t) {
            return isNotNull(t, "Throwable");
        }
    }

}
