/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * This utility class offers methods for checking conditions.
 *
 * @since 1.0.0
 */
@Immutable
public final class Conditions {

    private Conditions() {
        throw new AssertionError();
    }

    /**
     * Checks if the given entity is {@code null}. If so, it throws an appropriate NullPointerException.
     *
     * @param entity the entity which must not be {@code null}.
     * @param entityName the name of the entity for the exception message if {@code entity} is {@code null}.
     * @return the original entity.
     * @throws NullPointerException if {@code entity} is {@code null}.
     */
    public static <T> T isNotNull(@Nullable final T entity, @Nonnull final String entityName) {
        if (null == entity) {
            throw new NullPointerException(MessageFormat.format("The <{0}> must not be null!", entityName));
        }
        return entity;
    }

    /**
     * Checks if the given argument is {@code null} or empty. If so, it throws an appropriate NullPointerException or
     * an IllegalArgumentException.
     *
     * @param argument the argument which must neither be {@code null} nor empty.
     * @param argumentName the name of the argument for the exception message if {@code argument} is {@code null} or
     *        empty.
     * @return the original argument.
     * @throws NullPointerException if @{@code argument} is {@code null}.
     * @throws IllegalArgumentException if {@code argument} is empty.
     */
    public static String argumentNotEmpty(@Nullable final String argument, @Nonnull final String argumentName) {
        isNotNull(argument, argumentName);
        if (argument.isEmpty()) {
            final String message = MessageFormat.format("The <{0}> must not be an empty String!", argumentName);
            throw new IllegalArgumentException(message);
        }
        return argument;
    }

    /**
     * Checks if the given argument is {@code null} or empty. If so, it throws an appropriate NullPointerException or
     * an IllegalArgumentException.
     *
     * @param argument the argument which must neither be {@code null} nor empty.
     * @param argumentName the name of the argument for the exception message if {@code argument} is {@code null} or
     *        empty.
     * @return the original argument.
     * @throws NullPointerException if {@code argument} is {@code null}.
     * @throws IllegalArgumentException if {@code argument} is empty.
     */
    public static CharSequence argumentNotEmpty(@Nullable final CharSequence argument, @Nonnull final String argumentName) {
        isNotNull(argument, argumentName);
        if (0 == argument.length()) {
            final String message = MessageFormat.format("The <{0}> must not be an empty CharSequence!", argumentName);
            throw new IllegalArgumentException(message);
        }
        return argument;
    }

}
