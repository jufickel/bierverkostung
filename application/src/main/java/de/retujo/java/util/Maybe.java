/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class is a container for an Object which may or may not be a {@code null} value.
 * If a value is not {@code null}, {@link #isPresent()} will return {@code true} and
 * {@code get()} will return the value.
 *
 * @param <T> the type of the contained value.
 *
 * @since 1.0.0
 */
public final class Maybe<T> {

    private final T value;

    private Maybe(final T theValue) {
        value = theValue;
    }

    /**
     * Returns a new instance of {@code Maybe} which contains the specified value. The value must not be {@code null}.
     *
     * @param value the value to be contained in the result.
     * @param <T> the type of the value.
     * @return the instance.
     * @throws NullPointerException if {@code value} is {@code null}.
     */
    @Nonnull
    public static <T> Maybe<T> of(@Nonnull final T value) {
        isNotNull(value, "value");
        return new Maybe<>(value);
    }

    /**
     * Returns a new instance of {@code Maybe} which contains the specified value. The value may be {@code null}.
     *
     * @param value the value to be contained in the result.
     * @param <T> the type of the value.
     * @return the instance.
     */
    @Nonnull
    public static <T> Maybe<T> ofNullable(@Nullable final T value) {
        return new Maybe<>(value);
    }

    /**
     * Returns an empty Maybe.
     *
     * @return an empty Maybe.
     */
    public static <T> Maybe<T> empty() {
        return new Maybe<>(null);
    }

    /**
     * Indicates whether this Maybe contains a value or not.
     *
     * @return {@code false} if this Maybe contains a value different from {@code null}, {@code true} else.
     */
    public boolean isAbsent() {
        return null == value;
    }

    /**
     * Indicates whether this Maybe contains a value or not.
     *
     * @return {@code true} if this Maybe contains a value different from {@code null}, {@code false} else.
     */
    public boolean isPresent() {
        return !isAbsent();
    }

    /**
     * If this Maybe contains a value this method provides the value to the specified acceptor.
     *
     * @param acceptor receives the value of this Maybe for further processing.
     * @throws NullPointerException if {@code acceptor} is {@code null}.
     * @since 1.2.0
     */
    public void ifPresent(@Nonnull final Acceptor<T> acceptor) {
        isNotNull(acceptor, "acceptor for the value");
        if (isPresent()) {
            acceptor.accept(value);
        }
    }

    /**
     * Returns the value of this Maybe.
     *
     * @return the value or {@code null}.
     * @throws NullPointerException if this Maybe is empty.
     */
    @Nonnull
    public T get() {
        if (isAbsent()) {
            throw new NullPointerException();
        }
        return value;
    }

    /**
     * Uses the specified function to map the value of this Maybe to another type.
     *
     * @param mappingFunction the function to map the value of this Maybe to another type.
     * @return a new Maybe containing the result of the mapping function.
     * @throws NullPointerException if {@code mappingFunction} is {@code null}.
     * @since 1.2.0
     */
    @Nonnull
    public <R> Maybe<R> map(@Nonnull final Function<? super T, ? extends R> mappingFunction) {
        isNotNull(mappingFunction, "mapping function");

        if (isPresent()) {
            return Maybe.ofNullable(mappingFunction.apply(value));
        } else {
            return Maybe.empty();
        }
    }

    /**
     * Same as {@link #get()} if the value is present. Otherwise the specified alternative result is returned.
     *
     * @param alternativeResult the value to return if this Maybe does not contain a value.
     * @return either the value of this Maybe &ndash; if present &ndash; or {@code alternativeResult}.
     */
    public T orElse(final T alternativeResult) {
        if (isPresent()) {
            return get();
        }
        return alternativeResult;
    }

    /**
     * Returns the value of this {@code Maybe} if present or else an alternative result which is provided by the
     * specified Provider.
     *
     * @param alternativeResultProvider a provider for an alternative result.
     * @return the alternative result by {@code alternativeResultProvider}.
     */
    public T orElseGet(final Provider<T> alternativeResultProvider) {
        if (isPresent()) {
            return get();
        }
        return alternativeResultProvider.get();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + "value=" + value + "}";
    }

}
