/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.java.util;

import java.util.Arrays;

import javax.annotation.Nullable;

/**
 * Utility class for high level operations on Objects.
 *
 * @since 1.0.0
 */
public final class ObjectUtil {

    private ObjectUtil() {
        throw new AssertionError();
    }

    /**
     * Returns {@code true} if the two given Objects are equal to each other, {@code false} otherwise.
     *
     * @param blue an Object.
     * @param green an Object.
     * @return {@code true} if {@code blue} is equal to {@code green}, {@code false} else.
     */
    public static boolean areEqual(@Nullable final Object blue, @Nullable final Object green) {
        if (null == blue) {
            return null == green;
        }
        return blue == green || blue.equals(green);
    }

    /**
     * Returns the hash code for the specified value or values.
     * <p>
     * When only a single object reference is supplied, the returned value equals the hash code of that object
     * reference.
     *
     * @param value a value to be hashed.
     * @param furtherValues further values to be hashed.
     * @return the hash code.
     */
    public static int hashCode(@Nullable final Object value, @Nullable final Object... furtherValues) {
        if (null != value) {
           if (null != furtherValues && 0 < furtherValues.length) {
               final Object[] values = new Object[1 + furtherValues.length];
               values[0] = value;
               System.arraycopy(furtherValues, 0, values, 1, furtherValues.length);
               return Arrays.hashCode(values);
           } else {
               return value.hashCode();
           }
        }
        return 0;
    }

}
