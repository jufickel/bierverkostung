/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Environment;

import java.io.File;

import javax.annotation.concurrent.NotThreadSafe;

/**
 * This class provides global application state.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class BierverkostungApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context = null;

    @SuppressWarnings("squid:S2696")
    @Override
    public void onCreate() {
        super.onCreate();
        BierverkostungApplication.context = getApplicationContext();
    }

    /**
     * Returns the <em>ApplicationContext</em> of the application.
     *
     * @return the application context.
     * @throws IllegalStateException if the context of the application was not initialised.
     * @see Context#getApplicationContext()
     */
    public static Context getContext() {
        if (null == context) {
            throw new IllegalStateException("The application Context was not initialised!");
        }
        return context;
    }

    /**
     * Returns the ContentResolver from the application context.
     *
     * @return the ContentResolver.
     * @see #getContext()
     */
    public static ContentResolver getAppContentResolver() {
        return getContext().getContentResolver();
    }

    /**
     * Returns the external files dir where the pictures of the application are stored.
     *
     * @return the directory File.
     * @throws IllegalStateException if the context of the application was not initialised.
     * @see Context#getExternalFilesDir(String)
     * @see Environment#DIRECTORY_PICTURES
     */
    public static File getPicturesDir() {
        return getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

}
