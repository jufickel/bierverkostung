/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.about.AboutFragment;
import de.retujo.bierverkostung.beer.SelectBeerFragment;
import de.retujo.bierverkostung.beerstyle.SelectBeerStyleFragment;
import de.retujo.bierverkostung.brewery.SelectBreweryFragment;
import de.retujo.bierverkostung.exchange.ExportImportDatabaseFragment;
import de.retujo.bierverkostung.tasting.SelectTastingFragment;

/**
 * This is the main Activity of the Bierverkostung application.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        drawerToggle =
                new ActionBarDrawerToggle(this, drawer, toolbar, R.string.main_drawer_open, R.string.main_drawer_close);
        drawer.addDrawerListener(drawerToggle);

        initMainContentView();
        initNavigationView();
    }

    private void initMainContentView() {
        switchMainContentView(R.id.nav_menu_tastings);
    }

    private void initNavigationView() {
        final NavigationView navigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this::switchMainContentView);
    }

    private boolean switchMainContentView(final MenuItem menuItem) {
        final Fragment mainContentViewFragment = switchMainContentView(menuItem.getItemId());
        if (null != mainContentViewFragment) {
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            drawer.closeDrawers();
            return true;
        }
        return false;
    }

    private Fragment switchMainContentView(final int menuItemId) {
        final Fragment result;
        switch (menuItemId) {
            case R.id.nav_menu_tastings:
                result = new SelectTastingFragment();
                break;
            case R.id.nav_menu_beers:
                result = new SelectBeerFragment();
                break;
            case R.id.nav_menu_beer_styles:
                result = new SelectBeerStyleFragment();
                break;
            case R.id.nav_menu_breweries:
                result = new SelectBreweryFragment();
                break;
            case R.id.nav_menu_about:
                result = new AboutFragment();
                break;
            case R.id.nav_menu_export_import_database:
                result = new ExportImportDatabaseFragment();
                break;
            default:
                result = null;
                break;
        }

        if (null != result) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_main_content_frame, result)
                    .commit();
        }

        return result;
    }


    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

}
