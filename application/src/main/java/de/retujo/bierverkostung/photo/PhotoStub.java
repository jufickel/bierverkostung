/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.photo;

import android.net.Uri;

import java.util.UUID;

/**
 * This interface represents a stub of a photo. It is a simple means of managing photo information without the
 * complexity of an associated file.
 *
 * @since 1.2.0
 */
public interface PhotoStub {

    /**
     * Returns the ID of this photo stub.
     *
     * @return the ID.
     */
    UUID getId();

    /**
     * Returns the URI of this photo stub.
     *
     * @return the URI.
     */
    Uri getUri();

    /**
     * Returns the name of the photo stub.
     *
     * @return the name.
     */
    String getName();

    /**
     * Returns the status of this photo stub.
     *
     * @return the status.
     */
    PhotoStatus getStatus();

}
