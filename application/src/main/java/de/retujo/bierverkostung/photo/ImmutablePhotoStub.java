/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.photo;

import android.net.Uri;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * An immutable implementation of {@link PhotoStub}.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class ImmutablePhotoStub implements PhotoStub {

    private final UUID id;
    private final Uri uri;
    private final PhotoStatus status;

    private ImmutablePhotoStub(final UUID theId, final Uri theUri, final PhotoStatus theStatus) {
        id = isNotNull(theId, "ID");
        uri = isNotNull(theUri, "URI");
        status = isNotNull(theStatus, "status");
    }

    /**
     * Returns an instance of {@code ImmutablePhotoStub}.
     *
     * @param id the ID of the photo stub.
     * @param uri the URI of the photo stub.
     * @param status the status of the photo stub.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static ImmutablePhotoStub getInstance(final UUID id, final Uri uri, final PhotoStatus status) {
        return new ImmutablePhotoStub(id, uri, status);
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public Uri getUri() {
        return uri;
    }

    @Override
    public String getName() {
        return uri.getLastPathSegment();
    }

    @Override
    public PhotoStatus getStatus() {
        return status;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ImmutablePhotoStub that = (ImmutablePhotoStub) o;
        return ObjectUtil.areEqual(id, that.id) && ObjectUtil.areEqual(uri, that.uri) && status == that.status;
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(id, uri, status);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "id=" + id +
                ", uri=" + uri +
                ", status=" + status +
                "}";
    }

}
