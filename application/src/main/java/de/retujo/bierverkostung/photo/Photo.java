/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.photo;

import android.net.Uri;

import java.util.UUID;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.AllNonnull;

/**
 * This interface represents a photo. A Photo is basically the same as a {@link PhotoStub} but additionally it has an
 * associated {@link PhotoFile} and is a {@link Jsonable} {@link DataEntity}.
 *
 * @since 1.2.0
 */
@AllNonnull
public interface Photo extends PhotoStub, DataEntity, Jsonable {

    /**
     * Returns the representation of the physical file of the photo.
     *
     * @return the photo file.
     */
    PhotoFile getFile();

    /**
     * Sets the specified PhotoFile to a copy of this {@code Photo} object. <em>Note:</em> the revision will not be
     * incremented.
     *
     * @param newPhotoFile the PhotoFile to be set.
     * @return a copy of this Photo object with {@code newPhotoFile} set or this Photo object if it already has the
     * same file.
     * @throws NullPointerException if {@code newPhotoFile} is {@code null}.
     */
    Photo setFile(PhotoFile newPhotoFile);

    /**
     * Returns the ID of the entity to which this Photo belongs.
     *
     * @return the reference ID.
     */
    UUID getReferenceId();

    /**
     * Sets the specified reference ID to a copy of this {@code Photo} object. <em>Note:</em> the revision will not be
     * incremented.
     *
     * @param newReferenceId the reference ID to be set.
     * @return a copy of this Photo object with {@code newReferenceId} set or this Photo object if it already has the
     * same reference ID.
     * @throws NullPointerException if {@code newReferenceId} is {@code null}.
     */
    Photo setReferenceId(UUID newReferenceId);

    /**
     * Returns the URI of this photo.
     *
     * @return the URI.
     */
    Uri getUri();

    /**
     * Returns the name of the photo. This is the name of the photo file.
     *
     * @return the name.
     * @see PhotoFile#getFileName()
     */
    String getName();

    /**
     * Returns the status of this photo.
     *
     * @return the status.
     */
    PhotoStatus getStatus();

}
