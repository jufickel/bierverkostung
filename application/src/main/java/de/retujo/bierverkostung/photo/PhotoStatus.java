package de.retujo.bierverkostung.photo;

/**
 * An enumeration of statuses of photos.
 *
 * @since 1.2.0
 */
public enum PhotoStatus {

    /**
     * Denotes a photo for which an entry in the database exists.
     */
    EXISTING,

    /**
     * Denotes a photo which should not be persisted in the database.
     */
    TRANSIENT,

    /**
     * Denotes a photo which is not yet persisted in the database.
     */
    PENDING

}
