/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.photo;

import android.net.Uri;

import java.util.UUID;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;

/**
 * A factory for creating immutable instances of the interfaces the photo package provides.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
public final class PhotoFactory {

    private PhotoFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link PhotoStub}.
     * @param uri the URI of the photo stub.
     * @param status the status of the photo stub.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static PhotoStub newPhotoStub(final Uri uri, final PhotoStatus status) {
        return ImmutablePhotoStub.getInstance(UUID.randomUUID(), uri, status);
    }

}
