/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.about;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

/**
 * Shows information about this application.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class AboutFragment extends Fragment {

    /**
     * Constructs a new {@code AboutFragment} object. This constructor is required by Android.
     */
    public AboutFragment() {
        super();
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.fragment_about, null);
        initVersion(result);
        return result;
    }

    private static void initVersion(final View parentView) {
        final TextView versionNameTextView = (TextView) parentView.findViewById(R.id.about_version_name);
        final String versionName = tryToGetVersionName(parentView.getContext());
        if (null != versionName) {
            versionNameTextView.setText(versionName);
        } else {
            versionNameTextView.setVisibility(View.GONE);
            final View versionNameLabelTextView = parentView.findViewById(R.id.about_version_name_label);
            versionNameLabelTextView.setVisibility(View.GONE);
        }
    }

    private static String tryToGetVersionName(final Context context) {
        try {
            return getVersionName(context);
        } catch (final PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private static String getVersionName(final Context context) throws PackageManager.NameNotFoundException {
        final PackageManager packageManager = context.getPackageManager();
        final PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
        return packageInfo.versionName;
    }

}
