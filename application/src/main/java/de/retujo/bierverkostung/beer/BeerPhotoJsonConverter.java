/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.bierverkostung.photo.PhotoStatus;
import de.retujo.java.util.AllNonnull;

/**
 * Converts a {@link BeerPhoto} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class BeerPhotoJsonConverter extends DataEntityJsonConverter<BeerPhoto> {

    @Immutable
    static final class JsonName {
        static final String BEER_ID = "beer_id";
        static final String PHOTO_FILE = "photo_file";
        static final String STATUS = "status";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private BeerPhotoJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code BeerPhotoJsonConverter}.
     *
     * @return the instance.
     */
    public static BeerPhotoJsonConverter getInstance() {
        return new BeerPhotoJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final BeerPhoto photo) throws JSONException {
        targetJsonObject.put(JsonName.BEER_ID, photo.getBeerId().toString());
        targetJsonObject.put(JsonName.PHOTO_FILE, photo.getFile().toJson());
        targetJsonObject.put(JsonName.STATUS, photo.getStatus().name());
    }

    @Override
    protected BeerPhoto createEntityInstanceFromJson(final JSONObject sourceJsonObject,
            final EntityCommonData commonData) throws JSONException {

        final UUID beerId = UUID.fromString(sourceJsonObject.getString(JsonName.BEER_ID));
        final PhotoFile photoFile = getBeerPhotoFile(sourceJsonObject);
        final PhotoStatus photoStatus = PhotoStatus.valueOf(sourceJsonObject.getString(JsonName.STATUS));

        return BeerPhoto.getInstance(commonData, beerId, photoFile, photoStatus);
    }

    private static PhotoFile getBeerPhotoFile(final JSONObject sourceJsonObject) throws JSONException {
        final JsonConverter<BeerPhotoFile> jsonConverter = BeerPhotoFileJsonConverter.getInstance();
        return jsonConverter.fromJson(sourceJsonObject.getJSONObject(JsonName.PHOTO_FILE));
    }

}
