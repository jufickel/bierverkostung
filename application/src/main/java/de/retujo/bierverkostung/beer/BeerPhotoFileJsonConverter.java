/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.java.util.AllNonnull;

/**
 * Converts a {@link BeerPhotoFile} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class BeerPhotoFileJsonConverter extends DataEntityJsonConverter<BeerPhotoFile> {

    @Immutable
    private static final class JsonName {
        private static final String NAME = "name";

        private JsonName() {
            throw new AssertionError();
        }
    }

    /**
     * Returns an instance of {@code BeerPhotoFileJsonConverter}.
     *
     * @return the instance.
     */
    public static JsonConverter<BeerPhotoFile> getInstance() {
        return new BeerPhotoFileJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final BeerPhotoFile beerPhotoFile)
            throws JSONException {

        targetJsonObject.put(JsonName.NAME, beerPhotoFile.getFileName());
    }

    @Override
    protected BeerPhotoFile createEntityInstanceFromJson(final JSONObject sourceJsonObject,
            final EntityCommonData commonData) throws JSONException {

        return BeerPhotoFile.getInstance(commonData, sourceJsonObject.getString(JsonName.NAME));
    }

}
