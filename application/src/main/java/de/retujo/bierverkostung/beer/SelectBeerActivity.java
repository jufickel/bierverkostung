/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;

/**
 * This Activity shows a list of all persisted beers. The user has the possibility to add a new beer and to delete or
 * edit beers by swiping them out of the list.
 * <p>
 * A short click on a beer item finishes the Activity and sets the selected beer as extra in the result Intent.
 * The key to obtain the selected beer is <code>"{@value #SELECTED_BEER}"</code>.
 * </p>
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class SelectBeerActivity extends BaseActivity {

    /**
     * Key to retrieve the selected beer from this Activity's result Intent.
     */
    public static final String SELECTED_BEER = "selectedBeer";

    private static final int NEW_BEER_REQUEST_CODE = 66;

    /**
     * Constructs a new {@code SelectBeerActivity} object.
     */
    public SelectBeerActivity() {
        super();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_beer);

        final Fragment selectBeerFragment = SelectBeerFragment.newInstance(view -> {
            final Beer beer = (Beer) view.getTag();
            finishWithResult(beer);
        });
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_select_beer_container, selectBeerFragment)
                .commit();
    }

    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // This overwrites the default OnClickListener which was already registered by the Fragment.
        initAddBeerButton();
    }

    private void initAddBeerButton() {
        final FloatingActionButton addBeerButton = findView(R.id.select_beer_fab_new_beer);
        addBeerButton.setOnClickListener(v -> {
            final Intent addBeerIntent = new Intent(SelectBeerActivity.this, EditBeerActivity.class);
            startActivityForResult(addBeerIntent, NEW_BEER_REQUEST_CODE);
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (NEW_BEER_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode) {
            final Beer newBeer = data.getParcelableExtra(EditBeerActivity.EDITED_BEER);
            finishWithResult(newBeer);
        }
    }

    private void finishWithResult(final Beer selectedBeer) {
        final Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_BEER, selectedBeer);
        setResult(Activity.RESULT_OK, returnIntent);

        finish();
    }

}
