/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.DateUtil;
import de.retujo.bierverkostung.common.ProgressUpdate;
import de.retujo.bierverkostung.common.ProgressUpdateFactory;
import de.retujo.bierverkostung.data.BierverkostungContract;
import de.retujo.bierverkostung.exchange.ExportImportException;
import de.retujo.bierverkostung.exchange.FileNameReviser;
import de.retujo.bierverkostung.exchange.ZipFileWriter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTask exports a single Beer, multiple Beers or all Beers into a ZIP file together with the associated
 * Photos of the Beer(s).
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class BeerExporter extends AsyncTask<Beer, ProgressUpdate<Beer>, Void> {

    static final String PHOTO_FILENAME_PREFIX = "photos/";
    static final String BEER_JSON_FILENAME_PREFIX = "Beer_";
    static final String BEER_JSON_FILENAME_SUFFIX = ".json";

    private static final String ZIP_FILE_PREFIX = "Beers_";
    private static final String ZIP_FILE_SUFFIX = ".zip";

    @SuppressLint("StaticFieldLeak") // Is Application Context.
    private final Context context;
    private final ContentResolver contentResolver;
    private final File selectedOutputDirectory;
    @Nullable private final Acceptor<ProgressUpdate<Beer>> progressUpdateAcceptor;

    private BeerExporter(final Context theContext, final File theSelectedOutputDirectory,
            @Nullable final Acceptor<ProgressUpdate<Beer>> theProgressUpdateAcceptor) {

        context = theContext;
        contentResolver = theContext.getContentResolver();
        selectedOutputDirectory =  theSelectedOutputDirectory;
        progressUpdateAcceptor = theProgressUpdateAcceptor;
    }

    /**
     * Returns a new instance of {@code BeerExporter}.
     *
     * @param context the context which provides the content resolver for getting all Beers from persistence.
     * @param selectedDirectory the target directory where the ZIP file to be created is located.
     * @param progressUpdateAcceptor if provided this object receives progress updates of the export task.
     * @return the instance.
     * @throws NullPointerException if any argument but {@code progressUpdateAcceptor} is {@code null}.
     */
    public static BeerExporter newInstance(final Context context, final File selectedDirectory,
            @Nullable final Acceptor<ProgressUpdate<Beer>> progressUpdateAcceptor) {

        isNotNull(context, "context");
        isNotNull(context, "selected directory");

        return new BeerExporter(context.getApplicationContext(), selectedDirectory, progressUpdateAcceptor);
    }

    @Override
    protected Void doInBackground(final Beer... beers) {
        if (null != beers && 0 < beers.length) {
            exportBeers(beers);
        } else {
            exportAllBeers();
        }

        return null;
    }

    private void exportBeers(final Beer[] beers) {
        final File outputZipFile;
        final int toBeExportedAmount = beers.length;
        if (1 == toBeExportedAmount) {
            outputZipFile = getZipFileForSingleBeer(beers[0]);
        } else {
            outputZipFile = getZipFileForMultipleBeers();
        }
        final ZipFileWriter zipFileWriter = ZipFileWriter.getInstance(outputZipFile);
        try {
            for (int i = 0; i < toBeExportedAmount; i++) {
                final Beer beer = beers[i];
                exportBeer(beer, zipFileWriter);
                publishProgress(ProgressUpdateFactory.determinate(i + 1, toBeExportedAmount, beer));
            }
        } finally {
            tryToClose(zipFileWriter);
        }
    }

    private File getZipFileForSingleBeer(final Beer beer) {
        final String beerFileName = getBeerFileName(beer);
        return new File(selectedOutputDirectory, beerFileName + ZIP_FILE_SUFFIX);
    }

    private static String getBeerFileName(final Beer beer) {
        final String beerId = String.valueOf(beer.getId());
        final String[] beerIdSegments = beerId.split("-");

        return BEER_JSON_FILENAME_PREFIX + beerIdSegments[0] + "_" + getRevisedBeerName(beer);
    }

    private static String getRevisedBeerName(final Beer beer) {
        final FileNameReviser fileNameReviser = FileNameReviser.getInstance();
        return fileNameReviser.apply(beer.getName());
    }

    private File getZipFileForMultipleBeers() {
        final Date currentDateTime = new Date(System.currentTimeMillis());
        final DateUtil dateUtil = DateUtil.getInstance(context);
        final String dateTimeLongIsoString = dateUtil.getLongIsoString(currentDateTime);

        return new File(selectedOutputDirectory, ZIP_FILE_PREFIX + dateTimeLongIsoString + ZIP_FILE_SUFFIX);
    }

    private static void exportBeer(@Nullable final Beer beer, final ZipFileWriter zipFileWriter) {
        if (null == beer) {
            return;
        }

        final String beerFileName = getBeerFileName(beer);
        zipFileWriter.writeEntry(beerFileName + BEER_JSON_FILENAME_SUFFIX, beer);
        for (final Photo photo : beer.getPhotos()) {
            final PhotoFile photoFile = photo.getFile();
            zipFileWriter.writeEntry(PHOTO_FILENAME_PREFIX + photoFile.getFileName(), photoFile.getFile());
        }
    }

    private static void tryToClose(final ZipFileWriter zipFileWriter) {
        try {
            zipFileWriter.close();
        } catch (final IOException e) {
            throw new ExportImportException("Failed to close ZipFileWriter!", e);
        }
    }

    private void exportAllBeers() {
        final Cursor beersCursor = queryAllBeers();
        if (null != beersCursor) {
            exportAllBeers(beersCursor);
        }
    }

    @Nullable
    private Cursor queryAllBeers() {
        return contentResolver.query(BierverkostungContract.BeerEntry.TABLE.getContentUri(), null, null, null, null);
    }

    private void exportAllBeers(final Cursor beersCursor) {
        final File outputZipFile = getZipFileForMultipleBeers();
        final ZipFileWriter zipFileWriter = ZipFileWriter.getInstance(outputZipFile);
        final int toBeExportedAmount = beersCursor.getCount();
        try {
            int i = 0;
            do {
                @Nullable final Beer beer = getBeerForExport(beersCursor);
                exportBeer(beer, zipFileWriter);
                ++i;
                publishProgress(ProgressUpdateFactory.determinate(i, toBeExportedAmount, beer));
            } while (beersCursor.moveToNext());
        } finally {
            tryToClose(zipFileWriter);
        }
    }

    @Nullable
    private static Beer getBeerForExport(final Cursor beersCursor) {
        return BeerFactory.newBeerFromCursor(beersCursor);
    }

    @SafeVarargs
    @Override
    protected final void onProgressUpdate(final ProgressUpdate<Beer>... progressUpdates) {
        super.onProgressUpdate(progressUpdates);
        if (null != progressUpdateAcceptor) {
            progressUpdateAcceptor.accept(progressUpdates[0]);
        }
    }

}
