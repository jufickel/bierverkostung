/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.ProgressUpdate;
import de.retujo.bierverkostung.common.ProgressUpdateFactory;
import de.retujo.bierverkostung.exchange.JsonObjectOutputStream;
import de.retujo.bierverkostung.exchange.ZipFileReader;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Logger;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTask reads provided ZIP files and tries to convert the ZIP file entries to Beer and Photo entities and
 * insert them into the database. Existing entities with a less revision will be overwritten, otherwise the existing
 * entities are used and the import of that entities is skipped.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class BeerZipFileReader extends AsyncTask<File, ProgressUpdate<Beer>, List<Beer>>
        implements ZipFileReader.ZipFileConsumer {

    private static final Pattern PHOTO_FILENAME_PATTERN = Pattern.compile("(" + BeerExporter.PHOTO_FILENAME_PREFIX +
            ")(.*)");
    private static final Logger LOGGER = Logger.forSimpleClassName(BeerZipFileReader.class);

    private final ContentResolver contentResolver;
    @Nullable private final Acceptor<ProgressUpdate<Beer>> progressUpdateAcceptor;
    private final List<Beer> importedBeers;

    private BeerZipFileReader(final ContentResolver theContentResolver,
            @Nullable final Acceptor<ProgressUpdate<Beer>> theProgressUpdateAcceptor) {

        contentResolver = isNotNull(theContentResolver, "ContentResolver");
        progressUpdateAcceptor = theProgressUpdateAcceptor;
        importedBeers = new ArrayList<>();
    }

    /**
     * Returns an instance of {@code BeerZipFileReader}.
     *
     * @param contentResolver the ContentResolver for accessing the database.
     * @param progressUpdateAcceptor if provided this object receives progress updates of the import task.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static BeerZipFileReader getInstance(final ContentResolver contentResolver,
            @Nullable final Acceptor<ProgressUpdate<Beer>> progressUpdateAcceptor) {

        return new BeerZipFileReader(contentResolver, progressUpdateAcceptor);
    }


    @Override
    protected List<Beer> doInBackground(final File... zipFiles) {
        for (final File zipFile : zipFiles) {
            final ZipFileReader zipFileReader = ZipFileReader.getInstance(zipFile);
            zipFileReader.run(this);
        }
        return importedBeers;
    }

    @Override
    public Maybe<OutputStream> getOutputStreamFor(final String fileName) throws IOException {
        if (isBeerJsonFile(fileName)) {
            LOGGER.debug("Returning BeerJsonFileImporter for <{0}>.", fileName);
            return Maybe.of(new BeerJsonFileImporter(JsonObjectOutputStream.getInstance()));
        }
        final Matcher photoFilenameMatcher = PHOTO_FILENAME_PATTERN.matcher(fileName);
        if (photoFilenameMatcher.matches()) {
            final PhotoFile beerPhotoFile = BeerFactory.newBeerPhotoFile(photoFilenameMatcher.group(2));
            final File targetFile = beerPhotoFile.getFile();
            if (!targetFile.exists() && createTargetFile(targetFile)) {
                LOGGER.debug("Returning FileOutputStream for <{0}>.", targetFile);
                return Maybe.of(new FileOutputStream(targetFile));
            }
        }
        return Maybe.empty();
    }

    private static boolean isBeerJsonFile(final String fileName) {
        return fileName.startsWith(BeerExporter.BEER_JSON_FILENAME_PREFIX) &&
                fileName.endsWith(BeerExporter.BEER_JSON_FILENAME_SUFFIX);
    }

    private static boolean createTargetFile(final File file) throws IOException {
        final File directory = file.getParentFile();
        if (!directory.exists()) {
            return directory.mkdirs() && file.createNewFile();
        }
        return file.createNewFile();
    }

    @Override
    public void zipFileRead() {
        // Nothing to do.
    }

    @SafeVarargs
    @Override
    protected final void onProgressUpdate(final ProgressUpdate<Beer>... values) {
        super.onProgressUpdate(values);
        if (null != progressUpdateAcceptor) {
            progressUpdateAcceptor.accept(values[0]);
        }
    }

    @Override
    protected void onPostExecute(final List<Beer> beers) {
        if (null != progressUpdateAcceptor) {
            final int importedBeersCount = beers.size();
            progressUpdateAcceptor.accept(ProgressUpdateFactory.determinate(importedBeersCount, importedBeersCount,
                    null));
        }
    }

    private final class BeerJsonFileImporter extends OutputStream {
        private final JsonObjectOutputStream jsonObjectOutputStream;

        private BeerJsonFileImporter(final JsonObjectOutputStream theJsonObjectOutputStream) {
            jsonObjectOutputStream = theJsonObjectOutputStream;
        }

        @Override
        public void write(final int b) {
            jsonObjectOutputStream.write(b);
        }

        @Override
        public void write(@NonNull final byte[] b, final int off, final int len) {
            jsonObjectOutputStream.write(b, off, len);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void close() throws IOException {
            jsonObjectOutputStream.close();
            final JSONObject beerJsonObject = jsonObjectOutputStream.toJson();
            LOGGER.debug("Read Beer JSON file: {0}", beerJsonObject);

            final BeerImporter beerImporter = BeerImporter.getInstance(contentResolver);
            final Beer importedBeer = beerImporter.importOrGetExisting(BeerFactory.newBeer(beerJsonObject));
            importedBeers.add(importedBeer);
            publishProgress(ProgressUpdateFactory.indeterminate(1, 0, importedBeer));
        }

    }

}
