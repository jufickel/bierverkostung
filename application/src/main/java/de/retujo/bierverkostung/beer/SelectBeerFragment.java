/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import javax.annotation.concurrent.ThreadSafe;

import de.retujo.bierverkostung.MainActivity;
import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractItemSwipeHandler;
import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.common.HideFabOnScrollListener;
import de.retujo.bierverkostung.common.Toaster;
import de.retujo.bierverkostung.exchange.FileChooserDialogue;
import de.retujo.java.util.AllNonnull;

/**
 * This Fragment shows a list of all persisted beers. The user has the possibility to add a new beer or delete beers by
 * swiping them off the list.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class SelectBeerFragment extends Fragment {

    private FloatingActionButton addBeerButton;
    private RecyclerView beersRecyclerView;
    private View.OnClickListener onBeerSelectedListener;

    /**
     * Constructs a new {@code SelectBeerActivity} object.
     */
    public SelectBeerFragment() {
        addBeerButton = null;
        beersRecyclerView = null;
        onBeerSelectedListener = null;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Returns a new instance of {@code SelectBeerFragment}.
     *
     * @param onBeerSelectedListener a listener to be notified if a beer item is select or {@code null}. If no
     * listener is specified, a default listener will be used which shows the details of the selected beer.
     * @return the instance.
     */
    @Nonnull
    public static SelectBeerFragment newInstance(@Nullable final View.OnClickListener onBeerSelectedListener) {
        final SelectBeerFragment result = new SelectBeerFragment();
        result.onBeerSelectedListener = onBeerSelectedListener;
        return result;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        final View result = inflater.inflate(R.layout.fragment_select_beer, null);

        if (null == onBeerSelectedListener) {
            onBeerSelectedListener = new DefaultOnBeerSelectedListener();
        }

        initAddBeerButton(result);

        final BeerCursorAdapter cursorAdapter = new BeerCursorAdapter(getContext(), onBeerSelectedListener);
        final AbstractLoaderCallbacks cursorLoaderCallbacks = initBeersLoader(cursorAdapter);
        initBeersRecyclerView(result, cursorAdapter, cursorLoaderCallbacks);

        return result;
    }

    private void initAddBeerButton(final View parentView) {
        addBeerButton = (FloatingActionButton) parentView.findViewById(R.id.select_beer_fab_new_beer);
        addBeerButton.setOnClickListener(v -> {
            final Intent addBeerIntent = new Intent(getContext(), EditBeerActivity.class);
            startActivity(addBeerIntent);
        });
    }

    private AbstractLoaderCallbacks initBeersLoader(final BeerCursorAdapter cursorAdapter) {
        final BeerLoaderCallbacks result = new BeerLoaderCallbacks(getContext(), cursorAdapter);
        final LoaderManager supportLoaderManager = getLoaderManager();
        supportLoaderManager.initLoader(result.getId(), null, result);
        return result;
    }

    private void initBeersRecyclerView(final View parentView, final BeerCursorAdapter cursorAdapter,
            final AbstractLoaderCallbacks cursorLoaderCallbacks) {

        beersRecyclerView = (RecyclerView) parentView.findViewById(R.id.select_beer_recycler_view);
        beersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        beersRecyclerView.setAdapter(cursorAdapter);
        beersRecyclerView.addOnScrollListener(HideFabOnScrollListener.of(addBeerButton));

        final SimpleCallback deleteBeerOnSwipeHandler = new BeerItemSwipeHandler(cursorLoaderCallbacks);
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(deleteBeerOnSwipeHandler);
        itemTouchHelper.attachToRecyclerView(beersRecyclerView);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.select_beer_options_menu, menu);
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            menu.removeItem(R.id.select_beer_options_menu_import);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final Context context = getContext();
        if (R.id.select_beer_options_menu_import == item.getItemId()) {
            FileChooserDialogue.forFile(context, Environment.getExternalStorageDirectory(), "zip")
                    .setOnFileSelectedListener(selectedFile -> {
                        final ImportBeerNotificator importNotificator =
                                ImportBeerNotificator.getInstance(context, new Intent(context, MainActivity.class));
                        final BeerZipFileReader zipFileReader =
                                BeerZipFileReader.getInstance(context.getContentResolver(), importNotificator::update);
                        zipFileReader.execute(selectedFile);
                    })
                    .show();
            return true;
        } else if (R.id.select_beer_options_menu_export == item.getItemId()) {
            FileChooserDialogue.forDirectory(context, Environment.getExternalStorageDirectory())
                    .setOnFileSelectedListener(selectedDirectory -> {
                        final ExportBeerNotificator exportNotificator = ExportBeerNotificator.getInstance(context,
                                new Intent(context, MainActivity.class));
                        final BeerExporter beerExporter = BeerExporter.newInstance(context, selectedDirectory,
                                exportNotificator::update);
                        beerExporter.execute();
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Without the following code the item slot would be empty after starting edit activity but not saving.
        final RecyclerView.Adapter beersRecyclerViewAdapter = beersRecyclerView.getAdapter();
        beersRecyclerViewAdapter.notifyDataSetChanged();
    }

    /**
     * This listener reacts when a beer was selected. It then starts an Activity which shows the details of the
     * selected beer.
     *
     * @since 1.0.0
     */
    @ThreadSafe
    private final class DefaultOnBeerSelectedListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final Beer beer =  (Beer) v.getTag();
            final Intent showBeerIntent = new Intent(getContext(), ShowBeerActivity.class);
            showBeerIntent.putExtra(ShowBeerActivity.BEER, beer);
            startActivity(showBeerIntent);
        }
    }

    /**
     * This class deletes a beer when the user swipes it from the list. Beforehand the deletion has to be verified by
     * the user; this is done with an AlertDialog.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @NotThreadSafe
    private final class BeerItemSwipeHandler extends AbstractItemSwipeHandler<Beer> {
        /**
         * Constructs a new {@code BeerItemSwipeHandler} object.
         *
         * @param theLoaderCallbacks are called when a beer gets deleted.
         * @throws NullPointerException if {@code theLoaderCallbacks} is {@code null}.
         */
        private BeerItemSwipeHandler(final AbstractLoaderCallbacks theLoaderCallbacks) {
            super(getContext(), getLoaderManager(), theLoaderCallbacks);
        }

        @Override
        protected int getDialogTitle() {
            return R.string.delete_beer_dialog_title;
        }

        @Override
        protected String getDialogMessage(final Beer beer) {
            return MessageFormat.format(getString(R.string.delete_beer_dialog_message), beer.getName());
        }

        @Override
        protected void onDeleteEntityFailed(final Beer beer) {
            Toaster.showLong(getContext(), R.string.delete_beer_dialog_failed_to_delete, beer.getName());
        }

        @Override
        protected void onEntityDeleted(final Beer deletedBeer) {
            DeleteBeerPhotosTask.getInstance(getContentResolver()).execute(deletedBeer);
        }
    }

}
