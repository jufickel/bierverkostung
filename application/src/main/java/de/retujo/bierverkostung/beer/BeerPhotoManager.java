/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoFileEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.CopyFileException;
import de.retujo.bierverkostung.exchange.FileCopier;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.bierverkostung.photo.PhotoStatus;
import de.retujo.bierverkostung.photo.PhotoStub;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Logger;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * TODO Javadoc.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class BeerPhotoManager {

    private static final Logger LOGGER = Logger.forSimpleClassName(BeerPhotoManager.class);

    private final ContentResolver contentResolver;
    private final UUID beerId;
    private final Map<Uri, PersistPhotoFlow> persistPhotoFlows;
    private final Map<Uri, DeletePhotoFlow> deletePhotoFlows;

    private BeerPhotoManager(final ContentResolver theContentResolver, final UUID theBeerId) {
        contentResolver = theContentResolver;
        beerId = theBeerId;

        persistPhotoFlows = new HashMap<>(PhotoGridViewAdapter.MAX_NUMBER_PHOTOS);
        deletePhotoFlows = new HashMap<>(PhotoGridViewAdapter.MAX_NUMBER_PHOTOS);
    }

    /**
     * Returns an instance of {@code BeerPhotoManager}.
     *
     * @param contentResolver the ContentResolver to be used for database interactions.
     * @param beerId the ID of the beer to manage photos for.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static BeerPhotoManager getInstance(final ContentResolver contentResolver, final UUID beerId) {
        return new BeerPhotoManager(contentResolver, beerId);
    }

    public void putPending(final PhotoStub photoStub) {
        persistPhotoFlows.put(isNotNull(photoStub, "stub of the photo").getUri(),
                new PersistPhotoFlow(contentResolver, beerId, photoStub));
    }

    public void removePending(final Uri photoUri) {
        persistPhotoFlows.remove(photoUri);
    }

    public boolean hasPending() {
        return 0 < persistPhotoFlows.size();
    }

    public void deleteExisting(final PhotoStub photoStub) {
        deletePhotoFlows.put(isNotNull(photoStub, "URI of the existing photo stub").getUri(),
                new DeletePhotoFlow(contentResolver, photoStub, beerId));
    }

    public List<Photo> persistAllPhotos() {
        final Map<Uri, Photo> persisted = new HashMap<>(persistPhotoFlows.size());
        for (final Map.Entry<Uri, PersistPhotoFlow> entry : persistPhotoFlows.entrySet()) {
            final PersistPhotoFlow persistPhotoFlow = entry.getValue();
            persisted.put(entry.getKey(), persistPhotoFlow.persist());
        }
        try {
            return new ArrayList<>(persisted.values());
        } finally {
            for (final Uri uri : persisted.keySet()) {
                persistPhotoFlows.remove(uri);
            }
        }
    }

    public List<PhotoStub> deleteAllPhotos() {
        final List<PhotoStub> result = new ArrayList<>(deletePhotoFlows.size());
        for (final DeletePhotoFlow deletePhotoFlow : deletePhotoFlows.values()) {
            result.add(deletePhotoFlow.delete());
        }
        try {
            return result;
        } finally {
            for (final PhotoStub deletedPhotoStub : result) {
                deletePhotoFlows.remove(deletedPhotoStub.getUri());
            }
        }
    }

    @AllNonnull
    @NotThreadSafe
    private static final class PersistPhotoFlow {
        private final ContentResolver contentResolver;
        private final UUID beerId;
        private final PhotoStub photoStub;

        private PersistPhotoFlow(final ContentResolver theContentResolver, final UUID theBeerId,
                final PhotoStub thePhotoStub) {

            contentResolver = theContentResolver;
            beerId = theBeerId;
            photoStub = thePhotoStub;
        }

        Photo persist() {
            final PhotoFile photoFile;
            final Maybe<PhotoFile> existingPhotoFile = getExistingPhotoFile();
            if (existingPhotoFile.isAbsent()) {
                photoFile = BeerFactory.newBeerPhotoFile(photoStub.getName());
            } else {
                photoFile = existingPhotoFile.get();
            }

            final File targetFile = photoFile.getFile();
            if (!targetFile.exists()) {
                copyFileToTargetDirectory(targetFile);
            }
            if (existingPhotoFile.isAbsent()) {
                insertBeerPhotoFileEntry(photoFile);
            }

            final Photo photo = BeerFactory.newBeerPhoto(beerId, photoFile, PhotoStatus.EXISTING);
            insertBeerPhotoEntry(photo);
            return photo;
        }

        private Maybe<PhotoFile> getExistingPhotoFile() {
            final Selection s = Selection.where(BeerPhotoFileEntry.COLUMN_NAME).is(photoStub.getName()).build();
            final Cursor cursor = contentResolver.query(BeerPhotoFileEntry.TABLE.getContentUri(), null,
                    s.getSelectionString(), s.getSelectionArgs(), null);

            if (null == cursor || 0 == cursor.getCount()) {
                return Maybe.empty();
            }

            try {
                return Maybe.ofNullable(BeerFactory.newBeerPhotoFileFromCursor(cursor));
            } finally {
                cursor.close();
            }
        }

        private void copyFileToTargetDirectory(final File targetFile) {
            final FileCopier copier = FileCopier.getInstance(tryToGetInputStream(), targetFile, FileCopier.Mode.CANCEL);
            final int copiedBytes = copier.run();
            if (1 > copiedBytes) {
                final String msgPattern = "Failed to copy <{0}> to <{1}>!";
                throw new CopyFileException(MessageFormat.format(msgPattern, photoStub.getUri(), targetFile));
            }
            LOGGER.debug("Copied <{0}> to <{1}>.", photoStub.getName(), targetFile.getParent());
        }

        private InputStream tryToGetInputStream() {
            try {
                return getInputStreamFor(photoStub.getUri());
            } catch (final FileNotFoundException e) {
                final String pattern = "Failed to get InputStream for photo URI <{0}>!";
                throw new CopyFileException(MessageFormat.format(pattern, photoStub.getUri()), e);
            }
        }

        @SuppressWarnings("ConstantConditions")
        private InputStream getInputStreamFor(final Uri photoUri) throws FileNotFoundException {
            return contentResolver.openInputStream(photoUri);
        }

        private void insertBeerPhotoEntry(final Photo photo) {
            contentResolver.insert(photo.getContentUri(), photo.asContentValues());
        }

        private void insertBeerPhotoFileEntry(final PhotoFile photoFile) {
            contentResolver.insert(photoFile.getContentUri(), photoFile.asContentValues());
        }
    }

    @AllNonnull
    @NotThreadSafe
    private final class DeletePhotoFlow {
        private final ContentResolver contentResolver;
        private final PhotoStub photoStub;
        private final UUID beerId;

        private DeletePhotoFlow(final ContentResolver theContentResolver, final PhotoStub thePhotoStub,
                final UUID theBeerId) {

            contentResolver = theContentResolver;
            photoStub = thePhotoStub;
            beerId = theBeerId;
        }

        PhotoStub delete() {
            if (deleteBeerPhotoEntry() && deleteBeerPhotoFileEntry()) {
                final File file = new File(photoStub.getUri().getPath());
                if (file.delete()) {
                    LOGGER.debug("Deleted file <{0}>.", file.getAbsolutePath());
                } else {
                    LOGGER.warn("Failed to delete file <{0}>!", file.getAbsolutePath());
                }
            }
            return photoStub;
        }

        private boolean deleteBeerPhotoEntry() {
            final Selection s = Selection.where(BeerPhotoEntry.COLUMN_ID).is(photoStub.getId()).build();

            return 1 == contentResolver.delete(BeerPhotoEntry.TABLE.getContentUri(), s.getSelectionString(),
                    s.getSelectionArgs());
        }

        private boolean deleteBeerPhotoFileEntry() {
            if (isFileSafeToBeDeleted()) {
                final Selection s = Selection.where(BeerPhotoFileEntry.COLUMN_NAME).is(photoStub.getName()).build();
                return 1 == contentResolver.delete(BeerPhotoFileEntry.TABLE.getContentUri(), s.getSelectionString(),
                        s.getSelectionArgs());
            }
            return false;
        }

        private boolean isFileSafeToBeDeleted() {
            final Selection s = Selection.where(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID).is(photoStub.getId()).build();
            final Cursor cursor = contentResolver.query(BeerPhotoEntry.TABLE.getContentUri(), null,
                    s.getSelectionString(), s.getSelectionArgs(), null);

            if (null != cursor) {
                try {
                    return 0 == cursor.getCount();
                } finally {
                    cursor.close();
                }
            }
            return false;
        }
    }

}
