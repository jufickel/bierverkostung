/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.common.CommaDelimiterTrimmer;
import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.bierverkostung.data.Revision;
import de.retujo.bierverkostung.data.Table;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoStub;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

import static android.text.TextUtils.isEmpty;
import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A mutable builder with a fluent API for an immutable {@link Beer}.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
public final class BeerBuilder {

    private EntityCommonData commonData;
    private String name;
    @Nullable private Brewery brewery;
    @Nullable private BeerStyle style;
    @Nullable private String originalWort;
    @Nullable private String alcohol;
    private int ibu;
    @Nullable private String ingredients;
    @Nullable private String specifics;
    @Nullable private String notes;
    private Set<Photo> photos;

    private BeerBuilder(final EntityCommonData theCommonData, final String theName) {
        commonData = theCommonData;
        name = theName;
        brewery = null;
        style = null;
        originalWort = null;
        alcohol = null;
        ibu = ImmutableBeer.NULL_IBU;
        ingredients = null;
        specifics = null;
        notes = null;
        photos = new HashSet<>();
    }

    /**
     * Returns a new instance of {@code BeerBuilder}.
     *
     * @param beerName the name of the beer.
     * @return the instance.
     * @throws NullPointerException if {@code beerName} is {@code null}.
     * @throws IllegalArgumentException if {@code beerName} is empty.
     */
    public static BeerBuilder newInstance(final CharSequence beerName) {
        return new BeerBuilder(EntityCommonData.getInstance(), argumentNotEmpty(beerName, "beerName").toString());
    }

    /**
     * Returns a new instance of {@code BeerBuilder} which is initialised with the properties of the provided beer.
     *
     * @param sourceBeer the Beer providing the initial properties of the returned builder.
     * @return the instance.
     * @throws NullPointerException if {@code sourceBeer} is {@code null}.
     * @since 1.2.0
     */
    public static BeerBuilder newInstance(final Beer sourceBeer) {
        isNotNull(sourceBeer, "source beer");

        return newInstance(sourceBeer.getName())
                .commonData(EntityCommonData.getInstance(sourceBeer.getId(), sourceBeer.getRevision()))
                .brewery(sourceBeer.getBrewery().orElse(null))
                .style(sourceBeer.getStyle().orElse(null))
                .originalWort(sourceBeer.getOriginalWort().orElse(null))
                .ibu(sourceBeer.getIbu().orElse(ImmutableBeer.NULL_IBU))
                .ingredients(sourceBeer.getIngredients().orElse(null))
                .specifics(sourceBeer.getSpecifics().orElse(null))
                .notes(sourceBeer.getNotes().orElse(null))
                .photos(sourceBeer.getPhotos());
    }

    /**
     * Sets the ID of the beer.
     *
     * @param id the ID.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code id} is {@code null}.
     */
    public BeerBuilder id(final UUID id) {
        return commonData(EntityCommonData.getInstance(id, commonData.getRevision()));
    }

    /**
     * Returns the universally unique identifier of the beer to be built by this BeerBuilder object.
     *
     * @return the ID.
     * @since 1.2.0
     */
    public UUID getId() {
        return commonData.getId();
    }

    /**
     * Sets the specified revision.
     * 
     * @param revision the revision to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code revision} is {@code null}.
     * @since 1.2.0
     */
    public BeerBuilder revision(final Revision revision) {
        return commonData(EntityCommonData.getInstance(commonData.getId(), isNotNull(revision, "revision")));
    }

    /**
     * Increments the revision.
     *
     * @return this builder instance to allow method chaining.
     * @since 1.2.0
     */
    public BeerBuilder incrementRevision() {
        commonData = commonData.incrementRevision();
        return this;
    }

    /**
     * Sets the specified common data like ID and revision.
     *
     * @param commonData the common data to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code commonData} is {@code null}.
     * @since 1.2.0
     */
    public BeerBuilder commonData(final EntityCommonData commonData) {
        this.commonData = isNotNull(commonData, "common data to be set");
        return this;
    }

    /**
     * Sets the specified name of the beer and the current timestamp if the old name differed.
     *
     * @param newName the name to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code name} is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     * @since 1.2.0
     */
    public BeerBuilder name(final CharSequence newName) {
        final String newNameAsString = argumentNotEmpty(newName, "beer name").toString();
        if (ObjectUtil.areEqual(newNameAsString, name)) {
            return this;
        }
        this.name = newNameAsString;
        return this;
    }

    /**
     * Returns the name of the beer to be built by this BeerBuilder object.
     *
     * @return the name.
     * @since 1.2.0
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the brewery of the beer and the current timestamp if the old brewery differed.
     *
     * @param newBrewery the Brewery or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder brewery(@Nullable final Brewery newBrewery) {
        if (ObjectUtil.areEqual(newBrewery, brewery)) {
            return this;
        }
        this.brewery = newBrewery;
        return this;
    }

    /**
     * Sets the style of the beer and the current timestamp if the old beer style differed.
     *
     * @param newBeerStyle the style or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder style(@Nullable final BeerStyle newBeerStyle) {
        if (ObjectUtil.areEqual(newBeerStyle, style)) {
            return this;
        }
        this.style = newBeerStyle;
        return this;
    }

    /**
     * Sets the original wort of the beer and the current timestamp if the old original wort differed.
     *
     * @param newOriginalWort the original wort or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder originalWort(@Nullable final CharSequence newOriginalWort) {
        if (ObjectUtil.areEqual(newOriginalWort, originalWort)) {
            return this;
        }
        if (null != newOriginalWort) {
            this.originalWort = newOriginalWort.toString();
        } else {
            this.originalWort = null;
        }
        return this;
    }

    /**
     * Sets the original wort of the beer and the current timestamp if the old original wort differed.
     *
     * @param originalWort the original wort.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder originalWort(final double originalWort) {
        return originalWort(String.valueOf(originalWort));
    }

    /**
     * Sets the volume alcohol of the beer and the current timestamp if the old volume alcohol differed.
     *
     * @param newAlcohol the alcohol value or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder alcohol(@Nullable final CharSequence newAlcohol) {
        if (ObjectUtil.areEqual(newAlcohol, alcohol)) {
            return this;
        }
        if (newAlcohol != null) {
            this.alcohol = newAlcohol.toString();
        } else {
            this.alcohol = null;
        }
        return this;
    }

    /**
     * Sets the volume alcohol of the beer and the current timestamp if the old volume alcohol differed.
     *
     * @param newAlcohol the alcohol value.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder alcohol(final double newAlcohol) {
        return alcohol(String.valueOf(newAlcohol));
    }

    /**
     * Sets the IBU value of the beer and the current timestamp if the old IBU value differed.
     *
     * @param newIbu the IBU value or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder ibu(@Nullable final CharSequence newIbu) {
        if (!isEmpty(newIbu)) {
            return ibu(Integer.parseInt(String.valueOf(newIbu)));
        }
        return ibu(ImmutableBeer.NULL_IBU);
    }

    /**
     * Sets the IBU value of the beer and the current timestamp if the old IBU value differed.
     *
     * @param newIbu the IBU value.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder ibu(final int newIbu) {
        if (ObjectUtil.areEqual(newIbu, ibu)) {
            return this;
        }
        this.ibu = newIbu;
        return this;
    }

    /**
     * Sets the ingredients of the beer and the current timestamp if the old ingredients differed.
     *
     * @param newIngredients the ingredients or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder ingredients(@Nullable final CharSequence newIngredients) {
        if (ObjectUtil.areEqual(newIngredients, ingredients)) {
            return this;
        }
        if (newIngredients != null) {
            this.ingredients = CommaDelimiterTrimmer.trim(newIngredients);
        } else {
            this.ingredients = null;
        }
        return this;
    }

    /**
     * Sets the specifics of the beer and the current timestamp if the old specifics differed
     *
     * @param newSpecifics the specifics or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder specifics(@Nullable final CharSequence newSpecifics) {
        if (ObjectUtil.areEqual(newSpecifics, specifics)) {
            return this;
        }
        if (newSpecifics != null) {
            this.specifics = newSpecifics.toString();
        } else {
            this.specifics = null;
        }
        return this;
    }

    /**
     * Sets the notes of the beer and the current timestamp if the old notes differed.
     *
     * @param newNotes the notes or {@code null}.
     * @return this builder instance to allow method chaining.
     */
    public BeerBuilder notes(@Nullable final CharSequence newNotes) {
        if (ObjectUtil.areEqual(newNotes, notes)) {
            return this;
        }
        if (null != newNotes) {
            this.notes = newNotes.toString();
        }
        return this;
    }

    /**
     * Adds the specified photos to the beer and the current timestamp if the old photos differed.
     *
     * @param newPhotos the photos to be added.
     * @return this builder instance to allow method chaining.
     * @since 1.2.0
     */
    public BeerBuilder photos(@Nullable final Collection<? extends Photo> newPhotos) {
        if (ObjectUtil.areEqual(newPhotos, photos)) {
            return this;
        }
        if (null != newPhotos) {
            this.photos = new HashSet<>(newPhotos);
        } else {
            this.photos = new HashSet<>();
        }
        return this;
    }

    /**
     * Adds the specified photos to the photos of this BeerBuilder object.
     *
     * @param additionalPhotos the photos to be added.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code additionalPhotos} is {@code null}.
     * @since 1.2.0
     */
    public BeerBuilder addPhotos(final Collection<Photo> additionalPhotos) {
        photos.addAll(additionalPhotos);
        return this;
    }

    /**
     * Removes the photos with the same ID like the specified photo stubs from this BeerBuilder object.
     *
     * @param photoStubs the photo stubs whose IDs determine the photos to be removed.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code photoStubs} is {@code null}.
     * @since 1.2.0
     */
    public BeerBuilder removePhotos(final Collection<PhotoStub> photoStubs) {
        for (final PhotoStub photoStub : photoStubs) {
            removePhoto(photoStub);
        }
        return this;
    }

    /**
     * Removes the photo with the same ID like the specified photo stub from this BeerBuilder object.
     *
     * @param photoStub the photo stub whose ID determines the photo to be removed.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code photoStub} is {@code null}.
     * @since 1.2.0
     */
    public BeerBuilder removePhoto(final PhotoStub photoStub) {
        final Iterator<Photo> photoIterator = photos.iterator();
        while (photoIterator.hasNext()) {
            final Photo photo = photoIterator.next();
            if (ObjectUtil.areEqual(photo.getId(), photoStub.getId())) {
                photoIterator.remove();
            }
        }
        return this;
    }

    /**
     * Returns a new immutable instance of {@code Beer} with the properties of this builder.
     *
     * @return the new Beer.
     */
    public Beer build() {
        return new ImmutableBeer(this);
    }

    @AllNonnull
    @Immutable
    static final class ImmutableBeer implements Beer {
        /**
         * Creator which creates instances of {@code ImmutableBeer} from a Parcel.
         */
        public static final Parcelable.Creator<Beer> CREATOR = new ImmutableBeerCreator();

        /**
         * This value represents a semantic {@code null} value for IBU.
         */
        private static final int NULL_IBU = 0;

        private static final Table TABLE = BeerEntry.TABLE;

        private final EntityCommonData commonData;
        private final String name;
        @Nullable private final Brewery brewery;
        @Nullable private final BeerStyle style;
        @Nullable private final String originalWort;
        @Nullable private final String alcohol;
        private final int ibu;
        @Nullable private final String ingredients;
        @Nullable private final String specifics;
        @Nullable private final String notes;
        private final Set<Photo> photos;

        private ImmutableBeer(final BeerBuilder builder) {
            commonData = builder.commonData;
            name = builder.name;
            brewery = builder.brewery;
            style = builder.style;
            originalWort = builder.originalWort;
            alcohol = builder.alcohol;
            ibu = builder.ibu;
            ingredients = builder.ingredients;
            specifics = builder.specifics;
            notes = builder.notes;
            photos = Collections.unmodifiableSet(new HashSet<>(builder.photos));
        }

        @Override
        public UUID getId() {
            return commonData.getId();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Maybe<Brewery> getBrewery() {
            return Maybe.ofNullable(brewery);
        }

        @Override
        public Maybe<BeerStyle> getStyle() {
            return Maybe.ofNullable(style);
        }

        @Override
        public Maybe<String> getOriginalWort() {
            return Maybe.ofNullable(originalWort);
        }

        @Override
        public Maybe<String> getAlcohol() {
            return Maybe.ofNullable(alcohol);
        }

        @Override
        public Maybe<Integer> getIbu() {
            if (NULL_IBU != ibu) {
                return Maybe.of(ibu);
            }
            return Maybe.empty();
        }

        @Override
        public Maybe<String> getIngredients() {
            return Maybe.ofNullable(ingredients);
        }

        @Override
        public Maybe<String> getSpecifics() {
            return Maybe.ofNullable(specifics);
        }

        @Override
        public Maybe<String> getNotes() {
            return Maybe.ofNullable(notes);
        }

        @Override
        public Set<Photo> getPhotos() {
            return photos;
        }

        @Override
        public Uri getContentUri() {
            return commonData.getContentUri(TABLE);
        }

        @Override
        public Revision getRevision() {
            return commonData.getRevision();
        }

        @Override
        public ContentValues asContentValues() {
            final ContentValues result = commonData.asContentValues(TABLE);
            result.put(BeerEntry.COLUMN_NAME.toString(), name);
            if (null != brewery) {
                result.put(BeerEntry.COLUMN_BREWERY_ID.toString(), brewery.getId().toString());
            }
            if (null != style) {
                result.put(BeerEntry.COLUMN_STYLE_ID.toString(), style.getId().toString());
            }
            if (!isEmpty(originalWort)) {
                result.put(BeerEntry.COLUMN_ORIGINAL_WORT.toString(), Double.parseDouble(originalWort));
            }
            if (!isEmpty(alcohol)) {
                result.put(BeerEntry.COLUMN_ALCOHOL.toString(), Double.parseDouble(alcohol));
            }
            result.put(BeerEntry.COLUMN_IBU.toString(), ibu);
            if (!isEmpty(ingredients)) {
                result.put(BeerEntry.COLUMN_INGREDIENTS.toString(), ingredients);
            }
            if (!isEmpty(specifics)) {
                result.put(BeerEntry.COLUMN_SPECIFICS.toString(), specifics);
            }
            if (!isEmpty(notes)) {
                result.put(BeerEntry.COLUMN_NOTES.toString(), notes);
            }
            // Photos are persisted in a custom table.
            return result;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(final Parcel dest, final int flags) {
            commonData.writeToParcel(dest, flags);
            ParcelWrapper.of(dest)
                    .wrap(name)
                    .wrap(brewery)
                    .wrap(style)
                    .wrap(originalWort)
                    .wrap(alcohol)
                    .wrap(ibu)
                    .wrap(ingredients)
                    .wrap(specifics)
                    .wrap(notes);
            dest.writeTypedList(new ArrayList<>(photos));
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImmutableBeer that = (ImmutableBeer) o;

            return ObjectUtil.areEqual(commonData, that.commonData) &&
                    ObjectUtil.areEqual(name, that.name) &&
                    ObjectUtil.areEqual(brewery, that.brewery) &&
                    ObjectUtil.areEqual(style, that.style) &&
                    ObjectUtil.areEqual(originalWort, that.originalWort) &&
                    ObjectUtil.areEqual(alcohol, that.alcohol) &&
                    ibu == that.ibu &&
                    ObjectUtil.areEqual(ingredients, that.ingredients) &&
                    ObjectUtil.areEqual(specifics, that.specifics) &&
                    ObjectUtil.areEqual(notes, that.notes) &&
                    ObjectUtil.areEqual(photos, that.photos);
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(commonData, name, brewery, style, originalWort, alcohol, ibu, ingredients,
                    specifics, notes, photos);
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {"
                    + commonData
                    + ", name=" + name
                    + ", brewery=" + brewery
                    + ", style=" + style
                    + ", originalWort='" + originalWort + '\''
                    + ", alcohol='" + alcohol + '\''
                    + ", ibu=" + ibu
                    + ", ingredients='" + ingredients + '\''
                    + ", specifics='" + specifics + '\''
                    + ", notes='" + notes + '\''
                    + ", photos=" + photos
                    + "}";
        }

        @Nonnull
        @Override
        public JSONObject toJson() {
            final BeerJsonConverter converter = BeerJsonConverter.getInstance();
            return converter.toJson(this);
        }
    }

    /**
     * This class creates instances of {@code ImmutableBeer} from a Parcel.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class ImmutableBeerCreator extends BaseParcelableCreator<Beer> {
        @Override
        protected Beer createFromParcel(final Parcel source, final EntityCommonData commonData) {
            final ParcelUnwrapper unwrapper = ParcelUnwrapper.of(source);
            final String readName = unwrapper.unwrapString();

            final BeerBuilder builder = BeerBuilder.newInstance(readName)
                    .commonData(commonData)
                    .brewery(unwrapper.unwrapParcelable())
                    .style(unwrapper.unwrapParcelable())
                    .originalWort(unwrapper.unwrapString())
                    .alcohol(unwrapper.unwrapString())
                    .ibu(unwrapper.unwrapInt())
                    .ingredients(unwrapper.unwrapString())
                    .specifics(unwrapper.unwrapString())
                    .notes(unwrapper.unwrapString());

            final List<Photo> readPhotos = new ArrayList<>();
            source.readTypedList(readPhotos, BeerPhoto.CREATOR);
            builder.photos(readPhotos);

            return builder.build();
        }

        @Override
        public Beer[] newArray(final int size) {
            return new Beer[size];
        }
    }

}
