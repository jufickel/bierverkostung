package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.util.AttributeSet;

/**
 * This is an ImageView which has always the same width and height, based on the width. The code is copied from the
 * examples of the
 * <a href="https://github.com/square/picasso/blob/master/picasso-sample/src/main/java/com/example/picasso
 * /SquaredImageView.java">Picasso library.</a>
 */
final class SquaredImageView extends android.support.v7.widget.AppCompatImageView {

    /**
     * Constructs a new {@code SquaredImageView} object.
     *
     * @param context
     */
    public SquaredImageView(final Context context) {
        super(context);
    }

    /**
     * Constructs a new {@code SquaredImageView} object.
     *
     * @param context
     * @param attrs
     */
    public SquaredImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }

}
