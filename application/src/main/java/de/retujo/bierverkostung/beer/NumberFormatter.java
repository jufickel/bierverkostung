/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.Provider;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class provides a formatted string for a specified Number.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class NumberFormatter implements Provider<String> {

    private final Number number;
    private final int fractionDigits;

    private NumberFormatter(final Number theNumber, final int theFractionDigits) {
        number = theNumber;
        fractionDigits = theFractionDigits;
    }

    /**
     * Returns a new instance of {@code NumberFormatter}.
     *
     * @param number the number to be formatted.
     * @param fractionDigits the minimum and maximum fraction digits of the result.
     * @return the instance.
     */
    public static NumberFormatter newInstance(@Nonnull final Number number, final int fractionDigits) {
        isNotNull(number, "number to be formatted");
        return new NumberFormatter(number, fractionDigits);
    }

    @Override
    public String get() {
        final NumberFormat decimalFormat = DecimalFormat.getInstance(Locale.ENGLISH);
        decimalFormat.setMinimumFractionDigits(fractionDigits);
        decimalFormat.setMaximumFractionDigits(fractionDigits);

        return decimalFormat.format(number);
    }

}
