/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;

import java.util.Collection;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.AbstractDeleteTask;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoStub;
import de.retujo.java.util.AllNonnull;

/**
 * This AsyncTask deletes the photos of the specified beers. This includes database entries as well as files.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class DeleteBeerPhotosTask extends AbstractDeleteTask<DeleteBeerPhotosTask, Beer> {

    private final ContentResolver contentResolver;

    private DeleteBeerPhotosTask(final ContentResolver theContentResolver) {
        contentResolver = theContentResolver;
    }

    public static DeleteBeerPhotosTask getInstance(final ContentResolver contentResolver) {
        return new DeleteBeerPhotosTask(contentResolver);
    }

    @Override
    protected boolean doDelete(final Beer beer) {
        final BeerPhotoManager beerPhotoManager = BeerPhotoManager.getInstance(contentResolver, beer.getId());
        final Collection<Photo> beerPhotos = beer.getPhotos();
        for (final Photo beerPhoto : beerPhotos) {
            beerPhotoManager.deleteExisting(beerPhoto);
        }
        final Collection<PhotoStub> deletedPhotos = beerPhotoManager.deleteAllPhotos();

        return deletedPhotos.size() == beerPhotos.size();
    }

}
