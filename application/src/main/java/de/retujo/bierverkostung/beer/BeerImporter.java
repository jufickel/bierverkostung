/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.beerstyle.BeerStyleImporter;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.brewery.BreweryImporter;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Imports an external Beer or returns or updates an existing Beer.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class BeerImporter extends DataEntityImporter<Beer> {

    private final DataEntityImporter<Brewery> breweryImporter;
    private final DataEntityImporter<BeerStyle> beerStyleImporter;
    private final DataEntityImporter<Photo> beerPhotoImporter;
    private Collection<Photo> externalBeerPhotos;

    private BeerImporter(final ContentResolver contentResolver,
            final DataEntityImporter<Brewery> theBreweryImporter,
            final DataEntityImporter<BeerStyle> theBeerStyleImporter,
            final DataEntityImporter<Photo> theBeerPhotoImporter) {

        super(contentResolver, BeerEntry.TABLE, BeerFactory::newBeerFromCursorWithoutPhotos);
        breweryImporter = isNotNull(theBreweryImporter, "brewery importer");
        beerStyleImporter = isNotNull(theBeerStyleImporter, "beer style importer");
        beerPhotoImporter = isNotNull(theBeerPhotoImporter, "beer photo importer");
        externalBeerPhotos = Collections.emptySet();
    }

    /**
     * Returns an instance of {@code BeerImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing beers or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static BeerImporter getInstance(final ContentResolver contentResolver) {
        return new BeerImporter(contentResolver, BreweryImporter.getInstance(contentResolver),
                BeerStyleImporter.getInstance(contentResolver), BeerPhotoImporter.getInstance(contentResolver));
    }

    @Override
    protected Beer importSubEntities(@Nonnull final Beer externalBeer) {
        // Postpone import of photos until the beer was imported because the beer ID is required for photo import.
        externalBeerPhotos = externalBeer.getPhotos();

        return BeerBuilder.newInstance(externalBeer)
                .style(externalBeer.getStyle().map(beerStyleImporter::importOrGetExisting).orElse(null))
                .brewery(externalBeer.getBrewery().map(breweryImporter::importOrGetExisting).orElse(null))
                .build();
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final Beer externalBeer) {
        final Selection.AppendWhereStep builder = Selection.where(BeerEntry.COLUMN_NAME).is(externalBeer.getName());
        externalBeer.getBrewery()
                .map(Brewery::getId)
                .ifPresent(breweryId -> builder.andWhere(BeerEntry.COLUMN_BREWERY_ID).is(breweryId));
        externalBeer.getStyle()
                .map(BeerStyle::getId)
                .ifPresent(beerStyleId -> builder.andWhere(BeerEntry.COLUMN_STYLE_ID).is(beerStyleId));
        externalBeer.getAlcohol().ifPresent(alcohol -> builder.andWhere(BeerEntry.COLUMN_ALCOHOL).is(alcohol));
        externalBeer.getIbu().ifPresent(ibu -> builder.andWhere(BeerEntry.COLUMN_IBU).is(ibu));
        externalBeer.getIngredients()
                .ifPresent(ingredients -> builder.andWhere(BeerEntry.COLUMN_INGREDIENTS).is(ingredients));
        externalBeer.getOriginalWort()
                .ifPresent(originalWort -> builder.andWhere(BeerEntry.COLUMN_ORIGINAL_WORT).is(originalWort));
        externalBeer.getSpecifics()
                .ifPresent(specifics -> builder.andWhere(BeerEntry.COLUMN_SPECIFICS).is(specifics));
        externalBeer.getNotes().ifPresent(notes -> builder.andWhere(BeerEntry.COLUMN_NOTES).is(notes));

        return builder.build();
    }

    @Override
    protected Beer modifyImportedEntityBeforeCaching(@Nonnull final Beer importedBeer) {
        /*
         * Importing the photos had to be postponed until here to have the final Beer ID before inserting any Photo
         * into the database.
         */
        return BeerBuilder.newInstance(importedBeer)
                .photos(importExternalPhotos(importedBeer.getId()))
                .build();
    }

    private Set<Photo> importExternalPhotos(final UUID finalBeerId) {
        final Set<Photo> result = new HashSet<>(externalBeerPhotos.size());
        for (final Photo externalBeerPhoto : externalBeerPhotos) {
            result.add(beerPhotoImporter.importOrGetExisting(externalBeerPhoto.setReferenceId(finalBeerId)));
        }
        try {
            return result;
        } finally {
            externalBeerPhotos = Collections.emptySet();
        }
    }

}
