/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Imports an external BeerPhoto or returns or updates an existing BeerPhoto.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class BeerPhotoImporter extends DataEntityImporter<Photo> {

    private final DataEntityImporter<PhotoFile> beerPhotoFileImporter;

    private BeerPhotoImporter(final ContentResolver contentResolver,
            final DataEntityImporter<PhotoFile> theBeerPhotoFileImporter) {

        super(contentResolver, BeerPhotoEntry.TABLE, BeerFactory::newBeerPhotoFromCursor);
        beerPhotoFileImporter = isNotNull(theBeerPhotoFileImporter, "beer photo file importer");
    }

    /**
     * Returns an instance of {@code BeerPhotoImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing beer photos or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static BeerPhotoImporter getInstance(final ContentResolver contentResolver) {
        return new BeerPhotoImporter(contentResolver, BeerPhotoFileImporter.getInstance(contentResolver));
    }

    @Override
    protected Photo importSubEntities(@Nonnull final Photo externalBeerPhoto) {
        return externalBeerPhoto.setFile(beerPhotoFileImporter.importOrGetExisting(externalBeerPhoto.getFile()));
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final Photo externalBeerPhoto) {
        final PhotoFile beerPhotoFile = externalBeerPhoto.getFile();

        return Selection.where(BeerPhotoEntry.COLUMN_BEER_ID).is(externalBeerPhoto.getReferenceId())
                .andWhere(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID).is(beerPhotoFile.getId())
                .build();
    }

}
