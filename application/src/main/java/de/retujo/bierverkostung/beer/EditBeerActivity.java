/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.beerstyle.SelectBeerStyleActivity;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.brewery.SelectBreweryActivity;
import de.retujo.bierverkostung.common.AbstractTextWatcher;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.SaveEntityDialogue;
import de.retujo.bierverkostung.common.Toaster;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.DbColumnArrayAdapter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFactory;
import de.retujo.bierverkostung.photo.PhotoStatus;
import de.retujo.bierverkostung.photo.PhotoStub;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;
import de.retujo.java.util.Provider;

/**
 * Activity for creating a new beer or for editing an existing beer. The only mandatory property is the name; thus as
 * soon as a name is entered the button for saving becomes enabled.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class EditBeerActivity extends BaseActivity {

    /**
     * Key to retrieve the edited beer from this Activity's result Intent.
     */
    public static final String EDITED_BEER = "editedBeer";

    /**
     * Key to set an existing Beer to be edited to the Intent for starting an EditBeerActivity.
     */
    public static final String EDIT_BEER = "beerToBeEdited";

    private static final int SELECT_BREWERY_REQUEST_CODE = 160;
    private static final int SELECT_STYLE_REQUEST_CODE = 145;
    private static final String NEW_BEER_BREWERY = "newBeerBrewery";
    private static final String NEW_BEER_STYLE = "newBeerStyle";

    private TextView beerNameTextView;
    private TextView breweryTextView;
    private TextView styleTextView;
    private TextView originalWortTextView;
    private TextView alcoholTextView;
    private TextView ibuTextView;
    private MultiAutoCompleteTextView ingredientsTextView;
    private TextView specificsTextView;
    private TextView notesTextView;
    private GridView photoGridView;
    private Button saveBeerButton;
    private Beer originalBeer;
    private BeerBuilder beerBuilder;
    private BeerPhotoManager beerPhotoManager;

    /**
     * Constructs a new {@code EditBeerActivity} object.
     */
    public EditBeerActivity() {
        beerNameTextView = null;
        breweryTextView = null;
        styleTextView = null;
        originalWortTextView = null;
        alcoholTextView = null;
        saveBeerButton = null;
        ibuTextView = null;
        ingredientsTextView = null;
        specificsTextView = null;
        photoGridView = null;
        notesTextView = null;
        originalBeer = null;
        beerBuilder = null;
        beerPhotoManager = null;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_beer);

        initSaveBeerButton();
        initBeerNameTextView();
        initBreweryTextView();
        initStyleTextView();

        originalWortTextView = findView(R.id.edit_beer_original_wort_edit_text);
        alcoholTextView = findView(R.id.edit_beer_alcohol_edit_text);
        ibuTextView = findView(R.id.edit_beer_ibu_edit_text);
        initIngredientsTextView();
        specificsTextView = findView(R.id.edit_beer_specifics_edit_text);
        notesTextView = findView(R.id.edit_beer_notes_edit_text);

        final Intent afferentIntent = getIntent();
        originalBeer = afferentIntent.getParcelableExtra(EDIT_BEER);
        if (null == originalBeer) {
            setTitle(getString(R.string.add_beer_activity_name));
            restore(savedInstanceState);
            beerBuilder = BeerBuilder.newInstance("no name yet");
            beerPhotoManager = BeerPhotoManager.getInstance(getContentResolver(), beerBuilder.getId());
            initPhotosGridView(Collections.emptyList());
        } else {
            setTitle(getString(R.string.edit_beer_activity_name));
            beerBuilder = BeerBuilder.newInstance(originalBeer);
            beerPhotoManager = BeerPhotoManager.getInstance(getContentResolver(), originalBeer.getId());
            initViewsWithBeerProperties(originalBeer);
            initPhotosGridView(originalBeer.getPhotos());
        }
    }

    private void initSaveBeerButton() {
        saveBeerButton = findView(R.id.edit_beer_save_button);
        saveBeerButton.setEnabled(false);
    }

    private void initBeerNameTextView() {
        beerNameTextView = findView(R.id.edit_beer_name_edit_text);
        beerNameTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (!TextUtils.isEmpty(s) && !ObjectUtil.areEqual(beerBuilder.getName(), s)) {
                    beerBuilder.name(s);
                    setSaveButtonEnabled(true);
                } else {
                    setSaveButtonEnabled(false);
                }
            }
        });
    }

    private void setSaveButtonEnabled(final boolean enable) {
        saveBeerButton.setEnabled(!TextUtils.isEmpty(beerNameTextView.getEditableText()) && enable);
    }

    private void initBreweryTextView() {
        breweryTextView = findView(R.id.edit_beer_brewery_text_view);
        breweryTextView.setOnClickListener(v -> {
            final Intent selectBreweryIntent = new Intent(EditBeerActivity.this, SelectBreweryActivity.class);
            startActivityForResult(selectBreweryIntent, SELECT_BREWERY_REQUEST_CODE);
        });
    }

    private void initStyleTextView() {
        styleTextView = findView(R.id.edit_beer_style_text_view);
        styleTextView.setOnClickListener(v -> {
            final Intent selectBeerStyleIntent = new Intent(EditBeerActivity.this, SelectBeerStyleActivity.class);
            startActivityForResult(selectBeerStyleIntent, SELECT_STYLE_REQUEST_CODE);
        });
    }

    private void initIngredientsTextView() {
        ingredientsTextView = findView(R.id.edit_beer_ingredients_edit_text);
        final DbColumnArrayAdapter arrayAdapter = DbColumnArrayAdapter.getInstance(this,
                BeerEntry.CONTENT_URI_SINGLE_COLUMN, BeerEntry.COLUMN_INGREDIENTS);
        ingredientsTextView.setAdapter(arrayAdapter);
        ingredientsTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        ingredientsTextView.setThreshold(1);
        ingredientsTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(!ObjectUtil.areEqual(originalBeer.getIngredients(), s));
                } else {
                    setSaveButtonEnabled(true);
                }
                beerBuilder.ingredients(s);
            }
        });
    }

    private void restore(final Bundle savedInstanceState) {
        if (null != savedInstanceState) {
            final Brewery brewery = savedInstanceState.getParcelable(NEW_BEER_BREWERY);
            if (null != brewery) {
                breweryTextView.setText(brewery.getName());
                breweryTextView.setTag(brewery);
            }
            final BeerStyle beerStyle = savedInstanceState.getParcelable(NEW_BEER_STYLE);
            if (null != beerStyle) {
                styleTextView.setText(beerStyle.getName());
                styleTextView.setTag(beerStyle);
            }
        }
    }

    private void initViewsWithBeerProperties(final Beer beer) {
        beerNameTextView.setText(beer.getName());
        final Maybe<Brewery> breweryMaybe = beer.getBrewery();
        if (breweryMaybe.isPresent()) {
            final Brewery brewery = breweryMaybe.get();
            breweryTextView.setText(brewery.getName());
            breweryTextView.setTag(brewery);
        }
        final Maybe<BeerStyle> styleMaybe = beer.getStyle();
        if (styleMaybe.isPresent()) {
            final BeerStyle beerStyle = styleMaybe.get();
            styleTextView.setText(beerStyle.getName());
            styleTextView.setTag(beerStyle);
        }
        setTextIfPresent(alcoholTextView, beer.getAlcohol());
        setTextIfPresent(originalWortTextView, beer.getOriginalWort());
        final Maybe<Integer> ibu = beer.getIbu();
        if (ibu.isPresent()) {
           ibuTextView.setText(String.valueOf(ibu.get()));
        }
        setTextIfPresent(ingredientsTextView, beer.getIngredients());
        setTextIfPresent(specificsTextView, beer.getSpecifics());
        setTextIfPresent(notesTextView, beer.getNotes());
    }

    private static void setTextIfPresent(final TextView textView, final Maybe<String> maybe) {
        if (maybe.isPresent()) {
            textView.setText(maybe.get());
        }
    }

    private void initPhotosGridView(final Collection<Photo> photos) {
        photoGridView = findView(R.id.edit_beer_images_grid_view);
        final PhotoGridViewAdapter photoGridViewAdapter = initPhotoGridViewAdapter(photos);
        photoGridView.setAdapter(photoGridViewAdapter);
        photoGridView.setOnItemClickListener(new PhotoOnClickListener(photoGridViewAdapter));
    }

    private PhotoGridViewAdapter initPhotoGridViewAdapter(final Collection<Photo> photos) {
        return PhotoGridViewAdapter.getInstance(EditBeerActivity.this, photos.toArray(new Photo[photos.size()]),
                getCameraDrawableAsPhotoStub());
    }

    private PhotoStub getCameraDrawableAsPhotoStub() {
        final int resourceId = android.R.drawable.ic_menu_camera;
        final Resources resources = getResources();
        final Uri cameraDrawableUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                + resources.getResourcePackageName(resourceId)
                + '/' + resources.getResourceTypeName(resourceId)
                + '/' + resources.getResourceEntryName(resourceId));

        return PhotoFactory.newPhotoStub(cameraDrawableUri, PhotoStatus.TRANSIENT);
    }

    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        originalWortTextView.addTextChangedListener(new CalculateAlcoholTextWatcher());
        originalWortTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(originalBeer.getOriginalWort()
                            .map(originalWort -> !ObjectUtil.areEqual(originalWort, s))
                            .orElseGet(() -> !TextUtils.isEmpty(s)));
                } else {
                    setSaveButtonEnabled(true);
                }
                beerBuilder.originalWort(s);
            }
        });
        alcoholTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(originalBeer.getAlcohol()
                            .map(alcohol -> !ObjectUtil.areEqual(alcohol, s))
                            .orElseGet(() -> !TextUtils.isEmpty(s)));
                } else {
                    setSaveButtonEnabled(true);
                }
                beerBuilder.alcohol(s);
            }
        });
        specificsTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(originalBeer.getSpecifics()
                            .map(specifics -> !ObjectUtil.areEqual(specifics, s))
                            .orElseGet(() -> !TextUtils.isEmpty(s)));
                } else {
                    setSaveButtonEnabled(true);
                }
                beerBuilder.specifics(s);
            }
        });
        ibuTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(originalBeer.getIbu()
                            .map(String::valueOf)
                            .map(ibu -> !ObjectUtil.areEqual(ibu, s))
                            .orElseGet(() -> !TextUtils.isEmpty(s)));
                } else {
                    setSaveButtonEnabled(true);
                }
            }
        });
        notesTextView.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (null != originalBeer) {
                    setSaveButtonEnabled(originalBeer.getNotes()
                            .map(notes -> !ObjectUtil.areEqual(notes, s))
                            .orElseGet(() -> !TextUtils.isEmpty(s)));
                } else {
                    setSaveButtonEnabled(true);
                }
                beerBuilder.notes(s);
            }
        });
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save state of TextViews which are not automatically saved by Android as they are no EditTexts.
        final Brewery brewery = (Brewery) breweryTextView.getTag();
        if (null != brewery) {
            outState.putParcelable(NEW_BEER_BREWERY, brewery);
        }
        final BeerStyle style = (BeerStyle) styleTextView.getTag();
        if (null != style) {
            outState.putParcelable(NEW_BEER_STYLE, style);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            if (SELECT_BREWERY_REQUEST_CODE == requestCode) {
                saveSelectedBreweryAsTag(data);
            } else if (SELECT_STYLE_REQUEST_CODE == requestCode) {
                saveSelectedStyleAsTag(data);
            }
        }
    }

    private void saveSelectedBreweryAsTag(final Intent data) {
        // Save selected brewery as tag in brewery name TextView
        final Brewery selectedBrewery = data.getParcelableExtra(SelectBreweryActivity.SELECTED_BREWERY);
        beerBuilder.brewery(selectedBrewery);
        breweryTextView.setText(selectedBrewery.getName());
        breweryTextView.setTag(selectedBrewery);
    }

    private void saveSelectedStyleAsTag(final Intent data) {
        // Save selected style as tag in style TextView
        final BeerStyle selectedBeerStyle = data.getParcelableExtra(SelectBeerStyleActivity.SELECTED_BEER_STYLE);
        beerBuilder.style(selectedBeerStyle);
        styleTextView.setText(selectedBeerStyle.getName());
        styleTextView.setTag(selectedBeerStyle);
    }

    /**
     * Persists the beer which is derived from the entered data.
     *
     * @param view the View is ignored by this method.
     */
    public void saveBeer(final View view) {
        final SaveAction saveAction = (null != originalBeer) ? new SaveEditedBeerAction() : new SaveNewBeerAction();
        saveAction.execute();
    }

    @Override
    protected void onFinish() {
        if (saveBeerButton.isEnabled()) {
            final SaveEntityDialogue saveDialogue = SaveEntityDialogue.getBuilder(this)
                    .setTitle(R.string.edit_beer_exit_title)
                    .setMessage(getString(R.string.edit_beer_save_before_exit))
                    .setOnSaveEntityListener(aVoid -> {
                       saveBeer(null);
                       super.onFinish();
                    })
                    .setOnDiscardEntityListener(aVoid -> super.onFinish())
                    .build();
            saveDialogue.show();
        } else {
            super.onFinish();
        }
    }

    /**
     * This TextWatcher calculates the alcohol based on the original wort. This only applies if no alcohol value was
     * entered directly.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class CalculateAlcoholTextWatcher extends AbstractTextWatcher {
        private boolean alcoholTextViewChangedByThis;

        private CalculateAlcoholTextWatcher() {
            alcoholTextViewChangedByThis = false;
        }

        @Override
        public void afterTextChanged(final Editable s) {
            if (!TextUtils.isEmpty(s)) {
                if (isAlcoholTextViewCurrentlyEmpty() || alcoholTextViewChangedByThis) {

                    // otherwise an alcohol value for the beer was already set by the user or loaded from database
                    calculateAlcohol(s);
                }
            } else if (alcoholTextViewChangedByThis) {
                alcoholTextView.setText(null);
            }
        }

        private boolean isAlcoholTextViewCurrentlyEmpty() {
            return TextUtils.isEmpty(alcoholTextView.getText());
        }

        private void calculateAlcohol(final CharSequence s) {
            final double originalWort = Double.parseDouble(s.toString());
            final Provider<BigDecimal> alcoholProvider = AlcoholProvider.newInstance(originalWort);
            final BigDecimal alcoholValue = alcoholProvider.get();
            if (0 < alcoholValue.compareTo(BigDecimal.ZERO)) {
                final NumberFormatter numberFormatter = NumberFormatter.newInstance(alcoholValue, 1);
                alcoholTextView.setText(numberFormatter.get());
                alcoholTextViewChangedByThis = true;
            }
        }
    }

    /**
     * Abstract base implementation of an action for saving a new or edited beer.
     *
     * @since 1.2.0
     */
    @NotThreadSafe
    private abstract class SaveAction extends AsyncTask<Void, Void, Maybe<Beer>> {
        /**
         * Constructs a new {@code AbstractSaveAction} object.
         */
        protected SaveAction() {
            super();
        }

        @Override
        protected Maybe<Beer> doInBackground(final Void ... noArg) {
            beerBuilder.addPhotos(beerPhotoManager.persistAllPhotos());
            beerBuilder.removePhotos(beerPhotoManager.deleteAllPhotos());
            return persistBeer(getContentResolver());
        }

        protected abstract Maybe<Beer> persistBeer(ContentResolver contentResolver);

        @Override
        protected void onPostExecute(final Maybe<Beer> beerMaybe) {
            super.onPostExecute(beerMaybe);
            if (beerMaybe.isPresent()) {
                setActivityResult(beerMaybe.get());
                saveBeerButton.setEnabled(false);
                finish();
            } else {
                Toaster.showLong(EditBeerActivity.this, R.string.edit_beer_save_failed);
            }
        }

        private void setActivityResult(final Beer beer) {
            final Intent resultIntent = new Intent();
            resultIntent.putExtra(EDITED_BEER, beer);
            setResult(Activity.RESULT_OK, resultIntent);
        }
    }

    /**
     * This class inserts a new Beer into the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveNewBeerAction extends SaveAction {
        @Override
        protected Maybe<Beer> persistBeer(final ContentResolver contentResolver) {
            final Beer newBeer = beerBuilder.build();
            final Uri uri = contentResolver.insert(newBeer.getContentUri(), newBeer.asContentValues());
            if (null != uri) {
                return Maybe.of(newBeer);
            }
            return Maybe.empty();
        }
    }

    /**
     * This class updates an existing Beer in the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveEditedBeerAction extends SaveAction {
        @Override
        protected Maybe<Beer> persistBeer(final ContentResolver contentResolver) {
            beerBuilder.incrementRevision();
            final Beer editedBeer = beerBuilder.build();
            final int updatedRowCount = contentResolver.update(editedBeer.getContentUri(), editedBeer.asContentValues(),
                    null, null);
            if (1 == updatedRowCount) {
                return Maybe.of(editedBeer);
            }
            return Maybe.empty();
        }
    }

    /**
     * This listener shows a dialogue for managing the selected photo.
     *
     * @since 1.2.0
     */
    @NotThreadSafe
    private final class PhotoOnClickListener implements AdapterView.OnItemClickListener {
        private final PhotoGridViewAdapter photoGridViewAdapter;

        private PhotoOnClickListener(final PhotoGridViewAdapter photoGridViewAdapter) {
            this.photoGridViewAdapter = photoGridViewAdapter;
        }

        @Override
        public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
            final FragmentManager supportFragmentManager = getSupportFragmentManager();
            final ManagePhotoDialogFragment.Builder dialogueBuilder = ManagePhotoDialogFragment.getBuilder();

            final PhotoStub photoOfCurrentSlot = (PhotoStub) view.getTag();
            final PhotoStatus photoPhotoStatus = photoOfCurrentSlot.getStatus();
            if (PhotoStatus.TRANSIENT == photoPhotoStatus) {
                dialogueBuilder.withOnPhotoSelectedListener(photoUri -> {
                    final PhotoStub photoStub = PhotoFactory.newPhotoStub(photoUri, PhotoStatus.PENDING);
                    photoGridViewAdapter.putPhoto(position, photoStub);
                    beerPhotoManager.putPending(photoStub);
                    setSaveButtonEnabled(true);
                });
                dialogueBuilder.withOnPhotoCapturedListener(photoUri -> {
                    final PhotoStub photoStub = PhotoFactory.newPhotoStub(photoUri, PhotoStatus.PENDING);
                    photoGridViewAdapter.putPhoto(position, photoStub);
                    beerPhotoManager.putPending(photoStub);
                    setSaveButtonEnabled(true);
                });
            } else if (PhotoStatus.EXISTING == photoPhotoStatus) {
                dialogueBuilder.withOnDeletePhotoListener((Void aVoid) -> {
                    photoGridViewAdapter.setPlaceholder(position);
                    beerPhotoManager.deleteExisting(photoOfCurrentSlot);
                    setSaveButtonEnabled(true);
                });
            } else if (PhotoStatus.PENDING == photoPhotoStatus) {
                dialogueBuilder.withOnPhotoDeselectListener(v -> {
                    photoGridViewAdapter.setPlaceholder(position);
                    beerPhotoManager.removePending(photoOfCurrentSlot.getUri());
                    if (!beerPhotoManager.hasPending()) {
                        setSaveButtonEnabled(false);
                    }
                });
            }

            final ManagePhotoDialogFragment dialogue = dialogueBuilder.build();
            dialogue.show(supportFragmentManager, getString(R.string.add_photo_dialogue_title));
        }
    }

}
