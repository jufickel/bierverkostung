/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This fragment shows the details of a {@link Beer} and its associated
 * {@link de.retujo.bierverkostung.brewery.Brewery}. Only available data is shown, i. e. the layout may vary from beer
 * to beer.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class BeerDetailsFragment extends Fragment {

    /**
     * Key for the Beer argument.
     */
    public static final String ARG_BEER = "beer";

    private volatile Beer beer;

    /**
     * Constructs a new {@code BeerDetailsFragment}.
     * <p>
     * <em>Do not call this constructor.</em> It is required by Android internally.
     */
    public BeerDetailsFragment() {
        // This ensures that the beer can be put into the arguments in the Activity's "onAttachFragment" method.
        setArguments(new Bundle());
        beer = null;
    }

    /**
     * Returns a new instance of {@code BeerDetailsFragment}.
     *
     * @param beer the beer to be shown in this fragment.
     * @return the instance.
     * @throws NullPointerException if {@code beer} is {@code null}.
     */
    @Nonnull
    public static BeerDetailsFragment newInstance(@Nonnull final Beer beer) {
        isNotNull(beer, "beer");
        final BeerDetailsFragment result = new BeerDetailsFragment();

        final Bundle args = new Bundle();
        args.putParcelable(ARG_BEER, beer);
        result.setArguments(args);

        return result;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
       if (null != arguments) {
            beer = arguments.getParcelable(ARG_BEER);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        final View result = inflater.inflate(R.layout.fragment_beer_details, container, false);
        if (null != beer) {
            initView(result);
        }
        return result;
    }

    private void initView(final View view) {
        final BeerInitializer beerInitializer = new BeerInitializer(view);
        beerInitializer.run();
    }

    /**
     * This class shows the available properties of this fragment's Beer.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class BeerInitializer implements Runnable {
        private final View parentView;

        private BeerInitializer(final View parentView) {
            this.parentView = parentView;
        }

        @Override
        public void run() {
            initBeerName();
            initBeerStyle();
            initIngredients();
            initOriginalWort();
            initAlcohol();
            initIbu();
            initSpecifics();
            initNotes();
            initPhotos();
        }

        private void initBeerName() {
            final TextView beerNameTextView = getViewAndMakeVisible(R.id.show_beer_beer_name);
            beerNameTextView.setText(beer.getName());
        }

        private void initBeerStyle() {
            final Maybe<BeerStyle> styleMaybe = beer.getStyle();
            if (styleMaybe.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_beer_style_label);
                final TextView styleTextView = getViewAndMakeVisible(R.id.show_beer_beer_style);
                final BeerStyle beerStyle = styleMaybe.get();
                styleTextView.setText(beerStyle.getName());
            }
        }

        private void initIngredients() {
            final Maybe<String> ingredients = beer.getIngredients();
            if (ingredients.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_ingredients_label);
                final TextView ingredientsTextView = getViewAndMakeVisible(R.id.show_beer_ingredients);
                ingredientsTextView.setText(ingredients.get());
            }
        }

        private void initOriginalWort() {
            final Maybe<String> originalWort = beer.getOriginalWort();
            if (originalWort.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_original_wort_label);
                final TextView originalWortTextView = getViewAndMakeVisible(R.id.show_beer_original_wort);
                originalWortTextView.setText(originalWort.get());
            }
        }

        private void initAlcohol() {
            final Maybe<String> alcohol = beer.getAlcohol();
            if (alcohol.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_alcohol_label);
                final TextView alcoholTextView = getViewAndMakeVisible(R.id.show_beer_alcohol);
                alcoholTextView.setText(alcohol.get());
            }
        }

        private void initIbu() {
            final Maybe<Integer> ibu = beer.getIbu();
            if (ibu.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_ibu_label);
                final TextView ibuTextView = getViewAndMakeVisible(R.id.show_beer_ibu);
                ibuTextView.setText(String.valueOf(ibu.get()));
            }
        }

        private void initSpecifics() {
            final Maybe<String> specifics = beer.getSpecifics();
            if (specifics.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_specifics_label);
                final TextView specificsTextView = getViewAndMakeVisible(R.id.show_beer_specifics);
                specificsTextView.setText(specifics.get());
            }
        }

        private void initNotes() {
            final Maybe<String> notes = beer.getNotes();
            if (notes.isPresent()) {
                getViewAndMakeVisible(R.id.show_beer_notes_label);
                final TextView notesTextView = getViewAndMakeVisible(R.id.show_beer_notes);
                notesTextView.setText(notes.get());
            }
        }

        private void initPhotos() {
            final Collection<Photo> photos = beer.getPhotos();
            if (!photos.isEmpty()) {
                getViewAndMakeVisible(R.id.show_beer_images_label);
            }
            final Photo[] photosArray = photos.toArray(new Photo[photos.size()]);
            final GridView imagesGridView = getViewAndMakeVisible(R.id.show_beer_images_grid_view);
            imagesGridView.setAdapter(
                    PhotoGridViewAdapter.getInstance(getContext(), photosArray, null));
            imagesGridView.setOnItemClickListener((parent, view, position, id) -> {
                final Intent intent = new Intent(Intent.ACTION_VIEW);
                final Photo photo = photosArray[position];
                intent.setDataAndType(photo.getUri(),"image/*");
                final Context context = getContext();
                context.startActivity(intent);
            });
        }

        @SuppressWarnings("unchecked")
        private <T extends View> T getViewAndMakeVisible(final int textViewId) {
            final View result = parentView.findViewById(textViewId);
            result.setVisibility(View.VISIBLE);
            return (T) result;
        }
    }

}
