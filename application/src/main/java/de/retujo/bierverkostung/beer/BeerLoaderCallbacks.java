/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;

/**
 * The callbacks for the beers CursorLoader.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class BeerLoaderCallbacks extends AbstractLoaderCallbacks {

    /**
     * Identifier of the CursorLoader for beers.
     */
    public static final int ID = 149;

    private static final String SORT_ORDER = BeerEntry.COLUMN_NAME.getQualifiedName() + " ASC";

    /**
     * Constructs a new {@code CountriesLoaderCallbacks} object.
     *
     * @param context the current context.
     * @param cursorAdapter the adapter for binding the countries to the UI.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public BeerLoaderCallbacks(@Nonnull final Context context, @Nonnull final BeerCursorAdapter cursorAdapter) {
        super(ID, context, cursorAdapter);
    }

    @Override
    protected Loader<Cursor> doCreateLoader(@Nonnull final Context context) {
        return new CursorLoader(context, BeerEntry.TABLE.getContentUri(), null, null, null, SORT_ORDER);
    }

}
