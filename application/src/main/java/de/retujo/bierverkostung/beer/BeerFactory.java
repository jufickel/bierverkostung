/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentResolver;
import android.database.Cursor;

import org.json.JSONObject;

import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.BierverkostungApplication;
import de.retujo.bierverkostung.beerstyle.BeerStyleFactory;
import de.retujo.bierverkostung.brewery.BreweryFactory;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoFileEntry;
import de.retujo.bierverkostung.data.Column;
import de.retujo.bierverkostung.data.CursorReader;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.ExtendedBaseColumns;
import de.retujo.bierverkostung.data.Revision;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.data.Table;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.bierverkostung.photo.PhotoStatus;
import de.retujo.java.util.AllNonnull;

/**
 * A factory for immutable instances of {@link Beer}.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
public final class BeerFactory {

    private static final String PHOTOS_SORT_ORDER = BeerPhotoFileEntry.COLUMN_NAME + " ASC";

    private BeerFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link Beer} from the specified Cursor. This method loads the photos which
     * are associated with the beer, too.
     *
     * @param cursor the cursor to provide the beer data.
     * @return the instance.
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     */
    @Nullable
    public static Beer newBeerFromCursor(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final BeerBuilder beerBuilder = newBeerBuilderFromCursor(cursor);
        beerBuilder.photos(loadPhotosOfBeer(beerBuilder.getId()));

        return beerBuilder.build();
    }

    /**
     * Returns a new immutable instance of {@link Beer} from the specified Cursor. The returned Beer contains no photos.
     *
     * @param cursor the cursor to provide the beer data.
     * @return the instance.
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     * @see #newBeerFromCursor(Cursor)
     */
    @Nullable
    public static Beer newBeerFromCursorWithoutPhotos(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        return newBeerBuilderFromCursor(cursor).build();
    }

    private static BeerBuilder newBeerBuilderFromCursor(final Cursor cursor) {
        final CursorReader cr = CursorReader.of(cursor);

        final EntityCommonData beerCommonData = getEntityCommonData(cr, BeerEntry.TABLE);

        return BeerBuilder.newInstance(cr.getString(BeerEntry.COLUMN_NAME))
                .commonData(beerCommonData)
                .brewery(BreweryFactory.newBrewery(cursor))
                .style(BeerStyleFactory.newBeerStyle(cursor))
                .originalWort(cr.getString(BeerEntry.COLUMN_ORIGINAL_WORT))
                .alcohol(cr.getString(BeerEntry.COLUMN_ALCOHOL))
                .ibu(cr.getInt(BeerEntry.COLUMN_IBU))
                .ingredients(cr.getString(BeerEntry.COLUMN_INGREDIENTS))
                .specifics(cr.getString(BeerEntry.COLUMN_SPECIFICS))
                .notes(cr.getString(BeerEntry.COLUMN_NOTES));
    }

    private static EntityCommonData getEntityCommonData(final CursorReader cursorReader, final Table table) {
        final UUID id = readId(cursorReader, table.getColumnOrThrow(ExtendedBaseColumns._ID));
        final Revision revision = readRevision(cursorReader, table.getColumnOrThrow(ExtendedBaseColumns._REVISION));

        return EntityCommonData.getInstance(id, revision);
    }

    private static UUID readId(final CursorReader cursorReader, final Column<String> idColumn) {
        return UUID.fromString(cursorReader.getString(idColumn));
    }

    private static Revision readRevision(final CursorReader cursorReader, final Column<Integer> revisionColumn) {
        return Revision.of(cursorReader.getInt(revisionColumn));
    }

    private static Collection<Photo> loadPhotosOfBeer(final UUID beerId) {
        final ContentResolver contentResolver = BierverkostungApplication.getAppContentResolver();
        final Selection s = Selection.where(BeerPhotoEntry.COLUMN_BEER_ID).is(beerId).build();
        final Cursor cursor = contentResolver.query(BeerPhotoEntry.TABLE.getContentUri(), null,
                s.getSelectionString(), s.getSelectionArgs(), PHOTOS_SORT_ORDER);
        if (null != cursor) {
            try {
                return newBeerPhotosFromCursor(cursor);
            } finally {
                // Because of the scope of this Cursor object it has to be closed right here.
                cursor.close();
            }
        }
        return Collections.emptySet();
    }

    private static Collection<Photo> newBeerPhotosFromCursor(@Nullable final Cursor cursor) {
        final Collection<Photo> result = new TreeSet<>();

        boolean hasMore = null != cursor && cursor.moveToFirst();
        while (hasMore) {
            result.add(newBeerPhotoFromCursor(cursor));
            hasMore = cursor.moveToNext();
        }

        return result;
    }

    /**
     * Returns a new immutable Photo from the specified Cursor.
     *
     * @param cursor the Cursor providing the Photo data or {@code null}.
     * @return the Photo or {@code null}.
     */
    @Nullable
    public static Photo newBeerPhotoFromCursor(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final CursorReader cr = CursorReader.of(cursor);

        final EntityCommonData commonData = getEntityCommonData(cr, BeerPhotoEntry.TABLE);
        final UUID beerId = UUID.fromString(cr.getString(BeerPhotoEntry.COLUMN_BEER_ID));
        final PhotoFile photoFile = newBeerPhotoFileFromCursor(cursor);

        return BeerPhoto.getInstance(commonData, beerId, photoFile, PhotoStatus.EXISTING);
    }

    /**
     * Returns a PhotoFile object from the data provided by the specified CursorReader.
     *
     * @param cursor the Cursor to obtain the properties of the photo file or {@code null}.
     * @return the PhotoFile object or {@code null}.
     * @since 1.2.0
     */
    @Nullable
    public static PhotoFile newBeerPhotoFileFromCursor(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }
        final CursorReader cursorReader = CursorReader.of(cursor);
        final EntityCommonData commonData = getEntityCommonData(cursorReader, BeerPhotoFileEntry.TABLE);
        final String photoName = cursorReader.getString(BeerPhotoFileEntry.COLUMN_NAME);

        return BeerPhotoFile.getInstance(commonData, photoName);
    }

    /**
     * Returns an new immutable instance of a photo file with the specified name. The ID of the returned object is
     * randomly generated.
     *
     * @param fileName the name of the photo file.
     * @return the new PhotoFile object.
     * @throws NullPointerException if {@code fileName} is {@code null}.
     * @throws IllegalArgumentException if {@code fileName} is empty.
     */
    public static PhotoFile newBeerPhotoFile(final CharSequence fileName) {
        return BeerPhotoFile.getInstance(EntityCommonData.getInstance(), fileName);
    }

    /**
     * Returns a new immutable BeerPhoto with the specified properties and a randomly generated ID.
     *
     * @param beerId the ID of the beer to which the result belongs.
     * @param photoFile the representation of the physical file of the result.
     * @param status the status of the result.
     * @return the new Photo object.
     * @throws NullPointerException if any argument is {@code null}.
     * @since 1.2.0
     */
    public static Photo newBeerPhoto(final UUID beerId, final PhotoFile photoFile, final PhotoStatus status) {
        return BeerPhoto.getInstance(EntityCommonData.getInstance(), beerId, photoFile, status);
    }

    /**
     * Returns a new immutable instance of {@link Beer} from the specified {@link JSONObject}.
     *
     * @param beerJsonObject the JSON object to be converted to a Beer.
     * @return the beer.
     * @throws NullPointerException if {@code beerJsonObject} is {@code null}.
     * @throws de.retujo.bierverkostung.exchange.JsonConversionException if {@code beerJsonObject} cannot be converted to
     * a Beer.
     * @since 1.2.0
     */
    public static Beer newBeer(final JSONObject beerJsonObject) {
        final BeerJsonConverter converter = BeerJsonConverter.getInstance();
        return converter.fromJson(beerJsonObject);
    }

}
