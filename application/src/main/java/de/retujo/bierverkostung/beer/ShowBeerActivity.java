/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.text.MessageFormat;
import java.util.Collection;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.brewery.BreweryDetailsFragment;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.DeleteEntityDialogue;
import de.retujo.bierverkostung.common.Toaster;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;
import de.retujo.bierverkostung.exchange.FileChooserDialogue;
import de.retujo.java.util.Maybe;

/**
 * This activity shows the details of a {@link Beer}. The beer can be obtained from the Intent with the key
 * <code>"{@value #BEER}"</code>.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class ShowBeerActivity extends BaseActivity {

    /**
     * Key for the Beer of which the details are shown by this Activity.
     */
    public static final String BEER = "beer";

    private static final int EDIT_BEER_REQUEST_CODE = 68;

    private Beer shownBeer;

    /**
     * Constructs a new {@code ShowBeerActivity} object.
     */
    public ShowBeerActivity() {
        shownBeer = null;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_beer);

        final Intent afferentIntent = getIntent();
        shownBeer = afferentIntent.getParcelableExtra(BEER);

        final Fragment beerDetailsFragment = BeerDetailsFragment.newInstance(shownBeer);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.show_beer_details_container, beerDetailsFragment);

        final Maybe<Brewery> brewery = shownBeer.getBrewery();
        if (brewery.isPresent()) {
            final Fragment breweryDetailsFragment = BreweryDetailsFragment.newInstance(brewery.get());
            fragmentTransaction.add(R.id.show_beer_details_container, breweryDetailsFragment);
        }

        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.entity_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int itemId = item.getItemId();
        if (R.id.options_menu_edit == itemId) {
            final Intent editBeerIntent = new Intent(this, EditBeerActivity.class);
            editBeerIntent.putExtra(EditBeerActivity.EDIT_BEER, shownBeer);
            startActivityForResult(editBeerIntent, EDIT_BEER_REQUEST_CODE);
            return true;
        } else if (R.id.options_menu_delete == itemId) {
            final String beerName = shownBeer.getName();
            final DeleteEntityDialogue dialogue = DeleteEntityDialogue.getBuilder(this, shownBeer)
                    .setTitle(R.string.delete_beer_dialog_title)
                    .setMessage(MessageFormat.format(getString(R.string.delete_beer_dialog_message), beerName))
                    .setOnDeleteEntityListener(this::deleteBeerAndItsPhotos)
                    .build();
            dialogue.show();
            return true;
        } else if (R.id.options_menu_export == itemId) {
            FileChooserDialogue.forDirectory(this, Environment.getExternalStorageDirectory())
                    .setOnFileSelectedListener(selectedDirectory -> {
                        final Intent intent = new Intent(this, getClass());
                        intent.putExtra(BEER, shownBeer);
                        final ExportBeerNotificator exportNotificator = ExportBeerNotificator.getInstance(this, intent);
                        final BeerExporter beerExporter = BeerExporter.newInstance(this, selectedDirectory,
                                exportNotificator::update);
                        beerExporter.execute(shownBeer);
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteBeerAndItsPhotos(final Beer beerToBeDeleted) {
        DeleteDbEntityTask.<Beer>getInstance(getContentResolver())
                .setOnFailedToDeleteListener(notDeletedBeers -> showDeleteBeerFailedToast())
                .setOnDeletedListener(this::deletePhotos)
                .execute(beerToBeDeleted);
    }

    private void deletePhotos(final Collection<Beer> beers) {
        DeleteBeerPhotosTask.getInstance(getContentResolver())
                .setOnDeletedListener(deletedPhotos -> finish())
                .setOnFailedToDeleteListener(notDeletedPhotos -> finish())
                .execute(beers.toArray(new Beer[beers.size()]));
    }

    private void showDeleteBeerFailedToast() {
        final Context context = ShowBeerActivity.this;
        Toaster.showLong(context, R.string.delete_beer_dialog_failed_to_delete, shownBeer.getName());
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (Activity.RESULT_OK == resultCode && EDIT_BEER_REQUEST_CODE == requestCode) {
            final Beer editedBeer = data.getParcelableExtra(EditBeerActivity.EDITED_BEER);
            final Intent showEditedBeerIntent = new Intent(this, getClass());
            showEditedBeerIntent.putExtra(BEER, editedBeer);
            startActivity(showEditedBeerIntent);
            finish();
        }
    }

}
