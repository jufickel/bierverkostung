/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import java.util.Set;

import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * This interface represents a beer.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface Beer extends DataEntity, Jsonable {

    /**
     * Returns the name of this beer.
     *
     * @return the name.
     */
    String getName();

    /**
     * Returns the brewery of this beer.
     *
     * @return the brewery or an empty Maybe.
     */
    Maybe<Brewery> getBrewery();

    /**
     * Returns the style of this beer.
     *
     * @return the style or an empty Maybe.
     */
    Maybe<BeerStyle> getStyle();

    /**
     * Returns the original wort of this beer.
     *
     * @return the original wort or an empty Maybe.
     */
    Maybe<String> getOriginalWort();

    /**
     * Returns the volume alcohol of this beer.
     *
     * @return the volume alcohol or an empty Maybe.
     */
    Maybe<String> getAlcohol();

    /**
     * Returns the IBU of this beer.
     *
     * @return the IBU or an empty Maybe.
     */
    Maybe<Integer> getIbu();

    /**
     * Returns the ingredients of this beer.
     *
     * @return the ingredients or an empty Maybe.
     */
    Maybe<String> getIngredients();

    /**
     * Returns the specifics of this beer.
     *
     * @return the specifics or an empty Maybe.
     */
    Maybe<String> getSpecifics();

    /**
     * Returns the notes of this beer.
     *
     * @return the notes or an empty Maybe.
     */
    Maybe<String> getNotes();

    /**
     * Returns an unmodifiable unsorted collection containing the photos which are associated with this beer.
     *
     * @return the photos or an empty collection.
     * @since 1.2.0
     */
    Set<Photo> getPhotos();

}
