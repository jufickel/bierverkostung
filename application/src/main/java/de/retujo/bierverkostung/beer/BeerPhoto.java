/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.bierverkostung.photo.PhotoStatus;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;
import de.retujo.bierverkostung.data.Revision;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This immutable implementation of {@link Photo} represents a photo of a Beer, i. e. it knows the ID of the beer it
 * belongs to.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class BeerPhoto implements Photo, Comparable<Photo> {

    /**
     * Creator which creates instances of {@code BeerPhoto} from a Parcel.
     */
    public static final Parcelable.Creator<Photo> CREATOR = new BeerPhotoCreator();

    private final EntityCommonData commonData;
    private final UUID beerId;
    private final PhotoFile photoFile;
    private final Uri photoFileUri;
    private final PhotoStatus status;

    private BeerPhoto(final EntityCommonData theCommonData,
            final UUID theBeerId,
            final PhotoFile thePhotoFile,
            final PhotoStatus theStatus) {

        commonData = isNotNull(theCommonData, "common data");
        beerId = isNotNull(theBeerId, "beer ID");
        photoFile = isNotNull(thePhotoFile, "photo file");
        photoFileUri = Uri.fromFile(photoFile.getFile());
        status = isNotNull(theStatus, "status");
    }

    /**
     * Returns an instance of {@code BeerPhoto} with the specified properties.
     *
     * @param commonData the common data of the photo.
     * @param beerId the ID of the beer to which the photo belongs.
     * @param photoFile the representation of the physical photo file.
     * @param photoStatus the status of the photo.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static BeerPhoto getInstance(final EntityCommonData commonData,
            final UUID beerId,
            final PhotoFile photoFile,
            final PhotoStatus photoStatus) {

        return new BeerPhoto(commonData, beerId, photoFile, photoStatus);
    }

    /**
     * Returns the ID of the beer to which this photo belongs to.
     *
     * @return the beer ID.
     */
    public UUID getBeerId() {
        return beerId;
    }

    @Override
    public UUID getId() {
        return commonData.getId();
    }

    @Override
    public PhotoFile getFile() {
        return photoFile;
    }

    @Override
    public Photo setFile(final PhotoFile newPhotoFile) {
        isNotNull(newPhotoFile, "new PhotoFile");
        if (ObjectUtil.areEqual(photoFile, newPhotoFile)) {
            return this;
        }
        return getInstance(commonData, beerId, newPhotoFile, status);
    }

    @Override
    public UUID getReferenceId() {
        return getBeerId();
    }

    @Override
    public Photo setReferenceId(final UUID newReferenceId) {
        if (ObjectUtil.areEqual(beerId, isNotNull(newReferenceId, "new reference ID"))) {
            return this;
        }
        return getInstance(commonData, newReferenceId, photoFile, status);
    }

    @Override
    public Uri getUri() {
        return photoFileUri;
    }

    @Override
    public String getName() {
        return photoFile.getFileName();
    }

    @Override
    public PhotoStatus getStatus() {
        return status;
    }

    @Override
    public int compareTo(final Photo o) {
        return getName().compareTo(o.getName());
    }

    @Override
    public ContentValues asContentValues() {
        final ContentValues result = commonData.asContentValues(BeerPhotoEntry.TABLE);
        result.put(BeerPhotoEntry.COLUMN_BEER_ID.toString(), beerId.toString());
        result.put(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID.toString(), photoFile.getId().toString());
        return result;
    }

    @Override
    public Uri getContentUri() {
        return commonData.getContentUri(BeerPhotoEntry.TABLE);
    }

    @Override
    public Revision getRevision() {
        return commonData.getRevision();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        commonData.writeToParcel(dest, flags);
        dest.writeString(beerId.toString());
        dest.writeParcelable(photoFile, flags);
        dest.writeString(status.toString());
    }

    @Override
    public JSONObject toJson() {
        final JsonConverter<BeerPhoto> jsonConverter = BeerPhotoJsonConverter.getInstance();
        return jsonConverter.toJson(this);
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BeerPhoto other = (BeerPhoto) o;

        return commonData.equals(other.commonData) &&
                beerId.equals(other.beerId) &&
                photoFile.equals(other.photoFile) &&
                status == other.status;
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(commonData, beerId, photoFile, status);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                commonData +
                ", beerId=" + beerId +
                ", photoFile=" + photoFile +
                ", photoFileUri=" + photoFileUri +
                ", status=" + status +
                "}";
    }

    private static class BeerPhotoCreator extends BaseParcelableCreator<Photo> {
        @Override
        protected BeerPhoto createFromParcel(final Parcel source, final EntityCommonData commonData) {
            final UUID beerId = UUID.fromString(source.readString());
            final PhotoFile photoFile = source.readParcelable(getClass().getClassLoader());
            final PhotoStatus photoStatus = PhotoStatus.valueOf(source.readString());

            return new BeerPhoto(commonData, beerId, photoFile, photoStatus);
        }

        @Override
        public BeerPhoto[] newArray(final int size) {
            return new BeerPhoto[0];
        }
    }

}
