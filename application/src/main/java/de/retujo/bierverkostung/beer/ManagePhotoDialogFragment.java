/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.CommonFileProvider;
import de.retujo.bierverkostung.common.Toaster;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.RandomStringProvider;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This fragment creates and shows an AlertDialog which shows management tasks for a particular photo slot like
 * "select", "capture" or "delete". The available tasks depend on the registered listeners.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class ManagePhotoDialogFragment extends DialogFragment {

    private static final int SELECT_PHOTO = 138;
    private static final int CAPTURE_PHOTO = 158;

    @Nullable private Acceptor<Uri> onPhotoSelectedListener;
    @Nullable private Acceptor<Uri> onPhotoCapturedListener;
    @Nullable private Acceptor<Void> onDeselectPhotoListener;
    @Nullable private Acceptor<Void> onDeletePhotoListener;
    private Dialog dialog;
    @Nullable private Uri capturedPhotoUri;

    /**
     * Constructs a new {@code ManagePhotoDialogFragment} object.
     * <p>
     * <em>Do not use this constructor directly.</em> It is technically required by Android. Use {@link #getBuilder()}
     * instead.
     */
    public ManagePhotoDialogFragment() {
        onPhotoSelectedListener = null;
        onPhotoCapturedListener = null;
        onDeselectPhotoListener = null;
        onDeletePhotoListener = null;
        dialog = null;
        capturedPhotoUri = null;
    }

    /**
     * Returns a new builder with a fluent API for a {@code ManagePhotoDialogFragment}.
     *
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static Builder getBuilder() {
        return new Builder();
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final View view = initView();
        initViewElements(view);

        dialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setNegativeButton(R.string.add_photo_dialogue_negative_button, (d, w) -> d.dismiss())
                .create();
        return dialog;
    }

    private View initView() {
        final FragmentActivity activity = getActivity();
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.add_photo_dialog, null);
    }

    private void initViewElements(final View parentView) {
        if (null != onPhotoSelectedListener) {
            initSelectPhotoButton(parentView);
        }
        if (null != onPhotoCapturedListener) {
            initCapturePhotoButton(parentView);
        }
        if (null != onDeselectPhotoListener) {
            initButton(parentView, R.id.add_photo_dialogue_deselect_photo_button, v -> {
                dialog.dismiss();
                onDeselectPhotoListener.accept(null);
            });
        }
        if (null != onDeletePhotoListener) {
            initButton(parentView, R.id.add_photo_dialogue_delete_photo_button, v -> {
                dialog.dismiss();
                onDeletePhotoListener.accept(null);
            });
        }
    }

    private void initSelectPhotoButton(final View parentView) {
        initButton(parentView, R.id.add_photo_dialogue_select_photo_button, v -> {
            final Intent selectPhotoIntent = new Intent(Intent.ACTION_PICK);
            selectPhotoIntent.setType("image/*");
            startActivityForResult(selectPhotoIntent, SELECT_PHOTO);
        });
    }

    private void initCapturePhotoButton(final View parentView) {
        initButton(parentView, R.id.add_photo_dialogue_capture_photo_button, new PhotoTaker());
    }

    private static void initButton(final View parentView, final int buttonId, final View.OnClickListener listener) {
        final Button button = (Button) parentView.findViewById(buttonId);
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(listener);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            dialog.dismiss();
            if (SELECT_PHOTO == requestCode) {
                final Uri photoUri = data.getData();
                onPhotoSelectedListener.accept(photoUri);
            } else if (CAPTURE_PHOTO == requestCode && null != capturedPhotoUri) {
                onPhotoCapturedListener.accept(capturedPhotoUri);
                capturedPhotoUri = null;
            }
        } else if (CAPTURE_PHOTO == requestCode && null != capturedPhotoUri) {
            final File temporaryPhotoFile = new File(capturedPhotoUri.getPath());
            temporaryPhotoFile.delete();
        }
    }

    /**
     * Mutable builder with a fluent API for creating a {@code ManagePhotoDialogFragment}.
     *
     * @since 1.2.0
     */
    @AllNonnull
    @NotThreadSafe
    static final class Builder {
        @Nullable private Acceptor<Uri> onPhotoSelectedListener;
        @Nullable private Acceptor<Uri> onPhotoCapturedListener;
        @Nullable private Acceptor<Void> onPhotoDeselectListener;
        @Nullable private Acceptor<Void> onDeletePhotoListener;

        private Builder() {
            onPhotoSelectedListener = null;
            onPhotoCapturedListener = null;
            onPhotoDeselectListener = null;
            onDeletePhotoListener = null;
        }

        /**
         * Sets the listener to be notified with the photo which was selected.
         *
         * @param onPhotoSelectedListener the listener to be set.
         * @return this builder instance to allow method chaining.
         * @throws NullPointerException if {@code onPhotoSelectedListener} is {@code null}.
         */
        public Builder withOnPhotoSelectedListener(final Acceptor<Uri> onPhotoSelectedListener) {
            validateOnPhotoSelectedListener(onPhotoSelectedListener);
            this.onPhotoSelectedListener = onPhotoSelectedListener;
            return this;
        }

        private static void validateOnPhotoSelectedListener(final Acceptor<Uri> onPhotoSelectedListener) {
            isNotNull(onPhotoSelectedListener, "onPhotoSelectedListener");
        }

        /**
         * Sets the listener to be notified with the photo which was captured.
         *
         * @param onPhotoCapturedListener the listener to be set.
         * @return this builder instance to allow method chaining.
         * @throws NullPointerException if {@code onPhotoCapturedListener} is {@code null}.
         */
        public Builder withOnPhotoCapturedListener(final Acceptor<Uri> onPhotoCapturedListener) {
            this.onPhotoCapturedListener = onPhotoCapturedListener;
            return this;
        }

        /**
         * Sets the listener to be notified when the user wants to deselect a photo. Setting this listener causes the
         * dialogue to show a Button which allows the user to actually deselect the photo.
         *
         * @param onPhotoDeselectListener the listener to be set.
         * @return this builder instance to allow method chaining.
         * @throws NullPointerException if onPhotoDeselectListener is {@code null}.
         */
        public Builder withOnPhotoDeselectListener(final Acceptor<Void> onPhotoDeselectListener) {
            this.onPhotoDeselectListener = isNotNull(onPhotoDeselectListener, "on photo deselect listener");
            return this;
        }

        /**
         * Sets the listener to be notified when the user wants to delete a photo. Setting this listener causes the
         * dialogue to show a Button which allows the user to actually delete the photo.
         *
         * @param onDeletePhotoListener the listener to be set.
         * @return this builder instance to allow method chaining.
         * @throws NullPointerException if onDeletePhotoListener is {@code null}.
         */
        public Builder withOnDeletePhotoListener(final Acceptor<Void> onDeletePhotoListener) {
            this.onDeletePhotoListener = isNotNull(onDeletePhotoListener, "on delete photo listener");
            return this;
        }

        /**
         * Create a new {@code ManagePhotoDialogFragment} object.
         *
         * @return the created object.
         */
        public ManagePhotoDialogFragment build() {
            final ManagePhotoDialogFragment result = new ManagePhotoDialogFragment();
            result.onPhotoSelectedListener = onPhotoSelectedListener;
            result.onPhotoCapturedListener = onPhotoCapturedListener;
            result.onDeselectPhotoListener = onPhotoDeselectListener;
            result.onDeletePhotoListener = onDeletePhotoListener;

            return result;
        }
    }

    /**
     * This class starts the camera app. The photo is saved in an intermediary file within the cache directory of
     * this application.
     *
     * @since 1.2.0
     */
    @NotThreadSafe
    private final class PhotoTaker implements View.OnClickListener {
        private final Context context;

        private PhotoTaker() {
            context = getContext();
        }

        @Override
        public void onClick(final View v) {
            final Intent capturePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (null != capturePhotoIntent.resolveActivity(context.getPackageManager())) {
                final RandomStringProvider randomStringProvider = RandomStringProvider.getInstance();
                final String intermediaryPhotoName = randomStringProvider.get();
                final File intermediaryPhotoFile = tryToCreateIntermediaryPhotoFile(intermediaryPhotoName);
                if (null != intermediaryPhotoFile) {
                    capturedPhotoUri = getCapturedPhotoUri(intermediaryPhotoFile);
                    capturePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedPhotoUri);
                    startActivityForResult(capturePhotoIntent, CAPTURE_PHOTO);
                }
            }
        }

        private Uri getCapturedPhotoUri(final File intermediaryPhotoFile) {
            final int currentSdkVersion = Build.VERSION.SDK_INT;
            if (Build.VERSION_CODES.N > currentSdkVersion) {
                return Uri.fromFile(intermediaryPhotoFile);
            }
            return FileProvider.getUriForFile(getContext(), CommonFileProvider.AUTHORITY, intermediaryPhotoFile);
        }

        @Nullable
        private File tryToCreateIntermediaryPhotoFile(final String photoName) {
            try {
                return createIntermediaryPhotoFile(photoName);
            } catch (final IOException e) {
                Toaster.showLong(context, "Failed to create intermediary photo file!");
                return null;
            }
        }

        private File createIntermediaryPhotoFile(final String photoName) throws IOException {
            final File cacheDir = context.getExternalCacheDir();
            return File.createTempFile(photoName, ".jpg", cacheDir);
        }
    }

}
