/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractCursorAdapter;
import de.retujo.java.util.AllNonnull;

/**
 * This CursorAdapter creates and binds {@link BeerViewHolder}s, that hold the data of a beer, to a
 * RecyclerView to efficiently display the data.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
final class BeerCursorAdapter extends AbstractCursorAdapter<BeerViewHolder> {

    /**
     * Constructs a new {@code BeerCursorAdapter} object.
     *
     * @param context the current context.
     * @param onBeerSelectedListener a listener to be notified when a particular beer was clicked.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    public BeerCursorAdapter(final Context context, @Nullable final View.OnClickListener onBeerSelectedListener) {
        super(context, R.layout.beer_item, onBeerSelectedListener);
    }

    @Override
    protected BeerViewHolder doCreateViewHolder(final View view,
            @Nullable final View.OnClickListener onBeerSelectedListener) {

        return new BeerViewHolder(view, onBeerSelectedListener);
    }

    @Override
    protected void doBindViewHolder(final BeerViewHolder viewHolder, final Cursor cursor, final Context context) {
        final Beer beer = BeerFactory.newBeerFromCursor(cursor);
        if (null != beer) {
            viewHolder.setDomainObject(beer);
        }
    }

}