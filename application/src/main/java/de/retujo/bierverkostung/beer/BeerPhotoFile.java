/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.io.File;
import java.util.UUID;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.BierverkostungApplication;
import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.Table;
import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.AllNonnull;
import de.retujo.bierverkostung.data.Revision;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Immutable implementation of {@link PhotoFile}. A BeerPhotoFile is inherently tied to a dedicated directory where
 * all photos of a beer are located.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class BeerPhotoFile implements PhotoFile {

    /**
     * Creator which creates instances of {@code BeerPhotoFile} from a Parcel.
     */
    public static final Parcelable.Creator<BeerPhotoFile> CREATOR = new BeerPhotoFileCreator();

    // Within the external files "Pictures" directory.
    private static final String BEER_PHOTOS_DIRNAME = "Beer";

    private static final Table TABLE = BierverkostungContract.BeerPhotoFileEntry.TABLE;
    private final EntityCommonData commonData;
    private final File file;

    private BeerPhotoFile(final EntityCommonData theCommonData, final File theFile) {
        commonData = isNotNull(theCommonData, "common data");
        file = theFile;
    }

    /**
     * Returns an instance of {@code BeerPhotoFile}.
     *
     * @param commonData the common data like ID and Revision of the photo file.
     * @param fileName the name of the photo file.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code fileName} is empty.
     */
    public static BeerPhotoFile getInstance(final EntityCommonData commonData, final CharSequence fileName) {
        return new BeerPhotoFile(commonData, createFileHandle(fileName));
    }

    private static File createFileHandle(final CharSequence fileName) {
        argumentNotEmpty(fileName, "file name");
        final File externalPictureFilesDir = BierverkostungApplication.getPicturesDir();
        final File beerPhotosDir = new File(externalPictureFilesDir, BEER_PHOTOS_DIRNAME);

        return new File(beerPhotosDir, fileName.toString());
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public String getFileName() {
        return file.getName();
    }

    @Override
    public UUID getId() {
        return commonData.getId();
    }

    @Override
    public Uri getContentUri() {
        return commonData.getContentUri(TABLE);
    }

    @Override
    public Revision getRevision() {
        return commonData.getRevision();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        commonData.writeToParcel(dest, flags);
        dest.writeString(getFileName());
    }

    @Override
    public ContentValues asContentValues() {
        final ContentValues result = commonData.asContentValues(TABLE);
        result.put(BierverkostungContract.BeerPhotoFileEntry.COLUMN_NAME.toString(), getFileName());
        return result;
    }

    @Override
    public JSONObject toJson() {
        final JsonConverter<BeerPhotoFile> jsonConverter = BeerPhotoFileJsonConverter.getInstance();
        return jsonConverter.toJson(this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BeerPhotoFile that = (BeerPhotoFile) o;
        return ObjectUtil.areEqual(commonData, that.commonData) && ObjectUtil.areEqual(file, that.file);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(commonData, file);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                commonData +
                ", file=" + file +
                "}";
    }

    @AllNonnull
    @NotThreadSafe
    private static final class BeerPhotoFileCreator extends BaseParcelableCreator<BeerPhotoFile> {
        @Override
        protected BeerPhotoFile createFromParcel(final Parcel source, final EntityCommonData commonData) {
            return BeerPhotoFile.getInstance(commonData, source.readString());
        }

        @Override
        public BeerPhotoFile[] newArray(final int size) {
            return new BeerPhotoFile[0];
        }
    }

}
