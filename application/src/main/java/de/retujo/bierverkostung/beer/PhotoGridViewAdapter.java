/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Arrays;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoStub;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * An adapter for {@link Photo}s. It places the photos in a {@link SquaredImageView} when
 * {@link #getView(int, View, ViewGroup)} is called. The maximal capacity of this adapter is delimited to
 * {@link #MAX_NUMBER_PHOTOS} photos.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class PhotoGridViewAdapter extends BaseAdapter {

    /**
     * The maximal number of photos this adapter can hold.
     */
    static final byte MAX_NUMBER_PHOTOS = 3;

    private final Context context;
    private final PhotoStub[] photos;
    @Nullable private final PhotoStub placeholderPhoto;

    private PhotoGridViewAdapter(final Context theContext, final PhotoStub[] thePhotos,
            @Nullable final PhotoStub thePlaceholderPhoto) {

        context = theContext;
        photos = thePhotos;
        placeholderPhoto = thePlaceholderPhoto;
    }

    /**
     * Returns an instance of {@code PhotoGridViewAdapter}.
     *
     * @param context the context to be used when showing images.
     * @param initialPhotos the initial photos of this adapter. The length of this array determines the maximum
     * number of photos the returned adapter can hold. The array may be empty.
     * @param placeholderPhoto a placeholder to be shown if no actual photo for a particular position exists. May be
     * {@code null} to show nothing.
     * @return the instance.
     * @throws NullPointerException if {@code context} or {@code initialPhotos} is {@code null}.
     */
    public static PhotoGridViewAdapter getInstance(final Context context, final PhotoStub[] initialPhotos,
            @Nullable final PhotoStub placeholderPhoto) {

        isNotNull(context, "context");
        isNotNull(initialPhotos, "initial photos");

        final PhotoStub[] allPhotos = new PhotoStub[MAX_NUMBER_PHOTOS];
        System.arraycopy(initialPhotos, 0, allPhotos, 0, initialPhotos.length);
        if (initialPhotos.length < MAX_NUMBER_PHOTOS && null != placeholderPhoto) {
            Arrays.fill(allPhotos, initialPhotos.length, MAX_NUMBER_PHOTOS, placeholderPhoto);
        }

        return new PhotoGridViewAdapter(context, allPhotos, placeholderPhoto);
    }

    @Override
    public int getCount() {
        int result = 0;
        for (final PhotoStub photo : photos) {
            if (null != photo) {
                result++;
            }
        }
        return result;
    }

    @Nullable
    @Override
    public PhotoStub getItem(final int position) {
        if (position < photos.length) {
            return photos[position];
        }
        return null;
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        ImageView view = (ImageView) convertView;
        if (null == view) {
            view = createSquaredImageView();
        }

        final PhotoStub photo = getItem(position);
        if (null != photo) {
            Picasso.with(context)
                    .load(photo.getUri())
                    .fit()
                    .tag(context)
                    .into(view);

            view.setTag(photo);
        }

        return view;
    }

    private ImageView createSquaredImageView() {
        final SquaredImageView result = new SquaredImageView(context);
        result.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return result;
    }

    /**
     * Sets the specified photo to the specified position. If applicable {@link #notifyDataSetChanged()} is
     * automatically invoked.
     *
     * @param position the position where the placeholder {@code newPhoto} is set.
     * @param newPhoto the photo to be set at {@code position}.
     * @throws NullPointerException if {@code newPhoto} is {@code null}.
     */
    public void putPhoto(final int position, final PhotoStub newPhoto) {
        isNotNull(newPhoto, "new photo");
        final PhotoStub oldPhoto = getItem(position);
        if (!ObjectUtil.areEqual(oldPhoto, newPhoto) && position < photos.length) {
            photos[position] = newPhoto;
            notifyDataSetChanged();
        }
    }

    /**
     * Sets the placeholder photo to the specified position. If applicable {@link #notifyDataSetChanged()} is
     * automatically invoked. This method has only an effect if this instance of PhotoGridViewAdapter has a
     * placeholder photo.
     *
     * @param position the position where the placeholder photo is set.
     */
    public void setPlaceholder(final int position) {
        if (null != placeholderPhoto) {
            putPhoto(position, placeholderPhoto);
        }
    }

}
