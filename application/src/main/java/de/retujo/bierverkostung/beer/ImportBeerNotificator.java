/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.Notificator;
import de.retujo.bierverkostung.common.ProgressUpdate;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * When importing a Beer this class shows the progress in {@link Notification}s visually and textually.
 *
 * @since 1.2.0
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
final class ImportBeerNotificator implements Notificator<Beer> {

    /**
     * Identifier of the import beer notification. It can be used to access the notification after it is displayed.
     * This can be handy when the notification needs to be cancelled or updated.
     */
    private static final int IMPORT_BEER_NOTIFICATION_ID = 269;

    private static final int PENDING_INTENT_ID = 280;

    private final Context context;
    private final NotificationManager notificationManager;
    private final NotificationCompat.Builder notificationBuilder;

    private ImportBeerNotificator(final Context theContext, final NotificationManager theNotificationManager,
            final NotificationCompat.Builder theNotificationBuilder) {

        context = theContext;
        notificationManager = theNotificationManager;
        notificationBuilder = theNotificationBuilder;
    }

    /**
     * Returns an instance of {@code ImportBeerNotificator}.
     *
     * @param context the context.
     * @param intent the Intent to be sent when the Notification is clicked.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static ImportBeerNotificator getInstance(final Context context, final Intent intent) {
        isNotNull(context, "context");
        isNotNull(intent, "notificationBuilder");

        return new ImportBeerNotificator(context, getNotificationManager(context),
                getNotificationBuilder(context, intent));
    }

    private static NotificationManager getNotificationManager(final Context context) {
        return (NotificationManager) isNotNull(context.getSystemService(Context.NOTIFICATION_SERVICE),
                "NotificationManager");
    }

    private static NotificationCompat.Builder getNotificationBuilder(final Context context, final Intent intent) {
        return new NotificationCompat.Builder(context)
                .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setSmallIcon(R.drawable.bierverkostung_logo)
                .setLargeIcon(getLargeIcon(context.getResources()))
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentIntent(getContentIntent(context, intent))
                .setContentTitle(context.getString(R.string.import_beer_notification_title))
                .setAutoCancel(true);
    }

    private static Bitmap getLargeIcon(final Resources resources) {
        return BitmapFactory.decodeResource(resources, R.drawable.bierverkostung_logo);
    }

    private static PendingIntent getContentIntent(final Context context, final Intent intent) {
        return PendingIntent.getActivity(context, PENDING_INTENT_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void update(final ProgressUpdate<Beer> progressUpdate) {
        updateNotification(getNotification(isNotNull(progressUpdate, "progress update")));
    }

    private Notification getNotification(final ProgressUpdate<Beer> progressUpdate) {
        final String contentText = getContentText(progressUpdate);

        return notificationBuilder.setContentText(contentText)
                .setStyle(getStyle(contentText))
                .setProgress(progressUpdate.getMaxProgress(), progressUpdate.getCurrentProgress(), false)
                .build();
    }

    private String getContentText(final ProgressUpdate<Beer> progressUpdate) {
        final int maxProgress = progressUpdate.getMaxProgress();
        if (0 < maxProgress || 0 == maxProgress && 0 == progressUpdate.getCurrentProgress()) {
            // The maximum progress is reached. This is supposed to be the last received progress update.
            final Resources resources = context.getResources();
            return resources.getQuantityString(R.plurals.import_beer_notification_finished, maxProgress, maxProgress);
        }
        final Beer beer = getBeerOrThrow(progressUpdate);
        return context.getString(R.string.import_beer_notification_importing, beer.getName());
    }

    private static Beer getBeerOrThrow(final ProgressUpdate<Beer> progressUpdate) {
        final Maybe<Beer> beerMaybe = progressUpdate.getEntity();
        if (beerMaybe.isPresent()) {
            return beerMaybe.get();
        }
        throw new NullPointerException("The beer which is currently imported must not be null!");
    }

    private static NotificationCompat.Style getStyle(final CharSequence contentText) {
        final NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        return bigTextStyle.bigText(contentText);
    }

    private void updateNotification(final Notification notification) {
        notificationManager.notify(IMPORT_BEER_NOTIFICATION_ID, notification);
    }

}
