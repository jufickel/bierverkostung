/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.view.View;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.common.AbstractViewHolder;
import de.retujo.java.util.Maybe;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} binds Beer-related view contents.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class BeerViewHolder extends AbstractViewHolder<Beer> {

    private final TextView beerNameTextView;
    private final TextView beerBreweryTextView;
    private final TextView beerStyleTextView;

    /**
     * Constructs a new {@code BeerViewHolder} object.
     *
     * @param beerItemView the view inflated in onCreateViewHolder.
     * @param onBeerSelectedListener a listener to be notified when a particular beer was clicked.
     * @throws NullPointerException if {@code beerNameTextView} is {@code null}.
     */
    public BeerViewHolder(@Nonnull final View beerItemView,
            @Nullable final View.OnClickListener onBeerSelectedListener) {
        super(beerItemView);

        beerItemView.setOnClickListener(onBeerSelectedListener);
        beerNameTextView = (TextView) beerItemView.findViewById(R.id.beer_item_name_text_view);
        beerBreweryTextView = (TextView) beerItemView.findViewById(R.id.beer_item_brewery_text_view);
        beerStyleTextView = (TextView) beerItemView.findViewById(R.id.beer_item_style_text_view);
    }

    @Override
    protected void handleDomainObjectSet(@Nonnull final Beer beer) {
        beerNameTextView.setText(beer.getName());
        final Maybe<Brewery> breweryMaybe = beer.getBrewery();
        if (breweryMaybe.isPresent()) {
            final Brewery brewery = breweryMaybe.get();
            beerBreweryTextView.setText(brewery.getName());
        } else {
            beerBreweryTextView.setText(null);
        }
        final Maybe<BeerStyle> styleMaybe = beer.getStyle();
        if (styleMaybe.isPresent()) {
            final BeerStyle style = styleMaybe.get();
            beerStyleTextView.setText(style.getName());
        } else {
            beerStyleTextView.setText(null);
        }
    }

}
