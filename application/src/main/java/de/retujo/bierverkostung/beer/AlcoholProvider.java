/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.Provider;

/**
 * This class roughly calculates the alcohol of a beer.
 *
 * @since 1.0.0
 */
@Immutable
final class AlcoholProvider implements Provider<BigDecimal> {

    private static final MathContext MATH_CONTEXT = MathContext.DECIMAL64;

    private final BigDecimal originalWort;

    private AlcoholProvider(final BigDecimal theOriginalWort) {
        originalWort = theOriginalWort;
    }

    /**
     * Returns a new instance of {@code AlcoholProvider}.
     *
     * @param originalWortPlato the original value in Plato.
     * @return the instance.
     */
    @Nonnull
    public static AlcoholProvider newInstance(final double originalWortPlato) {
        return new AlcoholProvider(BigDecimal.valueOf(originalWortPlato));
    }

    @Nullable
    @Override
    public BigDecimal get() {
        final BigDecimal firstApproximation = calculateFirstApproximation();
        final BigDecimal secondApproximation = calculateSecondApproximation();
        return calculateMean(firstApproximation, secondApproximation);
    }

    private BigDecimal calculateFirstApproximation() {
        final double subtrahendValue = 2.0D;
        final double divisorValue = 2.0D;

        final BigDecimal intermediateResult = originalWort.subtract(BigDecimal.valueOf(subtrahendValue));
        return intermediateResult.divide(BigDecimal.valueOf(divisorValue), MATH_CONTEXT);
    }

    private BigDecimal calculateSecondApproximation() {
        final double divisorValue = 3.0D;
        final double multiplicandValue = 1.25D;

        return originalWort.divide(BigDecimal.valueOf(divisorValue), MATH_CONTEXT)
                .multiply(BigDecimal.valueOf(multiplicandValue));
    }

    private static BigDecimal calculateMean(final BigDecimal firstApproximation, final BigDecimal secondApproximation) {
        final double divisorValue = 2.0D;

        final BigDecimal sum = firstApproximation.add(secondApproximation);
        final BigDecimal result = sum.divide(BigDecimal.valueOf(divisorValue), MATH_CONTEXT);
        return result.stripTrailingZeros();
    }

}
