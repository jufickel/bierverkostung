/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.beerstyle.BeerStyleFactory;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.brewery.BreweryFactory;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * Converts a {@link Beer} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class BeerJsonConverter extends DataEntityJsonConverter<Beer> {

    @Immutable
    private static final class JsonName {
        private static final String NAME = "name";
        private static final String BREWERY = "brewery";
        private static final String STYLE = "style";
        private static final String ORIGINAL_WORT = "originalWort";
        private static final String ALCOHOL = "alcohol";
        private static final String IBU = "ibu";
        private static final String INGREDIENTS = "ingredients";
        private static final String SPECIFICS = "specifics";
        private static final String NOTES = "notes";
        private static final String PHOTOS = "photos";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private BeerJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code BeerJsonConverter}.
     *
     * @return the instance.
     */
    public static BeerJsonConverter getInstance() {
        return new BeerJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final Beer beer) throws JSONException {
        targetJsonObject.putOpt(JsonName.NAME, beer.getName());
        targetJsonObject.putOpt(JsonName.BREWERY, beer.getBrewery()
                .map(Brewery::toJson)
                .orElse(null));
        targetJsonObject.putOpt(JsonName.STYLE, beer.getStyle()
                .map(BeerStyle::toJson)
                .orElse(null));
        targetJsonObject.putOpt(JsonName.ORIGINAL_WORT, beer.getOriginalWort().orElse(null));
        targetJsonObject.putOpt(JsonName.ALCOHOL, beer.getAlcohol().orElse(null));
        final Maybe<Integer> ibu = beer.getIbu();
        if (ibu.isPresent()) {
            targetJsonObject.put(JsonName.IBU, ibu.get());
        }
        targetJsonObject.putOpt(JsonName.INGREDIENTS, beer.getIngredients().orElse(null));
        targetJsonObject.putOpt(JsonName.SPECIFICS, beer.getSpecifics().orElse(null));
        targetJsonObject.putOpt(JsonName.NOTES, beer.getNotes().orElse(null));
        targetJsonObject.putOpt(JsonName.PHOTOS, getPhotosAsJsonArrayOrNull(beer.getPhotos()));
    }

    @Nullable
    private static JSONArray getPhotosAsJsonArrayOrNull(final Collection<Photo> photos) {
        JSONArray result = null;
        if (!photos.isEmpty()) {
            result = new JSONArray();
            for (final Photo photo : photos) {
                result.put(photo.toJson());
            }
        }

        return result;
    }

    @Override
    protected Beer createEntityInstanceFromJson(final JSONObject sourceJsonObject, final EntityCommonData commonData)
            throws JSONException {

        final BeerBuilder beerBuilder = BeerBuilder.newInstance(sourceJsonObject.getString(JsonName.NAME));
        beerBuilder.commonData(commonData);
        if (sourceJsonObject.has(JsonName.BREWERY)) {
            final JSONObject breweryJsonObject = sourceJsonObject.getJSONObject(JsonName.BREWERY);
            beerBuilder.brewery(BreweryFactory.newBrewery(breweryJsonObject));
        }
        if (sourceJsonObject.has(JsonName.STYLE)) {
            final JSONObject beerStyleJsonObject = sourceJsonObject.getJSONObject(JsonName.STYLE);
            beerBuilder.style(BeerStyleFactory.newBeerStyle(beerStyleJsonObject));
        }
        if (sourceJsonObject.has(JsonName.ORIGINAL_WORT)) {
            beerBuilder.originalWort(sourceJsonObject.getString(JsonName.ORIGINAL_WORT));
        }
        if (sourceJsonObject.has(JsonName.ALCOHOL)) {
            beerBuilder.alcohol(sourceJsonObject.getString(JsonName.ALCOHOL));
        }
        if (sourceJsonObject.has(JsonName.IBU)) {
            beerBuilder.ibu(sourceJsonObject.getInt(JsonName.IBU));
        }
        if (sourceJsonObject.has(JsonName.INGREDIENTS)) {
            beerBuilder.ingredients(sourceJsonObject.getString(JsonName.INGREDIENTS));
        }
        if (sourceJsonObject.has(JsonName.SPECIFICS)) {
            beerBuilder.specifics(sourceJsonObject.getString(JsonName.SPECIFICS));
        }
        if (sourceJsonObject.has(JsonName.NOTES)) {
            beerBuilder.notes(sourceJsonObject.getString(JsonName.NOTES));
        }
        if (sourceJsonObject.has(JsonName.PHOTOS)) {
            beerBuilder.photos(getAsPhotoCollection(sourceJsonObject.getJSONArray(JsonName.PHOTOS)));
        }


        return beerBuilder.build();
    }

    private static Collection<Photo> getAsPhotoCollection(final JSONArray photosJsonArray) throws JSONException {
        final List<Photo> result = new ArrayList<>(photosJsonArray.length());
        final JsonConverter<BeerPhoto> beerPhotoJsonConverter = BeerPhotoJsonConverter.getInstance();
        for (int i = 0; i < photosJsonArray.length(); i++) {
            final JSONObject photoJsonObject = photosJsonArray.getJSONObject(i);
            result.add(beerPhotoJsonConverter.fromJson(photoJsonObject));
        }

        return result;
    }


}
