/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class writes single entries into a ZIP file. Duplicate entries are avoided by checking the entry paths.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class ZipFileWriter implements Closeable {

    private static final int BUFFER_SIZE = 2048;

    private final ZipOutputStream zipOutputStream;
    private final Set<String> writtenPaths;

    private ZipFileWriter(final ZipOutputStream theZipOutputStream) {
        zipOutputStream = theZipOutputStream;
        writtenPaths = new HashSet<>();
    }

    /**
     * Returns a new instance of {@code ZipFileWriter}.
     *
     * @param outputZipFile the ZIP file to be written into.
     * @return the instance.
     * @throws NullPointerException if {@code outputZipFile} is {@code null}.
     * @throws IllegalArgumentException if for {@code outputZipFile} no FileOutputStream can be created.
     */
    public static ZipFileWriter getInstance(@Nonnull final File outputZipFile) {
        isNotNull(outputZipFile, "output ZIP file");

        return new ZipFileWriter(new ZipOutputStream(tryToCreateFileOutputStream(outputZipFile)));
    }

    private static FileOutputStream tryToCreateFileOutputStream(final File outputZipFile) {
        try {
            return new FileOutputStream(outputZipFile);
        } catch (final FileNotFoundException e) {
            final String msgTemplate = "Failed to create a FileOutputStream for <{0}>!";
            throw new IllegalArgumentException(MessageFormat.format(msgTemplate, outputZipFile), e);
        }
    }

    /**
     * Adds a {@link ZipEntry} to the ZIP file to be written.
     *
     * @param entryPath the name of the ZipEntry. The name is actually a path, and may contain {@code "/"}
     * characters.
     * @param jsonable provides the content of the ZipEntry as a JSON string.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code entryPath} is empty.
     */
    public void writeEntry(@Nonnull final String entryPath, @Nonnull final Jsonable jsonable) {
        writeEntry(entryPath, JsonStringInputStream.of(jsonable));
    }

    /**
     * Adds a {@link ZipEntry} to the ZIP file to be written.
     *
     * @param entryPath the name of the ZipEntry. The name is actually a path, and may contain {@code "/"}
     * characters.
     * @param file provides the content of the ZipEntry.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code entryPath} is empty or if {@code file} does not exist.
     */
    public void writeEntry(@Nonnull final String entryPath, @Nonnull final File file) {
        writeEntry(entryPath, tryToGetFileInputStream(file));
    }

    private static FileInputStream tryToGetFileInputStream(final File file) {
        try {
            return getFileInputStream(file);
        } catch (final FileNotFoundException e) {
            final String msgTemplate = "Failed to create FileInputStream for <{0}>!";
            throw new IllegalArgumentException(MessageFormat.format(msgTemplate, file), e);
        }
    }

    private static FileInputStream getFileInputStream(final File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    /**
     * Adds a {@link ZipEntry} to the ZIP file to be written.
     *
     * @param entryPath the name of the ZipEntry. The name is actually a path, and may contain {@code "/"}
     * characters.
     * @param entryInputStream this input stream provides the content of the ZipEntry.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code entryPath} is empty.
     */
    public void writeEntry(@Nonnull final String entryPath, @Nonnull final InputStream entryInputStream) {
        argumentNotEmpty(entryPath, "entry path");
        isNotNull(entryInputStream, "entry InputStream");

        if (!writtenPaths.contains(entryPath)) {
            tryToWriteZipEntryToZipOutputStream(new ZipEntrySource(entryPath, entryInputStream));
        } else {
            tryToCloseStream(entryInputStream);
        }
    }

    private void tryToWriteZipEntryToZipOutputStream(final ZipEntrySource zipEntrySource) {
        try {
            writeZipEntryToZipOutputStream(zipEntrySource);
        } catch (final IOException e) {
            final String msgTemplate = "Failed to write <{0}> to ZIP OutputStream!";
            throw new ExportImportException(MessageFormat.format(msgTemplate, zipEntrySource.path), e);
        }
    }

    private void writeZipEntryToZipOutputStream(final ZipEntrySource zipEntrySource) throws IOException {
        final BufferedInputStream zipEntryOrigin = new BufferedInputStream(zipEntrySource.inputStream, BUFFER_SIZE);
        try {
            zipOutputStream.putNextEntry(new ZipEntry(zipEntrySource.path));

            final byte[] buffer = new byte[BUFFER_SIZE];
            int byteCount;
            while ((byteCount = tryToReadFromOrigin(zipEntryOrigin, buffer)) != -1) {
                zipOutputStream.write(buffer, 0, byteCount);
            }
        } finally {
            tryToCloseStream(zipEntryOrigin);
            writtenPaths.add(zipEntrySource.path);
        }
    }

    private static int tryToReadFromOrigin(final BufferedInputStream origin, final byte[] buffer) {
        try {
            return readFromOrigin(origin, buffer);
        } catch (final IOException e) {
            throw new ExportImportException("Failed to read from origin!", e);
        }
    }

    private static int readFromOrigin(final BufferedInputStream origin, final byte[] buffer) throws IOException {
        return origin.read(buffer, 0, BUFFER_SIZE);
    }

    private static void tryToCloseStream(final Closeable closeable) {
        try {
            closeable.close();
        } catch (final IOException e) {
            throw new ExportImportException("Failed to close stream!", e);
        }
    }

    @Override
    public void close() throws IOException {
        zipOutputStream.close();
    }

    private static final class ZipEntrySource {
        private final String path;
        private final InputStream inputStream;

        private ZipEntrySource(final String thePath, final InputStream theInputStream) {
            path = thePath;
            inputStream = theInputStream;
        }
    }

}
