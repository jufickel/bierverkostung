/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

/**
 * This exception is thrown if a File, i. e. a file or directory, cannot be copied to a particular destination.
 *
 * @since 1.2.0
 */
public class CopyFileException extends RuntimeException {

    private static final long serialVersionUID = 6818200780157638328L;

    /**
     * Constructs a new {@code CopyFileException} object.
     *
     * @param message the detail message for this exception.
     */
    public CopyFileException(final String message) {
        super(message);
    }

    /**
     * Constructs a new {@code CopyFileException} object.
     *
     * @param cause the cause of this exception.
     */
    public CopyFileException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new {@code CopyFileException} object.
     *
     * @param message the detail message for this exception.
     * @param cause the cause of this exception.
     */
    public CopyFileException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
