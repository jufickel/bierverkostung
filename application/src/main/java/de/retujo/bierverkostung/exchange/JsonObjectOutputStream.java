/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;

/**
 * This OutputStream produces a {@link JSONObject} from the bytes it was provided.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class JsonObjectOutputStream extends OutputStream {

    private static final int DEFAULT_SIZE = 256;

    private final ByteArrayOutputStream byteArrayOutputStream;

    private JsonObjectOutputStream(final ByteArrayOutputStream theByteArrayOutputStream) {
        byteArrayOutputStream = theByteArrayOutputStream;
    }

    /**
     * Returns an instance of {@code JsonObjectOutputStream}.
     *
     * @return the instance.
     */
    public static JsonObjectOutputStream getInstance() {
        return new JsonObjectOutputStream(new ByteArrayOutputStream(DEFAULT_SIZE));
    }

    @Override
    public void write(final int b) {
        byteArrayOutputStream.write(b);
    }

    @Override
    public void write(final byte[] b) throws IOException {
        byteArrayOutputStream.write(b);
    }

    @Override
    public void write(final byte[] b, final int off, final int len) {
        byteArrayOutputStream.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        byteArrayOutputStream.flush();
    }

    @Override
    public void close() throws IOException {
        byteArrayOutputStream.close();
    }

    /**
     * Returns the contents of this stream as {@link JSONObject}.
     *
     * @return the contents of this stream as JSON object.
     * @throws ExportImportException if the contents of this stream could not be converted to a JSON object.
     */
    public JSONObject toJson() {
        return tryToCreateJsonObjectFromString(tryToGetContentsOfByteArrayOutputStreamAsString());
    }

    private String tryToGetContentsOfByteArrayOutputStreamAsString() {
        final String charsetName = "UTF-8";
        try {
            return byteArrayOutputStream.toString(charsetName);
        } catch (final UnsupportedEncodingException e) {
            final String msgTemplate =
                    "The charset <{0}> cannot be used to get the contents of ByteArrayOutputStream as string!";
            throw new ExportImportException(MessageFormat.format(msgTemplate, charsetName), e);
        }
    }

    private static JSONObject tryToCreateJsonObjectFromString(final String jsonString) {
        try {
            return new JSONObject(jsonString);
        } catch (final JSONException e) {
            final String msgTemplate = "Failed to create JSON object from string <{0}>!";
            throw new ExportImportException(MessageFormat.format(msgTemplate, jsonString));
        }
    }

}
