/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.Function;

/**
 * Replaces invalid characters with a {@code "_"} to obtain a valid file name.
 *
 * @since 1.2.0
 */
@Immutable
public final class FileNameReviser implements Function<CharSequence, String> {

    private static final Pattern VALID_CHARS_PATTERN = Pattern.compile("[^a-zA-Z0-9.-]");
    private static final String REPLACEMENT = "_";

    private FileNameReviser() {
        super();
    }

    /**
     * Returns an instance of {@code FileNameReviser}.
     *
     * @return the instance.
     */
    public static FileNameReviser getInstance() {
        return new FileNameReviser();
    }

    @Override
    public String apply(@Nullable final CharSequence rawFileName) {
        if (null == rawFileName) {
            return null;
        }

        final Matcher matcher = VALID_CHARS_PATTERN.matcher(rawFileName);

        return matcher.replaceAll(REPLACEMENT);
    }

}
