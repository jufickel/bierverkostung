/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.java.util.AllNonnull;
import de.retujo.bierverkostung.data.Revision;

/**
 * This implementation of {@link JsonConverter} converts the {@link EntityCommonData} of a {@link DataEntity} to a
 * {@link JSONObject} and vice versa. It is up to the sub-classes to convert the additional properties of the entity.
 *
 * @param <T> the type of the DataEntity this converter can convert.
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public abstract class DataEntityJsonConverter<T extends DataEntity> extends JsonConverter<T> {

    @Immutable
    public static final class DataEntityJsonName {
        /**
         * JSON field name of the ID string.
         */
        public static final String ID = "id";

        /**
         * JSON field name of the revision {@code int} value.
         */
        public static final String REVISION = "revision";

       private DataEntityJsonName() {
           throw new AssertionError();
       }
    }

    @Override
    protected void putAllValuesTo(final JSONObject targetJsonObject, final T entity) throws JSONException {
        targetJsonObject.put(DataEntityJsonName.ID, entity.getId());
        targetJsonObject.put(DataEntityJsonName.REVISION, entity.getRevision().getRevisionNumber());
        putEntityValuesTo(targetJsonObject, entity);
    }

    /**
     * Puts the entity-specific values to the specified target JSON object. At this time the common entity values
     * like ID and revision were already put to the target.
     *
     * @param targetJsonObject the target JSON object of this conversion.
     * @param entity the DataEntity which provides values to be put to {@code targetJsonObject}.
     * @throws JSONException indicates a problem while putting a value to the target.
     */
    protected abstract void putEntityValuesTo(JSONObject targetJsonObject, T entity) throws JSONException;

    @Override
    protected T createEntityInstanceFromJson(final JSONObject jsonObject) throws JSONException {
        final UUID id = UUID.fromString(jsonObject.getString(DataEntityJsonName.ID));
        final Revision revision = Revision.of(jsonObject.getInt(DataEntityJsonName.REVISION));

        final EntityCommonData commonData = EntityCommonData.getInstance(id, revision);

        return createEntityInstanceFromJson(jsonObject, commonData);
    }

    /**
     * Creates an {@link DataEntity} with properties obtained from the specified JSON object and the specified common
     * data.
     *
     * @param sourceJsonObject the JSON object which provides the values for the entity.
     * @param commonData the already obtained common data of the entity like row ID, UUID and timestamp.
     * @return the entity with the properties obtained from {@code sourceJsonObject}.
     * @throws JSONException indicates a problem while getting the entity values from {@code sourceJsonObject}.
     */
    protected abstract T createEntityInstanceFromJson(JSONObject sourceJsonObject, EntityCommonData commonData)
            throws JSONException;

}
