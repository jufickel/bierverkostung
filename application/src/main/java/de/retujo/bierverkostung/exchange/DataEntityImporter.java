/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.LruCache;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.data.Table;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Function;
import de.retujo.java.util.Logger;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Abstract base implementation for importing {@link DataEntity}s.
 *
 * @param <T> the type of the DataEntity this importer takes care of.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public abstract class DataEntityImporter<T extends DataEntity> {

    private static final short DEFAULT_CACHE_SIZE = 100;
    private static final Logger LOGGER = Logger.forSimpleClassName(DataEntityImporter.class);

    private final ContentResolver contentResolver;
    private final Table table;
    private final Function<Cursor, T> cursorToDataEntity;
    private final LruCache<UUID, T> importedEntitiesCache;

    /**
     * Constructs a new {@code DataEntityImporter} object.
     *
     * @param contentResolver the ContentResolver to be used for database access.
     * @param table the table which is associated with the entity.
     * @param cursorToDataEntity a Function for getting an entity from a Cursor.
     * @throws NullPointerException if any argument is {@code null}.
     */
    protected DataEntityImporter(final ContentResolver contentResolver, final Table table,
            final Function<Cursor, T> cursorToDataEntity) {

        this.contentResolver = isNotNull(contentResolver, "ContentResolver");
        this.table = isNotNull(table, "table");
        this.cursorToDataEntity = isNotNull(cursorToDataEntity, "Function for getting a DataEntity for a Cursor");
        importedEntitiesCache = new LruCache<>(DEFAULT_CACHE_SIZE);
    }

    /**
     * Imports the specified entity. Before it is actually inserted into the database it is ensured that it not
     * already existing. If the entity already exists the revisions are compared: if the external entity has a higher
     * revision number it will overwrite the existing entity.
     *
     * @param externalEntity the external entity to be imported.
     * @return the imported or existing entity.
     * @throws NullPointerException if {@code externalEntity} is {@code null}.
     */
    public T importOrGetExisting(final T externalEntity) {
        isNotNull(externalEntity, "DataEntity to be imported");

        final T result;

        final T existingEntity = importedEntitiesCache.get(externalEntity.getId());
        if (null != existingEntity && !externalEntity.getRevision().isGreaterThan(existingEntity.getRevision())) {
            result = existingEntity;
            LOGGER.debug("Using cached <{0}>.", result);
        } else {
            final Importer importer = new Importer(importSubEntities(externalEntity));
            result = importer.run();
            importedEntitiesCache.put(externalEntity.getId(), result);
            importedEntitiesCache.put(result.getId(), result);
        }

        return result;
    }

    /**
     * This method can be overridden if the specified external entity has sub-entities which have to be imported
     * first.
     *
     * @param externalEntity the entity to import the sub-entities for.
     * @return either {@code externalEntity} or a derived entity containing imported sub-entities.
     */
    protected T importSubEntities(final T externalEntity) {
        return externalEntity;
    }

    /**
     * Returns a selection which contains all properties of the entity which should be used to identify the (same)
     * existing entity in the database. The ID and revision should not be part of the comparison as both were already
     * checked in a preceding step.
     *
     * @param externalEntity the entity to be imported which provides the property values too look after in the
     * database.
     * @return the Selection.
     */
    protected abstract Selection selectSameProperties(T externalEntity);

    /**
     * This hook method allows to intercept right after the entity was imported, updated or loaded and just before it
     * is going to be cached for potential future usage.
     *
     * @param importedEntity the entity to be modified.
     * @return a derivation of {@code importedEntity} to be cached. By default it is {@code importedEntity}.
     */
    protected T modifyImportedEntityBeforeCaching(final T importedEntity) {
        return importedEntity;
    }

    private final class Importer {
        private final T externalEntity;

        private Importer(final T theExternalEntity) {
            externalEntity = theExternalEntity;
        }

        private T run() {
            final T result;

            T existingEntity = queryForExisting(selectSameId());
            if (null != existingEntity) {
                if (externalEntity.getRevision().isGreaterThan(existingEntity.getRevision())) {
                    updateWithExternal();
                    result = externalEntity;
                } else {
                    result = existingEntity;
                    LOGGER.debug("Using existing with same ID: <{0}>.", result);
                }
            } else {
                existingEntity = queryForExisting(selectSameProperties(externalEntity));
                if (null != existingEntity) {
                    result = existingEntity;
                    LOGGER.debug("Using existing with same properties: <{0}>.", result);
                } else {
                    insertExternal();
                    result = externalEntity;
                }
            }

            return modifyImportedEntityBeforeCaching(result);
        }

        private Selection selectSameId() {
            return Selection.where(table.getColumnOrThrow(BaseColumns._ID)).is(externalEntity.getId()).build();
        }

        @Nullable
        private T queryForExisting(final Selection selection) {
            try (final Cursor c = contentResolver.query(table.getContentUri(), null,
                    selection.getSelectionString(), selection.getSelectionArgs(), null)) {
                return cursorToDataEntity.apply(c);
            }
        }

        private void updateWithExternal() {
            contentResolver.update(externalEntity.getContentUri(), externalEntity.asContentValues(), null, null);
        }

        private void insertExternal() {
            contentResolver.insert(externalEntity.getContentUri(), externalEntity.asContentValues());
        }
    }

}
