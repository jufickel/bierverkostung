/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A {@code JsonConverter} converts any appropriate entity to a {@link JSONObject} or the other way back. It deals
 * with potentially thrown {@link org.json.JSONException}s by wrapping them in a {@link JsonConversionException}.
 * 
 * @param <T> the type of the entity to be converted to or from JSON.
 * @since 1.2.0
 */
@AllNonnull
@Immutable
public abstract class JsonConverter<T> {

    /**
     * Constructs a new {@code JsonConverter} object.
     *
     */
    protected JsonConverter() {
        super();
    }

    /**
     * Converts the specified entity to a JSON object.
     *
     * @param entity the entity to be converted.
     * @return the JSONObject as result of the conversion.
     * @throws NullPointerException if {@code entity} is {@code null}.
     * @throws JsonConversionException if {@code entity} cannot be converted for some reason.
     */
    public JSONObject toJson(final T entity) {
        isNotNull(entity, "entity to be converted");
        final JSONObject result = new JSONObject();
        tryToPutAllValuesTo(result, entity);
        return result;
    }

    private void tryToPutAllValuesTo(final JSONObject targetJsonObject, final T entity) {
        try {
            putAllValuesTo(targetJsonObject, entity);
        } catch (final Exception e) {
            final String msgTemplate = "Failed to convert <{0}> to a JSON object!";
            throw new JsonConversionException(MessageFormat.format(msgTemplate, entity), e);
        }
    }

    protected abstract void putAllValuesTo(JSONObject targetJsonObject, T entity) throws JSONException;

    /**
     * Converts the specified JSON object ot an entity.
     *
     * @param jsonObject the JSON object to be converted.
     * @return the entity as result of the conversion.
     * @throws NullPointerException if {@code jsonObject} is {@code null}.
     * @throws JsonConversionException if {@code jsonObject} cannot be converted for some reason.
     */
    public T fromJson(final JSONObject jsonObject) {
        return tryToCreateEntityInstanceFromJson(jsonObject);
    }

    private T tryToCreateEntityInstanceFromJson(final JSONObject jsonObject) {
        try {
            return createEntityInstanceFromJson(jsonObject);
        } catch (final Exception e) {
            final String msgTemplate = "Failed to convert <{0}> to an entity instance!";
            throw new JsonConversionException(MessageFormat.format(msgTemplate, jsonObject), e);
        }
    }

    protected abstract T createEntityInstanceFromJson(JSONObject jsonObject) throws JSONException;

}
