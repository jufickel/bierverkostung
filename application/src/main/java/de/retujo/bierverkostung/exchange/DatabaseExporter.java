/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.DateUtil;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Exports the application's database to a specified directory.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class DatabaseExporter extends AbstractDatabaseExchanger {

    private OnExportFinishedListener onExportFinishedListener;
    private File exportedDatabasePath;
    private ExportImportException exception;

    private DatabaseExporter(final Context applicationContext) {
        super(applicationContext);
        onExportFinishedListener = (exportedBackupFile, throwable) -> {
            // Nothing to do.
        };
        exportedDatabasePath = null;
        exception = null;
    }

    /**
     * Returns a new instance of {@code DatabaseExporter}.
     *
     * @param applicationContext the application's context.
     * @return the instance.
     * @throws NullPointerException if {@code applicationContext} is {@code null}.
     */
    @Nonnull
    public static DatabaseExporter newInstance(@Nonnull final Context applicationContext) {
        return new DatabaseExporter(applicationContext);
    }

    /**
     * Sets the listener to be notified when the export operation is finished either successfully or erroneous.
     *
     * @param listener the listener.
     */
    public void setOnExportFinishedListener(@Nullable final OnExportFinishedListener listener) {
        if (null != listener) {
            onExportFinishedListener = listener;
        }
    }

    @Override
    protected Void doInBackground(final File... directory) {
        isNotNull(directory, "directory to save the database to");
        if (0 == directory.length) {
            throw new IllegalArgumentException("No directory to save the database to was specified!");
        }

        final File dir = directory[0];
        if (dir.canWrite()) {
            final File targetDatabaseFilePath = new File(dir, getTargetDatabaseName());
            tryToExportDatabase(targetDatabaseFilePath);
        } else {
            final String message = getString(R.string.database_exporter_target_not_writable, dir.getPath());
            exception = new ExportImportException(message);
        }

        return null;
    }

    private String getTargetDatabaseName() {
        final Date currentDateTime = new Date(System.currentTimeMillis());
        final DateUtil dateUtil = DateUtil.getInstance(getApplicationContext());
        final String dateTimeLongIsoString = dateUtil.getLongIsoString(currentDateTime);

        return DATABASE_NAME_PLAIN
                .concat("_backup_")
                .concat(dateTimeLongIsoString)
                .concat(DATABASE_FILE_EXTENSION);
    }

    private void tryToExportDatabase(final File targetDatabaseFilePath) {
        try {
            exportDatabase(targetDatabaseFilePath);
        } catch (final IOException e) {
            exception = new ExportImportException(e.getMessage(), e);
        }
    }

    private void exportDatabase(final File targetDatabasePath) throws IOException {
        transferSourceToTarget(getInternalDatabasePath(), targetDatabasePath);
        exportedDatabasePath = targetDatabasePath;
    }

    @Override
    protected void onPostExecute(final Void aVoid) {
        super.onPostExecute(aVoid);
        onExportFinishedListener.onExportFinished(exportedDatabasePath, exception);
    }

    /**
     * A listener which is notified when the export is finished either successfully or erroneous.
     *
     * @since 1.0.0
     */
    interface OnExportFinishedListener {
        /**
         * This method is called when the database export is finished.
         *
         * @param exportedBackupFile the file which is the exported database or {@code null} if the export failed.
         * @param exception an exception which describes why the export failed or {@code null} if the export was
         * successful.
         */
        void onExportFinished(@Nullable File exportedBackupFile, @Nullable ExportImportException exception);
    }

}
