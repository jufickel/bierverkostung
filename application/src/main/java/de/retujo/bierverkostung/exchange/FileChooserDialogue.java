/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class implements a simple dialogue for selecting either a single writable directory or a single readable file.
 * It is heavily inspired by
 * <a href="https://rogerkeays.com/simple-android-file-chooser">Simple Android File Chooser</a> of Roger Keays.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class FileChooserDialogue {

    private static final String PARENT_DIRECTORY = "..";

    private final Context context;
    private final ListView listView;
    private final AlertDialog dialogue;
    private OnFileSelectedListener onFileSelectedListener;
    private File currentPath;
    private File selectedFile;

    private FileChooserDialogue(final Context theContext, final File startDirectory, final FileFilter directoriesFilter,
            final FileFilter filesFilter) {
        context= theContext;
        listView = new ListView(context);
        dialogue = initDialogue(startDirectory, listView);
        final Refresher refresher = new Refresher(directoriesFilter, filesFilter);
        initListView(refresher);
        onFileSelectedListener = fileOrDirectory -> {
            // Nothing to do.
        };
        currentPath = refresher.refresh(startDirectory);
        selectedFile = null;
    }

    private AlertDialog initDialogue(final File startDirectory, final View view) {
        return new AlertDialog.Builder(context)
                .setTitle(startDirectory.getPath())
                .setNegativeButton(context.getString(R.string.file_chooser_cancel_button_text), (d, w) -> d.dismiss())
                .setView(view)
                .create();
    }

    private void initListView(final Refresher refresher) {
        listView.setOnItemClickListener((parent, view, position, id) -> {
            final String selected = (String) listView.getItemAtPosition(position);
            selectedFile = getSelectedFile(selected);
            if (selectedFile.isDirectory()) {
                currentPath = refresher.refresh(selectedFile);
            } else {
                onFileSelectedListener.onFileSelected(selectedFile);
                dialogue.dismiss();
            }
        });
    }

    private File getSelectedFile(final String selected) {
        if (PARENT_DIRECTORY.equals(selected)) {
            return currentPath.getParentFile();
        } else {
            return new File(currentPath, selected);
        }
    }

    /**
     * Returns a new instance of {@code FileChooserDialogue} for selecting a single directory. All shown directories
     * are known to be writable.
     *
     * @param context the application context.
     * @param startDirectory the directory of which the directories are shown initially.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code startDirectory} is not a directory or does not exist.
     */
    @Nonnull
    public static FileChooserDialogue forDirectory(@Nonnull final Context context, @Nonnull final File startDirectory) {
        validateArguments(context, startDirectory);

        final FileFilter writableDirectoriesOnly = file -> file.isDirectory() && file.canWrite();
        final FileFilter noFilesAtAll = file -> !file.isDirectory() && !file.isFile();

        final FileChooserDialogue result = new FileChooserDialogue(context, startDirectory, writableDirectoriesOnly,
                noFilesAtAll);
        result.selectedFile = startDirectory;
        result.dialogue.setButton(AlertDialog.BUTTON_POSITIVE,
                context.getString(R.string.file_chooser_select_button_text), (d, which) -> {
            result.onFileSelectedListener.onFileSelected(result.selectedFile);
            d.dismiss();
        });
        return result;
    }

    private static void validateArguments(final Context context, final File startDirectory) {
        isNotNull(context, "context");
        isNotNull(startDirectory, "start directory");
        if (!startDirectory.isDirectory()) {
            throw new IllegalArgumentException(MessageFormat.format("<{0}> is not a directory!", startDirectory));
        }
        if (!startDirectory.exists()) {
            throw new IllegalArgumentException(MessageFormat.format("<{0}> does not exist!", startDirectory));
        }
    }

    /**
     * Returns a new instance of {@code FileChooserDialogue} for selecting a single file. The type of the shown and
     * selectable files can be determined by the file extension. The shown files are known to be readable.
     *
     * @param context the application context.
     * @param startDirectory the directory of which the directories are shown initially.
     * @param extension determines the type of files to be shown or {@code null} if every type of file is selectable.
     * @return the instance.
     * @throws NullPointerException if {@code Context} or {@code startDirectory} is {@code null}.
     * @throws IllegalArgumentException if {@code startDirectory} is not a directory or does not exist.
     */
    @Nonnull
    public static FileChooserDialogue forFile(@Nonnull final Context context, @Nonnull final File startDirectory,
            @Nullable final CharSequence extension) {
        validateArguments(context, startDirectory);

        final FileFilter readableDirectoriesOnly = file -> file.isDirectory() && file.canRead();

        final FileFilter readableFilesOnly = file -> {
            if (!file.isFile() || !file.canRead()) {
                return false;
            }
            if (null != extension) {
                final String fileName = file.getName();
                final String fileNameLowerCase = fileName.toLowerCase(Locale.ENGLISH);
                return fileNameLowerCase.endsWith(extension.toString());
            } else {
                return true;
            }
        };

        return new FileChooserDialogue(context, startDirectory, readableDirectoriesOnly, readableFilesOnly);
    }

    /**
     * Shows the dialogue for selecting a directory or a file.
     */
    public void show() {
        dialogue.show();
    }

    /**
     * Sets the listener to be notified when a directory or a file was selected by the user.
     *
     * @param listener the listener or {@code null}.
     * @return this dialogue instance to allow method chaining.
     */
    public FileChooserDialogue setOnFileSelectedListener(@Nullable final OnFileSelectedListener listener) {
        if (null != listener) {
            onFileSelectedListener = listener;
        } else {
            onFileSelectedListener = selected -> {
                // Nothing to do.
            };
        }
        return this;
    }

    /**
     * A listener which is notified when a directory or a file was selected by the user.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("squid:S1609")
    public interface OnFileSelectedListener {
        /**
         * This method is called when the user selected a directory or file.
         * 
         * @param selected the selected directory or file.
         */
        void onFileSelected(@Nonnull File selected);
    }

    /**
     * This class updates the ListView adapter with the names of directories and files of a particular path. These items
     * are alphabetically  sorted.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class Refresher {
        private final FileFilter directoriesFileFilter;
        private final FileFilter filesFileFilter;

        private Refresher(final FileFilter directoriesFileFilter, final FileFilter filesFileFilter) {
            this.directoriesFileFilter = directoriesFileFilter;
            this.filesFileFilter = filesFileFilter;
        }

        /**
         * Refreshes the ListView adapter with the sorted names of the directories and files of the specified path.
         *
         * @param path the path to list the directories and files of.
         * @return the current path.
         */
        @Nonnull
        private File refresh(final File path) {
            dialogue.setTitle(path.getPath());

            final List<String> directoryNames = getSortedNamesOrEmpty(path, directoriesFileFilter);
            final List<String> fileNames = getSortedNamesOrEmpty(path, filesFileFilter);
            final List<String> directoryAndFileNames = new ArrayList<>(1 + directoryNames.size() + fileNames.size());
            if (null != path.getParentFile()) {
                directoryAndFileNames.add(PARENT_DIRECTORY);
            }
            directoryAndFileNames.addAll(directoryNames);
            directoryAndFileNames.addAll(fileNames);

            listView.setAdapter(createAdapter(directoryAndFileNames));
            return path;
        }

        private List<String> getSortedNamesOrEmpty(final File path, final FileFilter fileFilter) {
            File[] files = path.listFiles(fileFilter);
            if (null != files) {
                Arrays.sort(files);
            } else {
                files = new File[0];
            }

            final List<String> result = new ArrayList<>(files.length);
            for (final File file : files) {
                result.add(file.getName());
            }
            return result;
        }

        private ArrayAdapter<String> createAdapter(final List<String> directoryAndFileNames) {
            return new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, directoryAndFileNames);
        }
    }

}
