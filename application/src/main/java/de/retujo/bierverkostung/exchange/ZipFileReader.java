/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Reads a particular ZIP file. Reading a ZIP file requires a {@link ZipFileConsumer} callback which provides the
 * OutputStreams to write the contents of the file to.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class ZipFileReader {

    private static final int BUFFER_SIZE = 2048;

    private final ZipInputStream zipInputStream;

    private ZipFileReader(final ZipInputStream theZipInputStream) {
        zipInputStream = theZipInputStream;
    }

    /**
     * Returns an instance of {@code ZipFileReader}.
     *
     * @param zipFile the ZIP file to be read.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static ZipFileReader getInstance(final File zipFile) {
        return new ZipFileReader(new ZipInputStream(tryToCreateFileInputStream(isNotNull(zipFile, "ZIP file"))));
    }

    private static FileInputStream tryToCreateFileInputStream(final File inputZipFile) {
        try {
            return new FileInputStream(inputZipFile);
        } catch (final FileNotFoundException e) {
            final String msgTemplate = "Failed to create a FileInputStream for <{0}>!";
            throw new IllegalArgumentException(MessageFormat.format(msgTemplate, inputZipFile), e);
        }
    }

    /**
     * Reads the ZIP file which was provided to the constructor.
     *
     * @param zipFileConsumer callback which consumes the entries of {@code zipFile}.
     * @throws NullPointerException if {@code zipFileConsumer} is {@code null}.
     */
    public void run(final ZipFileConsumer zipFileConsumer) {
        try {
            ZipEntry zipEntry;
            while ((zipEntry = tryToGetNextZipEntry()) != null) {
                tryToWriteZipEntryToZipInputStream(zipEntry, isNotNull(zipFileConsumer, "ZIP file consumer"));
            }
        } finally {
            tryToCloseStream(zipInputStream);
            zipFileConsumer.zipFileRead();
        }
    }

    @Nullable
    private ZipEntry tryToGetNextZipEntry() {
        try {
            return zipInputStream.getNextEntry();
        } catch (final IOException e) {
            throw new ExportImportException("Failed to get next ZipEntry from ZipInputStream!", e);
        }
    }

    private void tryToWriteZipEntryToZipInputStream(final ZipEntry zipEntry, final ZipFileConsumer zipFileConsumer) {
        try {
            writeZipEntryToOutputStream(zipEntry, zipFileConsumer);
        } catch (final IOException e) {
            final String msgTemplate = "Failed to read <{0}> ZipEntry!";
            throw new ExportImportException(MessageFormat.format(msgTemplate, zipEntry.getName()), e);
        }
    }

    private void writeZipEntryToOutputStream(final ZipEntry zipEntry, final ZipFileConsumer zipFileConsumer)
            throws IOException {

        final String fileName = zipEntry.getName();
        final Maybe<OutputStream> outputStreamMaybe = zipFileConsumer.getOutputStreamFor(fileName);
        if (outputStreamMaybe.isPresent()) {
            final OutputStream outputStream = outputStreamMaybe.get();
            final byte[] dataBuffer = new byte[BUFFER_SIZE];
            int count;
            try {
                while ((count = zipInputStream.read(dataBuffer)) != -1) {
                    outputStream.write(dataBuffer, 0, count);
                }
            } finally {
                outputStream.flush();
                tryToCloseStream(outputStream);
            }
        }
    }

    private static void tryToCloseStream(final Closeable closeable) {
        try {
            closeable.close();
        } catch (final IOException e) {
            throw new ExportImportException("Failed to close stream!", e);
        }
    }

    /**
     * Callback for consuming the entries of a ZIP file.
     *
     * @since 1.2.0
     */
    @AllNonnull
    public interface ZipFileConsumer {

        /**
         * Returns an OutputStream for the specified file name to write data of a particular ZIP entry to.
         * <em>The returned OutputStream stream gets closed after the ZipEntry was completely read.</em>
         * It is recommended that the implementer of this method returns a wrapped OutputStream to be able to
         * appropriately react when a ZipEntry was read.
         *
         * @param fileName the name of the ZIP entry to be consumed.
         * @return the OutputStream or an empty Maybe.
         * @throws IOException if the OutputStream cannot be obtained for some reason.
         */
        Maybe<OutputStream> getOutputStreamFor(String fileName) throws IOException;

        /**
         * Signals that the ZIP file is completely read.
         */
        void zipFileRead();

    }

}
