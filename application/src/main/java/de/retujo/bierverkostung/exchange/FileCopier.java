/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class copies a source {@link File} to a target File.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class FileCopier {

    private final InputStream sourceStream;
    private final File targetPath;
    private final Mode mode;

    private FileCopier(final InputStream theSourceStream, final File theTargetPath, final Mode theMode) {
        sourceStream = theSourceStream;
        targetPath = theTargetPath;
        mode = theMode;
    }

    /**
     * Returns an instance of {@code FileCopier}.
     *
     * @param sourcePath the source to be copied.
     * @param targetPath the target which to which the bytes of {@code sourcePath} are copied.
     * @param mode the mode how the result behaves in the case that the target already exists.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static FileCopier getInstance(final File sourcePath, final File targetPath, final Mode mode)
            throws IOException {

        return new FileCopier(new FileInputStream(isNotNull(sourcePath, "source path")), targetPath, mode);
    }

    /**
     * Returns an instance of {@code FileCopier}.
     *
     * @param sourceInputStream an input stream of the source to be copied.
     * @param targetPath the target which to which the bytes of {@code sourcePath} are copied.
     * @param mode the mode how the result behaves in the case that the target already exists.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @since 1.2.0
     */
    public static FileCopier getInstance(final InputStream sourceInputStream, final File targetPath, final Mode mode) {
        isNotNull(sourceInputStream, "source input stream");
        isNotNull(targetPath, "target path");
        isNotNull(mode, "mode");

        return new FileCopier(sourceInputStream, targetPath, mode);
    }

    /**
     * Runs the copy operation.
     *
     * @return the number of written bytes. If the target path already exists and the copy mode is
     * {@link Mode#CANCEL} then {@code 1} is returned.
     * @throws CopyFileException if the File cannot be copied.
     */
    @SuppressWarnings({"TryFinallyCanBeTryWithResources", "squid:S2093"})
    public int run() {
        if (targetPath.exists()) {
            if (Mode.CANCEL == mode) {
                return 1;
            }
        } else {
            createTargetPath();
        }

        return tryToCopy();
    }

    private void createTargetPath() {
        if (targetPath.exists()) {
            throw new CopyFileException(MessageFormat.format("<{0}> already exists!", targetPath));
        }
        final FileCreator fileCreator = FileCreator.getInstance();
        fileCreator.accept(targetPath);
    }

    private int tryToCopy() {
        try {
            return copy();
        } catch (final IOException e) {
            final String msgPattern = "Failed to copy File to <{0}>!";
            throw new CopyFileException(MessageFormat.format(msgPattern, targetPath.getAbsolutePath()), e);
        }
    }

    private int copy() throws IOException {
        try (final FileOutputStream fileOutputStream = new FileOutputStream(targetPath)) {
            final byte[] buffer = new byte[sourceStream.available()];
            final OutputStream targetStream = new BufferedOutputStream(fileOutputStream, buffer.length);
            return doCopy(targetStream, buffer);
        }
    }

    private int doCopy(final OutputStream targetStream, final byte[] buffer) throws IOException {
        int count = 0;
        int n;
        try {
            while (-1 != (n = sourceStream.read(buffer))) {
                targetStream.write(buffer, 0, n);
                count += n;
            }
            return count;
        } finally {
            sourceStream.close();
        }
    }

    /**
     * The mode how a FileCopier works.
     *
     * @since 1.2.0
     */
    public enum Mode {
        /**
         * Overwrite the target file if it already exists.
         */
        OVERWRITE,

        /**
         * Cancels the copy action and leaves the target file in place if it already exists.
         */
        CANCEL
    }

}
