/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.content.Context;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Imports the application's database from a specified *.db file.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class DatabaseImporter extends AbstractDatabaseExchanger {

    private OnImportFinishedListener onImportFinishedListener;
    private File importedDatabaseFile;
    private ExportImportException exception;

    private DatabaseImporter(final Context applicationContext) {
        super(applicationContext);
        importedDatabaseFile = null;
        onImportFinishedListener = (importedFile, throwable) -> {
            // Nothing to do.
        };
        exception = null;
    }

    /**
     * Returns a new instance of {@code DatabaseImporter}.
     *
     * @param applicationContext the application's context.
     * @return the instance.
     * @throws NullPointerException if {@code applicationContext} is {@code null}.
     */
    @Nonnull
    public static DatabaseImporter newInstance(@Nonnull final Context applicationContext) {
        return new DatabaseImporter(applicationContext);
    }

    /**
     * Sets the listener to be notified when the import operation is finished either successfully or erroneous.
     *
     * @param listener the listener.
     */
    public void setOnImportFinishedListener(@Nullable final OnImportFinishedListener listener) {
        if (null != listener) {
            onImportFinishedListener = listener;
        }
    }

    @Override
    protected Void doInBackground(final File... sourceFiles) {
        isNotNull(sourceFiles, "source database file to read from");
        if (0 == sourceFiles.length) {
            throw new IllegalArgumentException("No source file to read the database from was specified!");
        }
        final File sourceFile = sourceFiles[0];
        if (!sourceFile.isFile()) {
            final String message = getString(R.string.database_importer_source_not_a_file, sourceFile.getPath());
            exception = new ExportImportException(message);
        } else if (!sourceFile.canRead()) {
            final String message = getString(R.string.database_importer_source_not_readable, sourceFile.getPath());
            exception = new ExportImportException(message);
        } else {
            tryToImportDatabase(sourceFile);
        }

        return null;
    }

    private void tryToImportDatabase(final File sourceDatabaseFilePath) {
        try {
            importDatabase(sourceDatabaseFilePath);
        } catch (final IOException e) {
            exception = new ExportImportException(e.getMessage(), e);
        }
    }

    private void importDatabase(final File sourceDatabaseFilePath) throws IOException {
        transferSourceToTarget(sourceDatabaseFilePath, getInternalDatabasePath());
        importedDatabaseFile = sourceDatabaseFilePath;
    }

    @Override
    protected void onPostExecute(final Void aVoid) {
        super.onPostExecute(aVoid);
        onImportFinishedListener.onImportFinished(importedDatabaseFile, exception);
    }

    /**
     * A listener which is notified when the import is finished either successfully or erroneous.
     *
     * @since 1.0.0
     */
    interface OnImportFinishedListener {
        /**
         * This method is called when the database import is finished.
         *
         * @param importedDatabaseFile the database file which was imported or {@code null} if the import failed.
         * @param exception an exception which describes why the import failed or {@code null} if the import was
         * successful.
         */
        void onImportFinished(@Nullable File importedDatabaseFile, @Nullable ExportImportException exception);
    }

}
