/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.content.Context;
import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Abstract base implementation for classes which somehow exchange the database like for example exporting or importing.
 *
 * @since 1.0.0
 */
@NotThreadSafe
abstract class AbstractDatabaseExchanger extends AsyncTask<File, Void, Void> {

    /**
     * The plain name of the internal SQLite database, i. e. without path or file extension.
     */
    protected static final String DATABASE_NAME_PLAIN = "bierverkostung";

    /**
     * Extension of a SQLite database file.
     */
    static final String DATABASE_FILE_EXTENSION = ".db";

    private static final String DATABASE_NAME_FULL = DATABASE_NAME_PLAIN + DATABASE_FILE_EXTENSION;

    private final Context applicationContext;

    /**
     * Constructs a new {@code AbstractDatabaseExchanger} object.
     *
     * @param applicationContext the application's context.
     * @throws NullPointerException if {@code applicationContext} is {@code null}.
     */
    protected AbstractDatabaseExchanger(@Nonnull final Context applicationContext) {
        isNotNull(applicationContext, "application context");
        this.applicationContext = applicationContext;
    }

    protected final File getInternalDatabasePath() {
        return applicationContext.getDatabasePath(DATABASE_NAME_FULL);
    }

    protected final String getString(final int resourceId, final Object ... formatArgs) {
        return applicationContext.getString(resourceId, formatArgs);
    }

    /**
     * Returns the application's context.
     *
     * @return the context.
     */
    @Nonnull
    protected final Context getApplicationContext() {
        return applicationContext;
    }

    /**
     * Transfers the bytes of a source to a target. Afterwards both, source and target, are equal.
     *
     * @param sourcePath the source to be transferred.
     * @param targetPath the target which to which the bytes of {@code sourcePath} are transferred.
     * @return the amount of transferred bytes.
     * @throws IOException if copying the database failed.
     */
    protected static int transferSourceToTarget(final File sourcePath, final File targetPath) throws IOException {
        final FileCopier fileCopier = FileCopier.getInstance(sourcePath, targetPath, FileCopier.Mode.OVERWRITE);
        return fileCopier.run();
    }

}
