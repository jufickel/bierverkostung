/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import javax.annotation.Nullable;

/**
 * A RuntimeException to be thrown if data import or export failed for some reason.
 *
 * @since 1.0.0
 */
public class ExportImportException extends RuntimeException {

    private static final long serialVersionUID = -649416386649394700L;

    /**
     * Constructs a new {@code ExportImportException} object.
     */
    public ExportImportException() {
        super();
    }

    /**
     * Constructs a new {@code ExportImportException} object.
     *
     * @param message the detail message for this exception or {@code null}.
     */
    public ExportImportException(@Nullable final String message) {
        super(message);
    }

    /**
     * Constructs a new {@code ExportImportException} object.
     *
     * @param message the detail message for this exception or {@code null}.
     * @param cause the cause of this exception or {@code null} if it has no cause.
     */
    public ExportImportException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

}
