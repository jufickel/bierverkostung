/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.Acceptor;

/**
 * This class recursively creates a specified file or directory. If the target cannot be created a
 * {@link CreateFileException} is thrown; this also applies if the target already exists.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class FileCreator implements Acceptor<File> {

    private FileCreator() {
        super();
    }

    /**
     * Returns an instance of {@code FileCreator}.
     *
     * @return the instance.
     */
    public static FileCreator getInstance() {
        return new FileCreator();
    }

    @Override
    public void accept(final File target) {
        if (target.exists()) {
            throw new CreateFileException(MessageFormat.format("<{0}> already exists!", target));
        }
        recursivelyCreateParentDirectory(target.getParentFile());
        final boolean isTargetFileCreated = tryToCreateTarget(target);
        if (!isTargetFileCreated) {
            final String msgTemplate = "Failed to create <{0}> because of an unknown reason!";
            throw new CreateFileException(MessageFormat.format(msgTemplate, target));
        }
    }

    private static boolean tryToCreateTarget(final File target) {
        try {
            return createTarget(target);
        } catch (final IOException e) {
            throw new CreateFileException(MessageFormat.format("Failed to create <{0}>!", target), e);
        }
    }

    private static void recursivelyCreateParentDirectory(final File parentDirectory) {
        if (null != parentDirectory && !parentDirectory.exists()) {
            final boolean isDirectoryCreated = parentDirectory.mkdirs();
            if (!isDirectoryCreated) {
                final String msgPattern = "Failed to create dir <{0}>!";
                throw new CreateFileException(MessageFormat.format(msgPattern, parentDirectory.getAbsolutePath()));
            }
        }
    }

    private static boolean createTarget(final File targetFile) throws IOException {
        return targetFile.createNewFile();
    }

}
