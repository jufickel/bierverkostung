/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A {@code JsonStringInputStream} contains a JSON string that may be read from the stream. Technically all method
 * calls are delegated to a {@link ByteArrayInputStream}.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class JsonStringInputStream extends InputStream {

    private final InputStream delegee;

    private JsonStringInputStream(final InputStream theDelegee) {
        delegee = theDelegee;
    }

    /**
     * Returns a new instance of {@code JsonStringInputStream} for the given {@link Jsonable}.
     *
     * @param jsonable provides a JSON String which feeds the returned stream.
     * @return the new instance.
     * @throws NullPointerException if {@code jsonable} is {@code null}.
     * @throws IllegalArgumentException if {@code jsonable} cannot be transformed to a JSONObject.
     */
    public static JsonStringInputStream of(final Jsonable jsonable) {
        final JSONObject jsonObject = isNotNull(jsonable, "Jsonable").toJson();
        final String jsonString = jsonObject.toString();

        return new JsonStringInputStream(new ByteArrayInputStream(jsonString.getBytes(Charset.forName("UTF-8"))));
    }

    @Override
    public int read() throws IOException {
        return delegee.read();
    }

    @Override
    public int read(final byte[] b) throws IOException {
        return delegee.read(b);
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        return delegee.read(b, off, len);
    }

    @Override
    public long skip(final long n) throws IOException {
        return delegee.skip(n);
    }

    @Override
    public int available() throws IOException {
        return delegee.available();
    }

    @Override
    public void close() throws IOException {
        delegee.close();
    }

    @Override
    public synchronized void mark(final int readLimit) {
        delegee.mark(readLimit);
    }

    @Override
    public boolean markSupported() {
        return delegee.markSupported();
    }

}
