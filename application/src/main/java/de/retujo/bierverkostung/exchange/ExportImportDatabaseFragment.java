/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

/**
 * This Fragment offers the user the possibility to export and import the complete database of the application. After
 * the export or import action was finished the appropriate action status is displayed.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class ExportImportDatabaseFragment extends Fragment {

    private Button exportButton;
    private Button importButton;

    /**
     * Constructs a new {@code ExportImportDatabaseFragment} object.
     */
    public ExportImportDatabaseFragment() {
        exportButton = null;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
            @Nullable final Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.fragment_export_import_database, null);
        initExportButton(result);
        initImportButton(result);
        return result;
    }

    private void initExportButton(final View parentView) {
        exportButton = (Button) parentView.findViewById(R.id.export_database_button);
        exportButton.setOnClickListener(v -> {
            final Context context = getContext();
            final Context appContext = context.getApplicationContext();
            final File sdCardDir = Environment.getExternalStorageDirectory();
            final FileChooserDialogue fileChooserDialogue = FileChooserDialogue.forDirectory(context, sdCardDir);
            fileChooserDialogue.setOnFileSelectedListener(selected -> {
                final DatabaseExporter databaseExporter = DatabaseExporter.newInstance(appContext);
                databaseExporter.setOnExportFinishedListener(new ShowExportStatusListener(parentView));
                databaseExporter.execute(selected);
            });
            fileChooserDialogue.show();
        });
    }

    private void initImportButton(final View parentView) {
        importButton = (Button) parentView.findViewById(R.id.import_database_button);
        importButton.setOnClickListener(v -> {
            final Context context = getContext();
            final Context appContext = context.getApplicationContext();
            final File sdCardDir = Environment.getExternalStorageDirectory();
            final FileChooserDialogue fileChooserDialogue = FileChooserDialogue.forFile(context, sdCardDir,
                    AbstractDatabaseExchanger.DATABASE_FILE_EXTENSION);
            fileChooserDialogue.setOnFileSelectedListener(selected -> {
                final DatabaseImporter databaseImporter = DatabaseImporter.newInstance(appContext);
                databaseImporter.setOnImportFinishedListener(new ShowImportStatusListener(parentView));
                databaseImporter.execute(selected);
            });
            fileChooserDialogue.show();
        });
    }

    /**
     * This class shows the status for a finished export action.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class ShowExportStatusListener implements DatabaseExporter.OnExportFinishedListener {
        private final View parentView;

        private ShowExportStatusListener(final View theParentView) {
            parentView = theParentView;
        }

        @Override
        public void onExportFinished(@Nullable final File exportedBackupFile,
                @Nullable final ExportImportException exception) {

            final View exportButtonLabel = parentView.findViewById(R.id.export_database_button_label);
            exportButtonLabel.setVisibility(View.GONE);
            exportButton.setVisibility(View.GONE);
            final TextView statusTextView = (TextView) parentView.findViewById(R.id.export_database_status_text_view);
            statusTextView.setVisibility(View.VISIBLE);

            if (null != exportedBackupFile) {
                final String backupPath = exportedBackupFile.getPath();
                statusTextView.setTextColor(Color.GREEN);
                statusTextView.setText(getString(R.string.export_database_status_succeeded, backupPath));
            } else {
                final String text = getString(R.string.export_database_status_failed, exception.getMessage());
                statusTextView.setTextColor(Color.RED);
                statusTextView.setText(text);
            }
        }
    }

    /**
     * This class shows the status for a finished import action.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class ShowImportStatusListener implements DatabaseImporter.OnImportFinishedListener {
        private final View parentView;

        private ShowImportStatusListener(final View theParentView) {
            parentView = theParentView;
        }

        @Override
        public void onImportFinished(@Nullable final File importedDatabaseFile,
                @Nullable final ExportImportException exception) {

            final View importButtonLabel = parentView.findViewById(R.id.import_database_button_label);
            importButtonLabel.setVisibility(View.GONE);
            importButton.setVisibility(View.GONE);
            final TextView statusTextView = (TextView) parentView.findViewById(R.id.import_database_status_text_view);
            statusTextView.setVisibility(View.VISIBLE);

            if (null != importedDatabaseFile) {
                final String sourceFilePath = importedDatabaseFile.getPath();
                statusTextView.setTextColor(Color.GREEN);
                statusTextView.setText(getString(R.string.import_database_status_succeeded, sourceFilePath));
            } else {
                final String text = getString(R.string.import_database_status_failed, exception.getMessage());
                statusTextView.setTextColor(Color.RED);
                statusTextView.setText(text);
            }
        }
    }

}
