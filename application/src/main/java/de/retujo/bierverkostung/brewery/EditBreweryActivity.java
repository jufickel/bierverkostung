/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.ButtonEnablingTextWatcher;
import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.country.SelectCountryActivity;
import de.retujo.bierverkostung.data.InsertHandler;
import de.retujo.bierverkostung.data.UpdateHandler;
import de.retujo.java.util.Maybe;

/**
 * Activity for creating a new brewery or for editing an existing brewery. This activity is finished when the save
 * button was pressed. If a new brewery was added, the activity's result is a {@link Brewery} which can be obtained with
 * the {@link #EDITED_BREWERY} key from the result Intent's extra.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class EditBreweryActivity extends BaseActivity {

    /**
     * Key to retrieve the edited brewery from this Activity's result Intent.
     */
    public static final String EDITED_BREWERY = "editedBrewery";

    /**
     * Key to set an existing Brewery to be edited to the Intent for starting an EditBreweryActivity.
     */
    public static final String EDIT_BREWERY = "breweryToBeEdited";

    private static final int SELECT_COUNTRY_REQUEST_CODE = 135;
    private static final String NEW_BREWERY_NAME = "newBreweryName";
    private static final String NEW_BREWERY_LOCATION = "newBreweryLocation";
    private static final String NEW_BREWERY_COUNTRY = "newBreweryCountry";

    private TextView nameTextView;
    private TextView locationTextView;
    private TextView countryTextView;
    private Button saveBreweryButton;
    private Runnable saveAction;

    /**
     * Constructs a new {@code EditBreweryActivity} object.
     */
    public EditBreweryActivity() {
        nameTextView = null;
        locationTextView = null;
        countryTextView = null;
        saveBreweryButton = null;
        saveAction = null;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_brewery);

        initSaveBreweryButton();
        initBreweryNameTextView();
        initBreweryLocationTextView();
        initBreweryCountryTextView();

        final Intent afferentIntent = getIntent();
        final Brewery breweryToBeEdited = afferentIntent.getParcelableExtra(EDIT_BREWERY);
        if (null == breweryToBeEdited) {
            setTitle(getString(R.string.add_brewery_activity_name));
            restore(savedInstanceState);
            saveAction = new SaveNewBreweryAction();
        } else {
            setTitle(getString(R.string.edit_brewery_activity_name));
            initViewsWithBreweryProperties(breweryToBeEdited);
            saveAction = new SaveEditedBreweryAction(breweryToBeEdited);
        }
    }

    private void initSaveBreweryButton() {
        saveBreweryButton = findView(R.id.edit_brewery_save_button);
        saveBreweryButton.setEnabled(false);
    }

    private void initBreweryNameTextView() {
        nameTextView = findView(R.id.edit_brewery_name_edit_text);
        nameTextView.addTextChangedListener(new ButtonEnablingTextWatcher(saveBreweryButton));
    }

    private void initBreweryLocationTextView() {
        locationTextView = findView(R.id.edit_brewery_location_edit_text);
    }

    private void initBreweryCountryTextView() {
        countryTextView = findView(R.id.edit_brewery_country_text_view);
        countryTextView.setOnClickListener(v -> {
            final Intent selectCountryIntent = new Intent(EditBreweryActivity.this, SelectCountryActivity.class);
            startActivityForResult(selectCountryIntent, SELECT_COUNTRY_REQUEST_CODE);
        });
    }

    private void restore(final Bundle savedInstanceState) {
        if (null != savedInstanceState) {
            nameTextView.setText(savedInstanceState.getCharSequence(NEW_BREWERY_NAME));
            locationTextView.setText(savedInstanceState.getCharSequence(NEW_BREWERY_LOCATION));

            final Country country = savedInstanceState.getParcelable(NEW_BREWERY_COUNTRY);
            if (null != country) {
                countryTextView.setText(country.getName());
                countryTextView.setTag(country);
            }
        }
    }

    private void initViewsWithBreweryProperties(final Brewery brewery) {
        nameTextView.setText(brewery.getName());
        final Maybe<String> location = brewery.getLocation();
        if (location.isPresent()) {
            locationTextView.setText(location.get());
        }
        final Maybe<Country> countryMaybe = brewery.getCountry();
        if (countryMaybe.isPresent()) {
            final Country country = countryMaybe.get();
            countryTextView.setText(country.getName());
            countryTextView.setTag(country);
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(NEW_BREWERY_NAME, nameTextView.getText());
        outState.putCharSequence(NEW_BREWERY_LOCATION, nameTextView.getText());
        final Country country = (Country) countryTextView.getTag();
        if (null != country) {
            outState.putParcelable(NEW_BREWERY_COUNTRY, country);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (SELECT_COUNTRY_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode) {
            // Save selected country as tag in country name TextView
            final Country selectedCountry = data.getParcelableExtra(SelectCountryActivity.SELECTED_COUNTRY);
            countryTextView.setText(selectedCountry.getName());
            countryTextView.setTag(selectedCountry);
        }
    }

    /**
     * Persists the brewery which is derived from the entered data.
     *
     * @param view the View is ignored by this method.
     */
    public void saveBrewery(final View view) {
        saveAction.run();
        finish();
    }

    private void setResult(final Brewery brewery) {
        final Intent resultIntent = new Intent();
        resultIntent.putExtra(EDITED_BREWERY, brewery);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    /**
     * This class inserts a new Brewery into the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveNewBreweryAction implements Runnable {
        @Override
        public void run() {
            final InsertHandler<Brewery> insertHandler = InsertHandler.async(getContentResolver(),
                    EditBreweryActivity.this::setResult);

            final CharSequence breweryName = nameTextView.getText();
            final CharSequence breweryLocation = locationTextView.getText();
            final Country breweryCountry = (Country) countryTextView.getTag();

            insertHandler.startInsert(BreweryFactory.newBrewery(breweryName, breweryLocation, breweryCountry));
        }
    }

    /**
     * This class updates an existing Brewery in the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveEditedBreweryAction implements Runnable {
        private final Brewery breweryToBeEdited;

        private SaveEditedBreweryAction(final Brewery breweryToBeEdited) {
            this.breweryToBeEdited = breweryToBeEdited;
        }

        @Override
        public void run() {
            final UpdateHandler<Brewery> updateHandler =
                    UpdateHandler.async(getContentResolver(), EditBreweryActivity.this::setResult);

            final Brewery editedBrewery = breweryToBeEdited.setName(nameTextView.getText())
                    .setLocation(locationTextView.getText())
                    .setCountry((Country) countryTextView.getTag());

            updateHandler.startUpdate(editedBrewery);
        }
    }

}
