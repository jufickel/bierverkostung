/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;

/**
 * This Activity shows a list of all persisted breweries. The user has the possibility to add a new brewery and to
 * delete breweries by swiping them out of the list.
 * <p>
 * A short click on a brewery item finishes the Activity and sets the selected brewery as extra in the result Intent.
 * The key to obtain the selected brewery is <code>"{@value #SELECTED_BREWERY}"</code>.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class SelectBreweryActivity extends BaseActivity {

    /**
     * Key to retrieve the selected brewery from this Activity's result Intent.
     */
    public static final String SELECTED_BREWERY = "selectedBrewery";

    private static final int NEW_BREWERY_REQUEST_CODE = 138;

    /**
     * Constructs a new {@code SelectBreweryActivity} object.
     */
    public SelectBreweryActivity() {
        super();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_brewery);

        final Fragment selectBreweryFragment = SelectBreweryFragment.newInstance(view -> {
            final Brewery brewery =  (Brewery) view.getTag();
            finishWithResult(brewery);
        });
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_select_brewery_container, selectBreweryFragment)
                .commit();
    }

    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // This overwrites the default OnClickListener which was already registered by the Fragment.
        initAddBreweryButton();
    }

    private void initAddBreweryButton() {
        final FloatingActionButton addBreweryButton = findView(R.id.select_brewery_fab_new_brewery);
        addBreweryButton.setOnClickListener(v -> {
            final Intent addBreweryIntent = new Intent(SelectBreweryActivity.this, EditBreweryActivity.class);
            startActivityForResult(addBreweryIntent, NEW_BREWERY_REQUEST_CODE);
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (NEW_BREWERY_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode) {
            final Brewery newBrewery = data.getParcelableExtra(EditBreweryActivity.EDITED_BREWERY);
            finishWithResult(newBrewery);
        }
    }

    private void finishWithResult(final Brewery selectedBrewery) {
        final Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_BREWERY, selectedBrewery);
        setResult(Activity.RESULT_OK, returnIntent);

        finish();
    }

}
