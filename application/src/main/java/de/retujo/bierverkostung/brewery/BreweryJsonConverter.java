/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.country.CountryFactory;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.java.util.AllNonnull;

/**
 * Converts a {@link Brewery} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class BreweryJsonConverter extends DataEntityJsonConverter<Brewery> {

    @Immutable
    static final class JsonName {
        static final String NAME = "name";
        static final String LOCATION = "location";
        static final String COUNTRY = "country";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private BreweryJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code BreweryJsonConverter}.
     *
     * @return the instance.
     */
    public static BreweryJsonConverter getInstance() {
        return new BreweryJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final Brewery brewery) throws JSONException {
        targetJsonObject.put(JsonName.NAME, brewery.getName());
        targetJsonObject.putOpt(JsonName.LOCATION, brewery.getLocation().orElse(null));
        targetJsonObject.putOpt(JsonName.COUNTRY, brewery.getCountry()
                .map(Country::toJson)
                .orElse(null));
    }

    @Override
    protected Brewery createEntityInstanceFromJson(final JSONObject sourceJsonObject, final EntityCommonData commonData)
            throws JSONException {

        final String name = sourceJsonObject.getString(JsonName.NAME);
        String location = null;
        if (sourceJsonObject.has(JsonName.LOCATION)) {
            location = sourceJsonObject.getString(JsonName.LOCATION);
        }
        Country country = null;
        if (sourceJsonObject.has(JsonName.COUNTRY)) {
            final JSONObject countryJsonObject = sourceJsonObject.getJSONObject(JsonName.COUNTRY);
            country = CountryFactory.newCountry(countryJsonObject);
        }

        return ImmutableBrewery.newInstance(commonData, name, location, country);
    }

}
