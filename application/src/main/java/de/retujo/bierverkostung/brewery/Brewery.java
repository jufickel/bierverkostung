/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import javax.annotation.Nullable;

import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * This interface represents a brewery.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface Brewery extends DataEntity, Jsonable {

    /**
     * Returns the name of the brewery.
     *
     * @return the name.
     */
    String getName();

    /**
     * Sets the specified name to a copy of this {@code Brewery} object. <em>Note:</em> the revision will not be
     * incremented.
     *
     * @param newName the name to be set.
     * @return a copy of this Brewery object with {@code newName} set or this Brewery object if it already has the
     * same name.
     * @throws NullPointerException if {@code newName} is {@code null}.
     * @throws IllegalArgumentException if {@code newName} is empty.
     */
    Brewery setName(CharSequence newName);

    /**
     * Returns the location of the brewery.
     *
     * @return the location or an empty Maybe.
     */
    Maybe<String> getLocation();

    /**
     * Sets the specified location to a copy of this {@code Brewery} object. <em>Note:</em> the revision will not be
     * incremented.
     *
     * @param newLocation the location to be set.
     * @return a copy of this Brewery object with {@code newLocation} set or this Brewery object if it already has the
     * same location.
     */
    Brewery setLocation(@Nullable CharSequence newLocation);

    /**
     * Returns the country of the brewery.
     *
     * @return the country or an empty Maybe.
     */
    Maybe<Country> getCountry();

    /**
     * Sets the specified country to a copy of this {@code Brewery} object. <em>Note:</em> the revision will not be
     * incremented.
     *
     * @param newCountry the country to be set.
     * @return a copy of this Brewery object with {@code newCountry} set or this Brewery object if it already has the
     * same country.
     */
    Brewery setCountry(@Nullable Country newCountry);

}
