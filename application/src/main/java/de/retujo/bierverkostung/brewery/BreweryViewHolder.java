/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.view.View;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractViewHolder;
import de.retujo.bierverkostung.country.Country;
import de.retujo.java.util.Maybe;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} binds Brewery-related view contents.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class BreweryViewHolder extends AbstractViewHolder<Brewery> {

    private final TextView breweryNameTextView;
    private final TextView breweryLocationTextView;
    private final TextView breweryCountryTextView;

    /**
     * Constructs a new {@code BreweryViewHolder} object.
     *
     * @param breweryItemView the view inflated in onCreateViewHolder.
     * @param onCountrySelectedListener a listener to be notified when a particular brewery was clicked.
     * @throws NullPointerException if {@code breweryNameTextView} is {@code null}.
     */
    public BreweryViewHolder(@Nonnull final View breweryItemView,
            @Nullable final View.OnClickListener onCountrySelectedListener) {
        super(breweryItemView);

        breweryItemView.setOnClickListener(onCountrySelectedListener);
        breweryNameTextView = (TextView) breweryItemView.findViewById(R.id.brewery_name_text_view);
        breweryLocationTextView = (TextView) breweryItemView.findViewById(R.id.brewery_location_text_view);
        breweryCountryTextView = (TextView) breweryItemView.findViewById(R.id.brewery_country_text_view);
    }

    @Override
    protected void handleDomainObjectSet(@Nonnull final Brewery brewery) {
        breweryNameTextView.setText(brewery.getName());
        final Maybe<String> locationMaybe = brewery.getLocation();
        if (locationMaybe.isPresent()) {
            breweryLocationTextView.setText(locationMaybe.get());
        }
        final Maybe<Country> countryMaybe = brewery.getCountry();
        if (countryMaybe.isPresent()) {
            final Country country = countryMaybe.get();
            breweryCountryTextView.setText(country.getName());
        }
    }

}
