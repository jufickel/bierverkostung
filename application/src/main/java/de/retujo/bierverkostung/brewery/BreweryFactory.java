/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.database.Cursor;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.country.CountryFactory;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.CursorReader;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.java.util.AllNonnull;
import de.retujo.bierverkostung.data.Revision;

import static de.retujo.java.util.Conditions.argumentNotEmpty;

/**
 * A factory for creating {@link Brewery} instance.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
public final class BreweryFactory {

    private BreweryFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link Brewery}.
     *
     * @param breweryName the breweryName of the brewery.
     * @param breweryLocation the breweryLocation of the brewery.
     * @param breweryCountry the breweryCountry of the brewery.
     * @return the instance.
     * @throws NullPointerException if {@code breweryName} is {@code null}.
     * @throws IllegalArgumentException if {@code breweryName} is empty.
     */
    public static Brewery newBrewery(final CharSequence breweryName,
            @Nullable final CharSequence breweryLocation, @Nullable final Country breweryCountry) {

        return newBrewery(EntityCommonData.getInstance(), breweryName, breweryLocation, breweryCountry);
    }

    /**
     * Returns a new immutable instance of {@link Brewery}.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param breweryName the breweryName of the brewery.
     * @param breweryLocation the breweryLocation of the brewery.
     * @param breweryCountry the breweryCountry of the brewery.
     * @return the instance.
     * @throws NullPointerException if {@code breweryName} is {@code null}.
     * @throws IllegalArgumentException if {@code breweryName} is empty.
     */
    public static Brewery newBrewery(final EntityCommonData commonData,
            final CharSequence breweryName,
            @Nullable final CharSequence breweryLocation,
            @Nullable final Country breweryCountry) {

        argumentNotEmpty(breweryName, "brewery breweryName");
        return ImmutableBrewery.newInstance(commonData, breweryName, breweryLocation, breweryCountry);
    }

    /**
     * Returns a new immutable instance of {@link Brewery} from the specified Cursor.
     *
     * @param cursor the cursor to provide the brewery data.
     * @return the instance
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     */
    @Nullable
    public static Brewery newBrewery(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final CursorReader cr = CursorReader.of(cursor);

        final String breweryIdAsString = cr.getString(BreweryEntry.COLUMN_ID);
        if (null != breweryIdAsString) {
            final UUID id = UUID.fromString(breweryIdAsString);
            final Revision revision = Revision.of(cr.getInt(BreweryEntry.COLUMN_REVISION));
            final String breweryName = cr.getString(BreweryEntry.COLUMN_NAME);
            final String breweryLocation = cr.getString(BreweryEntry.COLUMN_LOCATION);
            @Nullable final Country breweryCountry = CountryFactory.newCountry(cursor);

            return newBrewery(EntityCommonData.getInstance(id, revision), breweryName, breweryLocation,
                    breweryCountry);
        }
        return null;
    }

    /**
     * Returns a new immutable instance of {@link Brewery} from the specified {@link JSONObject}.
     *
     * @param jsonObject the JSON object to be converted to a Brewery.
     * @return the brewery.
     * @throws NullPointerException if {@code jsonObject} is {@code null}.
     * @throws de.retujo.bierverkostung.exchange.JsonConversionException if {@code jsonObject} cannot be converted to
     * a Brewery.
     * @since 1.2.0
     */
    public static Brewery newBrewery(final JSONObject jsonObject) {
        final BreweryJsonConverter converter = BreweryJsonConverter.getInstance();
        return converter.fromJson(jsonObject);
    }

}
