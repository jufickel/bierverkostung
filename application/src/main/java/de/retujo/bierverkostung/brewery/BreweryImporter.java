/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.country.CountryImporter;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Imports an external Brewery or returns or updates an existing Brewery.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class BreweryImporter extends DataEntityImporter<Brewery> {

    private final DataEntityImporter<Country> countryImporter;

    private BreweryImporter(final ContentResolver contentResolver,
            final DataEntityImporter<Country> theCountryImporter) {

        super(contentResolver, BreweryEntry.TABLE, BreweryFactory::newBrewery);
        countryImporter = isNotNull(theCountryImporter, "country importer");
    }

    /**
     * Returns an instance of {@code BreweryImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing breweries or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static BreweryImporter getInstance(final ContentResolver contentResolver) {
        return new BreweryImporter(contentResolver, CountryImporter.getInstance(contentResolver));
    }

    @Override
    protected Brewery importSubEntities(@Nonnull final Brewery externalBrewery) {
        return externalBrewery.getCountry()
                .map(countryImporter::importOrGetExisting)
                .map(externalBrewery::setCountry)
                .orElse(externalBrewery);
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final Brewery externalBrewery) {
        final Selection.AppendWhereStep b = Selection.where(BreweryEntry.COLUMN_NAME).is(externalBrewery.getName());
        externalBrewery.getLocation().ifPresent(location -> b.andWhere(BreweryEntry.COLUMN_LOCATION).is(location));
        externalBrewery.getCountry()
                .map(Country::getId)
                .ifPresent(countryId -> b.andWhere(BreweryEntry.COLUMN_COUNTRY_ID).is(countryId));

        return b.build();
    }

}
