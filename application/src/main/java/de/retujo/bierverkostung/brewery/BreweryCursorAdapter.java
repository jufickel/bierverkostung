/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractCursorAdapter;

/**
 * This CursorAdapter creates and binds {@link BreweryViewHolder}s, that hold the data of a Brewery, to a
 * RecyclerView to efficiently display the data.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@ParametersAreNonnullByDefault
@NotThreadSafe
final class BreweryCursorAdapter extends AbstractCursorAdapter<BreweryViewHolder> {

    /**
     * Constructs a new {@code BreweryCursorAdapter} object.
     *
     * @param context the current context.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    public BreweryCursorAdapter(final Context context, @Nullable final View.OnClickListener onBrewerySelectedListener) {
        super(context, R.layout.brewery_item, onBrewerySelectedListener);
    }

    @Nonnull
    @Override
    protected BreweryViewHolder doCreateViewHolder(final View view,
            @Nullable final View.OnClickListener onBrewerySelectedListener) {

        return new BreweryViewHolder(view, onBrewerySelectedListener);
    }

    @Override
    protected void doBindViewHolder(final BreweryViewHolder viewHolder, final Cursor cursor, final Context context) {
        final Brewery brewery = BreweryFactory.newBrewery(cursor);
        if (null != brewery) {
            viewHolder.setDomainObject(brewery);
        }
    }

}