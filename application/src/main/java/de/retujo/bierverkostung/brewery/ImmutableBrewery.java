/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.bierverkostung.data.Table;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;
import de.retujo.bierverkostung.data.Revision;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Immutable implementation of {@link Brewery}.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
final class ImmutableBrewery implements Brewery {

    /**
     * Creator which creates instances of {@code ImmutableBrewery} from a Parcel.
     */
    public static final Parcelable.Creator<Brewery> CREATOR = new ImmutableBreweryCreator();

    private static final Table TABLE = BreweryEntry.TABLE;

    private final EntityCommonData commonData;
    private final String name;
    @Nullable private final String location;
    @Nullable private final Country country;

    private ImmutableBrewery(final EntityCommonData theCommonData,
            final CharSequence theName,
            @Nullable final CharSequence theLocation,
            @Nullable final Country theCountry) {

        commonData = isNotNull(theCommonData, "common data");
        name = checkBreweryName(theName);
        location = null != theLocation ? theLocation.toString() : null;
        country = theCountry;
    }

    private static String checkBreweryName(final CharSequence name) {
        return argumentNotEmpty(name, "brewery name").toString();
    }

    /**
     * Returns a new instance of {@code ImmutableBrewery}.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param name the name of the brewery.
     * @param location the location of the brewery.
     * @param country the country of the brewery.
     * @return the instance.
     * @throws NullPointerException if {@code name} is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     */
    public static ImmutableBrewery newInstance(final EntityCommonData commonData,
            final CharSequence name,
            @Nullable final CharSequence location,
            @Nullable final Country country) {

        return new ImmutableBrewery(commonData, name, location, country);
    }

    @Override
    public UUID getId() {
        return commonData.getId();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Brewery setName(final CharSequence newName) {
        if (ObjectUtil.areEqual(checkBreweryName(newName), name)) {
            return this;
        }
        return newInstance(commonData, newName, location, country);
    }

    @Override
    public Maybe<String> getLocation() {
        return Maybe.ofNullable(location);
    }

    @Override
    public Brewery setLocation(@Nullable final CharSequence newLocation) {
        if (ObjectUtil.areEqual(newLocation, location)) {
            return this;
        }
        return newInstance(commonData, name, newLocation, country);
    }

    @Override
    public Maybe<Country> getCountry() {
        return Maybe.ofNullable(country);
    }

    @Override
    public Brewery setCountry(@Nullable final Country newCountry) {
        if (ObjectUtil.areEqual(newCountry, country)) {
            return this;
        }
        return newInstance(commonData, name, location, newCountry);
    }

    @Override
    public Uri getContentUri() {
        return commonData.getContentUri(TABLE);
    }

    @Override
    public Revision getRevision() {
        return commonData.getRevision();
    }

    @Override
    public ContentValues asContentValues() {
        final ContentValues result = commonData.asContentValues(TABLE);
        result.put(BreweryEntry.COLUMN_NAME.toString(), name);
        if (null != country) {
            result.put(BreweryEntry.COLUMN_COUNTRY_ID.toString(), country.getId().toString());
        }
        if (!TextUtils.isEmpty(location)) {
            result.put(BreweryEntry.COLUMN_LOCATION.toString(), location);
        }
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        commonData.writeToParcel(dest, flags);
        ParcelWrapper.of(dest)
                .wrap(name)
                .wrap(location)
                .wrap(country);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImmutableBrewery that = (ImmutableBrewery) o;
        return ObjectUtil.areEqual(commonData, that.commonData) &&
                ObjectUtil.areEqual(name, that.name) &&
                ObjectUtil.areEqual(location, that.location) &&
                ObjectUtil.areEqual(country, that.country);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(commonData, name, location, country);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {"
                + commonData
                + ", name='" + name + '\''
                + ", location='" + location + '\''
                + ", country=" + country
                + "}";
    }

    @Override
    public JSONObject toJson() {
        final BreweryJsonConverter converter = BreweryJsonConverter.getInstance();
        return converter.toJson(this);
    }

    /**
     * This class creates instances of {@code ImmutableBrewery} from a Parcel.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class ImmutableBreweryCreator extends BaseParcelableCreator<Brewery> {
        @Override
        public Brewery createFromParcel(final Parcel source, final EntityCommonData commonData) {
            final ParcelUnwrapper unwrapper = ParcelUnwrapper.of(source);
            final String readName = unwrapper.unwrapString();
            final String readLocation = unwrapper.unwrapString();
            final Country readCountry = unwrapper.unwrapParcelable();

            return ImmutableBrewery.newInstance(commonData, readName, readLocation, readCountry);
        }

        @Override
        public Brewery[] newArray(final int size) {
            return new Brewery[size];
        }
    }

}
