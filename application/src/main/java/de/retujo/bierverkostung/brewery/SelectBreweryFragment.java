/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import javax.annotation.concurrent.ThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractItemSwipeHandler;
import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.common.HideFabOnScrollListener;
import de.retujo.bierverkostung.common.Toaster;

/**
 * This Fragment shows a list of all persisted breweries. The user has the possibility to add a new brewery or to delete
 * breweries by swiping them off the list.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class SelectBreweryFragment extends Fragment {

    private FloatingActionButton addBreweryButton;
    private RecyclerView breweriesRecyclerView;
    private View.OnClickListener onBrewerySelectedListener;

    /**
     * Constructs a new {@code SelectBreweryFragment} object.
     */
    public SelectBreweryFragment() {
        addBreweryButton = null;
        breweriesRecyclerView = null;
        onBrewerySelectedListener = null;
    }

    /**
     * Returns a new instance of {@code SelectBreweryFragment}.
     *
     * @param onBrewerySelectedListener a listener to be notified if a brewery item is select or {@code null}. If no
     * listener is specified, a default listener will be used which shows the details of the selected brewery.
     * @return the instance.
     */
    @Nonnull
    public static SelectBreweryFragment newInstance(@Nullable final View.OnClickListener onBrewerySelectedListener) {
        final SelectBreweryFragment result = new SelectBreweryFragment();
        result.onBrewerySelectedListener = onBrewerySelectedListener;
        return result;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        final View result = inflater.inflate(R.layout.fragment_select_brewery, null);

        if (null == onBrewerySelectedListener) {
            onBrewerySelectedListener = new DefaultOnBrewerySelectedListener();
        }

        initAddBreweryButton(result);
        final BreweryCursorAdapter cursorAdapter = new BreweryCursorAdapter(getContext(), onBrewerySelectedListener);
        final AbstractLoaderCallbacks cursorLoaderCallbacks = initBreweriesLoader(cursorAdapter);
        initBreweriesRecyclerView(result, cursorAdapter, cursorLoaderCallbacks);

        return result;
    }

    private void initAddBreweryButton(final View parentView) {
        addBreweryButton = (FloatingActionButton) parentView.findViewById(R.id.select_brewery_fab_new_brewery);
        addBreweryButton.setOnClickListener(v -> {
            final Intent addBreweryIntent = new Intent(getContext(), EditBreweryActivity.class);
            startActivity(addBreweryIntent);
        });
    }

    private AbstractLoaderCallbacks initBreweriesLoader(final BreweryCursorAdapter cursorAdapter) {
        final BreweryLoaderCallbacks result = new BreweryLoaderCallbacks(getContext(), cursorAdapter);
        final LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(result.getId(), null, result);
        return result;
    }

    private void initBreweriesRecyclerView(final View parentView, final BreweryCursorAdapter cursorAdapter,
            final AbstractLoaderCallbacks cursorLoaderCallbacks) {

        breweriesRecyclerView = (RecyclerView) parentView.findViewById(R.id.select_brewery_recycler_view);
        breweriesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        breweriesRecyclerView.setAdapter(cursorAdapter);
        breweriesRecyclerView.addOnScrollListener(HideFabOnScrollListener.of(addBreweryButton));

        final SimpleCallback deleteBreweryOnSwipeHandler = new BreweryItemSwipeHandler(cursorLoaderCallbacks);
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(deleteBreweryOnSwipeHandler);
        itemTouchHelper.attachToRecyclerView(breweriesRecyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Without the following code the item slot would be empty after starting edit activity but not saving.
        final RecyclerView.Adapter breweryRecyclerViewAdapter = breweriesRecyclerView.getAdapter();
        breweryRecyclerViewAdapter.notifyDataSetChanged();
    }

    /**
     * This listener reacts when a brewery was selected. It then starts an Activity which shows the details of the
     * selected brewery.
     *
     * @since 1.0.0
     */
    @ThreadSafe
    private final class DefaultOnBrewerySelectedListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final Brewery brewery =  (Brewery) v.getTag();
            final Intent showBreweryIntent = new Intent(getContext(), ShowBreweryActivity.class);
            showBreweryIntent.putExtra(ShowBreweryActivity.BREWERY, brewery);
            startActivity(showBreweryIntent);
        }
    }

    /**
     * This class deletes a brewery when the user swipes it left or right. Beforehand the deletion has to be verified
     * by the user; this is done with an AlertDialog.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class BreweryItemSwipeHandler extends AbstractItemSwipeHandler<Brewery> {
        /**
         * Constructs a new {@code BreweryItemSwipeHandler} object.
         *
         * @param theLoaderCallbacks are called when a brewery gets deleted.
         * @throws NullPointerException if {@code theLoaderCallbacks} is {@code null}.
         */
        private BreweryItemSwipeHandler(@Nonnull final AbstractLoaderCallbacks theLoaderCallbacks) {
            super(getContext(), getLoaderManager(), theLoaderCallbacks);
        }

        @Override
        protected int getDialogTitle() {
            return R.string.delete_brewery_dialog_title;
        }

        @Nonnull
        @Override
        protected String getDialogMessage(@Nonnull final Brewery brewery) {
            return MessageFormat.format(getString(R.string.delete_brewery_dialog_message), brewery.getName());
        }

        @Override
        protected void onDeleteEntityFailed(final Brewery brewery) {
            Toaster.showLong(getContext(), R.string.delete_brewery_dialog_failed_to_delete, brewery.getName());
        }
    }

}
