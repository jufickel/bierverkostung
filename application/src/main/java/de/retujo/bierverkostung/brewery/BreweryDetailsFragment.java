/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.country.Country;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This fragment shows the details of a {@link Brewery}. Only available data is shown, i. e. the layout may vary from
 * brewery to brewery.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class BreweryDetailsFragment extends Fragment {

    /**
     * Key for the Beer argument.
     */
    public static final String ARG_BREWERY = "brewery";

    private volatile Brewery brewery;

    /**
     * Constructs a new {@code BreweryDetailsFragment}.
     * <p>
     * <em>Do not call this constructor.</em> It is required by Android internally.
     */
    public BreweryDetailsFragment() {
        // This ensures that the brewery can be put into the arguments in the Activity's "onAttachFragment" method.
        setArguments(new Bundle());
        brewery = null;
    }

    /**
     * Returns a new instance of {@code BreweryDetailsFragment}.
     *
     * @param brewery the brewery to be shown in this fragment.
     * @return the instance.
     * @throws NullPointerException if {@code brewery} is {@code null}.
     */
    @Nonnull
    public static BreweryDetailsFragment newInstance(@Nonnull final Brewery brewery) {
        isNotNull(brewery, "brewery");
        final BreweryDetailsFragment result = new BreweryDetailsFragment();

        final Bundle args = new Bundle();
        args.putParcelable(ARG_BREWERY, brewery);
        result.setArguments(args);

        return result;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
       if (null != arguments) {
            brewery = arguments.getParcelable(ARG_BREWERY);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.fragment_brewery_details, container, false);
        if (null != brewery) {
            initView(result);
        }
        return result;
    }

    private void initView(final View view) {
        final BreweryInitializer breweryInitializer = new BreweryInitializer(view, brewery);
        breweryInitializer.run();
    }

    /**
     * This class shows the card with the available properties of the Brewery of this fragment's Beer.
     *
     * @since 1.0.0
     */
    private static final class BreweryInitializer implements Runnable {
        private final View parentView;
        private final Brewery brewery;

        private BreweryInitializer(final View parentView, final Brewery brewery) {
            this.parentView = parentView;
            this.brewery = brewery;
        }

        @Override
        public void run() {
            getViewAndMakeVisible(R.id.show_brewery_card_view);
            initName();
            initLocation();
        }

        private void initName() {
            final TextView nameTextView = getViewAndMakeVisible(R.id.show_brewery_name);
            nameTextView.setText(brewery.getName());
        }

        private void initLocation() {
            final Maybe<String> location = brewery.getLocation();
            final Maybe<Country> countryMaybe = brewery.getCountry();

            // The text parentView contains the location as well as the country - dependent on what's available.
            if (location.isPresent()) {
                final TextView locationTextView = getViewAndMakeVisible(R.id.show_brewery_location);
                if (countryMaybe.isPresent()) {
                    final Country country = countryMaybe.get();
                    locationTextView.setText(location.get() + ", " + country.getName());
                } else {
                    locationTextView.setText(location.get());
                }
            } else if (countryMaybe.isPresent()) {
                final Country country = countryMaybe.get();
                final TextView locationTextView = getViewAndMakeVisible(R.id.show_brewery_location);
                locationTextView.setText(country.getName());
            }
        }

        @SuppressWarnings("unchecked")
        private <T extends View> T getViewAndMakeVisible(final int textViewId) {
            final View result = parentView.findViewById(textViewId);
            result.setVisibility(View.VISIBLE);
            return (T) result;
        }
    }

}
