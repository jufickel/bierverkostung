/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.text.MessageFormat;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.DeleteEntityDialogue;
import de.retujo.bierverkostung.common.Toaster;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;

/**
 * This activity shows the details of a {@link Brewery}. The brewery can be obtained from the Intent with the key
 * <code>"{@value #BREWERY}"</code>.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class ShowBreweryActivity extends BaseActivity {

    /**
     * Key for the Brewery of which the details are shown by this Activity.
     */
    public static final String BREWERY = "brewery";

    private static final int EDIT_BREWERY_REQUEST_CODE = 134;

    private Brewery shownBrewery;

    /**
     * Constructs a new {@code ShowBreweryActivity} object.
     */
    public ShowBreweryActivity() {
        shownBrewery = null;
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_brewery);

        final Intent afferentIntent = getIntent();
        shownBrewery = afferentIntent.getParcelableExtra(BREWERY);

        final Fragment breweryDetailsFragment = BreweryDetailsFragment.newInstance(shownBrewery);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.show_brewery_details_container, breweryDetailsFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.entity_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int itemId = item.getItemId();
        if (R.id.options_menu_edit == itemId) {
            final Intent editBreweryIntent = new Intent(this, EditBreweryActivity.class);
            editBreweryIntent.putExtra(EditBreweryActivity.EDIT_BREWERY, shownBrewery);
            startActivityForResult(editBreweryIntent, EDIT_BREWERY_REQUEST_CODE);
            return true;
        } else if (R.id.options_menu_delete == itemId) {
            final String breweryName = shownBrewery.getName();
            final DeleteEntityDialogue dialogue = DeleteEntityDialogue.getBuilder(this, shownBrewery)
                    .setTitle(R.string.delete_brewery_dialog_title)
                    .setMessage(MessageFormat.format(getString(R.string.delete_brewery_dialog_message), breweryName))
                    .setOnDeleteEntityListener(brewery -> DeleteDbEntityTask.getInstance(getContentResolver())
                            .setOnDeletedListener(deletedBreweries -> ShowBreweryActivity.this.finish())
                            .setOnFailedToDeleteListener(notDeletedBreweries -> {
                                final Context context = ShowBreweryActivity.this;
                                Toaster.showLong(context, R.string.delete_brewery_dialog_failed_to_delete,
                                        breweryName);
                            })
                            .execute(brewery))
                    .build();
            dialogue.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (Activity.RESULT_OK == resultCode && EDIT_BREWERY_REQUEST_CODE == requestCode) {
            final Brewery editedBrewery = data.getParcelableExtra(EditBreweryActivity.EDITED_BREWERY);
            final Intent showEditedBreweryIntent = new Intent(this, getClass());
            showEditedBreweryIntent.putExtra(BREWERY, editedBrewery);
            startActivity(showEditedBreweryIntent);
            finish();
        }
    }

}
