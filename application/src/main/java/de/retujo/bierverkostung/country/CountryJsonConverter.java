/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.java.util.AllNonnull;

/**
 * Converts a {@link Country} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class CountryJsonConverter extends DataEntityJsonConverter<Country> {

    @Immutable
    static final class JsonName {
        static final String NAME = "name";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private CountryJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code CountryJsonConverter}.
     *
     * @return the instance.
     */
    public static CountryJsonConverter getInstance() {
        return new CountryJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final Country country) throws JSONException {
        targetJsonObject.put(JsonName.NAME, country.getName());
    }

    @Override
    protected Country createEntityInstanceFromJson(final JSONObject sourceJsonObject, final EntityCommonData commonData)
            throws JSONException {

        return ImmutableCountry.newInstance(commonData, sourceJsonObject.getString(JsonName.NAME));
    }

}
