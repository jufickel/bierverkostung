/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.database.Cursor;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.CursorReader;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.Revision;
import de.retujo.java.util.AllNonnull;

/**
 * A factory for creating {@link Country} instances.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
public final class CountryFactory {

    private CountryFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link Country} with the given name.
     *
     * @param countryName the name of the country.
     * @return the instance.
     * @throws NullPointerException if {@code countryName} is {@code null}.
     * @throws IllegalArgumentException if {@code countryName} is empty.
     */
    public static Country newCountry(final CharSequence countryName) {
        return ImmutableCountry.newInstance(EntityCommonData.getInstance(), countryName);
    }

    /**
     * Returns a new immutable instance of {@link Country} with the given name.
     *
     * @param countryId the database row ID of the country.
     * @param countryName the name of the country.
     * @return the instance.
     * @throws NullPointerException if {@code countryName} is {@code null}.
     * @throws IllegalArgumentException if {@code countryName} is empty.
     */
    public static Country newCountry(final UUID countryId, final CharSequence countryName) {
        return newCountry(EntityCommonData.getInstance(countryId), countryName);
    }

    /**
     * Returns a new immutable instance of {@link Country} with the given name.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param countryName the name of the country.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code countryName} is empty.
     * @since 1.2.0
     */
    public static Country newCountry(final EntityCommonData commonData, final CharSequence countryName) {
        return ImmutableCountry.newInstance(commonData, countryName);
    }

    /**
     * Returns a new immutable instance of {@link Country} from the specified Cursor.
     *
     * @param cursor the cursor to provide the country data.
     * @return the instance.
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     */
    @Nullable
    public static Country newCountry(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final CursorReader cr = CursorReader.of(cursor);

        final String countryIdAsString = cr.getString(CountryEntry.COLUMN_ID);
        if (null != countryIdAsString) {
            final UUID countryId = UUID.fromString(countryIdAsString);
            final Revision revision = Revision.of(cr.getInt(CountryEntry.COLUMN_REVISION));
            final String countryName = cr.getString(CountryEntry.COLUMN_NAME);

            return newCountry(EntityCommonData.getInstance(countryId, revision), countryName);
        }
        return null;
    }

    /**
     * Returns a new immutable instance of {@link Country} from the specified {@link JSONObject}.
     *
     * @param jsonObject the JSON object to be converted to a Country.
     * @return the country.
     * @throws NullPointerException if {@code jsonObject} is {@code null}.
     * @throws de.retujo.bierverkostung.exchange.JsonConversionException if {@code jsonObject} cannot be converted to
     * a Country.
     * @since 1.2.0
     */
    public static Country newCountry(final JSONObject jsonObject) {
        final CountryJsonConverter converter = CountryJsonConverter.getInstance();
        return converter.fromJson(jsonObject);
    }

}
