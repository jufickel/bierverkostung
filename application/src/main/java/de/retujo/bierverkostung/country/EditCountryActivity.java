/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.ButtonEnablingTextWatcher;
import de.retujo.bierverkostung.data.InsertHandler;
import de.retujo.bierverkostung.data.UpdateHandler;

/**
 * Activity for creating a new country or for editing an existing country.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class EditCountryActivity extends BaseActivity {

    /**
     * Key to retrieve the newly created country from this Activity's result Intent.
     */
    public static final String NEW_COUNTRY = "newCountry";

    /**
     * Key to set an existing Country to be edited to the Intent for starting this Activity.
     */
    public static final String EDIT_COUNTRY = "countryToBeEdited";

    private TextView nameTextView;
    private Button saveButton;
    private Runnable saveAction;

    /**
     * Constructs a new {@code EditCountryActivity} object.
     */
    public EditCountryActivity() {
        nameTextView = null;
        saveButton = null;
        saveAction = null;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_country);

        initSaveCountryButton();
        initCountryNameTextView();

        final Intent afferentIntent = getIntent();
        final Bundle extras = afferentIntent.getExtras();
        if (isAddNewCountryActivity(extras)) {
            setTitle(getString(R.string.add_country_activity_name));
            saveAction = new SaveNewCountryAction();
        } else {
            setTitle(getString(R.string.edit_country_activity_name));
            final Country countryToBeEdited = extras.getParcelable(EDIT_COUNTRY);
            nameTextView.setText(countryToBeEdited.getName());
            saveAction = new SaveEditedCountryAction(countryToBeEdited);
        }
    }

    private void initSaveCountryButton() {
        saveButton = findView(R.id.edit_country_save_button);
        saveButton.setEnabled(false);
    }

    private void initCountryNameTextView() {
        nameTextView = findView(R.id.edit_country_name_edit_text);
        nameTextView.addTextChangedListener(new ButtonEnablingTextWatcher(saveButton));
    }

    private static boolean isAddNewCountryActivity(final Bundle bundle) {
        return null == bundle || !bundle.containsKey(EDIT_COUNTRY);
    }

    /**
     * Persists the country which is derived from the entered country name.
     *
     * @param view the View is ignored by this method.
     */
    public void saveCountry(final View view) {
        saveAction.run();
    }

    /**
     * This class inserts a new Country into the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveNewCountryAction implements Runnable {
        @Override
        public void run() {
            final InsertHandler<Country> insertHandler = InsertHandler.async(getContentResolver(), country -> {
                final Intent returnIntent = new Intent();
                returnIntent.putExtra(NEW_COUNTRY, country);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            });

            insertHandler.startInsert(CountryFactory.newCountry(nameTextView.getText()));
        }
    }

    /**
     * This class updates an existing Country in the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveEditedCountryAction implements Runnable {
        private final Country countryToBeEdited;

        private SaveEditedCountryAction(final Country countryToBeEdited) {
            this.countryToBeEdited = countryToBeEdited;
        }

        @Override
        public void run() {
            final Country editedCountry = countryToBeEdited.setName(nameTextView.getText());

            if (!countryToBeEdited.equals(editedCountry)) {
                final UpdateHandler<Country> updateHandler = UpdateHandler.async(getContentResolver(), c -> finish());
                updateHandler.startUpdate(editedCountry);
            } else {
                finish();
            }
        }
    }

}
