/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.View;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractItemSwipeHandler;
import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.common.BaseActivity;

/**
 * This Activity shows a list of all persisted countries. The user has the possibility to add new countries or to
 * delete countries by swiping them off the list.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class SelectCountryActivity extends BaseActivity {

    /**
     * Key to retrieve the selected country from this Activity's result Intent.
     */
    public static final String SELECTED_COUNTRY = "selectedCountry";

    private static final int NEW_COUNTRY_REQUEST_CODE = 134;

    /**
     * Constructs a new {@code SelectCountryActivity} object.
     */
    public SelectCountryActivity() {
        super();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_country);

        initAddCountryButton();
        final OnCountrySelectedListener onCountrySelectedListener = new OnCountrySelectedListener();
        final CountryCursorAdapter cursorAdapter = new CountryCursorAdapter(this, onCountrySelectedListener);
        final AbstractLoaderCallbacks cursorLoaderCallbacks = initCountriesLoader(cursorAdapter);
        initCountriesRecyclerView(cursorAdapter, cursorLoaderCallbacks);
    }

    private void initAddCountryButton() {
        final FloatingActionButton addCountryButton = findView(R.id.select_country_fab_new_country);
        addCountryButton.setOnClickListener(v -> {
            final Intent addCountryIntent = new Intent(SelectCountryActivity.this, EditCountryActivity.class);
            startActivityForResult(addCountryIntent, NEW_COUNTRY_REQUEST_CODE);
        });
    }

    private AbstractLoaderCallbacks initCountriesLoader(final CountryCursorAdapter cursorAdapter) {
        final CountriesLoaderCallbacks result = new CountriesLoaderCallbacks(this, cursorAdapter);
        final LoaderManager supportLoaderManager = getSupportLoaderManager();
        supportLoaderManager.initLoader(result.getId(), null, result);
        return result;
    }

    private void initCountriesRecyclerView(final CountryCursorAdapter cursorAdapter,
            final AbstractLoaderCallbacks cursorLoaderCallbacks) {
        final RecyclerView countriesRecyclerView = findView(R.id.select_country_recycler_view);
        countriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        countriesRecyclerView.setAdapter(cursorAdapter);

        final SimpleCallback deleteCountryOnSwipeHandler = new CountryItemSwipeHandler(cursorLoaderCallbacks);
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(deleteCountryOnSwipeHandler);
        itemTouchHelper.attachToRecyclerView(countriesRecyclerView);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (NEW_COUNTRY_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode) {
            final Country newCountry = data.getParcelableExtra(EditCountryActivity.NEW_COUNTRY);
            finishWithResult(newCountry);
        }
    }

    private void finishWithResult(final Country selectedCountry) {
        final Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_COUNTRY, selectedCountry);
        setResult(Activity.RESULT_OK, returnIntent);

        finish();
    }

    /**
     * This listener reacts when a country was selected. It then gets the country ID and name and sets these as result
     * of the current activity before the current activity is finished.
     */
    private final class OnCountrySelectedListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final Country country = (Country) v.getTag();
            finishWithResult(country);
        }
    }

    /**
     * This class deletes a country when the user swipes its item left or right. Beforehand the deletion has to be
     * verified by the user; this is done with an AlertDialog.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class CountryItemSwipeHandler extends AbstractItemSwipeHandler<Country> {
        /**
         * Constructs a new {@code CountryItemSwipeHandler} object.
         *
         * @param loaderCallbacks are called when a country gets deleted.
         * @throws NullPointerException if {@code loaderCallbacks} is {@code null}.
         */
        private CountryItemSwipeHandler(@Nonnull final AbstractLoaderCallbacks loaderCallbacks) {
            super(SelectCountryActivity.this, loaderCallbacks);
        }

        @Override
        protected int getDialogTitle() {
            return R.string.delete_country_dialog_title;
        }

        @Nonnull
        @Override
        protected String getDialogMessage(@Nonnull final Country country) {
            return MessageFormat.format(getString(R.string.delete_country_dialog_message), country.getName());
        }
    }

}
