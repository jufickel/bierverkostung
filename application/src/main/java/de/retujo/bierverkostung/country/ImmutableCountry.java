/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.Table;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;
import de.retujo.bierverkostung.data.Revision;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Immutable implementation of {@link Country}.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
final class ImmutableCountry implements Country {

    /**
     * Creator which creates instances of {@code ImmutableCountry} from a Parcel.
     */
    public static final Parcelable.Creator<Country> CREATOR = new ImmutableCountryCreator();

    private static final Table TABLE = CountryEntry.TABLE;

    private final EntityCommonData commonData;
    private final String name;

    private ImmutableCountry(final EntityCommonData theCommonData, final CharSequence theName) {
        commonData = isNotNull(theCommonData, "common data");
        name = argumentNotEmpty(theName, "country name").toString();
    }

    /**
     * Returns a new instance of {@code ImmutableCountry} with the given ID and name.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param name the name of the country.
     * @return the instance.
     * @throws NullPointerException if {@code name} is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     */
    public static ImmutableCountry newInstance(final EntityCommonData commonData, final CharSequence name) {
        return new ImmutableCountry(commonData, name);
    }

    @Override
    @Nonnull
    public UUID getId() {
        return commonData.getId();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Country setName(final CharSequence newName) {
        argumentNotEmpty(newName, "name to be set");
        if (ObjectUtil.areEqual(newName, name)) {
            return this;
        }
        return newInstance(commonData.incrementRevision(), newName);
    }

    @Override
    public Uri getContentUri() {
        return commonData.getContentUri(TABLE);
    }

    @Override
    public Revision getRevision() {
        return commonData.getRevision();
    }

    @Override
    public ContentValues asContentValues() {
        final ContentValues result = commonData.asContentValues(TABLE);
        result.put(CountryEntry.COLUMN_NAME.toString(), name);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        // All fields are always not null thus renouncing on use of ParcelWrapper.
        commonData.writeToParcel(dest, flags);
        dest.writeString(name);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImmutableCountry that = (ImmutableCountry) o;
        return ObjectUtil.areEqual(commonData, that.commonData) && ObjectUtil.areEqual(name, that.name);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(commonData, name);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + commonData + ", name='" + name + '\'' + "}";
    }

    @Nonnull
    @Override
    public JSONObject toJson() {
        final CountryJsonConverter converter = CountryJsonConverter.getInstance();
        return converter.toJson(this);
    }

    /**
     * This class creates instances of {@code ImmutableCountry} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableCountryCreator extends BaseParcelableCreator<Country> {
        @Override
        protected Country createFromParcel(final Parcel source, final EntityCommonData commonData) {
            return ImmutableCountry.newInstance(commonData, source.readString());
        }

        @Override
        public Country[] newArray(final int size) {
            return new Country[size];
        }
    }

}
