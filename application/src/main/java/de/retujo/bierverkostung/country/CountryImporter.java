/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.java.util.AllNonnull;

/**
 * Imports an external Country or returns or updates an existing Country.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class CountryImporter extends DataEntityImporter<Country> {

    private CountryImporter(final ContentResolver contentResolver) {
        super(contentResolver, CountryEntry.TABLE, CountryFactory::newCountry);
    }

    /**
     * Returns an instance of {@code CountryImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing countries or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static CountryImporter getInstance(final ContentResolver contentResolver) {
        return new CountryImporter(contentResolver);
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final Country externalCountry) {
        return Selection.where(CountryEntry.COLUMN_NAME).is(externalCountry.getName()).build();
    }

}
