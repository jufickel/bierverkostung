/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractViewHolder;
import de.retujo.bierverkostung.common.DeleteEntityDialogue;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;
import de.retujo.java.util.Maybe;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} binds Country-related view contents.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class CountryViewHolder extends AbstractViewHolder<Country> implements PopupMenu.OnMenuItemClickListener {

    private final TextView countryNameTextView;

    /**
     * Constructs a new {@code CountryViewHolder} object.
     *
     * @param countryItemView the view inflated in onCreateViewHolder.
     * @param onCountrySelectedListener a listener to be notified when a particular country was clicked.
     * @throws NullPointerException if {@code countryNameTextView} is {@code null}.
     */
    public CountryViewHolder(@Nonnull final View countryItemView,
            @Nullable final View.OnClickListener onCountrySelectedListener) {
        super(countryItemView);
        countryItemView.setOnClickListener(onCountrySelectedListener);
        countryNameTextView = (TextView) countryItemView.findViewById(R.id.country_item_name_text_view);
        final ImageView overflowIcon =
                (ImageView) countryItemView.findViewById(R.id.country_item_context_menu_image_view);
        overflowIcon.setOnClickListener(v -> {
            final PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
            popupMenu.inflate(R.menu.entity_options_menu);
            popupMenu.setOnMenuItemClickListener(CountryViewHolder.this);
            popupMenu.show();
        });
    }

    @Override
    protected void handleDomainObjectSet(@Nonnull final Country country) {
        countryNameTextView.setText(country.getName());
    }

    @Override
    public boolean onMenuItemClick(final MenuItem item) {
        final int itemId = item.getItemId();
        final Context context = getContext();
        if (R.id.options_menu_edit == itemId) {
            final Maybe<Country> domainObject = getDomainObject();
            if (domainObject.isPresent()) {
                final Intent editCountryIntent = new Intent(context, EditCountryActivity.class);
                editCountryIntent.putExtra(EditCountryActivity.EDIT_COUNTRY, domainObject.get());
                context.startActivity(editCountryIntent);
                return true;
            }
        } else if (R.id.options_menu_delete == itemId) {
            final Maybe<Country> countryMaybe = getDomainObject();
            if (countryMaybe.isPresent()) {
                final Country country = countryMaybe.get();
                final DeleteEntityDialogue dialogue = DeleteEntityDialogue.getBuilder(context, country)
                        .setTitle(R.string.delete_country_dialog_title)
                        .setMessage(MessageFormat.format(context.getString(R.string.delete_country_dialog_message),
                                country.getName()))
                        .setOnDeleteEntityListener(
                                countryToBeDeleted -> DeleteDbEntityTask.getInstance(context.getContentResolver())
                                        .execute(countryToBeDeleted))
                        .build();
                dialogue.show();
                return true;
            }
        }

        return false;
    }

}
