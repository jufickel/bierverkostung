/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.AllNonnull;

/**
 * This interface represents a country.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface Country extends DataEntity, Jsonable {

    /**
     * Returns the name of this country.
     *
     * @return the name.
     */
    String getName();

    /**
     * Sets the specified name and increases the revision on a copy of this Country object.
     *
     * @param newName the new name to be set.
     * @return a new Country object with {@code newName} as its name or this Country object if it already has that name.
     * @throws NullPointerException if {@code newName} is {@code null}.
     * @throws IllegalArgumentException if {@code newName} is empty.
     */
    Country setName(CharSequence newName);

}
