/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.widget.Toast;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * This utility class exists for conveniently shown {@link Toast}s.
 *
 * @since 1.1.0
 */
@Immutable
public final class Toaster {

    /**
     * Shows a long Toast. The text of the shown Toast is composed of a pattern and the specified arguments. The
     * pattern is referenced by the specified resource id.
     *
     * @param context the context for loading the pattern and showing the Toast.
     * @param patternResourceId the resource ID of the pattern string for composing the text of the Toast.
     * @param textArgs the optional arguments for composing the text of the toast.
     */
    public static void showLong(@Nonnull final Context context, final int patternResourceId,
            @Nonnull final Object... textArgs) {

        final String pattern = String.valueOf(context.getText(patternResourceId));
        final String text = MessageFormat.format(pattern, textArgs);
        showLong(context, text);
    }

    /**
     * Shows a long Toast. The text of the shown Toast is the specified argument.
     *
     * @param context the context for loading the pattern and showing the Toast.
     * @param text the text to be shown.
     * @since 1.2.0
     */
    public static void showLong(@Nonnull final Context context, @Nonnull final CharSequence text) {
        final Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }

    private Toaster() {
        throw new AssertionError();
    }

}
