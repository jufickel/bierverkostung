/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * This is a utility class to remove a trailing comma and optional whitespace from the end of a char sequence. This
 * is useful before persisting the text of MultiAutoCompleteEditTexts which will automatically add this delimiter after
 * the user selected a suggested value.
 *
 * @since 1.0.0
 */
@Immutable
public final class CommaDelimiterTrimmer {

    /**
     * This regular expression describes a delimiter at the end of a char sequence.
     */
    public static final String DELIMITER_REGEX = "(.*)(,\\s*$)";

    private static final Pattern PATTERN = Pattern.compile(DELIMITER_REGEX);

    private CommaDelimiterTrimmer() {
        throw new AssertionError();
    }

    /**
     * Truncates the specified char sequence by searching for {@value #DELIMITER_REGEX} and replacing it with an
     * empty String.
     *
     * @param charSequence the char sequence to be truncated.
     * @return {@code null} if {@code charSequence} is {@code null}, a new String which is {@code charSequence}
     * truncated by the delimiter or {@code charSequence} itself if it did not contain the delimiter.
     */
    @Nullable
    public static String trim(@Nullable final CharSequence charSequence) {
        if (null == charSequence) {
            return null;
        }
        if (0 < charSequence.length()) {
            final String string = charSequence.toString();
            final Matcher matcher = PATTERN.matcher(string);
            if (matcher.matches()) {
                return matcher.group(1);
            } else {
                return string;
            }
        }
        return "";
    }

}
