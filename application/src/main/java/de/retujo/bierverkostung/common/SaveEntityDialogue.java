/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This dialogue is shown before an Activity is finished where unsaved work is pending. It shows two buttons where the
 * positive button will save the entity and the negative button just discards the work. In either case an optional
 * listener will be notified.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class SaveEntityDialogue {

    private final AlertDialog alertDialog;

    private SaveEntityDialogue(final AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    /**
     * Returns a new instance of a builder with a fluent API for a {@code SaveEntityDialogue}.
     *
     * @param context the context.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code entity} has no ID.
     */
    public static Builder getBuilder(final Context context) {
        return new Builder(isNotNull(context, "context"));
    }

    /**
     * Starts the dialogue and shows it on the screen.
     *
     * @see AlertDialog#show()
     */
    public void show() {
        alertDialog.show();
    }

    /**
     * A mutable builder with a fluent API for a {@code SaveEntityDialogue}.
     */
    @NotThreadSafe
    public static final class Builder {
        private final AlertDialog.Builder dialogBuilder;
        @Nullable private Acceptor<Void> onSaveEntityListener;
        @Nullable private Acceptor<Void> onDiscardEntityListener;

        private Builder(final Context context) {
            dialogBuilder = new AlertDialog.Builder(context);
            onSaveEntityListener = null;
            onDiscardEntityListener = null;
        }

        /**
         * Sets the title using the specified ID.
         *
         * @param titleId the string resource ID.
         * @return this builder instance to allow method chaining.
         * @see AlertDialog.Builder#setTitle(int)
         */
        public Builder setTitle(final int titleId) {
            dialogBuilder.setTitle(titleId);
            return this;
        }

        /**
         * Sets the specified message to be displayed in the dialogue.
         *
         * @param message the message.
         * @return this builder instance to allow method chaining.
         * @see AlertDialog.Builder#setMessage(CharSequence)
         */
        public Builder setMessage(final CharSequence message) {
            dialogBuilder.setMessage(message);
            return this;
        }

        /**
         * Sets the listener to be notified when the entity should be saved.
         *
         * @param listener the listener.
         * @return this builder instance to allow method chaining.
         */
        public Builder setOnSaveEntityListener(@Nullable final Acceptor<Void> listener) {
            onSaveEntityListener = listener;
            return this;
        }

        /**
         * Sets the listener to be notified when the entity should not be saved.
         *
         * @param listener the listener or {@code null}.
         * @return this builder instance to allow method chaining.
         */
        public Builder setOnDiscardEntityListener(@Nullable final Acceptor<Void> listener) {
            onDiscardEntityListener = listener;
            return this;
        }

        /**
         * Creates a new instance of {@code SaveEntityDialogue}.
         *
         * @return the instance.
         */
        public SaveEntityDialogue build() {
            dialogBuilder.setPositiveButton(R.string.save_dialog_button_positive_label, (dialog, which) -> {
                dialog.dismiss();
                if (null != onSaveEntityListener) {
                    onSaveEntityListener.accept(null);
                }
            });
            dialogBuilder.setNegativeButton(R.string.save_dialog_button_negative_label, (dialog, which) -> {
                dialog.dismiss();
                if (null != onDiscardEntityListener) {
                    onDiscardEntityListener.accept(null);
                }
            });

            final AlertDialog alertDialog = dialogBuilder.create();
            return new SaveEntityDialogue(alertDialog);
        }
    }

}
