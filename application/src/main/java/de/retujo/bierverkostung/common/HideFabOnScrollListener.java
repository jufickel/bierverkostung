/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class holds a {@link FloatingActionButton} (FAB) which is supposed to be anchored to a {@link RecyclerView}. As
 * soon as this list gets scrolled down the FAB is hidden. It remains hidden until the list gets scrolled up. This
 * prevents the FAB to hide entries at the bottom of the list.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class HideFabOnScrollListener extends RecyclerView.OnScrollListener {

    private final FloatingActionButton fab;

    private HideFabOnScrollListener(final FloatingActionButton floatingActionButton) {
        fab = floatingActionButton;
    }

    /**
     * Returns a new instance of {@code HideFabOnScrollListener}.
     *
     * @param floatingActionButton the floating action button to be hidden/shown.
     * @return the instance.
     * @throws NullPointerException if {@code floatingActionButton} is {@code null}.
     */
    public static HideFabOnScrollListener of(final FloatingActionButton floatingActionButton) {
        isNotNull(floatingActionButton, "FloatingActionButton");
        return new HideFabOnScrollListener(floatingActionButton);
    }

    @Override
    public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
        if (0 < dy && fab.isShown()) {
            fab.hide();
        } else if (0 > dy && !fab.isShown()) {
            fab.show();
        }
    }

}
