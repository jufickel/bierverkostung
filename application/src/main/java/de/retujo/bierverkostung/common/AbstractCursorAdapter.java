/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class is an extension of {@link RecyclerView.Adapter} .
 *
 * @param <T> the type of the {@link RecyclerView.ViewHolder} of this adapter.
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
public abstract class AbstractCursorAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private final Context context;
    private final int viewResource;
    private final View.OnClickListener onViewClickListener;
    private Cursor cursor;

    /**
     * Constructs a new {@code AbstractCursorAdapter} object.
     *
     * @param context the context to be used.
     * @param viewResource ID of the view resource for the ViewHolder.
     * @param onViewClickListener a listener to be notified when a particular item was clicked.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    protected AbstractCursorAdapter(final Context context, final int viewResource,
            @Nullable final View.OnClickListener onViewClickListener) {

        this.context = isNotNull(context, "Context");
        this.viewResource = viewResource;
        this.onViewClickListener = onViewClickListener;
        cursor = null;
    }

    @Override
    public final T onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View view = layoutInflater.inflate(viewResource, parent, false);
        return doCreateViewHolder(view, onViewClickListener);
    }

    /**
     * Returns a new ViewHolder that holds the specified View of the given view type.
     *
     * @param view the View to be used by the returned ViewHolder.
     * @param onViewClickListener a listener to be notified when a particular item was clicked.
     * @return the ViewHolder.
     */
    @Nonnull
    protected abstract T doCreateViewHolder(View view, @Nullable View.OnClickListener onViewClickListener);

    @Override
    public final void onBindViewHolder(final T viewHolder, final int position) {
        cursor.moveToPosition(position);
        doBindViewHolder(viewHolder, cursor, context);
    }

    /**
     * Called after RecyclerView requested to display the data at the specified position. This method should update
     * the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at a certain position.
     * <p>
     * The Cursor is already moved to the appropriate position.
     * </p>
     *
     * @param viewHolder the ViewHolder to be updated to represent the contents of the item at the given position in
     * the data set.
     * @param cursor the Cursor which provides the data of the item.
     * @param context the context to be used.
     */
    protected abstract void doBindViewHolder(T viewHolder, Cursor cursor, Context context);

    /**
     * Returns the number of countries to display.
     *
     * @return the number of countries.
     */
    @Override
    public final int getItemCount() {
        if (null == cursor) {
            return 0;
        }
        return cursor.getCount();
    }

    /**
     * Swaps the old Cursor with the given newly updated Cursor. This occurs when data changed and are re-queried.
     *
     * @param updatedCursor the supposed newly updated Cursor.
     * @return the old Cursor or {@code null} if the given Cursor is the same as the old Cursor.
     */
    @Nullable
    public final Cursor swapCursor(@Nullable final Cursor updatedCursor) {
        if (cursor == updatedCursor) {
            return null;
        }
        final Cursor result = cursor;
        cursor = updatedCursor;

        if (null != updatedCursor) {
            notifyDataSetChanged();
        }
        return result;
    }

}
