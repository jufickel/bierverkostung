/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This dialogue is shown before an entity is finally deleted. It shows two buttons where the positive button will
 * delete the entity and the negative button just dismisses the dialogue. In either case an optional listener will be
 * notified.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
public final class DeleteEntityDialogue {

    private final AlertDialog alertDialog;

    private DeleteEntityDialogue(final AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    /**
     * Returns a new instance of a builder with a fluent API for a {@code DeleteEntityDialogue}.
     *
     * @param <T> the type of the entity to be deleted.
     * @param context the context.
     * @param entity the entity to be deleted.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code entity} has no ID.
     */
    public static <T> Builder<T> getBuilder(final Context context, final T entity) {
        return new Builder<>(isNotNull(context, "context"), isNotNull(entity, "entity to be deleted"));
    }

    /**
     * Starts the dialogue and shows it on the screen.
     *
     * @see AlertDialog#show()
     */
    public void show() {
        alertDialog.show();
    }

    /**
     * A mutable builder with a fluent API for a {@code DeleteEntityDialogue}.
     *
     * @param <T> the type of the entity to be deleted.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    public static final class Builder<T> {
        private final T entity;
        private final AlertDialog.Builder dialogBuilder;
        @Nullable private Acceptor<T> onDeleteEntityListener;
        @Nullable private Acceptor<T> onKeepEntityListener;

        private Builder(final Context context, final T entity) {
            this.entity = entity;
            dialogBuilder = new AlertDialog.Builder(context);
            onDeleteEntityListener = null;
            onKeepEntityListener = null;
        }

        /**
         * Sets the title using the specified ID.
         *
         * @param titleId the string resource ID.
         * @return this builder instance to allow method chaining.
         * @see AlertDialog.Builder#setTitle(int)
         */
        public Builder<T> setTitle(final int titleId) {
            dialogBuilder.setTitle(titleId);
            return this;
        }

        /**
         * Sets the specified message to be displayed in the dialogue.
         *
         * @param message the message.
         * @return this builder instance to allow method chaining.
         * @see AlertDialog.Builder#setMessage(CharSequence)
         */
        public Builder<T> setMessage(final CharSequence message) {
            dialogBuilder.setMessage(message);
            return this;
        }

        /**
         * Sets the listener to be notified when the entity should be deleted.
         *
         * @param listener the listener.
         * @return this builder instance to allow method chaining.
         * @since 1.2.0
         */
        public Builder<T> setOnDeleteEntityListener(@Nullable final Acceptor<T> listener) {
            onDeleteEntityListener = listener;
            return this;
        }

        /**
         * Sets the listener to be notified when the entity should be kept.
         *
         * @param listener the listener or {@code null}.
         * @return this builder instance to allow method chaining.
         */
        public Builder<T> setOnKeepEntityListener(@Nullable final Acceptor<T> listener) {
            onKeepEntityListener = listener;
            return this;
        }

        /**
         * Creates a new instance of {@code DeleteEntityDialogue}.
         *
         * @return the instance.
         */
        public DeleteEntityDialogue build() {
            dialogBuilder.setPositiveButton(R.string.delete_dialog_button_positive_label, (dialog, which) -> {
                dialog.dismiss();
                if (null != onDeleteEntityListener) {
                    onDeleteEntityListener.accept(entity);
                }
            });
            dialogBuilder.setNegativeButton(R.string.delete_dialog_button_negative_label, (dialog, which) -> {
                dialog.dismiss();
                if (null != onKeepEntityListener) {
                    onKeepEntityListener.accept(entity);
                }
            });

            final AlertDialog alertDialog = dialogBuilder.create();
            return new DeleteEntityDialogue(alertDialog);
        }
    }

}
