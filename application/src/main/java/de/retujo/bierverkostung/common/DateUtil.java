/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.util.Log;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.Conditions;
import de.retujo.java.util.Maybe;

/**
 * This is a utility class for date-related operations.
 *
 * @since 1.0.0
 */
@Immutable
public final class DateUtil {

    private static final String TAG = DateUtil.class.getName();

    private final DateFormat isoDateFormatShort;
    private final DateFormat isoDateFormatLong;
    private final DateFormat longDateFormat;

    private DateUtil(final DateFormat theIsoDateFormatShort, final DateFormat theIsoDateFormatLong,
            final DateFormat theLongDateFormat) {

        isoDateFormatShort = theIsoDateFormatShort;
        isoDateFormatLong = theIsoDateFormatLong;
        longDateFormat = theLongDateFormat;
    }

    /**
     * Returns an instance of {@code DateUtil}.
     *
     * @param context the application context for obtaining the appropriate DateFormat.
     * @return the instance;
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    @Nonnull
    public static DateUtil getInstance(@Nonnull final Context context) {
        Conditions.isNotNull(context, "context");
        final DateFormat isoDateFormatShort = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        final DateFormat isoDateFormatLong = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss", Locale.ENGLISH);
        final DateFormat longDateFormat = android.text.format.DateFormat.getLongDateFormat(context);

        return new DateUtil(isoDateFormatShort, isoDateFormatLong, longDateFormat);
    }

    /**
     * Returns the long-formatted date (e. g. 15. February 2017) for the specified string representation.
     *
     * @param inputDate the date to be formatted.
     * @return the input date as long date string or an empty Maybe if the input cannot be parsed.
     * @throws NullPointerException if {@code inputDate} is {@code null}.
     * @throws IllegalArgumentException if {@code inputDate} is empty.
     */
    @Nonnull
    public Maybe<String> getLongString(@Nonnull final CharSequence inputDate) {
        Conditions.argumentNotEmpty(inputDate, "input date");
        final Date date = tryToParseIsoDate(inputDate);
        if (null != date) {
            return Maybe.of(getLongString(date));
        }
        return Maybe.empty();
    }

    /**
     * Parses the specified ISO date representation to a Date.
     *
     * @param inputDate an ISO date representation to be parsed to a Date.
     * @return the parsed input date as Date.
     * @throws NullPointerException if {@code inputDate} is {@code null}.
     * @throws IllegalArgumentException if {@code inputDate} is empty.
     */
    @Nullable
    public Date tryToParseIsoDate(@Nonnull final CharSequence inputDate) {
        Conditions.argumentNotEmpty(inputDate, "input date");
        try {
            return isoDateFormatShort.parse(inputDate.toString());
        } catch (final ParseException e) {
            // This should never happen as we control the date input.
            Log.e(TAG, MessageFormat.format("Failed to parse date <{0}>!", inputDate), e);
            return null;
        }
    }

    /**
     * Returns the long-formatted date (e. g. 15. February 2017) string representation for the specified Date.
     *
     * @param inputDate the date to be formatted.
     * @return the formatted string.
     * @throws NullPointerException if {@code inputDate} is {@code null}.
     */
    @Nonnull
    public String getLongString(@Nonnull final Date inputDate) {
        checkDate(inputDate);
        return longDateFormat.format(inputDate);
    }

    private static void checkDate(final Date inputDate) {
        Conditions.isNotNull(inputDate, "date to be formatted");
    }

    /**
     * Returns the short ISO-formatted date (e. g. 2017-02-15) string representation for the specified Date.
     *
     * @param date the date to be formatted.
     * @return the formatted string.
     * @throws NullPointerException if {@code date} is {@code null}.
     */
    public String getShortIsoString(@Nonnull final Date date) {
        checkDate(date);
        return isoDateFormatShort.format(date);
    }

    /**
     * Returns the long pseudo-ISO-formatted date (e. g. 2017-02-15T174800) string representation for the specified
     * Date.
     *
     * @param date the date to be formatted.
     * @return the formatted string.
     * @throws NullPointerException if {@code date} is {@code null}.
     */
    public String getLongIsoString(@Nonnull final Date date) {
        checkDate(date);
        return isoDateFormatLong.format(date);
    }

}
