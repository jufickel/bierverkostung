/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * This class allows to implement only a subset of methods of the TextWatcher interface.
 *
 * @since 1.0.0
 */
public abstract class AbstractTextWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
        // Nothing to do here.
    }

    @Override
    public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        // Nothing to do here.
    }

    @Override
    public void afterTextChanged(final Editable s) {
        // Nothing to do here.
    }

}
