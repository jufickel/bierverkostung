/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

/**
 * Abstract base implementation for all activities but the main activity. This class initialises the toolbar.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // If the following code is placed in onCreate the ActionBar remains completely empty.
        final Toolbar toolbar = findView(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // enables proper up navigation
        if (android.R.id.home == item.getItemId()) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        onFinish();
    }

    /**
     * This hook is called when the activity is done and should be closed.
     * <p>
     *   Derived classes should call through to the base class for it to perform the default finish handling.
     * </p>
     */
    protected void onFinish() {
        super.finish();
    }

    /**
     * Returns the view that was identified by the ID attribute from the XML that was processed in
     * {@link #onCreate(Bundle)}.
     *
     * @param viewId the identifier of the View to be found.
     * @param <T> the expected type of the returned view.
     * @return the view or {@code null}.
     */
    @SuppressWarnings("unchecked")
    protected final <T extends View> T findView(final int viewId) {
        return (T) findViewById(viewId);
    }

}
