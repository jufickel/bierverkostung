/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.app.Notification;

import de.retujo.bierverkostung.data.DataEntity;

/**
 * This type represents the means to update a particular {@link Notification} with data supplied by
 * {@link ProgressUpdate}.
 *
 * @param <T> the type of the data entity which is subject of the notification.
 *
 * @since 1.2.0
 */
public interface Notificator<T extends DataEntity> {

    /**
     * Updates the associated Notification using the specified ProgressUpdate.
     *
     * @param progressUpdate the progress update supplies data for updating the Notification.
     * @throws NullPointerException if progressUpdate is {@code null}.
     */
    void update(ProgressUpdate<T> progressUpdate);

}
