/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import de.retujo.java.util.Maybe;

/**
 * Transports progress information.
 *
 * @param <T> the type of the entity of this progress update.
 *
 * 1.2.0
 */
public interface ProgressUpdate<T> {

    /**
     * Returns the entity of this progress update.
     *
     * @return the entity or an empty Maybe.
     */
    Maybe<T> getEntity();

    /**
     * Returns the current progress.
     *
     * @return the current progress.
     */
    int getCurrentProgress();

    /**
     * Returns the maximum progress.
     *
     * @return the maximum progress.
     */
    int getMaxProgress();

}
