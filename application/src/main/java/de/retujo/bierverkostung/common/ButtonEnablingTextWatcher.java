/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.text.Editable;
import android.text.TextUtils;
import android.widget.Button;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This TextWatcher enables a particular as soon as the entered text is not empty.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class ButtonEnablingTextWatcher extends AbstractTextWatcher {

    private final Button button;

    /**
     * Constructs a new {@code ButtonEnablingTextWatcher} object.
     *
     * @param theButton the Button to be enabled or disabled.
     * @throws NullPointerException if {@code theButton} is {@code null}.
     */
    public ButtonEnablingTextWatcher(@Nonnull final Button theButton) {
        button = isNotNull(theButton, "button");
    }

    @Override
    public void afterTextChanged(final Editable s) {
        if (!TextUtils.isEmpty(s)) {
            enableButton();
        } else {
            disableButton();
        }
    }

    private void enableButton() {
        button.setEnabled(true);
    }

    private void disableButton() {
        button.setEnabled(false);
    }

}
