/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import java.text.MessageFormat;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * A factory for various kinds of {@link ProgressUpdate}.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
public final class ProgressUpdateFactory {

    private ProgressUpdateFactory() {
        throw new AssertionError();
    }

    /**
     * Returns an update for a determinate progress. This means that the maximum progress is known and greater than
     * or equal to one and the current progress.
     *
     * @param currentProgress the current progress of the returned ProgressUpdate.
     * @param maxProgress the maximum progress of the returned ProgressUpdate.
     * @param entity the entity of the returned ProgressUpdate.
     * @param <T> the type of the entity the returned ProgressUpdate provides.
     * @return the instance.
     * @throws IllegalArgumentException if
     * <ul>
     *     <li>{@code currentProgress} is less than zero,</li>
     *     <li>{@code maxProgress} is less than one or</li>
     *     <li>{@code currentProgress} is greater than {@code maxProgress}.</li>
     * </ul>
     * @throws NullPointerException if {@code entity} is {@code null}.
     */
    public static <T extends DataEntity> ProgressUpdate<T> determinate(final int currentProgress,
            final int maxProgress, @Nullable final T entity) {

        if (0 > currentProgress) {
            final String msgPattern = "The current progress must not be lower than zero! Given was <{0}>.";
            throw new IllegalArgumentException(MessageFormat.format(msgPattern, currentProgress));
        }
        if (1 > maxProgress) {
            final String msgPattern = "The maximum progress must not be lower than one! Given was <{0}>.";
            throw new IllegalArgumentException(MessageFormat.format(msgPattern, currentProgress));
        }
        if (currentProgress > maxProgress) {
            final String msgPattern = "The current progress ({0}) must not be greater than the maximum progress ({1})!";
            throw new IllegalArgumentException(MessageFormat.format(msgPattern, currentProgress, maxProgress));
        }

        return new DefaultProgressUpdate<>(currentProgress, maxProgress, entity);
    }

    /**
     * Returns an update for an indeterminate progress. This means that the maximum progress is unknown.
     *
     * @param currentProgress the current progress of the returned ProgressUpdate.
     * @param maxProgress the maximum progress of the returned ProgressUpdate.
     * @param entity the entity of the returned ProgressUpdate.
     * @param <T> the type of the entity of the returned ProgressUpdate.
     * @return the instance.
     * @throws IllegalArgumentException if {@code currentProgress} is less than zero.
     */
    public static <T extends DataEntity> ProgressUpdate<T> indeterminate(final int currentProgress,
            final int maxProgress, @Nullable final T entity) {

        if (0 > currentProgress) {
            final String msgPattern = "The current progress must not be lower than zero! Given was <{0}>.";
            throw new IllegalArgumentException(MessageFormat.format(msgPattern, currentProgress));
        }

        return new DefaultProgressUpdate<>(currentProgress, maxProgress, entity);
    }

    private static final class DefaultProgressUpdate<T extends DataEntity> implements ProgressUpdate<T> {

        private final int currentProgress;
        private final int maxProgress;
        @Nullable private final T entity;

        private DefaultProgressUpdate(final int currentProgress, final int maxProgress, @Nullable final T entity) {
            this.entity = entity;
            this.currentProgress = currentProgress;
            this.maxProgress = maxProgress;
        }

        @Override
        public Maybe<T> getEntity() {
            return Maybe.ofNullable(entity);
        }

        @Override
        public int getCurrentProgress() {
            return currentProgress;
        }

        @Override
        public int getMaxProgress() {
            return maxProgress;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final DefaultProgressUpdate<?> that = (DefaultProgressUpdate<?>) o;

            if (currentProgress != that.currentProgress) {
                return false;
            }
            if (maxProgress != that.maxProgress) {
                return false;
            }
            return entity != null ? entity.equals(that.entity) : that.entity == null;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = entity != null ? entity.hashCode() : 0;
            result = prime * result + currentProgress;
            result = prime * result + maxProgress;
            return result;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {" +
                    "entity=" + entity +
                    ", currentProgress=" + currentProgress +
                    ", maxProgress=" + maxProgress +
                    "}";
        }

    }

}
