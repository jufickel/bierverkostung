/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.ContentResolver;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This abstract implementation of a {@link android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback} reacts
 * on swiping an item out of a RecyclerView both left and right. The attached action is to delete the swiped list
 * item Before it will be deleted the user has to verify deletion; for this reason an AlertDialog is shown.
 *
 * @param <T> type of the domain object to be deleted.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
public abstract class AbstractItemSwipeHandler<T extends DataEntity> extends ItemTouchHelper.SimpleCallback {

    private final Context context;
    private final LoaderManager loaderManager;
    private final AbstractLoaderCallbacks loaderCallbacks;

    /**
     * Constructs a new {@code AbstractItemSwipeHandler} object.
     *
     * @param fragmentActivity provides the current context as well as the support LoaderManager.
     * @param loaderCallbacks the loader callbacks of the loader which gets restarted if deletion is aborted to
     * restore the URI.
     * @throws NullPointerException if any argument is {@code null}.
     */
    protected AbstractItemSwipeHandler(final FragmentActivity fragmentActivity,
            final AbstractLoaderCallbacks loaderCallbacks) {

        this(fragmentActivity, fragmentActivity.getSupportLoaderManager(), loaderCallbacks);
    }

    /**
     * Constructs a new {@code AbstractItemSwipeHandler} object.
     *
     * @param context the context to be used for creating Dialogs and for providing the ContentResolver for deleting
     * entities from the database.
     * @param loaderCallbacks the loader callbacks of the loader which gets restarted if deletion is aborted to
     * restore the URI.
     * @throws NullPointerException if any argument is {@code null}.
     */
    protected AbstractItemSwipeHandler(final Context context, final LoaderManager loaderManager,
            final AbstractLoaderCallbacks loaderCallbacks) {

        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        isNotNull(context, "context");
        isNotNull(loaderManager, "LoaderManager");
        this.context = context;
        this.loaderManager = loaderManager;
        this.loaderCallbacks = isNotNull(loaderCallbacks, "Loader Callbacks");
    }

    @Override
    public boolean onMove(final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder,
            final RecyclerView.ViewHolder target) {

        return false;
    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction) {
        @SuppressWarnings("unchecked")
        final AbstractViewHolder<T> domainObjectViewHolder = (AbstractViewHolder<T>) viewHolder;
        final Maybe<T> domainObjectMaybe = domainObjectViewHolder.getDomainObject();

        if (domainObjectMaybe.isPresent()) {
            showVerifyDeleteDialog(domainObjectMaybe.get());
        }
    }

    private void showVerifyDeleteDialog(final T domainObject) {
        final DeleteEntityDialogue deleteEntityDialogue = DeleteEntityDialogue.getBuilder(context, domainObject)
                .setTitle(getDialogTitle())
                .setMessage(getDialogMessage(domainObject))
                .setOnKeepEntityListener(entity -> {
                    onKeepEntity(entity);
                    restartCursorLoader();
                })
                .setOnDeleteEntityListener(entity -> DeleteDbEntityTask.getInstance(getContentResolver())
                        .setOnDeletedListener(deletedEntities -> {
                            if (1 == deletedEntities.size()) {
                                onEntityDeleted(domainObject);
                            }
                        })
                        .setOnFailedToDeleteListener(notDeletedEntities -> {
                            onDeleteEntityFailed(entity);
                            restartCursorLoader();
                        })
                        .execute(entity)
                )
                .build();

        deleteEntityDialogue.show();
    }

    protected void onKeepEntity(final T entity) {
        // Nothing to do.
    }

    protected void onDeleteEntityFailed(final T entity) {
        // Nothing to do.
    }

    /**
     * This method gets called when the specified entity was successfully deleted.
     *
     * @param entity the deleted entity.
     * @since 1.2.0
     */
    protected void onEntityDeleted(final T entity) {
        // Nothing to do.
    }

    /**
     * Returns the ContentResolver.
     *
     * @return the ContentResolver.
     * @since 1.2.0
     */
    protected ContentResolver getContentResolver() {
        return context.getContentResolver();
    }

    private void restartCursorLoader() {
        loaderManager.restartLoader(loaderCallbacks.getId(), null, loaderCallbacks);
    }

    /**
     * Returns the title of the dialog for verifying deletion of the domain object.
     *
     * @return the ID of the title string.
     */
    protected abstract int getDialogTitle();

    /**
     * Returns the message of the dialog for verifying deletion of the domain object.
     *
     * @param domainObject the domain object to be deleted.
     * @return the message.
     */
    protected abstract String getDialogMessage(T domainObject);

}
