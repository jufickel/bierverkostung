/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Abstract base class for all implementations of Cursor-based {@link LoaderManager.LoaderCallbacks}.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public abstract class AbstractLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

    private final int id;
    private final Context context;
    private final AbstractCursorAdapter<?> cursorAdapter;

    /**
     * Constructs a new {@code AbstractLoaderCallbacks} object.
     *
     * @param id the ID of this callbacks.
     * @param context the current context.
     * @param cursorAdapter the adapter for binding the countries to the UI.
     * @throws NullPointerException if any argument is {@code null}.
     */
    protected AbstractLoaderCallbacks(final int id, @Nonnull final Context context,
            @Nonnull final AbstractCursorAdapter<?> cursorAdapter) {
        this.id = id;
        this.context = isNotNull(context, "context");
        this.cursorAdapter = isNotNull(cursorAdapter, "CursorAdapter");
    }

    @Override
    public final Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        if (this.id == id) {
            return doCreateLoader(context);
        }
        return null;
    }

    /**
     * Creates and returns a new Cursor Loader.
     *
     * @param context the current context.
     * @return the loader.
     */
    protected abstract Loader<Cursor> doCreateLoader(@Nonnull Context context);

    @Override
    public final void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        cursorAdapter.swapCursor(data);
    }

    @Override
    public final void onLoaderReset(final Loader<Cursor> loader) {
        cursorAdapter.swapCursor(null);
    }

    /**
     * Returns the ID of this callbacks.
     *
     * @return the ID.
     */
    public final int getId() {
        return id;
    }

}
