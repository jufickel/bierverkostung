/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Common base implementation of all {@link RecyclerView.ViewHolder}s which manage a domain object.
 *
 * @since 1.0.0
 *
 * @param <T> the type of the ViewHolder's domain object.
 */
@NotThreadSafe
public abstract class AbstractViewHolder<T extends DataEntity> extends RecyclerView.ViewHolder {

    private final Context context;
    private T domainObject;

    /**
     * Constructs a new {@code AbstractViewHolder} object.
     *
     * @param itemView the View which got inflated before this ViewHolder is created.
     * @throws NullPointerException if {@code itemView} is {@code null}.
     */
    protected AbstractViewHolder(@Nonnull final View itemView) {
        super(isNotNull(itemView, "item View"));
        context = itemView.getContext();
    }

    /**
     * Sets the given domain object to this ViewHolder.
     *
     * @param domainObject the domain object to be set.
     * @throws NullPointerException if {@code domainObject} is {@code null}.
     */
    public final void setDomainObject(@Nonnull final T domainObject) {
        this.domainObject = isNotNull(domainObject, "domain object");
        itemView.setTag(domainObject);
        handleDomainObjectSet(domainObject);
    }

    /**
     * This method gets called each time after a domain object was set to this ViewHolder and after it was set as tag
     * to this ViewHolder's {@code itemView}. It is up to the sub-class to implement this method appropriately.
     *
     * @param domainObject the set domain object.
     */
    protected abstract void handleDomainObjectSet(@Nonnull T domainObject);

    /**
     * Returns the context.
     *
     * @return the context.
     */
    @Nonnull
    protected final Context getContext() {
        return context;
    }

    /**
     * Returns the domain object of this ViewHolder.
     *
     * @return the domain object or an empty Maybe.
     */
    @Nonnull
    public final Maybe<T> getDomainObject() {
        return Maybe.ofNullable(domainObject);
    }

}
