/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcelable;

import javax.annotation.Nonnull;

import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.Maybe;

/**
 * This interface represents a particular component of the complex scent of a beer.
 *
 * @since 1.0.0
 */
interface TastingComponent extends Parcelable, Jsonable {

    /**
     * Returns the description of this scent component.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getDescription();

    /**
     * Returns the rating of this scent component.
     *
     * @return the rating.
     */
    @Nonnull
    Maybe<Integer> getRating();

    /**
     * Indicates whether this tasting component does neither contain a description nor a rating.
     *
     * @return {@code true} if this tasting component is empty, {@code false} else.
     */
    boolean isEmpty();

}
