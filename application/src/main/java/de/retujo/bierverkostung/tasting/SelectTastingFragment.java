/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.annotation.concurrent.NotThreadSafe;
import javax.annotation.concurrent.ThreadSafe;

import de.retujo.bierverkostung.MainActivity;
import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractItemSwipeHandler;
import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.common.HideFabOnScrollListener;
import de.retujo.bierverkostung.exchange.FileChooserDialogue;
import de.retujo.java.util.AllNonnull;

/**
 * This Fragment shows a list of all persisted tastings. The user has the possibility to add a new tasting and to delete
 * tastings by swiping.
 * <p>
 * A short click on a tasting item shows the details of the selected tasting in a new Activity.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class SelectTastingFragment extends Fragment {

    private static final int NEW_TASTING_REQUEST_CODE = 132;

    private RecyclerView tastingsRecyclerView;
    private FloatingActionButton addTastingButton;

    /**
     * Constructs a new {@code SelectTastingFragment} object.
     */
    public SelectTastingFragment() {
        tastingsRecyclerView = null;
        addTastingButton = null;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        final View result = inflater.inflate(R.layout.fragment_select_tasting, null);

        initAddTastingButton(result);
        final View.OnClickListener onTastingSelectedListener = new OnTastingSelectedListener();
        final TastingCursorAdapter cursorAdapter = new TastingCursorAdapter(getContext(), onTastingSelectedListener);
        final AbstractLoaderCallbacks cursorLoaderCallbacks = initTastingsLoader(cursorAdapter);
        initTastingsRecyclerView(result, cursorAdapter, cursorLoaderCallbacks);

        return result;
    }

    private void initAddTastingButton(final View parentView) {
        addTastingButton = (FloatingActionButton) parentView.findViewById(R.id.select_tasting_fab_new_tasting);
        addTastingButton.setOnClickListener(v -> {
            final Intent addTastingIntent = new Intent(getContext(), EditTastingActivity.class);
            startActivityForResult(addTastingIntent, NEW_TASTING_REQUEST_CODE);
        });
    }

    private AbstractLoaderCallbacks initTastingsLoader(final TastingCursorAdapter cursorAdapter) {
        final TastingLoaderCallbacks result = new TastingLoaderCallbacks(getContext(), cursorAdapter, getSortOrder());
        final LoaderManager supportLoaderManager = getLoaderManager();
        supportLoaderManager.initLoader(result.getId(), null, result);
        return result;
    }

    private String getSortOrder() {
        final SortOrderPreferences sortOrderPreferences = SortOrderPreferences.getInstance(getContext());
        return sortOrderPreferences.getPersistedSortOrderString();
    }

    private void initTastingsRecyclerView(final View parentView, final TastingCursorAdapter cursorAdapter,
            final AbstractLoaderCallbacks cursorLoaderCallbacks) {

        tastingsRecyclerView = (RecyclerView) parentView.findViewById(R.id.select_tasting_recycler_view);
        tastingsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        tastingsRecyclerView.setAdapter(cursorAdapter);
        tastingsRecyclerView.addOnScrollListener(HideFabOnScrollListener.of(addTastingButton));

        final SimpleCallback deleteTastingOnSwipeHandler = new TastingItemSwipeHandler(cursorLoaderCallbacks);
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(deleteTastingOnSwipeHandler);
        itemTouchHelper.attachToRecyclerView(tastingsRecyclerView);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.overview_options_menu, menu);
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            menu.removeItem(R.id.select_tasting_options_menu_import);
        }
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {
        super.onPrepareOptionsMenu(menu);
        final MenuItem exportMenuItem = menu.findItem(R.id.select_tasting_options_menu_export);
        exportMenuItem.setVisible(0 != tastingsRecyclerView.getAdapter().getItemCount());
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final Context context = getContext();
        if (R.id.select_tasting_options_menu_sort == item.getItemId()) {
            final DialogFragment dialogFragment = SortOptionsDialogFragment.getBuilder(getContext())
                    .withOnApplyListener(sortOrder -> {
                        final TastingCursorAdapter adapter = (TastingCursorAdapter) tastingsRecyclerView.getAdapter();
                        final TastingLoaderCallbacks lc = new TastingLoaderCallbacks(getContext(), adapter, sortOrder);
                        final LoaderManager supportLoaderManager = getLoaderManager();
                        supportLoaderManager.restartLoader(lc.getId(), null, lc);
                    })
                    .build();
            dialogFragment.show(getFragmentManager(), "SortOptions");
            return true;
        } else if (R.id.select_tasting_options_menu_export == item.getItemId()) {
            FileChooserDialogue.forDirectory(getContext(), Environment.getExternalStorageDirectory())
                    .setOnFileSelectedListener(selectedDirectory -> {
                        final ExportTastingNotificator exportNotificator = ExportTastingNotificator.getInstance(context,
                                new Intent(context, MainActivity.class));
                        final TastingExporter tastingExporter = TastingExporter.newInstance(context, selectedDirectory,
                                exportNotificator::update);
                        tastingExporter.execute();
                    })
                    .show();
            return true;
        } else if (R.id.select_tasting_options_menu_import == item.getItemId()) {
            FileChooserDialogue.forFile(context, Environment.getExternalStorageDirectory(), "zip")
                    .setOnFileSelectedListener(selectedFile -> {
                        final ImportTastingNotificator importNotificator =
                                ImportTastingNotificator.getInstance(context, new Intent(context, MainActivity.class));
                        final TastingZipFileReader zipFileReader =
                                TastingZipFileReader.getInstance(context.getContentResolver(),
                                        importNotificator::update);
                        zipFileReader.execute(selectedFile);
                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Without the following code the item slot would be empty after starting edit activity but not saving.
        final RecyclerView.Adapter adapter = tastingsRecyclerView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    /**
     * This listener reacts when a tasting was selected. It then starts an activity which shows the details of the
     * selected tasting.
     *
     * @since 1.0.0
     */
    @ThreadSafe
    private final class OnTastingSelectedListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final Tasting tasting =  (Tasting) v.getTag();
            final Intent showTastingIntent = new Intent(getContext(), ShowTastingActivity.class);
            showTastingIntent.putExtra(ShowTastingActivity.TASTING, tasting);
            startActivity(showTastingIntent);
        }
    }

    /**
     * This class deletes a tasting when the user swipes it left or right. Beforehand the deletion has to be verified by
     * the user; this is done with an AlertDialog.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @NotThreadSafe
    private final class TastingItemSwipeHandler extends AbstractItemSwipeHandler<Tasting> {
        /**
         * Constructs a new {@code TastingItemSwipeHandler} object.
         *
         * @param theLoaderCallbacks are called when a tasting gets deleted.
         * @throws NullPointerException if {@code theLoaderCallbacks} is {@code null}.
         */
        private TastingItemSwipeHandler(final AbstractLoaderCallbacks theLoaderCallbacks) {
            super(getContext(), getLoaderManager(), theLoaderCallbacks);
        }

        @Override
        protected int getDialogTitle() {
            return R.string.delete_tasting_dialog_title;
        }

        @Override
        protected String getDialogMessage(final Tasting tasting) {
            return getString(R.string.delete_tasting_dialog_message);
        }
    }

}
