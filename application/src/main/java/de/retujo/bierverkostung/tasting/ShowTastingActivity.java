/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.beer.BeerDetailsFragment;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.brewery.BreweryDetailsFragment;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.DateUtil;
import de.retujo.bierverkostung.common.DeleteEntityDialogue;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;
import de.retujo.bierverkostung.exchange.FileChooserDialogue;
import de.retujo.java.util.Maybe;

/**
 * This activity shows all available properties of a particular {@link Tasting}.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class ShowTastingActivity extends BaseActivity {

    /**
     * Key for the Tasting of which the details are shown by this Activity.
     */
    public static final String TASTING = "tasting";

    private static final int EDIT_TASTING_REQUEST_CODE = 128;

    private Tasting shownTasting;

    /**
     * Constructs a new {@code ShowTastingActivity} object.
     */
    public ShowTastingActivity() {
        shownTasting = null;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tasting);

        final Intent afferentIntent = getIntent();
        shownTasting = afferentIntent.getParcelableExtra(TASTING);

        final List<Runnable> initializers = new ArrayList<>();
        initializers.add(new FirstCardInitializer(shownTasting));
        initializers.add(new FoodRecommendationInitializer(shownTasting));
        initializers.add(new OpticalAppearanceInitializer(shownTasting));
        initializers.add(new ScentInitializer(shownTasting));
        initializers.add(new TasteInitializer(shownTasting));
        for (final Runnable initializer : initializers) {
            initializer.run();
        }
    }

    @SuppressWarnings("unchecked")
    private <T extends View> T getViewAndMakeVisible(final int textViewId) {
        final View result = findViewById(textViewId);
        result.setVisibility(View.VISIBLE);
        return (T) result;
    }

    private void initComponentView(final TastingComponent component, final int viewResourceId) {
        if (!component.isEmpty()) {
            final TastingComponentView componentView = getViewAndMakeVisible(viewResourceId);
            final Maybe<String> description = component.getDescription();
            if (description.isPresent()) {
                componentView.setDescriptionText(description.get());
            }
            final Maybe<Integer> rating = component.getRating();
            if (rating.isPresent()) {
                componentView.setRatingValue(rating.get());
            }
        }
    }

    @Override
    public void onAttachFragment(final android.support.v4.app.Fragment fragment) {
        super.onAttachFragment(fragment);

        final Intent afferentIntent = getIntent();
        final Tasting tasting = afferentIntent.getParcelableExtra(TASTING);
        final Beer beer = tasting.getBeer();

        if (R.id.show_tasting_beer_details_fragment == fragment.getId()) {
            final Bundle args = fragment.getArguments();
            args.putParcelable(BeerDetailsFragment.ARG_BEER, beer);
        } else if (R.id.show_tasting_brewery_details_fragment == fragment.getId()) {
            final Bundle args = fragment.getArguments();
            final Maybe<Brewery> brewery = beer.getBrewery();
            if (brewery.isPresent()) {
                args.putParcelable(BreweryDetailsFragment.ARG_BREWERY, brewery.get());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.entity_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int itemId = item.getItemId();
        if (R.id.options_menu_edit == itemId) {
            final Intent editTastingIntent = new Intent(this, EditTastingActivity.class);
            editTastingIntent.putExtra(EditTastingActivity.EDIT_TASTING, shownTasting);
            startActivityForResult(editTastingIntent, EDIT_TASTING_REQUEST_CODE);
            return true;
        } else if (R.id.options_menu_delete == itemId) {
            final DeleteEntityDialogue dialogue = DeleteEntityDialogue.getBuilder(this, shownTasting)
                    .setTitle(R.string.delete_tasting_dialog_title)
                    .setMessage(getString(R.string.delete_tasting_dialog_message))
                    .setOnDeleteEntityListener(t -> DeleteDbEntityTask.getInstance(getContentResolver())
                            .setOnDeletedListener(dt -> ShowTastingActivity.this.finish())
                            .execute(t))
                    .build();
            dialogue.show();
            return true;
        } else if (R.id.options_menu_export == itemId) {
            FileChooserDialogue.forDirectory(this, Environment.getExternalStorageDirectory())
                    .setOnFileSelectedListener(selectedDirectory -> {
                        final Intent intent = new Intent(this, getClass());
                        intent.putExtra(TASTING, shownTasting);
                        final ExportTastingNotificator exportNotificator =
                                ExportTastingNotificator.getInstance(this, intent);
                        final TastingExporter tastingExporter = TastingExporter.newInstance(this, selectedDirectory,
                                exportNotificator::update);
                        tastingExporter.execute(shownTasting);
                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (Activity.RESULT_OK == resultCode && EDIT_TASTING_REQUEST_CODE == requestCode) {
            final Tasting editedTasting = data.getParcelableExtra(EditTastingActivity.EDITED_TASTING);
            final Intent showEditedTastingIntent = new Intent(this, getClass());
            showEditedTastingIntent.putExtra(TASTING, editedTasting);
            startActivity(showEditedTastingIntent);
            finish();
        }
    }

    private final class FirstCardInitializer implements Runnable {
        private final Tasting tasting;

        private FirstCardInitializer(final Tasting tasting) {
            this.tasting = tasting;
        }

        @Override
        public void run() {
            initDate();
            initLocation();
            initTotalImpressionRating();
            initTotalImpression();
        }

        private void initDate() {
            final String rawDate = tasting.getDate();
            final DateUtil dateUtil = DateUtil.getInstance(ShowTastingActivity.this);
            final Maybe<String> formattedDate = dateUtil.getLongString(rawDate);
            final TextView dateTextView = (TextView) findViewById(R.id.show_tasting_date);
            dateTextView.setText(formattedDate.orElse(rawDate));
        }

        private void initLocation() {
            final Maybe<String> location = tasting.getLocation();
            if (location.isPresent()) {
                final TextView locationTextView = getViewAndMakeVisible(R.id.show_tasting_location);
                locationTextView.setText(location.get());
            }
        }

        private void initTotalImpressionRating() {
            final RatingBar totalImpressionRatingBar = getViewAndMakeVisible(R.id.show_tasting_total_impression_rating);
            totalImpressionRatingBar.setRating(tasting.getTotalImpressionRating());
        }

        private void initTotalImpression() {
            final Maybe<String> totalImpressionDescription = tasting.getTotalImpressionDescription();
            if (totalImpressionDescription.isPresent()) {
                final TextView totalImpression = getViewAndMakeVisible(R.id.show_tasting_total_impression);
                totalImpression.setText(totalImpressionDescription.get());
            }
        }
    }

    private final class FoodRecommendationInitializer implements Runnable {
        private final Tasting tasting;

        private FoodRecommendationInitializer(final Tasting tasting) {
            this.tasting = tasting;
        }

        @Override
        public void run() {
            final Maybe<String> foodRecommendation = tasting.getFoodRecommendation();
            if (foodRecommendation.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_food_recommendation_card_view);
                final TextView foodRecommendationTextView = getViewAndMakeVisible(R.id.show_tasting_food_recommendation);
                foodRecommendationTextView.setText(foodRecommendation.get());
            }
        }
    }

    private final class OpticalAppearanceInitializer implements Runnable {
        private final OpticalAppearance opticalAppearance;

        private OpticalAppearanceInitializer(final Tasting tasting) {
            opticalAppearance = tasting.getOpticalAppearance();
        }

        @Override
        public void run() {
            if (!opticalAppearance.isEmpty()) {
                getViewAndMakeVisible(R.id.show_tasting_optical_appearance_card_view);
                initColour();
                initEbc();
                initColourDescription();
                initClarity();
                initFoamColour();
                initFoamStructure();
                initFoamStability();
            }
        }

        private void initColour() {
            final Maybe<String> beerColour = opticalAppearance.getBeerColour();
            if (beerColour.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_colour_label);
                final TextView colourTextView = getViewAndMakeVisible(R.id.show_tasting_colour);
                colourTextView.setText(beerColour.get());
            }
        }

        private void initEbc() {
            final Maybe<Integer> ebc = opticalAppearance.getEbc();
            if (ebc.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_ebc_label);
                final TextView ebcTextView = getViewAndMakeVisible(R.id.show_tasting_ebc);
                final Integer ebcAsInt = ebc.get();
                ebcTextView.setBackgroundColor(EbcColourResolver.getInstance().apply(ebcAsInt));
                if (EbcColourResolver.DARK_EBC_VALUE_THRESHOLD < ebcAsInt) {
                    ebcTextView.setTextColor(Color.WHITE);
                }
                ebcTextView.setText(String.valueOf(ebcAsInt));
            }
        }

        private void initColourDescription() {
            final Maybe<String> beerColourDescription = opticalAppearance.getBeerColourDescription();
            if (beerColourDescription.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_colour_description_label);
                final TextView colourDescriptionTextView = getViewAndMakeVisible(R.id.show_tasting_colour_description);
                colourDescriptionTextView.setText(beerColourDescription.get());
            }
        }

        private void initClarity() {
            final Maybe<String> beerClarityDescription = opticalAppearance.getBeerClarityDescription();
            if (beerClarityDescription.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_clarity_label);
                final TextView clarityTextView = getViewAndMakeVisible(R.id.show_tasting_clarity);
                clarityTextView.setText(beerClarityDescription.get());
            }
        }

        private void initFoamColour() {
            final Maybe<String> foamColour = opticalAppearance.getFoamColour();
            if (foamColour.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_foam_colour_label);
                final TextView foamColourTextView = getViewAndMakeVisible(R.id.show_tasting_foam_colour);
                foamColourTextView.setText(foamColour.get());
            }
        }

        private void initFoamStructure() {
            final Maybe<String> foamStructureDescription = opticalAppearance.getFoamStructureDescription();
            if (foamStructureDescription.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_foam_structure_label);
                final TextView foamStructureTextView = getViewAndMakeVisible(R.id.show_tasting_foam_structure);
                foamStructureTextView.setText(foamStructureDescription.get());
            }
        }

        private void initFoamStability() {
            final Maybe<Integer> foamStability = opticalAppearance.getFoamStability();
            if (foamStability.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_foam_stability_label);
                final ProgressBar foamStabilityProgressBar = getViewAndMakeVisible(R.id.show_tasting_foam_stability);
                foamStabilityProgressBar.setProgress(foamStability.get());
            }
        }
    }

    private final class ScentInitializer implements Runnable {
        private final Scent scent;

        private ScentInitializer(final Tasting tasting) {
            scent = tasting.getScent();
        }

        @Override
        public void run() {
            if (!scent.isEmpty()) {
                getViewAndMakeVisible(R.id.show_tasting_scent_card_view);
                initComponentView(scent.getFruitComponent(), R.id.show_tasting_fruit_component_view);
                initComponentView(scent.getFlowerComponent(), R.id.show_tasting_flower_component_view);
                initComponentView(scent.getVegetalComponent(), R.id.show_tasting_vegetal_component_view);
                initComponentView(scent.getSpicyComponent(), R.id.show_tasting_spicy_component_view);
                initComponentView(scent.getWarmthMintedComponent(), R.id.show_tasting_warmth_minted_component_view);
                initComponentView(scent.getBiologicalComponent(), R.id.show_tasting_biological_component_view);
            }
        }
    }

    private final class TasteInitializer implements Runnable {
        private final Taste taste;

        private TasteInitializer(final Tasting tasting) {
            taste = tasting.getTaste();
        }

        @Override
        public void run() {
            if (!taste.isEmpty()) {
                getViewAndMakeVisible(R.id.show_tasting_taste_card_view);
                initBitternessRating();
                initSweetnessRating();
                initAcidityRating();
                initMouthFeelDescription();
                initFullBodiedRating();
                initBodyDescription();
                initAftertaste();
            }
        }

        private void initBitternessRating() {
            final Maybe<Integer> bitternessRating = taste.getBitternessRating();
            if (bitternessRating.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_bitterness_label);
                final ProgressBar bitternessRatingProgressBar = getViewAndMakeVisible(R.id.show_tasting_bitterness);
                bitternessRatingProgressBar.setProgress(bitternessRating.get());
            }
        }

        private void initSweetnessRating() {
            final Maybe<Integer> sweetnessRating = taste.getSweetnessRating();
            if (sweetnessRating.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_sweetness_label);
                final ProgressBar sweetnessRatingProgressBar = getViewAndMakeVisible(R.id.show_tasting_sweetness);
                sweetnessRatingProgressBar.setProgress(sweetnessRating.get());
            }
        }

        private void initAcidityRating() {
            final Maybe<Integer> acidityRating = taste.getAcidityRating();
            if (acidityRating.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_acidity_label);
                final ProgressBar acidityRatingProgressBar = getViewAndMakeVisible(R.id.show_tasting_acidity);
                acidityRatingProgressBar.setProgress(acidityRating.get());
            }
        }

        private void initMouthFeelDescription() {
            final Maybe<String> mouthfeelDescription = taste.getMouthfeelDescription();
            if (mouthfeelDescription.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_mouthfeel_label);
                final TextView mouthfeelDescriptionTextView = getViewAndMakeVisible(R.id.show_tasting_mouthfeel);
                mouthfeelDescriptionTextView.setText(mouthfeelDescription.get());
            }
        }

        private void initFullBodiedRating() {
            final Maybe<Integer> fullBodiedRating = taste.getFullBodiedRating();
            if (fullBodiedRating.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_body_fullness_label);
                final ProgressBar bodyFullnessProgressBar = getViewAndMakeVisible(R.id.show_tasting_body_fullness);
                bodyFullnessProgressBar.setProgress(fullBodiedRating.get());
            }
        }

        private void initBodyDescription() {
            final Maybe<String> bodyDescription = taste.getBodyDescription();
            if (bodyDescription.isPresent()) {
                getViewAndMakeVisible(R.id.show_tasting_body_description_label);
                final TextView bodyDescriptionTextView = getViewAndMakeVisible(R.id.show_tasting_body_description);
                bodyDescriptionTextView.setText(bodyDescription.get());
            }
        }

        private void initAftertaste() {
            initComponentView(taste.getAftertaste(), R.id.show_tasting_aftertaste_component_view);
        }
    }

}
