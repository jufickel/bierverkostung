/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.database.Cursor;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.beer.BeerFactory;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.Column;
import de.retujo.bierverkostung.data.CursorReader;
import de.retujo.bierverkostung.data.Revision;
import de.retujo.java.util.AllNonnull;

/**
 * A factory for immutable instances of {@link Tasting}.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
final class TastingFactory {

    private TastingFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link Tasting} from the specified Cursor.
     *
     * @param cursor the cursor to provide the tasting data.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     */
    public static Tasting newTastingFromCursor(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final CursorReader cr = CursorReader.of(cursor);
        return TastingBuilder.newInstance(cr.getString(TastingEntry.COLUMN_DATE), BeerFactory.newBeerFromCursor(cursor))
                .id(UUID.fromString(cr.getString(TastingEntry.COLUMN_ID)))
                .revision(Revision.of(cr.getInt(TastingEntry.COLUMN_REVISION)))
                .location(cr.getString(TastingEntry.COLUMN_LOCATION))
                .opticalAppearance(OpticalAppearanceFactory.newOpticalAppearance(cursor))
                .scent(ScentFactory.newScent(cursor))
                .taste(TasteFactory.newTaste(cursor))
                .foodRecommendation(cr.getString(TastingEntry.COLUMN_FOOD_RECOMMENDATION))
                .totalImpressionDescription(cr.getString(TastingEntry.COLUMN_TOTAL_IMPRESSION_DESCRIPTION))
                .totalImpressionRating(cr.getInt(TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING))
                .build();
    }

    /**
     * Returns a new immutable instance of {@link Tasting} from the specified {@link JSONObject}.
     *
     * @param tastingJsonObject the JSON object to be converted to a Tasting.
     * @return the tasting.
     * @throws NullPointerException if {@code tastingJsonObject} is {@code null}.
     * @throws de.retujo.bierverkostung.exchange.JsonConversionException if {@code tastingJsonObject} cannot be
     * converted to a Tasting.
     * @since 1.2.0
     */
    public static Tasting newTasting(final JSONObject tastingJsonObject) {
        final TastingJsonConverter jsonConverter = TastingJsonConverter.getInstance();
        return jsonConverter.fromJson(tastingJsonObject);
    }

    /**
     * A factory for immutable instances of {@link OpticalAppearance}.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class OpticalAppearanceFactory {
        private OpticalAppearanceFactory() {
            throw new AssertionError();
        }

        /**
         * Returns a new immutable instance of {@link OpticalAppearance} from the specified Cursor.
         *
         * @param cursor the cursor to provide the optical appearance data.
         * @return the instance.
         * @throws NullPointerException if {@code cursor} is {@code null}.
         * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
         */
        public static OpticalAppearance newOpticalAppearance(final Cursor cursor) {
            final CursorReader cr = CursorReader.of(cursor);
            return OpticalAppearanceBuilder.getInstance()
                    .beerColour(cr.getString(TastingEntry.COLUMN_BEER_COLOUR))
                    .ebc(cr.getInt(TastingEntry.COLUMN_COLOUR_EBC))
                    .beerColourDescription(cr.getString(TastingEntry.COLUMN_BEER_COLOUR_DESCRIPTION))
                    .beerClarityDescription(cr.getString(TastingEntry.COLUMN_CLARITY_DESCRIPTION))
                    .foamColour(cr.getString(TastingEntry.COLUMN_FOAM_COLOUR))
                    .foamStructureDescription(cr.getString(TastingEntry.COLUMN_FOAM_STRUCTURE))
                    .foamStability(cr.getInt(TastingEntry.COLUMN_FOAM_STABILITY))
                    .build();
        }
    }

    /**
     * A factory for immutable instances of {@link Scent}.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class ScentFactory {
        private ScentFactory() {
            throw new AssertionError();
        }

        /**
         * Returns a new immutable instance of {@link Scent} from the specified Cursor.
         *
         * @param cursor the cursor to provide the scent data.
         * @return the instance.
         * @throws NullPointerException if {@code cursor} is {@code null}.
         * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
         */
        public static Scent newScent(final Cursor cursor) {
            final CursorReader cr = CursorReader.of(cursor);
            return ScentBuilder.getInstance()
                    .fruit(newTastingComponent(cr, TastingEntry.COLUMN_FRUIT_DESCRIPTION,
                            TastingEntry.COLUMN_FRUIT_RATING))
                    .flower(newTastingComponent(cr, TastingEntry.COLUMN_FLOWER_DESCRIPTION,
                            TastingEntry.COLUMN_FLOWER_RATING))
                    .vegetal(newTastingComponent(cr, TastingEntry.COLUMN_VEGETAL_DESCRIPTION,
                            TastingEntry.COLUMN_VEGETAL_RATING))
                    .spicy(newTastingComponent(cr, TastingEntry.COLUMN_SPICY_DESCRIPTION,
                            TastingEntry.COLUMN_SPICY_RATING))
                    .warmthMinted(newTastingComponent(cr, TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION,
                            TastingEntry.COLUMN_WARMTH_MINTED_RATING))
                    .biological(newTastingComponent(cr, TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION,
                            TastingEntry.COLUMN_BIOLOGICAL_RATING))
                    .build();
        }

        private static TastingComponent newTastingComponent(final CursorReader cursorReader,
                final Column descriptionColumn, final Column ratingColumn) {

            final String description = cursorReader.getString(descriptionColumn);
            final int rating = cursorReader.getInt(ratingColumn);
            return ImmutableTastingComponent.of(description, rating);
        }
    }

    /**
     * A factory for immutable instances of {@link Taste}.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class TasteFactory {
        private TasteFactory() {
            throw new AssertionError();
        }

        /**
         * Returns a new immutable instance of {@link Taste} from the specified Cursor.
         *
         * @param cursor the cursor to provide the optical appearance data.
         * @return the instance.
         * @throws NullPointerException if {@code cursor} is {@code null}.
         * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
         */
        public static Taste newTaste(final Cursor cursor) {
            final CursorReader cr = CursorReader.of(cursor);
            return TasteBuilder.getInstance()
                    .bitternessRating(cr.getInt(TastingEntry.COLUMN_BITTERNESS_RATING))
                    .sweetnessRating(cr.getInt(TastingEntry.COLUMN_SWEETNESS_RATING))
                    .acidityRating(cr.getInt(TastingEntry.COLUMN_ACIDITY_RATING))
                    .mouthfeelDescription(cr.getString(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION))
                    .fullBodiedRating(cr.getInt(TastingEntry.COLUMN_FULL_BODIED_RATING))
                    .bodyDescription(cr.getString(TastingEntry.COLUMN_BODY_DESCRIPTION))
                    .aftertaste(ImmutableTastingComponent.of(cr.getString(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION),
                            cr.getInt(TastingEntry.COLUMN_AFTERTASTE_RATING)))
                    .build();
        }
    }

}
