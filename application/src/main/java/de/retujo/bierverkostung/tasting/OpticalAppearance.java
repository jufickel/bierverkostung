/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcelable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.bierverkostung.data.ContentValuesRepresentable;
import de.retujo.java.util.Maybe;

/**
 * This type groups all properties of a Tasting which describe the optical appearance of a Beer.
 *
 * @since 1.0.0
 */
@Immutable
interface OpticalAppearance extends ContentValuesRepresentable, Parcelable, Jsonable {

    /**
     * Returns the colour of the beer.
     *
     * @return the beer colour or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getBeerColour();

    /**
     * Returns the EBC.
     *
     * @return the EBC or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getEbc();

    /**
     * Returns the description of the beer colour.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getBeerColourDescription();

    /**
     * Returns the description of the beer clarity.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getBeerClarityDescription();

    /**
     * Returns the colour of the foam.
     *
     * @return the foam colour or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getFoamColour();

    /**
     * Returns the description of the foam structure.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getFoamStructureDescription();

    /**
     * Returns the value of the foam stability.
     *
     * @return the value or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getFoamStability();

    /**
     * Indicates if no property of the optical appearance of a Beer was specified at all.
     *
     * @return {@code true} if no property was specified, {@code false} else.
     */
    boolean isEmpty();

}
