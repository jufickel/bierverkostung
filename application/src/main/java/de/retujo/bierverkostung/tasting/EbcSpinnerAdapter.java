/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Function;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This adapter wraps an {@link ArrayAdapter}. Its purpose is to set the background and text colours of the spinner
 * items based on the EBC at the current position.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class EbcSpinnerAdapter extends BaseAdapter {

    private final ArrayAdapter<CharSequence> arrayAdapter;
    private final Function<Number, Integer> ebcColourResolver;

    private EbcSpinnerAdapter(final ArrayAdapter<CharSequence> theArrayAdapter,
            final Function<Number, Integer> theEbcColourResolver) {

        arrayAdapter = isNotNull(theArrayAdapter, "array adapter");
        ebcColourResolver = theEbcColourResolver;
    }

    /**
     * Returns an instance of {@code EbcSpinnerAdapter}.
     *
     * @param arrayAdapter this adapter is used to delegate to.
     * @return the instance.
     */
    public static EbcSpinnerAdapter getInstance(final ArrayAdapter<CharSequence> arrayAdapter) {
        return new EbcSpinnerAdapter(arrayAdapter, EbcColourResolver.getInstance());
    }

    @Override
    public int getCount() {
        return arrayAdapter.getCount();
    }

    @Nullable
    @Override
    public CharSequence getItem(final int position) {
        return arrayAdapter.getItem(position);
    }

    @Override
    public long getItemId(final int position) {
        return arrayAdapter.getItemId(position);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View result = arrayAdapter.getView(position, convertView, parent);
        @Nullable final CharSequence ebcAsCharSequence = getItem(position);
        if (null != ebcAsCharSequence && 0 < ebcAsCharSequence.length()) {
            final double ebcAsDouble = Double.parseDouble(ebcAsCharSequence.toString());
            result.setBackgroundColor(ebcColourResolver.apply(ebcAsDouble));
            if (EbcColourResolver.DARK_EBC_VALUE_THRESHOLD <= ebcAsDouble) {
                ((TextView) result).setTextColor(Color.WHITE);
            }
        }
        return result;
    }

}
