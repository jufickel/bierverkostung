/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractCursorAdapter;
import de.retujo.java.util.AllNonnull;

/**
 * This CursorAdapter creates and binds {@link TastingViewHolder}s, that hold the data of a tasting, to a RecyclerView
 * to efficiently display the data.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
final class TastingCursorAdapter extends AbstractCursorAdapter<TastingViewHolder> {

    /**
     * Constructs a new {@code TastingCursorAdapter} object.
     *
     * @param context the current context.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    public TastingCursorAdapter(final Context context, @Nullable final View.OnClickListener onTastingSelectedListener) {
        super(context, R.layout.tasting_item, onTastingSelectedListener);
    }

    @Nonnull
    @Override
    protected TastingViewHolder doCreateViewHolder(final View view,
            @Nullable final View.OnClickListener onTastingSelectedListener) {

        return new TastingViewHolder(view, onTastingSelectedListener);
    }

    @Override
    protected void doBindViewHolder(final TastingViewHolder viewHolder, final Cursor cursor, final Context context) {
        final Tasting tasting = TastingFactory.newTastingFromCursor(cursor);
        viewHolder.setDomainObject(tasting);
    }

}