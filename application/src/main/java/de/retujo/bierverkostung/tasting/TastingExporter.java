/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.common.DateUtil;
import de.retujo.bierverkostung.common.ProgressUpdate;
import de.retujo.bierverkostung.common.ProgressUpdateFactory;
import de.retujo.bierverkostung.data.BierverkostungContract;
import de.retujo.bierverkostung.exchange.ExportImportException;
import de.retujo.bierverkostung.exchange.FileNameReviser;
import de.retujo.bierverkostung.exchange.ZipFileWriter;
import de.retujo.bierverkostung.photo.Photo;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTask exports a single Tasting, multiple Tastings or all Tastings into a ZIP file together with the
 * associated Photos of the Beer(s).
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class TastingExporter extends AsyncTask<Tasting, ProgressUpdate<Tasting>, Void> {

    static final String PHOTO_FILENAME_PREFIX = "photos/";
    static final String BEER_PHOTO_FILENAME_PREFIX = PHOTO_FILENAME_PREFIX + "beer/";
    static final String TASTING_JSON_FILENAME_PREFIX = "Tasting_";
    static final String TASTING_JSON_FILENAME_SUFFIX = ".json";
    private static final String ZIP_FILE_PREFIX = "Tastings_";
    private static final String ZIP_FILE_SUFFIX = ".zip";

    @SuppressLint("StaticFieldLeak") // Is Application Context.
    private final Context context;
    private final ContentResolver contentResolver;
    private final File selectedOutputDirectory;
    @Nullable private final Acceptor<ProgressUpdate<Tasting>> progressUpdateAcceptor;

    private TastingExporter(final Context theContext, final File theSelectedOutputDirectory,
            @Nullable final Acceptor<ProgressUpdate<Tasting>> theProgressUpdateAcceptor) {

        context = theContext;
        contentResolver = theContext.getContentResolver();
        selectedOutputDirectory =  theSelectedOutputDirectory;
        progressUpdateAcceptor = theProgressUpdateAcceptor;
    }

    /**
     * Returns a new instance of {@code TastingExporter}.
     *
     * @param context the context which provides the content resolver for getting all Tastings from persistence.
     * @param selectedDirectory the target directory where the ZIP file to be created is located.
     * @param progressUpdateAcceptor if provided this object receives progress updates of the export task.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static TastingExporter newInstance(final Context context, final File selectedDirectory,
            @Nullable final Acceptor<ProgressUpdate<Tasting>> progressUpdateAcceptor) {

        isNotNull(context, "context");
        isNotNull(context, "selected directory");

        return new TastingExporter(context.getApplicationContext(), selectedDirectory, progressUpdateAcceptor);
    }

    @Override
    protected Void doInBackground(final Tasting... tastings) {
        if (null != tastings && 0 < tastings.length) {
            exportTastings(tastings);
        } else {
            exportAllTastings();
        }

        return null;
    }

    private void exportTastings(final Tasting[] tastings) {
        final File outputZipFile;
        final int toBeExportedAmount = tastings.length;
        if (1 == toBeExportedAmount) {
            outputZipFile = getZipFileForSingleTasting(tastings[0]);
        } else {
            outputZipFile = getZipFileForMultipleTastings();
        }
        final ZipFileWriter zipFileWriter = ZipFileWriter.getInstance(outputZipFile);
        try {
            for (int i = 0; i < toBeExportedAmount; i++) {
                final Tasting tasting = tastings[i];
                exportTasting(tasting, zipFileWriter);
                publishProgress(ProgressUpdateFactory.determinate(i + 1, toBeExportedAmount, tasting));
            }
        } finally {
            tryToClose(zipFileWriter);
        }
    }

    private File getZipFileForSingleTasting(final Tasting tasting) {
        final String tastingFileName = getTastingFileName(tasting);
        return new File(selectedOutputDirectory, tastingFileName + ZIP_FILE_SUFFIX);
    }

    private static String getTastingFileName(final Tasting tasting) {
        final String tastingId = String.valueOf(tasting.getId());
        final String[] tastingIdSegments = tastingId.split("-");

        return TASTING_JSON_FILENAME_PREFIX + tastingIdSegments[0] + "_" + getRevisedTastingName(tasting);
    }

    private static String getRevisedTastingName(final Tasting tasting) {
        final String date = tasting.getDate();
        final Beer tastedBeer = tasting.getBeer();
        final Maybe<String> locationMaybe = tasting.getLocation();

        final String tastingName = date + "_" + tastedBeer.getName() + locationMaybe.map(l -> "_" + l).orElse("");

        final FileNameReviser fileNameReviser = FileNameReviser.getInstance();
        return fileNameReviser.apply(tastingName);
    }

    private File getZipFileForMultipleTastings() {
        final Date currentDateTime = new Date(System.currentTimeMillis());
        final DateUtil dateUtil = DateUtil.getInstance(context);
        final String dateTimeLongIsoString = dateUtil.getLongIsoString(currentDateTime);

        return new File(selectedOutputDirectory, ZIP_FILE_PREFIX + dateTimeLongIsoString + ZIP_FILE_SUFFIX);
    }

    private static void exportTasting(final Tasting tasting, final ZipFileWriter zipFileWriter) {
        final String tastingFileName = getTastingFileName(tasting);
        zipFileWriter.writeEntry(tastingFileName + TASTING_JSON_FILENAME_SUFFIX, tasting);

        final Beer beer = tasting.getBeer();
        for (final Photo photo : beer.getPhotos()) {
            final PhotoFile photoFile = photo.getFile();
            zipFileWriter.writeEntry(BEER_PHOTO_FILENAME_PREFIX + photoFile.getFileName(), photoFile.getFile());
        }
    }

    private static void tryToClose(final ZipFileWriter zipFileWriter) {
        try {
            zipFileWriter.close();
        } catch (final IOException e) {
            throw new ExportImportException("Failed to close ZipFileWriter!", e);
        }
    }

    private void exportAllTastings() {
        final Cursor tastingsCursor = queryAllTastings();
        if (null != tastingsCursor) {
            exportAllTastings(tastingsCursor);
        }
    }

    private Cursor queryAllTastings() {
        return contentResolver.query(BierverkostungContract.TastingEntry.TABLE.getContentUri(), null, null, null, null);
    }

    private void exportAllTastings(final Cursor tastingsCursor) {
        final File outputZipFile = getZipFileForMultipleTastings();
        final ZipFileWriter zipFileWriter = ZipFileWriter.getInstance(outputZipFile);
        final int toBeExportedAmount = tastingsCursor.getCount();
        try {
            int i = 0;
            do {
                final Tasting tasting = TastingFactory.newTastingFromCursor(tastingsCursor);
                exportTasting(tasting, zipFileWriter);
                ++i;
                publishProgress(ProgressUpdateFactory.determinate(i, toBeExportedAmount, tasting));
            } while (tastingsCursor.moveToNext());
        } finally {
            tryToClose(zipFileWriter);
        }
    }

    @SafeVarargs
    @Override
    protected final void onProgressUpdate(ProgressUpdate<Tasting>... progressUpdates) {
        super.onProgressUpdate(progressUpdates);
        if (null != progressUpdateAcceptor) {
            progressUpdateAcceptor.accept(progressUpdates[0]);
        }
    }

}
