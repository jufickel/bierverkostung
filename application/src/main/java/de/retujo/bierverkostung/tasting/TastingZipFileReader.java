/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentResolver;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beer.BeerFactory;
import de.retujo.bierverkostung.common.ProgressUpdate;
import de.retujo.bierverkostung.common.ProgressUpdateFactory;
import de.retujo.bierverkostung.exchange.JsonObjectOutputStream;
import de.retujo.bierverkostung.exchange.ZipFileReader;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Logger;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTask reads provided ZIP files and tries to convert the ZIP file entries to Tasting entities and insert them
 * into the database. Existing entities with a less revision will be overwritten, otherwise the existing entities are
 * used and the import of that entities is skipped.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
final class TastingZipFileReader extends AsyncTask<File, ProgressUpdate<Tasting>, List<Tasting>>
        implements ZipFileReader.ZipFileConsumer {

    private static final Pattern BEER_PHOTO_FILENAME_PATTERN =
            Pattern.compile("(" + TastingExporter.BEER_PHOTO_FILENAME_PREFIX + ")(.*)");
    private static final Logger LOGGER = Logger.forSimpleClassName(TastingZipFileReader.class);

    private final ContentResolver contentResolver;
    @Nullable private final Acceptor<ProgressUpdate<Tasting>> progressUpdateAcceptor;
    private final List<Tasting> importedTastings;

    private TastingZipFileReader(final ContentResolver theContentResolver,
            @Nullable final Acceptor<ProgressUpdate<Tasting>> theProgressUpdateAcceptor) {

        contentResolver = isNotNull(theContentResolver, "ContentResolver");
        progressUpdateAcceptor = theProgressUpdateAcceptor;
        importedTastings = new ArrayList<>();
    }

    /**
     * Returns an instance of {@code TastingZipFileReader}.
     *
     * @param contentResolver the ContentResolver for accessing the database.
     * @param progressUpdateAcceptor if provided this object receives progress updates of the import task.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static TastingZipFileReader getInstance(final ContentResolver contentResolver,
            @Nullable final Acceptor<ProgressUpdate<Tasting>> progressUpdateAcceptor) {

        return new TastingZipFileReader(contentResolver, progressUpdateAcceptor);
    }


    @Override
    protected List<Tasting> doInBackground(final File... zipFiles) {
        for (final File zipFile : zipFiles) {
            final ZipFileReader zipFileReader = ZipFileReader.getInstance(zipFile);
            zipFileReader.run(this);
        }
        return importedTastings;
    }

    @Override
    public Maybe<OutputStream> getOutputStreamFor(final String fileName) throws IOException {
        if (isTastingJsonFile(fileName)) {
            LOGGER.debug("Returning TastingJsonFileImporter for <{0}>.", fileName);
            return Maybe.of(new TastingJsonFileImporter(JsonObjectOutputStream.getInstance()));
        }
        final Matcher photoFilenameMatcher = BEER_PHOTO_FILENAME_PATTERN.matcher(fileName);
        if (photoFilenameMatcher.matches()) {
            final PhotoFile beerPhotoFile = BeerFactory.newBeerPhotoFile(photoFilenameMatcher.group(2));
            final File targetFile = beerPhotoFile.getFile();
            if (!targetFile.exists() && createTargetFile(targetFile)) {
                LOGGER.debug("Returning FileOutputStream for <{0}>.", targetFile);
                return Maybe.of(new FileOutputStream(targetFile));
            }
        }
        return Maybe.empty();
    }

    private static boolean isTastingJsonFile(final String fileName) {
        return fileName.startsWith(TastingExporter.TASTING_JSON_FILENAME_PREFIX) &&
                fileName.endsWith(TastingExporter.TASTING_JSON_FILENAME_SUFFIX);
    }

    private static boolean createTargetFile(final File file) throws IOException {
        final File directory = file.getParentFile();
        if (!directory.exists()) {
            return directory.mkdirs() && file.createNewFile();
        }
        return file.createNewFile();
    }

    @Override
    public void zipFileRead() {
        // Nothing to do.
    }

    @SafeVarargs
    @Override
    protected final void onProgressUpdate(final ProgressUpdate<Tasting>... values) {
        super.onProgressUpdate(values);
        if (null != progressUpdateAcceptor) {
            progressUpdateAcceptor.accept(values[0]);
        }
    }

    @Override
    protected void onPostExecute(final List<Tasting> tastings) {
        if (null != progressUpdateAcceptor && !tastings.isEmpty()) {
            final int importedTastingsCount = tastings.size();
            progressUpdateAcceptor.accept(ProgressUpdateFactory.determinate(importedTastingsCount,
                    importedTastingsCount, null));
        }
    }

    private final class TastingJsonFileImporter extends OutputStream {
        private final JsonObjectOutputStream jsonObjectOutputStream;

        private TastingJsonFileImporter(final JsonObjectOutputStream theJsonObjectOutputStream) {
            jsonObjectOutputStream = theJsonObjectOutputStream;
        }

        @Override
        public void write(final int b) {
            jsonObjectOutputStream.write(b);
        }

        @Override
        public void write(@NonNull final byte[] b, final int off, final int len) {
            jsonObjectOutputStream.write(b, off, len);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void close() throws IOException {
            jsonObjectOutputStream.close();
            final JSONObject tastingJsonObject = jsonObjectOutputStream.toJson();
            LOGGER.debug("Read Tasting JSON file: {0}", tastingJsonObject);

            final TastingImporter tastingImporter = TastingImporter.getInstance(contentResolver);
            final Tasting importedTasting =
                    tastingImporter.importOrGetExisting(TastingFactory.newTasting(tastingJsonObject));
            importedTastings.add(importedTasting);
            publishProgress(ProgressUpdateFactory.indeterminate(1, 0, importedTasting));
        }

    }

}
