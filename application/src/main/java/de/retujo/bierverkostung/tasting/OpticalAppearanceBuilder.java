/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.CommaDelimiterTrimmer;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

import static android.text.TextUtils.isEmpty;
import static de.retujo.java.util.ObjectUtil.areEqual;

/**
 * A mutable builder with a fluent API for an immutable {@link OpticalAppearance}.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class OpticalAppearanceBuilder {

    private String beerColour;
    private int ebc;
    private String beerColourDescription;
    private String clarityDescription;
    private String foamColour;
    private String foamStructureDescription;
    private int foamStability;

    private OpticalAppearanceBuilder() {
        beerColour = null;
        ebc = ImmutableOpticalAppearance.NULL_EBC;
        beerColourDescription = null;
        clarityDescription = null;
        foamColour = null;
        foamStructureDescription = null;
        foamStability = ImmutableOpticalAppearance.NULL_FOAM_STABILITY;
    }

    /**
     * Returns an instance of {@code OpticalAppearanceBuilder}.
     *
     * @return the instance;
     */
    @Nonnull
    public static OpticalAppearanceBuilder getInstance() {
        return new OpticalAppearanceBuilder();
    }

    @Nonnull
    public OpticalAppearanceBuilder beerColour(@Nullable final CharSequence beerColour) {
        if (!isEmpty(beerColour)) {
            this.beerColour = CommaDelimiterTrimmer.trim(beerColour);
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder ebc(final CharSequence ebc) {
        if (!isEmpty(ebc)) {
            final int ebcIntValue = Integer.parseInt(ebc.toString());
            if (ImmutableOpticalAppearance.NULL_EBC != ebcIntValue) {
                this.ebc = ebcIntValue;
            }
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder ebc(final int ebc) {
        this.ebc = ebc;
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder beerColourDescription(@Nullable final CharSequence description) {
        if (!isEmpty(description)) {
            beerColourDescription = CommaDelimiterTrimmer.trim(description);
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder beerClarityDescription(@Nullable final CharSequence description) {
        if (!isEmpty(description)) {
            clarityDescription = CommaDelimiterTrimmer.trim(description);
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder foamColour(@Nullable final CharSequence foamColour) {
        if (!isEmpty(foamColour)) {
            this.foamColour = CommaDelimiterTrimmer.trim(foamColour);
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder foamStructureDescription(@Nullable final CharSequence description) {
        if (!isEmpty(description)) {
            this.foamStructureDescription = CommaDelimiterTrimmer.trim(description);
        }
        return this;
    }

    @Nonnull
    public OpticalAppearanceBuilder foamStability(final int foamStability) {
        if (ImmutableOpticalAppearance.NULL_FOAM_STABILITY != foamStability) {
            this.foamStability = foamStability;
        }
        return this;
    }

    /**
     * Returns a new immutable instance of {@link OpticalAppearance} with the properties provided to this builder.
     *
     * @return the instance.
     */
    @Nonnull
    public OpticalAppearance build() {
        return new ImmutableOpticalAppearance(this);
    }

    /**
     * Immutable implementation of {@link OpticalAppearance}.
     */
    @Immutable
    static final class ImmutableOpticalAppearance implements OpticalAppearance {
        /**
         * Creator which creates instances of {@code ImmutableOpticalAppearance} from a Parcel.
         */
        public static final Parcelable.Creator<OpticalAppearance> CREATOR = new ImmutableOpticalAppearanceCreator();

        /**
         * This value represents a semantic {@code null} EBC.
         */
        private static final byte NULL_EBC = 0;

        /**
         * This value represents a semantic {@code null} foam stability.
         */
        private static final byte NULL_FOAM_STABILITY = 0;

        private static final byte MAX_CONTENT_VALUES = 7;

        private final String beerColour;
        private final int ebc;
        private final String beerColourDescription;
        private final String clarityDescription;
        private final String foamColour;
        private final String foamStructureDescription;
        private final int foamStability;

        private ImmutableOpticalAppearance(final OpticalAppearanceBuilder builder) {
            beerColour = builder.beerColour;
            ebc = builder.ebc;
            beerColourDescription = builder.beerColourDescription;
            clarityDescription = builder.clarityDescription;
            foamColour = builder.foamColour;
            foamStructureDescription = builder.foamStructureDescription;
            foamStability = builder.foamStability;
        }

        @Nonnull
        @Override
        public Maybe<String> getBeerColour() {
            return Maybe.ofNullable(beerColour);
        }

        @Nonnull
        @Override
        public Maybe<Integer> getEbc() {
            if (NULL_EBC != ebc) {
                return Maybe.of(ebc);
            }
            return Maybe.empty();
        }

        @Nonnull
        @Override
        public Maybe<String> getBeerColourDescription() {
            return Maybe.ofNullable(beerColourDescription);
        }

        @Nonnull
        @Override
        public Maybe<String> getBeerClarityDescription() {
            return Maybe.ofNullable(clarityDescription);
        }

        @Nonnull
        @Override
        public Maybe<String> getFoamColour() {
            return Maybe.ofNullable(foamColour);
        }

        @Nonnull
        @Override
        public Maybe<String> getFoamStructureDescription() {
            return Maybe.ofNullable(foamStructureDescription);
        }

        @Nonnull
        @Override
        public Maybe<Integer> getFoamStability() {
            if (NULL_FOAM_STABILITY != foamStability) {
                return Maybe.of(foamStability);
            }
            return Maybe.empty();
        }

        @SuppressWarnings({"squid:S1067", "BooleanExpressionComplexityCheck"})
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty(beerColour)
                && (NULL_EBC == ebc)
                && TextUtils.isEmpty(beerColourDescription)
                && TextUtils.isEmpty(clarityDescription)
                && TextUtils.isEmpty(foamColour)
                && TextUtils.isEmpty(foamStructureDescription)
                && (NULL_FOAM_STABILITY == foamStability);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(final Parcel dest, final int flags) {
            ParcelWrapper.of(dest)
                    .wrap(beerColour)
                    .wrap(ebc)
                    .wrap(beerColourDescription)
                    .wrap(clarityDescription)
                    .wrap(foamColour)
                    .wrap(foamStructureDescription)
                    .wrap(foamStability);
        }

        @Nonnull
        @Override
        public ContentValues asContentValues() {
            final ContentValues result = new ContentValues(MAX_CONTENT_VALUES);
            putIfNotNull(TastingEntry.COLUMN_BEER_COLOUR, beerColour, result);
            result.put(TastingEntry.COLUMN_COLOUR_EBC.toString(), ebc);
            putIfNotNull(TastingEntry.COLUMN_BEER_COLOUR_DESCRIPTION, beerColourDescription, result);
            putIfNotNull(TastingEntry.COLUMN_CLARITY_DESCRIPTION, clarityDescription, result);
            putIfNotNull(TastingEntry.COLUMN_FOAM_COLOUR, foamColour, result);
            putIfNotNull(TastingEntry.COLUMN_FOAM_STRUCTURE, foamStructureDescription, result);
            result.put(TastingEntry.COLUMN_FOAM_STABILITY.toString(), foamStability);
            return result;
        }

        private static void putIfNotNull(final CharSequence key, final String value,
                final ContentValues contentValues) {
            if (!TextUtils.isEmpty(value)) {
                contentValues.put(key.toString(), value);
            }
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImmutableOpticalAppearance that = (ImmutableOpticalAppearance) o;

            return areEqual(beerColour, that.beerColour)
                    && ebc == that.ebc
                    && areEqual(beerColourDescription, that.beerColourDescription)
                    && areEqual(clarityDescription, that.clarityDescription)
                    && areEqual(foamColour, that.foamColour)
                    && areEqual(foamStructureDescription, that.foamStructureDescription)
                    && foamStability == that.foamStability;
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(beerColour, ebc, beerColourDescription, clarityDescription, foamColour,
                    foamStructureDescription, foamStability);
        }

        @Nonnull
        @Override
        public JSONObject toJson() {
            final OpticalAppearanceJsonConverter converter = OpticalAppearanceJsonConverter.getInstance();
            return converter.toJson(this);
        }
    }

    /**
     * This class creates instances of {@code ImmutableOpticalAppearance} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableOpticalAppearanceCreator implements Parcelable.Creator<OpticalAppearance> {
        @Override
        public OpticalAppearance createFromParcel(final Parcel source) {
            final ParcelUnwrapper parcelUnwrapper = ParcelUnwrapper.of(source);
            return OpticalAppearanceBuilder.getInstance()
                    .beerColour(parcelUnwrapper.unwrapString())
                    .ebc(parcelUnwrapper.unwrapInt())
                    .beerColourDescription(parcelUnwrapper.unwrapString())
                    .beerClarityDescription(parcelUnwrapper.unwrapString())
                    .foamColour(parcelUnwrapper.unwrapString())
                    .foamStructureDescription(parcelUnwrapper.unwrapString())
                    .foamStability(parcelUnwrapper.unwrapInt())
                    .build();
        }

        @Override
        public OpticalAppearance[] newArray(final int size) {
            return new OpticalAppearance[size];
        }
    }

}
