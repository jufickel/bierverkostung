/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.java.util.Maybe;

/**
 * Converts an {@link OpticalAppearance} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@Immutable
final class OpticalAppearanceJsonConverter extends JsonConverter<OpticalAppearance> {

    @Immutable
    private static final class JsonName {
        private static final String BEER_COLOUR = "beerColour";
        private static final String EBC = "ebc";
        private static final String BEER_COLOUR_DESCRIPTION = "beerColourDescription";
        private static final String CLARITY_DESCRIPTION = "clarityDescription";
        private static final String FOAM_COLOUR = "foamColour";
        private static final String FOAM_STRUCTURE_DESCRIPTION = "foamStructureDescription";
        private static final String FOAM_STABILITY = "foamStability";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private OpticalAppearanceJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code OpticalAppearanceJsonConverter}.
     *
     * @return the instance.
     */
    public static OpticalAppearanceJsonConverter getInstance() {
        return new OpticalAppearanceJsonConverter();
    }

    @Override
    protected void putAllValuesTo(@Nonnull final JSONObject targetJsonObject,
            @Nonnull final OpticalAppearance opticalAppearance) throws JSONException {

        final Maybe<String> beerColour = opticalAppearance.getBeerColour();
        if (beerColour.isPresent()) {
            targetJsonObject.put(JsonName.BEER_COLOUR, beerColour.get());
        }

        final Maybe<Integer> ebc = opticalAppearance.getEbc();
        if (ebc.isPresent()) {
            targetJsonObject.put(JsonName.EBC, ebc.get());
        }

        final Maybe<String> beerColourDescription = opticalAppearance.getBeerColourDescription();
        if (beerColourDescription.isPresent()) {
            targetJsonObject.put(JsonName.BEER_COLOUR_DESCRIPTION, beerColourDescription.get());
        }

        final Maybe<String> beerClarityDescription = opticalAppearance.getBeerClarityDescription();
        if (beerClarityDescription.isPresent()) {
            targetJsonObject.put(JsonName.CLARITY_DESCRIPTION, beerClarityDescription.get());
        }

        final Maybe<String> foamColour = opticalAppearance.getFoamColour();
        if (foamColour.isPresent()) {
            targetJsonObject.put(JsonName.FOAM_COLOUR, foamColour.get());
        }

        final Maybe<String> foamStructureDescription = opticalAppearance.getFoamStructureDescription();
        if (foamStructureDescription.isPresent()) {
            targetJsonObject.put(JsonName.FOAM_STRUCTURE_DESCRIPTION, foamStructureDescription.get());
        }

        final Maybe<Integer> foamStability = opticalAppearance.getFoamStability();
        if (foamStability.isPresent()) {
            targetJsonObject.put(JsonName.FOAM_STABILITY, foamStability.get());
        }
    }

    @Nonnull
    @Override
    protected OpticalAppearance createEntityInstanceFromJson(@Nonnull final JSONObject jsonObject) throws JSONException {
        final OpticalAppearanceBuilder opticalAppearanceBuilder = OpticalAppearanceBuilder.getInstance();

        if (jsonObject.has(JsonName.BEER_COLOUR)) {
            opticalAppearanceBuilder.beerColour(jsonObject.getString(JsonName.BEER_COLOUR));
        }

        if (jsonObject.has(JsonName.EBC)) {
            opticalAppearanceBuilder.ebc(jsonObject.getInt(JsonName.EBC));
        }

        if (jsonObject.has(JsonName.BEER_COLOUR_DESCRIPTION)) {
            opticalAppearanceBuilder.beerColourDescription(jsonObject.getString(JsonName.BEER_COLOUR_DESCRIPTION));
        }

        if (jsonObject.has(JsonName.CLARITY_DESCRIPTION)) {
            opticalAppearanceBuilder.beerClarityDescription(jsonObject.getString(JsonName.CLARITY_DESCRIPTION));
        }

        if (jsonObject.has(JsonName.FOAM_COLOUR)) {
            opticalAppearanceBuilder.foamColour(jsonObject.getString(JsonName.FOAM_COLOUR));
        }

        if (jsonObject.has(JsonName.FOAM_STRUCTURE_DESCRIPTION)) {
            opticalAppearanceBuilder.foamStructureDescription(
                    jsonObject.getString(JsonName.FOAM_STRUCTURE_DESCRIPTION));
        }

        if (jsonObject.has(JsonName.FOAM_STABILITY)) {
            opticalAppearanceBuilder.foamStability(jsonObject.getInt(JsonName.FOAM_STABILITY));
        }

        return opticalAppearanceBuilder.build();
    }

}
