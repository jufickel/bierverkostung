/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;

import static de.retujo.java.util.Conditions.argumentNotEmpty;

/**
 * The callbacks for the tastings CursorLoader.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class TastingLoaderCallbacks extends AbstractLoaderCallbacks {

    /**
     * Identifier of the CursorLoader for tastings.
     */
    public static final int ID = 163;

    private final String sortOrder;

    /**
     * Constructs a new {@code TastingLoaderCallbacks} object.
     *
     * @param context the current context.
     * @param cursorAdapter the adapter for binding the tastings to the UI.
     * @param sortOrder defines the sort order of the returned values.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code sortOrder} is empty.
     * @since 1.2.0
     */
    public TastingLoaderCallbacks(@Nonnull final Context context, @Nonnull final TastingCursorAdapter cursorAdapter,
            @Nonnull final CharSequence sortOrder) {
        super(ID, context, cursorAdapter);
        argumentNotEmpty(sortOrder, "sort order");
        this.sortOrder = sortOrder.toString();
    }

    @Override
    protected Loader<Cursor> doCreateLoader(@Nonnull final Context context) {
        return new CursorLoader(context, TastingEntry.TABLE.getContentUri(), null, null, null, sortOrder);
    }

}
