/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.AsyncTaskLoader;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.beer.SelectBeerActivity;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.CommaDelimiterTrimmer;
import de.retujo.bierverkostung.common.DateUtil;
import de.retujo.bierverkostung.common.SaveEntityDialogue;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.Column;
import de.retujo.bierverkostung.data.DbColumnArrayAdapter;
import de.retujo.bierverkostung.data.InsertHandler;
import de.retujo.bierverkostung.data.UpdateHandler;
import de.retujo.bierverkostung.tasting.ExpandableSuggestionListAdapter.Group;
import de.retujo.bierverkostung.tasting.TastingComponentDialogFragment.OnApplyListener;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

/**
 * This activity allows the user to add a new tasting or to change an existing tasting. Mandatory properties are the
 * date and the beer; without these the save button remains disabled.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class EditTastingActivity extends BaseActivity {

    /**
     * Key to retrieve the newly created tasting from this Activity's result Intent.
     */
    public static final String EDITED_TASTING = "editedTasting";

    /**
     * Key to set an existing Tasting to be edited to the Intent for starting an EditTastingActivity.
     */
    public static final String EDIT_TASTING = "breweryToBeEdited";

    private static final int SELECT_BEER_REQUEST_CODE = 95;
    private static final String BEER_KEY = "beer";

    private ScentSuggestions scentSuggestions;
    private TextView dateEditText = null;
    private AutoCompleteTextView locationEditText = null;
    private TextView beerTextView = null;
    private TextView beerColourTextView = null;
    private Spinner beerColourEbcSpinner = null;
    private TextInputEditText beerColourDescriptionEditText = null;
    private TextView beerClarityDescriptionTextView = null;
    private AutoCompleteTextView foamColourEditText = null;
    private TextView foamStructureTextView = null;
    private SeekBar foamStabilityRating;
    private TastingComponentView fruitScentComponent = null;
    private TastingComponentView flowerScentComponent = null;
    private TastingComponentView vegetalScentComponent = null;
    private TastingComponentView spicyScentComponent = null;
    private TastingComponentView warmthMintedScentComponent = null;
    private TastingComponentView biologicalScentComponent = null;
    private SeekBar bitternessRating = null;
    private SeekBar sweetnessRating = null;
    private SeekBar acidityRating = null;
    private TextView mouthfeelDescriptionTextView = null;
    private SeekBar fullBodiedRating = null;
    private TextView bodyDescriptionTextView = null;
    private TastingComponentView aftertasteComponent = null;
    private MultiAutoCompleteTextView foodRecommendationEditText = null;
    private TextInputEditText totalImpressionNotesEditText = null;
    private RatingBar totalImpressionRating = null;
    private Button saveTastingButton = null;
    private Runnable saveAction;

    /**
     * Constructs a new {@code EditTastingActivity} object.
     */
    public EditTastingActivity() {
        super();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tasting);

        scentSuggestions = initScentSuggestions();

        initViews();

        final Intent afferentIntent = getIntent();
        final Tasting tastingToBeEdited = afferentIntent.getParcelableExtra(EDIT_TASTING);
        if (null == tastingToBeEdited) {
            setTitle(getString(R.string.add_tasting_activity_name));
            restoreInstanceState(savedInstanceState);
            saveAction = new SaveNewTastingAction();
        } else {
            setTitle(getString(R.string.edit_tasting_activity_name));
            initViewsWithTastingProperties(tastingToBeEdited);
            saveAction = new SaveEditedTastingAction(tastingToBeEdited);
        }
    }

    private void initViews() {
        initDateTextView();
        initDatePickerButton();
        initLocationEditText();
        initBeerTextView();
        initBeerColourTextView();
        initEbcSpinner();
        beerColourDescriptionEditText = findView(R.id.edit_tasting_colour_description_edit_text);
        initBeerClarityDescriptionTextView();
        initFoamColourEditText();
        initFoamStructureEditText();
        foamStabilityRating = findView(R.id.edit_tasting_foam_stability_rating);
        initFruitScentComponent();
        initFlowerScentComponent();
        initVegetalScentComponent();
        initSpicyScentComponent();
        initWarmthMintedScentComponent();
        initBiologicalScentComponent();
        bitternessRating = findView(R.id.edit_tasting_bitterness_rating);
        sweetnessRating = findView(R.id.edit_tasting_sweetness_rating);
        acidityRating = findView(R.id.edit_tasting_acidity_rating);
        initMouthfeelDescriptionEditText();
        fullBodiedRating = findView(R.id.edit_tasting_body_fullness_rating);
        initBodyDescriptionEditText();
        initAftertasteComponent();
        initFoodRecommendationEditText();
        totalImpressionNotesEditText = findView(R.id.edit_tasting_total_impression_notes_edit_text);
        initTotalImpressionRatingBar();
        initSaveTastingButton();
    }

    private ScentSuggestions initScentSuggestions() {
        final AsyncTaskLoader<JSONObject> jsonLoader = JsonObjectLoader.getInstance(this, R.raw.scent_suggestions);
        final ScentSuggestionsProvider suggestionsProvider = ScentSuggestionsProvider.of(jsonLoader);
        return suggestionsProvider.get();
    }

    private void initDateTextView() {
        dateEditText = findView(R.id.edit_tasting_date_edit_text);
        final Date currentDate = new Date(System.currentTimeMillis());
        final DateUtil dateUtil = DateUtil.getInstance(this);
        dateEditText.setTag(currentDate);
        dateEditText.setText(dateUtil.getLongString(currentDate));
    }

    private void initDatePickerButton() {
        final ImageButton datePickerButton = findView(R.id.edit_tasting_date_picker);
        datePickerButton.setOnClickListener(new OnShowDatePickerListener());
    }

    private void initLocationEditText() {
        locationEditText = initAutoCompleteTextView(R.id.edit_tasting_location_edit_text, TastingEntry.COLUMN_LOCATION);
        locationEditText.requestFocus();
    }

    private void initBeerTextView() {
        beerTextView = findView(R.id.edit_tasting_beer_text_view);
        beerTextView.setOnClickListener(v -> {
            final Intent selectBeerIntent = new Intent(EditTastingActivity.this, SelectBeerActivity.class);
            startActivityForResult(selectBeerIntent, SELECT_BEER_REQUEST_CODE);
        });
    }

    private void initBeerColourTextView() {
        beerColourTextView =
                initSuggestionDialogueOpeningTextView(R.id.edit_tasting_beer_colour_text_view, R.array.beer_colours,
                        TastingEntry.COLUMN_BEER_COLOUR, R.string.edit_tasting_beer_colour_dialogue_title,
                        R.string.edit_tasting_beer_colour_label);
    }

    private TextView initSuggestionDialogueOpeningTextView(final int textViewId, final int suggestionsResourceId,
            final Column autoCompletionSourceColumn, final int dialogueTitleStringId, final int dialogueTagStringId) {
        final TextView result = findView(textViewId);
        result.setOnClickListener(v -> {
            final ListAdapter suggestionListAdapter = ArrayAdapter.createFromResource(this, suggestionsResourceId,
                    android.R.layout.simple_spinner_item);
            final DialogFragment dialogFragment = SuggestionListDialogFragment.getBuilder(EditTastingActivity.this,
                    autoCompletionSourceColumn)
                    .withDialogueTitle(getString(dialogueTitleStringId))
                    .withDescriptionText((CharSequence) result.getTag())
                    .withListAdapter(suggestionListAdapter)
                    .withOnApplyListener(colour -> {
                        result.setText(colour);
                        result.setTag(colour);
                    })
                    .build();
            dialogFragment.show(getSupportFragmentManager(), getString(dialogueTagStringId));
        });
        return result;
    }

    private void initEbcSpinner() {
        beerColourEbcSpinner = findView(R.id.edit_tasting_ebc_spinner);
        final ArrayAdapter<CharSequence> arrayAdapter =
                ArrayAdapter.createFromResource(this, R.array.ebc_values, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        beerColourEbcSpinner.setAdapter(EbcSpinnerAdapter.getInstance(arrayAdapter));
    }

    private void initBeerClarityDescriptionTextView() {
        beerClarityDescriptionTextView =
                initSuggestionDialogueOpeningTextView(R.id.edit_tasting_beer_clarity_text_view, R.array.beer_clarity,
                        TastingEntry.COLUMN_CLARITY_DESCRIPTION, R.string.edit_tasting_beer_clarity_dialogue_title,
                        R.string.edit_tasting_beer_clarity_label);
    }

    private void initFoamColourEditText() {
        foamColourEditText = initAutoCompleteTextView(R.id.edit_tasting_foam_colour_edit_text,
                TastingEntry.COLUMN_FOAM_COLOUR);
    }

    private void initFoamStructureEditText() {
        foamStructureTextView = initSuggestionDialogueOpeningTextView(R.id.edit_tasting_foam_structure_text_view,
                R.array.foam_structures, TastingEntry.COLUMN_FOAM_STRUCTURE,
                R.string.edit_tasting_foam_structure_dialogue_title, R.string.edit_tasting_foam_structure_label);
    }

    private void initFruitScentComponent() {
        fruitScentComponent = initTastingComponentView(R.id.edit_tasting_fruit_component_view,
                TastingEntry.COLUMN_FRUIT_DESCRIPTION, scentSuggestions.getFruityGroups());
    }

    private void initFlowerScentComponent() {
        flowerScentComponent = initTastingComponentView(R.id.edit_tasting_flower_component_view,
                TastingEntry.COLUMN_FLOWER_DESCRIPTION, scentSuggestions.getFloweryGroups());
    }

    private void initVegetalScentComponent() {
        vegetalScentComponent = initTastingComponentView(R.id.edit_tasting_vegetal_component_view,
                TastingEntry.COLUMN_VEGETAL_DESCRIPTION, scentSuggestions.getVegetalGroups());
    }

    private void initSpicyScentComponent() {
        spicyScentComponent = initTastingComponentView(R.id.edit_tasting_spicy_component_view,
                TastingEntry.COLUMN_SPICY_DESCRIPTION, scentSuggestions.getSpicyGroups());
    }

    private void initWarmthMintedScentComponent() {
        warmthMintedScentComponent = initTastingComponentView(R.id.edit_tasting_warmth_minted_component_view,
                TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION, scentSuggestions.getWarmthMintedGroups());
    }

    private void initBiologicalScentComponent() {
        biologicalScentComponent = initTastingComponentView(R.id.edit_tasting_biological_component_view,
                TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION, scentSuggestions.getBiologicalGroups());
    }

    private void initMouthfeelDescriptionEditText() {
        mouthfeelDescriptionTextView =
                initSuggestionDialogueOpeningTextView(R.id.edit_tasting_mouthfeel_description_text_view,
                        R.array.mouthfeel_descriptions, TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION,
                        R.string.edit_tasting_mouthfeel_dialogue_title, R.string.edit_tasting_mouthfeel_label);
    }

    private TastingComponentView initTastingComponentView(final int componentViewResourceId, final Column sourceColumn,
            final List<Group> suggestions) {
        final TastingComponentView result = findView(componentViewResourceId);
        result.setOnClickListener(v -> {
            final Context context = EditTastingActivity.this;
            ExpandableSuggestionListAdapter suggestionListAdapter = null;
            if (null != suggestions && !suggestions.isEmpty()) {
                suggestionListAdapter = ExpandableSuggestionListAdapter.getInstance(context, suggestions);
            }
            final DialogFragment dialogFragment = TastingComponentDialogFragment.getBuilder(context, sourceColumn)
                    .withDialogueTitle(result.getLabelText())
                    .withRatingLabel(getString(R.string.edit_tasting_rating_label))
                    .withSuggestionListAdapter(suggestionListAdapter)
                    .withSourceTastingComponent((TastingComponent) result.getTag())
                    .withOnApplyListener(newOnApplyListener(result))
                    .build();
            dialogFragment.show(getSupportFragmentManager(), result.getLabelText());
        });
        return result;
    }

    private static OnApplyListener newOnApplyListener(final TastingComponentView tastingComponentView) {
        return (descriptionText, ratingValue) -> {
            final String trimmedDescriptionText = CommaDelimiterTrimmer.trim(descriptionText);
            tastingComponentView.setTag(ImmutableTastingComponent.of(trimmedDescriptionText, ratingValue));
            tastingComponentView.setDescriptionText(trimmedDescriptionText);
            tastingComponentView.setRatingValue(ratingValue);
        };
    }

    private void initBodyDescriptionEditText() {
        bodyDescriptionTextView = initSuggestionDialogueOpeningTextView(R.id.edit_tasting_body_description_text_view,
                R.array.body_descriptions, TastingEntry.COLUMN_BODY_DESCRIPTION, R.string
                        .edit_tasting_body_dialogue_title, R.string.edit_tasting_body_label);
    }

    private void initAftertasteComponent() {
        aftertasteComponent = initTastingComponentView(R.id.edit_tasting_aftertaste_description_edit_text,
                TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION, null);
    }

    private void initFoodRecommendationEditText() {
        foodRecommendationEditText = initMultiAutoCompleteTextView(R.id.edit_tasting_food_recommendation_edit_text,
                TastingEntry.COLUMN_FOOD_RECOMMENDATION);
    }

    private AutoCompleteTextView initAutoCompleteTextView(final int resourceId, final Column<String> column) {
        final AutoCompleteTextView result = findView(resourceId);
        final DbColumnArrayAdapter arrayAdapter = DbColumnArrayAdapter.getInstance(this,
                TastingEntry.CONTENT_URI_SINGLE_COLUMN, column);
        result.setAdapter(arrayAdapter);
        result.setThreshold(1);
        return result;
    }

    private MultiAutoCompleteTextView initMultiAutoCompleteTextView(final int resourceId, final Column<String> column) {
        final MultiAutoCompleteTextView result = findView(resourceId);
        final DbColumnArrayAdapter arrayAdapter = DbColumnArrayAdapter.getInstance(this,
                TastingEntry.CONTENT_URI_SINGLE_COLUMN, column);
        result.setAdapter(arrayAdapter);
        result.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        result.setThreshold(1);
        return result;
    }

    private void initTotalImpressionRatingBar() {
        totalImpressionRating = findView(R.id.edit_tasting_total_impression_rating);
        totalImpressionRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            private static final float MIN_RATING = 1.0F;

            @Override
            public void onRatingChanged(final RatingBar ratingBar, final float rating, final boolean fromUser) {
                if (MIN_RATING > rating) {
                    totalImpressionRating.setRating(MIN_RATING);
                }
            }
        });
    }

    private void initSaveTastingButton() {
        saveTastingButton = findView(R.id.edit_tasting_save_button);
        saveTastingButton.setEnabled(false);
    }

    private void restoreInstanceState(final Bundle savedInstanceState) {
        if (null == savedInstanceState) {
            return;
        }
        restoreText(savedInstanceState, dateEditText);
        restoreText(savedInstanceState, locationEditText);
        restoreText(savedInstanceState, beerTextView);
        if (savedInstanceState.containsKey(BEER_KEY)) {
            beerTextView.setTag(savedInstanceState.getParcelable(BEER_KEY));
        }
        restoreText(savedInstanceState, beerColourTextView);
        beerColourEbcSpinner.setSelection(savedInstanceState.getInt(String.valueOf(R.id.edit_tasting_ebc_spinner), 0));
        restoreText(savedInstanceState, beerColourDescriptionEditText);
        restoreText(savedInstanceState, beerClarityDescriptionTextView);
        restoreText(savedInstanceState, foamColourEditText);
        restoreText(savedInstanceState, foamStructureTextView);
        restoreRating(savedInstanceState, foamStabilityRating);
        restoreTastingComponent(savedInstanceState, fruitScentComponent);
        restoreTastingComponent(savedInstanceState, flowerScentComponent);
        restoreTastingComponent(savedInstanceState, vegetalScentComponent);
        restoreTastingComponent(savedInstanceState, spicyScentComponent);
        restoreTastingComponent(savedInstanceState, warmthMintedScentComponent);
        restoreTastingComponent(savedInstanceState, biologicalScentComponent);
        restoreRating(savedInstanceState, bitternessRating);
        restoreRating(savedInstanceState, sweetnessRating);
        restoreRating(savedInstanceState, acidityRating);
        restoreText(savedInstanceState, mouthfeelDescriptionTextView);
        restoreRating(savedInstanceState, fullBodiedRating);
        restoreText(savedInstanceState, bodyDescriptionTextView);
        restoreTastingComponent(savedInstanceState, aftertasteComponent);
        restoreText(savedInstanceState, foodRecommendationEditText);
        restoreText(savedInstanceState, totalImpressionNotesEditText);
        restoreRating(savedInstanceState, totalImpressionRating);
        restoreButton(savedInstanceState, saveTastingButton);
    }

    private static void restoreText(@Nonnull final Bundle savedInstanceState, @Nonnull final TextView textView) {
        final String key = String.valueOf(textView.getId());
        if (savedInstanceState.containsKey(key)) {
            final CharSequence text = savedInstanceState.getCharSequence(key);
            textView.setText(text);
        }
    }

    private static void restoreRating(@Nonnull final Bundle savedInstanceState, @Nonnull final ProgressBar ratingBar) {
        final String key = String.valueOf(ratingBar.getId());
        if (savedInstanceState.containsKey(key)) {
            final int ratingValue = savedInstanceState.getInt(key);
            ratingBar.setProgress(ratingValue);
        }
    }

    private static void restoreTastingComponent(@Nonnull final Bundle savedInstanceState,
            @Nonnull final TastingComponentView tastingComponentView) {
        final String key = String.valueOf(tastingComponentView.getId());
        if (savedInstanceState.containsKey(key)) {
            final TastingComponent tastingComponent = savedInstanceState.getParcelable(key);
            tastingComponentView.setTag(tastingComponent);
            final Maybe<String> description = tastingComponent.getDescription();
            if (description.isPresent()) {
                tastingComponentView.setDescriptionText(description.get());
            }
            tastingComponentView.setRatingValue(tastingComponent.getRating().orElse(0));
        }
    }

    private static void restoreButton(@Nonnull final Bundle savedInstanceState, @Nonnull final Button button) {
        final String key = String.valueOf(button.getId());
        if (savedInstanceState.containsKey(key)) {
            final boolean enabled = savedInstanceState.getBoolean(key);
            button.setEnabled(enabled);
        }
    }

    private void initViewsWithTastingProperties(final Tasting tasting) {
        final String isoDateAsString = tasting.getDate();
        final DateUtil dateUtil = DateUtil.getInstance(this);
        final Date date = dateUtil.tryToParseIsoDate(isoDateAsString);
        if (null != date) {
            dateEditText.setText(dateUtil.getLongString(date));
            dateEditText.setTag(date);
        }
        locationEditText.setText(tasting.getLocation().orElse(null));
        saveSelectedBeerAsTag(tasting.getBeer());
        foodRecommendationEditText.setText(tasting.getFoodRecommendation().orElse(null));
        totalImpressionNotesEditText.setText(tasting.getTotalImpressionDescription().orElse(null));
        totalImpressionRating.setProgress(tasting.getTotalImpressionRating());

        initOpticalAppearanceViews(tasting.getOpticalAppearance());
        initScentViews(tasting.getScent());
        initTasteViews(tasting.getTaste());
    }

    private void initOpticalAppearanceViews(final OpticalAppearance opticalAppearance) {
        setTextAndTagIfPresent(beerColourTextView, opticalAppearance.getBeerColour());

        final String ebc = String.valueOf(opticalAppearance.getEbc().orElse(0));
        final SpinnerAdapter adapter = beerColourEbcSpinner.getAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            final Object item = adapter.getItem(i);
            if (ObjectUtil.areEqual(item, ebc)) {
                beerColourEbcSpinner.setSelection(i);
            }
        }

        beerColourDescriptionEditText.setText(opticalAppearance.getBeerColourDescription().orElse(null));
        setTextAndTagIfPresent(beerClarityDescriptionTextView, opticalAppearance.getBeerClarityDescription());
        foamColourEditText.setText(opticalAppearance.getFoamColour().orElse(null));
        setTextAndTagIfPresent(foamStructureTextView, opticalAppearance.getFoamStructureDescription());
        foamStabilityRating.setProgress(opticalAppearance.getFoamStability().orElse(0));
    }

    private static void setTextAndTagIfPresent(final TextView textView, final Maybe<String> textMaybe) {
        if (textMaybe.isPresent()) {
            textView.setText(textMaybe.get());
            textView.setTag(textMaybe.get());
        } else {
            textView.setText(null);
            textView.setTag(null);
        }
    }

    private void initScentViews(final Scent scent) {
        initTastingComponentView(fruitScentComponent, scent.getFruitComponent());
        initTastingComponentView(flowerScentComponent, scent.getFlowerComponent());
        initTastingComponentView(vegetalScentComponent, scent.getVegetalComponent());
        initTastingComponentView(spicyScentComponent, scent.getSpicyComponent());
        initTastingComponentView(warmthMintedScentComponent, scent.getWarmthMintedComponent());
        initTastingComponentView(biologicalScentComponent, scent.getBiologicalComponent());
    }

    private static void initTastingComponentView(final TastingComponentView tastingComponentView,
            final TastingComponent tastingComponent) {
        tastingComponentView.setTag(tastingComponent);
        tastingComponentView.setDescriptionText(tastingComponent.getDescription().orElse(null));
        tastingComponentView.setRatingValue(tastingComponent.getRating().orElse(0));
    }

    private void initTasteViews(final Taste taste) {
        bitternessRating.setProgress(taste.getBitternessRating().orElse(0));
        sweetnessRating.setProgress(taste.getSweetnessRating().orElse(0));
        acidityRating.setProgress(taste.getAcidityRating().orElse(0));
        setTextAndTagIfPresent(mouthfeelDescriptionTextView, taste.getMouthfeelDescription());
        fullBodiedRating.setProgress(taste.getFullBodiedRating().orElse(0));
        setTextAndTagIfPresent(bodyDescriptionTextView, taste.getBodyDescription());
        initTastingComponentView(aftertasteComponent, taste.getAftertaste());
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (Activity.RESULT_OK == resultCode && SELECT_BEER_REQUEST_CODE == requestCode) {
            saveSelectedBeerAsTag(data.getParcelableExtra(SelectBeerActivity.SELECTED_BEER));
            beerColourTextView.requestFocus();
        }
    }

    private void saveSelectedBeerAsTag(final Beer selectedBeer) {
        // Save selected beer as tag in beer name TextView
        String beerText = selectedBeer.getName();
        final Maybe<Brewery> breweryMaybe = selectedBeer.getBrewery();
        if (breweryMaybe.isPresent()) {
            final Brewery brewery = breweryMaybe.get();
            beerText = beerText + "\n" + brewery.getName();
        }
        beerTextView.setText(beerText);
        beerTextView.setTag(selectedBeer);

        enableSaveButtonIfPossible();
    }

    private void enableSaveButtonIfPossible() {
        if (null != dateEditText.getTag() && null != beerTextView.getTag()) {
            saveTastingButton.setEnabled(true);
        } else {
            saveTastingButton.setEnabled(false);
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        saveText(dateEditText, outState);
        saveText(locationEditText, outState);
        saveText(beerTextView, outState);
        final Object tag = beerTextView.getTag();
        if (null != tag) {
            outState.putParcelable(BEER_KEY, (Beer) tag);
        }
        saveText(beerColourTextView, outState);
        outState.putInt(String.valueOf(R.id.edit_tasting_ebc_spinner), beerColourEbcSpinner.getSelectedItemPosition());
        saveText(beerColourDescriptionEditText, outState);
        saveText(beerClarityDescriptionTextView, outState);
        saveText(foamColourEditText, outState);
        saveText(foamStructureTextView, outState);
        saveRating(foamStabilityRating, outState);
        saveTastingComponent(fruitScentComponent, outState);
        saveTastingComponent(flowerScentComponent, outState);
        saveTastingComponent(vegetalScentComponent, outState);
        saveTastingComponent(spicyScentComponent, outState);
        saveTastingComponent(warmthMintedScentComponent, outState);
        saveTastingComponent(biologicalScentComponent, outState);
        saveRating(bitternessRating, outState);
        saveRating(sweetnessRating, outState);
        saveRating(acidityRating, outState);
        saveText(mouthfeelDescriptionTextView, outState);
        saveRating(fullBodiedRating, outState);
        saveText(bodyDescriptionTextView, outState);
        saveTastingComponent(aftertasteComponent, outState);
        saveText(foodRecommendationEditText, outState);
        saveText(totalImpressionNotesEditText, outState);
        saveRating(totalImpressionRating, outState);
        saveButton(saveTastingButton, outState);
    }

    private static void saveText(final TextView textView, final Bundle outState) {
        outState.putCharSequence(String.valueOf(textView.getId()), textView.getText());
    }

    private static void saveRating(final ProgressBar progressBar, final Bundle outState) {
        outState.putInt(String.valueOf(progressBar.getId()), progressBar.getProgress());
    }

    private static void saveTastingComponent(final TastingComponentView tastingComponentView, final Bundle outState) {
        final Object tag = tastingComponentView.getTag();
        if (tag != null) {
            final TastingComponent tastingComponent = (TastingComponent) tag;
            outState.putParcelable(String.valueOf(tastingComponentView.getId()), tastingComponent);
        }
    }

    private static void saveButton(final Button button, final Bundle outState) {
        outState.putBoolean(String.valueOf(button.getId()), button.isEnabled());
    }

    /**
     * Persists the tasting which is derived from the entered data.
     *
     * @param view the View is ignored by this method.
     */
    public void saveTasting(final View view) {
        saveAction.run();
        saveTastingButton.setEnabled(false);
        finish();
    }

    private void setResult(final Tasting tasting) {
        final Intent resultIntent = new Intent();
        resultIntent.putExtra(EDITED_TASTING, tasting);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    @Override
    protected void onFinish() {
        if (saveTastingButton.isEnabled()) {
            final SaveEntityDialogue saveDialogue = SaveEntityDialogue.getBuilder(this)
                    .setTitle(R.string.edit_beer_exit_title)
                    .setMessage(getString(R.string.edit_beer_save_before_exit))
                    .setOnSaveEntityListener(aVoid -> {
                        saveTasting(null);
                        super.onFinish();
                    })
                    .setOnDiscardEntityListener(aVoid -> super.onFinish())
                    .build();
            saveDialogue.show();
        } else {
            super.onFinish();
        }
    }

    private final class OnShowDatePickerListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final Calendar calendar = Calendar.getInstance();
            final DatePickerDialog dialog = new DatePickerDialog(v.getContext(),
                    (view, year, monthOfYear, dayOfMonth) -> {
                        calendar.set(Calendar.DAY_OF_MONTH, 1);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        final Date selectedDate = calendar.getTime();
                        final DateUtil dateUtil = DateUtil.getInstance(v.getContext());
                        dateEditText.setTag(selectedDate);
                        dateEditText.setText(dateUtil.getLongString(selectedDate));
                        enableSaveButtonIfPossible();
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        }
    }

    /**
     * This class inserts a new Tasting into the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveNewTastingAction implements Runnable {
        @Override
        public void run() {
            final Date date = (Date) dateEditText.getTag();
            final Beer beer = (Beer) beerTextView.getTag();
            final Object selectedEbcItem = beerColourEbcSpinner.getSelectedItem();
            final OpticalAppearance opticalAppearance = OpticalAppearanceBuilder.getInstance()
                    .beerColour(beerColourTextView.getText())
                    .ebc(String.valueOf(selectedEbcItem))
                    .beerColourDescription(beerColourDescriptionEditText.getText())
                    .beerClarityDescription(beerClarityDescriptionTextView.getText())
                    .foamColour(foamColourEditText.getText())
                    .foamStructureDescription(foamStructureTextView.getText())
                    .foamStability(foamStabilityRating.getProgress())
                    .build();

            final Scent scent = ScentBuilder.getInstance()
                    .fruit((TastingComponent) fruitScentComponent.getTag())
                    .flower((TastingComponent) flowerScentComponent.getTag())
                    .vegetal((TastingComponent) vegetalScentComponent.getTag())
                    .spicy((TastingComponent) spicyScentComponent.getTag())
                    .warmthMinted((TastingComponent) warmthMintedScentComponent.getTag())
                    .biological((TastingComponent) biologicalScentComponent.getTag())
                    .build();

            final Taste taste = TasteBuilder.getInstance()
                    .bitternessRating(bitternessRating.getProgress())
                    .sweetnessRating(sweetnessRating.getProgress())
                    .acidityRating(acidityRating.getProgress())
                    .mouthfeelDescription(mouthfeelDescriptionTextView.getText())
                    .fullBodiedRating(fullBodiedRating.getProgress())
                    .bodyDescription(bodyDescriptionTextView.getText())
                    .aftertaste((TastingComponent) aftertasteComponent.getTag())
                    .build();

            final DateUtil dateUtil = DateUtil.getInstance(EditTastingActivity.this);

            final TastingBuilder tastingBuilder = TastingBuilder.newInstance(dateUtil.getShortIsoString(date), beer)
                    .location(locationEditText.getText())
                    .opticalAppearance(opticalAppearance)
                    .scent(scent)
                    .taste(taste)
                    .foodRecommendation(foodRecommendationEditText.getText())
                    .totalImpressionDescription(totalImpressionNotesEditText.getText())
                    .totalImpressionRating(totalImpressionRating.getProgress());

            final Tasting pendingTasting = tastingBuilder.build();

            final InsertHandler<Tasting> insertHandler =
                    InsertHandler.async(getContentResolver(), EditTastingActivity.this::setResult);
            insertHandler.startInsert(pendingTasting);
        }
    }

    /**
     * This class updates an existing Tasting in the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveEditedTastingAction implements Runnable {
        private final Tasting tastingToBeEdited;

        private SaveEditedTastingAction(final Tasting tastingToBeEdited) {
            this.tastingToBeEdited = tastingToBeEdited;
        }

        @Override
        public void run() {
            final Date date = (Date) dateEditText.getTag();
            final Beer beer = (Beer) beerTextView.getTag();

            final OpticalAppearance opticalAppearance = OpticalAppearanceBuilder.getInstance()
                    .beerColour(beerColourTextView.getText())
                    .beerColourDescription(beerColourDescriptionEditText.getText())
                    .beerClarityDescription(beerClarityDescriptionTextView.getText())
                    .foamColour(foamColourEditText.getText())
                    .foamStructureDescription(foamStructureTextView.getText())
                    .foamStability(foamStabilityRating.getProgress())
                    .build();

            final Scent scent = ScentBuilder.getInstance()
                    .fruit((TastingComponent) fruitScentComponent.getTag())
                    .flower((TastingComponent) flowerScentComponent.getTag())
                    .vegetal((TastingComponent) vegetalScentComponent.getTag())
                    .spicy((TastingComponent) spicyScentComponent.getTag())
                    .warmthMinted((TastingComponent) warmthMintedScentComponent.getTag())
                    .biological((TastingComponent) biologicalScentComponent.getTag())
                    .build();

            final Taste taste = TasteBuilder.getInstance()
                    .bitternessRating(bitternessRating.getProgress())
                    .sweetnessRating(sweetnessRating.getProgress())
                    .acidityRating(acidityRating.getProgress())
                    .mouthfeelDescription(mouthfeelDescriptionTextView.getText())
                    .fullBodiedRating(fullBodiedRating.getProgress())
                    .bodyDescription(bodyDescriptionTextView.getText())
                    .aftertaste((TastingComponent) aftertasteComponent.getTag())
                    .build();

            final DateUtil dateUtil = DateUtil.getInstance(EditTastingActivity.this);

            final Tasting editedTasting = TastingBuilder.newInstance(dateUtil.getShortIsoString(date), beer)
                    .id(tastingToBeEdited.getId())
                    .revision(tastingToBeEdited.getRevision().increment())
                    .location(locationEditText.getText())
                    .opticalAppearance(opticalAppearance)
                    .scent(scent)
                    .taste(taste)
                    .foodRecommendation(foodRecommendationEditText.getText())
                    .totalImpressionDescription(totalImpressionNotesEditText.getText())
                    .totalImpressionRating(totalImpressionRating.getProgress())
                    .build();

            final UpdateHandler<Tasting> updateHandler =
                    UpdateHandler.async(getContentResolver(), EditTastingActivity.this::setResult);
            updateHandler.startUpdate(editedTasting);
        }
    }

}
