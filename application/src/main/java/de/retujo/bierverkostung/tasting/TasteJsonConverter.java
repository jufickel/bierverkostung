/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.java.util.Maybe;

/**
 * Converts a {@link Taste} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@Immutable
final class TasteJsonConverter extends JsonConverter<Taste> {

    @Immutable
    private static final class JsonName {
        private static final String BITTERNESS_RATING = "bitternessRating";
        private static final String SWEETNESS_RATING = "sweetnessRating";
        private static final String ACIDITY_RATING = "acidityRating";
        private static final String MOUTHFEEL_DESCRIPTION = "mouthfeelDescription";
        private static final String FULL_BODIED_RATING = "fullBodiedRating";
        private static final String BODY_DESCRIPTION = "bodyDescription";
        private static final String AFTERTASTE = "aftertaste";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private TasteJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code TasteJsonConverter}.
     *
     * @return the instance.
     */
    public static TasteJsonConverter getInstance() {
        return new TasteJsonConverter();
    }

    @Override
    protected void putAllValuesTo(@Nonnull final JSONObject targetJsonObject, @Nonnull final Taste taste)
            throws JSONException {

        final Maybe<Integer> bitternessRating = taste.getBitternessRating();
        if (bitternessRating.isPresent()) {
            targetJsonObject.put(JsonName.BITTERNESS_RATING, bitternessRating.get());
        }

        final Maybe<Integer> sweetnessRating = taste.getSweetnessRating();
        if (sweetnessRating.isPresent()) {
            targetJsonObject.put(JsonName.SWEETNESS_RATING, sweetnessRating.get());
        }

        final Maybe<Integer> acidityRating = taste.getAcidityRating();
        if (acidityRating.isPresent()) {
            targetJsonObject.put(JsonName.ACIDITY_RATING, acidityRating.get());
        }

        final Maybe<String> mouthfeelDescription = taste.getMouthfeelDescription();
        if (mouthfeelDescription.isPresent()) {
            targetJsonObject.put(JsonName.MOUTHFEEL_DESCRIPTION, mouthfeelDescription.get());
        }

        final Maybe<Integer> fullBodiedRating = taste.getFullBodiedRating();
        if (fullBodiedRating.isPresent()) {
            targetJsonObject.put(JsonName.FULL_BODIED_RATING, fullBodiedRating.get());
        }

        final Maybe<String> bodyDescription = taste.getBodyDescription();
        if (bodyDescription.isPresent()) {
            targetJsonObject.put(JsonName.BODY_DESCRIPTION, bodyDescription.get());
        }

        targetJsonObject.put(JsonName.AFTERTASTE, taste.getAftertaste().toJson());
    }

    @Nonnull
    @Override
    protected Taste createEntityInstanceFromJson(@Nonnull final JSONObject jsonObject) throws JSONException {
        final TasteBuilder tasteBuilder = TasteBuilder.getInstance();
        if (jsonObject.has(JsonName.BITTERNESS_RATING)) {
            tasteBuilder.bitternessRating(jsonObject.getInt(JsonName.BITTERNESS_RATING));
        }
        if (jsonObject.has(JsonName.SWEETNESS_RATING)) {
            tasteBuilder.sweetnessRating(jsonObject.getInt(JsonName.SWEETNESS_RATING));
        }
        if (jsonObject.has(JsonName.ACIDITY_RATING)) {
            tasteBuilder.acidityRating(jsonObject.getInt(JsonName.ACIDITY_RATING));
        }
        if (jsonObject.has(JsonName.MOUTHFEEL_DESCRIPTION)) {
            tasteBuilder.mouthfeelDescription(jsonObject.getString(JsonName.MOUTHFEEL_DESCRIPTION));
        }
        if (jsonObject.has(JsonName.FULL_BODIED_RATING)) {
            tasteBuilder.fullBodiedRating(jsonObject.getInt(JsonName.FULL_BODIED_RATING));
        }
        if (jsonObject.has(JsonName.BODY_DESCRIPTION)) {
            tasteBuilder.bodyDescription(jsonObject.getString(JsonName.BODY_DESCRIPTION));
        }
        if (jsonObject.has(JsonName.AFTERTASTE)) {
            final TastingComponentJsonConverter jsonConverter = TastingComponentJsonConverter.getInstance();
            tasteBuilder.aftertaste(jsonConverter.fromJson(jsonObject.getJSONObject(JsonName.AFTERTASTE)));
        }
        return tasteBuilder.build();
    }

}
