/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.common.CommaDelimiterTrimmer;
import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;
import de.retujo.bierverkostung.data.Revision;

import static android.text.TextUtils.isEmpty;
import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A mutable builder with a fluent API for creating immutable instances of {@link Tasting}.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
final class TastingBuilder {

    private final String date;
    private Beer beer;
    private EntityCommonData commonData;
    @Nullable private String location;
    private OpticalAppearance opticalAppearance;
    private Scent scent;
    private Taste taste;
    @Nullable private String foodRecommendation;
    @Nullable private String totalImpressionDescription;
    private int totalImpressionRating;

    private TastingBuilder(final String theDate, final Beer theBeer) {
        date = theDate;
        beer = theBeer;

        commonData = EntityCommonData.getInstance();
        location = null;
        opticalAppearance = OpticalAppearanceBuilder.getInstance().build();
        scent = ScentBuilder.getInstance().build();
        taste = TasteBuilder.getInstance().build();
        foodRecommendation = null;
        totalImpressionDescription = null;
    }

    /**
     * Returns a new instance of {@code TastingBuilder}.
     *
     * @param date the date of the tasting in ISO 8601 format.
     * @param beer the tasted beer.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code date} is empty.
     */
    public static TastingBuilder newInstance(final String date, final Beer beer) {
        return new TastingBuilder(argumentNotEmpty(date, "date"), isNotNull(beer, "beer"));
    }

    /**
     * Returns a new instance of {@code TastingBuilder} which is initialised with the properties of the specified
     * tasting.
     *
     * @param sourceTasting the Tasting providing the initial properties of the returned builder.
     * @return the instance.
     * @throws NullPointerException if {@code sourceTasting} is {@code null}.
     * @since 1.2.0
     */
    public static TastingBuilder newInstance(final Tasting sourceTasting) {
        return newInstance(sourceTasting.getDate(), sourceTasting.getBeer())
                .id(sourceTasting.getId())
                .revision(sourceTasting.getRevision())
                .location(sourceTasting.getLocation().orElse(null))
                .opticalAppearance(sourceTasting.getOpticalAppearance())
                .scent(sourceTasting.getScent())
                .taste(sourceTasting.getTaste())
                .foodRecommendation(sourceTasting.getFoodRecommendation().orElse(null))
                .totalImpressionRating(sourceTasting.getTotalImpressionRating())
                .totalImpressionDescription(sourceTasting.getTotalImpressionDescription().orElse(null));
    }

    /**
     * Sets the specified ID.
     *
     * @param id the ID to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code id} is {@code null}.
     */
    public TastingBuilder id(final UUID id) {
        return commonData(EntityCommonData.getInstance(id, commonData.getRevision()));
    }

    /**
     * Sets the specified revision.
     *
     * @param revision the revision to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code revision} is {@code null}.
     */
    public TastingBuilder revision(final Revision revision) {
        return commonData(EntityCommonData.getInstance(commonData.getId(), isNotNull(revision, "revision")));
    }

    /**
     * Sets the specified common data like ID and revision.
     *
     * @param commonData the common data to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code commonData} is {@code null}.
     */
    public TastingBuilder commonData(final EntityCommonData commonData) {
        this.commonData = isNotNull(commonData, "common data to be set");
        return this;
    }

    public TastingBuilder location(@Nullable final CharSequence location) {
        if (!isEmpty(location)) {
            this.location = String.valueOf(location);
        }
        return this;
    }

    /**
     * Sets the specified beer.
     *
     * @param beer the beer to be set.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code beer} is {@code null}.
     */
    public TastingBuilder beer(final Beer beer) {
        this.beer = isNotNull(beer, "Beer to be set");
        return this;
    }

    public TastingBuilder opticalAppearance(@Nullable final OpticalAppearance opticalAppearance) {
        if (null != opticalAppearance) {
            this.opticalAppearance = opticalAppearance;
        }
        return this;
    }

    public TastingBuilder scent(@Nullable final Scent scent) {
        if (null != scent) {
            this.scent = scent;
        }
        return this;
    }

    public TastingBuilder taste(@Nullable final Taste taste) {
        if (null != taste) {
            this.taste = taste;
        }
        return this;
    }

    public TastingBuilder foodRecommendation(@Nullable final CharSequence foodRecommendation) {
        if (!isEmpty(foodRecommendation)) {
            this.foodRecommendation = CommaDelimiterTrimmer.trim(foodRecommendation);
        }
        return this;
    }

    public TastingBuilder totalImpressionRating(final int rating) {
        totalImpressionRating = rating;
        return this;
    }

    public TastingBuilder totalImpressionDescription(@Nullable final CharSequence description) {
        if (!isEmpty(description)) {
            totalImpressionDescription = String.valueOf(description);
        }
        return this;
    }

    public Tasting build() {
        return new ImmutableTasting(this);
    }

    /**
     * Immutable implementation of {@link Tasting}.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    static final class ImmutableTasting implements Tasting {
        /**
         * Creator which creates instances of {@code ImmutableTasting} from a Parcel.
         */
        public static final Parcelable.Creator<Tasting> CREATOR = new ImmutableTastingCreator();

        private final EntityCommonData commonData;
        private final String date;
        @Nullable private final String location;
        private final Beer beer;
        private final OpticalAppearance opticalAppearance;
        private final Scent scent;
        private final Taste taste;
        @Nullable private final String foodRecommendation;
        @Nullable private final String totalImpressionDescription;
        private final int totalImpressionRating;

        private ImmutableTasting(final TastingBuilder builder) {
            commonData = builder.commonData;
            date = builder.date;
            location = builder.location;
            beer = builder.beer;
            opticalAppearance = builder.opticalAppearance;
            scent = builder.scent;
            taste = builder.taste;
            foodRecommendation = builder.foodRecommendation;
            totalImpressionDescription = builder.totalImpressionDescription;
            totalImpressionRating = builder.totalImpressionRating;
        }

        @Override
        public UUID getId() {
            return commonData.getId();
        }

        @Override
        public String getDate() {
            return date;
        }

        @Override
        public Maybe<String> getLocation() {
            return Maybe.ofNullable(location);
        }

        @Override
        public Beer getBeer() {
            return beer;
        }

        @Override
        public OpticalAppearance getOpticalAppearance() {
            return opticalAppearance;
        }

        @Override
        public Scent getScent() {
            return scent;
        }

        @Override
        public Taste getTaste() {
            return taste;
        }

        @Override
        public Maybe<String> getFoodRecommendation() {
            return Maybe.ofNullable(foodRecommendation);
        }

        @Override
        public Maybe<String> getTotalImpressionDescription() {
            return Maybe.ofNullable(totalImpressionDescription);
        }

        @Override
        public int getTotalImpressionRating() {
            return totalImpressionRating;
        }

        @Override
        public Uri getContentUri() {
            return commonData.getContentUri(TastingEntry.TABLE);
        }

        @Override
        public Revision getRevision() {
            return commonData.getRevision();
        }

        @Override
        public ContentValues asContentValues() {
            final ContentValues result = commonData.asContentValues(TastingEntry.TABLE);
            result.put(TastingEntry.COLUMN_DATE.toString(), date);
            putIfNotEmpty(TastingEntry.COLUMN_LOCATION, location, result);
            result.put(TastingEntry.COLUMN_BEER_ID.toString(), beer.getId().toString());
            result.putAll(opticalAppearance.asContentValues());
            result.putAll(scent.asContentValues());
            result.putAll(taste.asContentValues());
            putIfNotEmpty(TastingEntry.COLUMN_FOOD_RECOMMENDATION, foodRecommendation, result);
            putIfNotEmpty(TastingEntry.COLUMN_TOTAL_IMPRESSION_DESCRIPTION, totalImpressionDescription, result);
            result.put(TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING.toString(), totalImpressionRating);
            return result;
        }

        private static void putIfNotEmpty(final CharSequence key, @Nullable final String value,
                final ContentValues contentValues) {

            if (!isEmpty(value)) {
                contentValues.put(key.toString(), value);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @SuppressWarnings({"squid:S1067", "squid:S3776"})
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImmutableTasting that = (ImmutableTasting) o;

            return ObjectUtil.areEqual(commonData, that.commonData) &&
                    ObjectUtil.areEqual(date, that.date) &&
                    ObjectUtil.areEqual(location, that.location) &&
                    ObjectUtil.areEqual(beer, that.beer) &&
                    ObjectUtil.areEqual(opticalAppearance, that.opticalAppearance) &&
                    ObjectUtil.areEqual(scent, that.scent) &&
                    ObjectUtil.areEqual(taste, that.taste) &&
                    ObjectUtil.areEqual(foodRecommendation, that.foodRecommendation) &&
                    ObjectUtil.areEqual(totalImpressionDescription, that.totalImpressionDescription) &&
                    totalImpressionRating == that.totalImpressionRating;
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(commonData, date, location, beer, opticalAppearance, scent, taste,
                    foodRecommendation, totalImpressionDescription, totalImpressionRating);
        }

        @Override
        public void writeToParcel(final Parcel dest, final int flags) {
            commonData.writeToParcel(dest, flags);
            ParcelWrapper.of(dest)
                    .wrap(date)
                    .wrap(beer)
                    .wrap(location)
                    .wrap(opticalAppearance)
                    .wrap(scent)
                    .wrap(taste)
                    .wrap(foodRecommendation)
                    .wrap(totalImpressionDescription)
                    .wrap(totalImpressionRating);
        }

        @Override
        public JSONObject toJson() {
            final TastingJsonConverter converter = TastingJsonConverter.getInstance();
            return converter.toJson(this);
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {" +
                    commonData +
                    ", date='" + date + '\'' +
                    ", location='" + location + '\'' +
                    ", beer=" + beer +
                    ", opticalAppearance=" + opticalAppearance +
                    ", scent=" + scent +
                    ", taste=" + taste +
                    ", foodRecommendation='" + foodRecommendation + '\'' +
                    ", totalImpressionDescription='" + totalImpressionDescription + '\'' +
                    ", totalImpressionRating=" + totalImpressionRating +
                    "}";
        }
    }

    /**
     * This class creates instances of {@code ImmutableTasting} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableTastingCreator extends BaseParcelableCreator<Tasting> {
        @Override
        protected Tasting createFromParcel(final Parcel source, final EntityCommonData commonData) {
            final ParcelUnwrapper parcelUnwrapper = ParcelUnwrapper.of(source);
            final String readDate = parcelUnwrapper.unwrapString();
            final Beer readBeer = parcelUnwrapper.unwrapParcelable();

            return TastingBuilder.newInstance(readDate, readBeer)
                    .commonData(commonData)
                    .location(parcelUnwrapper.unwrapString())
                    .opticalAppearance(parcelUnwrapper.unwrapParcelable())
                    .scent(parcelUnwrapper.unwrapParcelable())
                    .taste(parcelUnwrapper.unwrapParcelable())
                    .foodRecommendation(parcelUnwrapper.unwrapString())
                    .totalImpressionDescription(parcelUnwrapper.unwrapString())
                    .totalImpressionRating(parcelUnwrapper.unwrapInt())
                    .build();
        }

        @Override
        public Tasting[] newArray(final int size) {
            return new Tasting[size];
        }
    }

}
