/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * TODO Javadoc.
 *
 * @since 1.0.0
 */
final class ExpandableSuggestionListAdapter extends BaseExpandableListAdapter {

    private final List<Group> suggestions;
    private final LayoutInflater inflater;

    private ExpandableSuggestionListAdapter(final List<Group> theSuggestions, final LayoutInflater theInflater) {
        suggestions = theSuggestions;
        inflater = theInflater;
    }

    /**
     * Returns an instance of {@code ExpandableSuggestionListAdapter}.
     *
     * @param context is used to get the appropriate LayoutInflater.
     * @param suggestions the suggestions to be bound to the ExpandableList.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    @Nonnull
    public static ExpandableSuggestionListAdapter getInstance(@Nonnull final Context context,
            @Nonnull final List<Group> suggestions) {
        isNotNull(context, "context");
        isNotNull(suggestions, "suggestions");
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new ExpandableSuggestionListAdapter(suggestions, inflater);
    }

    @Override
    public int getGroupCount() {
        return suggestions.size();
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        final Group group = suggestions.get(groupPosition);
        final List<Child> children = group.getChildren();
        return children.size();
    }

    @Override
    public String getGroup(final int groupPosition) {
        final Group group = suggestions.get(groupPosition);
        return group.getLocalisedName();
    }

    @Override
    public String getChild(final int groupPosition, final int childPosition) {
        final Group group = suggestions.get(groupPosition);
        final List<Child> children = group.getChildren();
        final Child child = children.get(childPosition);
        return child.getLocalisedName();
    }

    @Override
    public long getGroupId(final int groupPosition) {
        final Group group = suggestions.get(groupPosition);
        return group.getId();
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        final Group group = suggestions.get(groupPosition);
        final Child child = group.getChild(childPosition);
        return child.getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, final View convertView,
            final ViewGroup parent) {
        final View result;
        if (null == convertView) {
            result = newGroupView(parent);
        } else {
            result = convertView;
        }
        bindView(result, R.id.list_group_title_text_view, getGroup(groupPosition));
        return result;
    }

    private View newGroupView(final ViewGroup parent) {
        return inflater.inflate(R.layout.list_group, parent, false);
    }

    private static void bindView(final View view, final int resourceId, final String titleText) {
        final TextView titleTextView = (TextView) view.findViewById(resourceId);
        titleTextView.setText(titleText);
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild,
            final View convertView, final ViewGroup parent) {
        final View result;
        if (null == convertView) {
            result = inflater.inflate(R.layout.list_item, null);
        } else {
            result = convertView;
        }
        bindView(result, R.id.list_item_title_text_view, getChild(groupPosition, childPosition));
        return result;
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return true;
    }

    /**
     * TODO Javadoc
     *
     * @since 1.0.0
     */
    @Immutable
    public interface Group {
        int getPosition();

        long getId();

        @Nonnull
        String getLocalisedName();

        @Nonnull
        List<Child> getChildren();

        Child getChild(int position);
    }

    /**
     * TODO Javadoc
     *
     * @since 1.0.0
     */
    @Immutable
    public interface Child {
        int getPosition();

        int getId();

        @Nonnull
        String getLocalisedName();
    }

    /**
     * An immutable implementation of {@code Child}.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableChild implements Child {
        private final int id;
        private final String localisedName;

        private ImmutableChild(final int id, @Nonnull final String localisedName) {
            this.id = id;
            this.localisedName = localisedName;
        }


        @Override
        public int getPosition() {
            return getId();
        }

        @Override
        public int getId() {
            return id;
        }

        @Nonnull
        @Override
        public String getLocalisedName() {
            return localisedName;
        }
    }

    /**
     * An immutable implementation of {@code Group}.
     *
     * @since 1.0.0
     */
    private static final class ImmutableGroup implements Group {
        private final int position;
        private final String localisedName;
        private final List<Child> children;

        private ImmutableGroup(final GroupBuilder builder) {
            this.position = builder.position;
            this.localisedName = builder.localisedName;
            this.children = Collections.unmodifiableList(new ArrayList<>(builder.children));
        }

        @Override
        public int getPosition() {
            return position;
        }

        @Override
        public long getId() {
            return getPosition();
        }

        @Nonnull
        @Override
        public String getLocalisedName() {
            return localisedName;
        }

        @Nonnull
        @Override
        public List<Child> getChildren() {
            return children;
        }

        @Override
        public Child getChild(final int position) {
            return children.get(position);
        }
    }

    /**
     * A mutable builder with a fluent API for an immutable {@code Group}.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    static final class GroupBuilder {
        private final List<Child> children;
        private String localisedName;
        private int position;

        private GroupBuilder() {
            children = new ArrayList<>();
            localisedName = null;
            position = -1;
        }

        /**
         * Returns a new instance of {@code GroupBuilder}.
         *
         * @return the instance.
         */
        @Nonnull
        public static GroupBuilder getInstance() {
            return new GroupBuilder();
        }

        @Nonnull
        public GroupBuilder localisedName(final CharSequence localisedName) {
            if (null != localisedName && 0 < localisedName.length()) {
                this.localisedName = localisedName.toString();
            }
            return this;
        }

        @Nonnull
        public GroupBuilder position(final int position) {
            this.position = position;
            return this;
        }

        @Nonnull
        public GroupBuilder children(final Collection<String> localisedChildNames) {
            if (null != localisedChildNames) {
                for (final String localisedChildName : localisedChildNames) {
                    child(localisedChildName);
                }
            }
            return this;
        }

        @Nonnull
        public GroupBuilder child(final CharSequence localisedChildName) {
            if (null != localisedChildName && 0 < localisedChildName.length()) {
                final int childId = children.size();
                children.add(new ImmutableChild(childId, localisedChildName.toString()));
            }
            return this;
        }

        @Nonnull
        public Group build() {
            return new ImmutableGroup(this);
        }
    }

}
