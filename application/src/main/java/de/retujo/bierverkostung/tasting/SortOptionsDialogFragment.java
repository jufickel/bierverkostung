/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.java.util.Acceptor;

/**
 * This fragment creates and shows an AlertDialog which contains options for sorting the items of the tastings overview.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class SortOptionsDialogFragment extends DialogFragment {

    private Acceptor<CharSequence> onApplyListener;
    private RadioGroup sortOrderRadioGroup;
    private RadioGroup sortCriteriaRadioGroup;
    private SortOrderPreferences sortOrderPreferences;

    /**
     * Constructs a new {@code SortOptionsDialogFragment} object.
     * <p>
     * <em>Do not use this constructor directly.</em> It is required by Android. Use
     * {@link #getBuilder(Context)} instead.
     */
    public SortOptionsDialogFragment() {
        onApplyListener = null;
        sortOrderRadioGroup = null;
        sortCriteriaRadioGroup = null;
        sortOrderPreferences = null;
    }

    /**
     * Returns a new builder with a fluent API for a {@code SortOptionsDialogFragment}.
     *
     * @param context provides the application context.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static Builder getBuilder(@Nonnull final Context context) {
        return new Builder(context);
    }

    @Nonnull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final View view = initView();
        initViewElements(view);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(R.string.sort_options_dialogue_positive_button, (dialog, w) -> {
                    persistSelection();
                    onApplyListener.accept(getSortOrderString());
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.sort_options_dialogue_negative_button, (dialog, w) -> dialog.dismiss())
                .create();
    }

    private View initView() {
        final FragmentActivity activity = getActivity();
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.sort_options_dialog, null);
    }

    private void initViewElements(final View view) {
        sortOrderRadioGroup = (RadioGroup) view.findViewById(R.id.sort_options_dialogue_sort_order_radio_group);
        sortCriteriaRadioGroup = (RadioGroup) view.findViewById(R.id.sort_options_dialogue_sort_criteria_radio_group);

        sortOrderRadioGroup.check(sortOrderPreferences.getPersistedSortOrder());
        sortCriteriaRadioGroup.check(sortOrderPreferences.getPersistedSortCriteria());
    }

    private void persistSelection() {
        sortOrderPreferences.edit()
                .sortOrder(sortOrderRadioGroup.getCheckedRadioButtonId())
                .sortCriteria(sortCriteriaRadioGroup.getCheckedRadioButtonId())
                .apply();
    }

    private String getSortOrderString() {
        return SortOrderPreferences.getSortOrderString(sortCriteriaRadioGroup.getCheckedRadioButtonId(),
                sortOrderRadioGroup.getCheckedRadioButtonId());
    }

    /**
     * Mutable builder with a fluent API for creating a {@code SortOptionsDialogFragment}.
     *
     * @since 1.1.0
     */
    @NotThreadSafe
    static final class Builder {
        private final Context context;
        private Acceptor<CharSequence> onApplyListener;

        private Builder(final Context context) {
            this.context = context;
            onApplyListener = value -> {
            };
        }

        /**
         * Sets the listener to be notified when the dialogue is closed by a click on the "positive button".
         *
         * @param onApplyListener the listener to be set.
         * @return this builder instance to allow method chaining.
         */
        @Nonnull
        public Builder withOnApplyListener(@Nullable final Acceptor<CharSequence> onApplyListener) {
            if (null != onApplyListener) {
                this.onApplyListener = onApplyListener;
            }
            return this;
        }

        /**
         * Create a new {@code SortOptionsDialogFragment} object.
         *
         * @return the created object.
         */
        @Nonnull
        public SortOptionsDialogFragment build() {
            final SortOptionsDialogFragment result = new SortOptionsDialogFragment();
            result.onApplyListener = onApplyListener;
            result.sortOrderPreferences = SortOrderPreferences.getInstance(context);

            return result;
        }
    }

}
