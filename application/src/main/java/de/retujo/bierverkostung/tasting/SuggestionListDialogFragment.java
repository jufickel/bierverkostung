/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.data.BierverkostungContract;
import de.retujo.bierverkostung.data.Column;
import de.retujo.bierverkostung.data.DbColumnArrayAdapter;
import de.retujo.java.util.Acceptor;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This fragment creates and shows an AlertDialog which contains an editable AutoCompleteTextView as well as a
 * ListView. Its purpose is to mimic the behaviour of a ComboBox which does not exist for Android.
 *
 * @since 1.1.0
 */
@NotThreadSafe
public final class SuggestionListDialogFragment extends DialogFragment {

    private TextView dialogueTitleTextView;
    private CharSequence dialogueTitle;
    private DbColumnArrayAdapter descriptionSuggestionsAdapter;
    private MultiAutoCompleteTextView descriptionEditText;
    private CharSequence descriptionText;
    private TextView suggestionListLabel;
    private ListAdapter suggestionListAdapter;
    private ListView suggestionListView;
    private Acceptor<CharSequence> onApplyListener;
    private AlertDialog dialog;

    /**
     * Constructs a new {@code SuggestionListDialogFragment} object.
     * <p>
     * <em>Do not use this constructor directly.</em> It is required by Android. Use
     * {@link #getBuilder(Context, Column)} instead.
     */
    public SuggestionListDialogFragment() {
        dialogueTitleTextView = null;
        dialogueTitle = "";
        descriptionSuggestionsAdapter = null;
        descriptionEditText = null;
        descriptionText = "";
        suggestionListLabel = null;
        suggestionListAdapter = null;
        suggestionListView = null;
        onApplyListener = null;
        dialog = null;
    }

    /**
     * Returns a new builder with a fluent API for a {@code SuggestionListDialogFragment}.
     *
     * @param context provides the application context for the used CursorLoader for suggestions.
     * @param sourceColumn the column to get the auto-completion suggestions from.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static Builder getBuilder(@Nonnull final Context context, @Nonnull final Column sourceColumn) {
        return new Builder(context, sourceColumn);
    }

    @Nonnull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final View view = initView();
        initViewElements(view);

        final AlertDialog result = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(R.string.suggestion_list_dialogue_positive_button, (dialog, w) -> {
                    if (null != onApplyListener) {
                        onApplyListener.accept(descriptionEditText.getText());
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.suggestion_list_dialogue_negative_button, (dialog, w) -> dialog.dismiss())
                .create();
        dialog = result;

        return result;
    }

    private View initView() {
        final FragmentActivity activity = getActivity();
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.suggestion_list_dialog, null);
    }

    private void initViewElements(final View view) {
        initDialogueTitleTextView(view);
        initDescriptionEditText(view);
        suggestionListLabel = (TextView) view.findViewById(R.id.suggestion_list_suggestions_label);
        initSuggestionListView(view);
    }

    private void initDialogueTitleTextView(final View view) {
        dialogueTitleTextView = (TextView) view.findViewById(R.id.suggestion_list_dialog_title);
        dialogueTitleTextView.setText(dialogueTitle);
    }

    private void initDescriptionEditText(final View view) {
        descriptionEditText = (MultiAutoCompleteTextView) view.findViewById(R.id.suggestion_list_description_edit_text);
        descriptionEditText.setAdapter(descriptionSuggestionsAdapter);
        descriptionEditText.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        descriptionEditText.setThreshold(1);
        descriptionEditText.setText(descriptionText);
    }

    private void initSuggestionListView(final View view) {
        suggestionListView = (ListView) view.findViewById(R.id.suggestion_list_suggestion_list);
        if (null != suggestionListAdapter) {
            suggestionListLabel.setVisibility(View.VISIBLE);
            suggestionListView.setAdapter(suggestionListAdapter);
            suggestionListView.setVisibility(View.VISIBLE);

            final OnListItemSelectedListener onListItemSelectedListener = new OnListItemSelectedListener();
            suggestionListView.setOnItemClickListener(onListItemSelectedListener);
            suggestionListView.setOnItemLongClickListener(onListItemSelectedListener);
        }
    }

    /**
     * This listener handles selection of a suggestion list item. A short click fills the description text with the
     * selected item. A long click does the same but immediately closes the dialogue and notifies the onApplyListener.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class OnListItemSelectedListener
            implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
        @Override
        public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
            appendSelectedItemNameToDescriptionEditText(position);
        }

        @Override
        public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position,
                final long id) {
            appendSelectedItemNameToDescriptionEditText(position);
            final Button applyButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            if (null != applyButton) {
                applyButton.performClick();
            }
            return true;
        }

        private boolean appendSelectedItemNameToDescriptionEditText(final int position) {
            final Object selectedItem = suggestionListAdapter.getItem(position);
            final String itemName = String.valueOf(selectedItem);
            final String descriptionText = String.valueOf(descriptionEditText.getText());
            if (descriptionText.isEmpty()) {
                descriptionEditText.setText(itemName);
            } else {
                descriptionEditText.setText(descriptionText + ", " + itemName);
            }
            final int cursorPosition = descriptionEditText.length();
            descriptionEditText.setSelection(cursorPosition);

            return true;
        }
    }

    /**
     * Mutable builder with a fluent API for creating a {@code SuggestionListDialogFragment}.
     *
     * @since 1.1.0
     */
    @NotThreadSafe
    static final class Builder {
        private final Context context;
        private final Column sourceColumn;
        private CharSequence dialogueTitle;
        private ListAdapter suggestionListAdapter;
        private Acceptor<CharSequence> onApplyListener;
        private CharSequence descriptionText;

        private Builder(final Context context, final Column sourceColumn) {
            this.context = context;
            this.sourceColumn = sourceColumn;
        }

        /**
         * Sets the title of the dialogue.
         *
         * @param dialogueTitle the title of the dialogue.
         * @return this builder instance to allow method chaining.
         */
        @Nonnull
        public Builder withDialogueTitle(@Nullable final CharSequence dialogueTitle) {
            this.dialogueTitle = dialogueTitle;
            return this;
        }

        /**
         * Sets the ListAdapter for the suggestions. If this method is not called the suggestion list will remain empty.
         *
         * @param suggestionListAdapter the adapter to be used.
         * @return this builder instance to allow method chaining.
         * @throws NullPointerException if {@code suggestionListAdapter} is {@code null}.
         */
        @Nonnull
        public Builder withListAdapter(@Nonnull final ListAdapter suggestionListAdapter) {
            isNotNull(suggestionListAdapter, "ListAdapter for suggestions");
            this.suggestionListAdapter = suggestionListAdapter;
            return this;
        }

        /**
         * Sets the listener to be notified when the dialogue is closed by a click on the "positive button".
         *
         * @param onApplyListener the listener to be set.
         * @return this builder instance to allow method chaining.
         */
        @Nonnull
        public Builder withOnApplyListener(@Nullable final Acceptor<CharSequence> onApplyListener) {
            this.onApplyListener = onApplyListener;
            return this;
        }

        /**
         * Sets the specified description text.
         *
         * @param descriptionText the description text.
         * @return this builder instance to allow method chaining.
         */
        @Nonnull
        public Builder withDescriptionText(@Nullable final CharSequence descriptionText) {
            this.descriptionText = descriptionText;
            return this;
        }

        /**
         * Create a new {@code SuggestionListDialogFragment} object.
         *
         * @return the created object.
         */
        @Nonnull
        public SuggestionListDialogFragment build() {
            final SuggestionListDialogFragment result = new SuggestionListDialogFragment();
            result.dialogueTitle = dialogueTitle;
            result.descriptionText = descriptionText;
            result.suggestionListAdapter = suggestionListAdapter;
            result.descriptionSuggestionsAdapter = DbColumnArrayAdapter.getInstance(context,
                    BierverkostungContract.TastingEntry.CONTENT_URI_SINGLE_COLUMN, sourceColumn);
            result.onApplyListener = onApplyListener;

            return result;
        }
    }

}
