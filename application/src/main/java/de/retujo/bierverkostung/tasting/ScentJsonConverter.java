/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.JsonConverter;

/**
 * Converts a {@link Scent} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@Immutable
final class ScentJsonConverter extends JsonConverter<Scent> {

    @Immutable
    private static final class JsonName {
        private static final String FRUIT = "fruit";
        private static final String FLOWER = "flower";
        private static final String VEGETAL = "vegetal";
        private static final String SPICY = "spicy";
        private static final String WARMTH_MINTED = "warmthMinted";
        private static final String BIOLOGICAL = "biological";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private ScentJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code ScentJsonConverter}.
     *
     * @return the instance.
     */
    public static ScentJsonConverter getInstance() {
        return new ScentJsonConverter();
    }

    @Override
    protected void putAllValuesTo(@Nonnull final JSONObject targetJsonObject, @Nonnull final Scent scent)
            throws JSONException {

        targetJsonObject.put(JsonName.FRUIT, scent.getFruitComponent().toJson());
        targetJsonObject.put(JsonName.FLOWER, scent.getFlowerComponent().toJson());
        targetJsonObject.put(JsonName.VEGETAL, scent.getVegetalComponent().toJson());
        targetJsonObject.put(JsonName.SPICY, scent.getSpicyComponent().toJson());
        targetJsonObject.put(JsonName.WARMTH_MINTED, scent.getWarmthMintedComponent().toJson());
        targetJsonObject.put(JsonName.BIOLOGICAL, scent.getBiologicalComponent().toJson());
    }

    @Nonnull
    @Override
    protected Scent createEntityInstanceFromJson(@Nonnull final JSONObject jsonObject) throws JSONException {
        return ScentBuilder.getInstance()
                .fruit(getTastingComponent(jsonObject, JsonName.FRUIT))
                .flower(getTastingComponent(jsonObject, JsonName.FLOWER))
                .vegetal(getTastingComponent(jsonObject, JsonName.VEGETAL))
                .spicy(getTastingComponent(jsonObject, JsonName.SPICY))
                .warmthMinted(getTastingComponent(jsonObject, JsonName.WARMTH_MINTED))
                .biological(getTastingComponent(jsonObject, JsonName.BIOLOGICAL))
                .build();
    }

    private static TastingComponent getTastingComponent(final JSONObject jsonObject, final String jsonName)
            throws JSONException {

        final TastingComponentJsonConverter converter = TastingComponentJsonConverter.getInstance();
        final JSONObject tastingComponentJsonObject = jsonObject.getJSONObject(jsonName);
        return converter.fromJson(tastingComponentJsonObject);
    }

}
