/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractTextWatcher;
import de.retujo.bierverkostung.data.Column;
import de.retujo.bierverkostung.data.DbColumnArrayAdapter;
import de.retujo.java.util.Maybe;

import static de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry.CONTENT_URI_SINGLE_COLUMN;

/**
 * This fragment shows a dialog for entering the values of a {@link TastingComponent}.
 *
 * @since 1.0.0
 */
public final class TastingComponentDialogFragment extends DialogFragment {

    private static final String ARG_TASTING_COMPONENT = "tastingComponent";

    private CharSequence dialogueTitle;
    private String ratingLabelText;
    private TextView dialogueTitleTextView;
    private MultiAutoCompleteTextView descriptionEditText;
    private TextView suggestionListLabel;
    private ExpandableListView suggestionListView;
    private TextView ratingLabel;
    private SeekBar ratingSeekBar;
    private DbColumnArrayAdapter descriptionSuggestionsAdapter;
    private ExpandableListAdapter suggestionListAdapter;
    private OnApplyListener onApplyListener;

    /**
     * Constructs a new {@code TastingComponentDialogFragment} object.
     * <p>
     * <em>Do not use this constructor directly.</em> It is required by Android. Use {@link #getBuilder(Context, Column)}
     * instead.
     */
    public TastingComponentDialogFragment() {
        dialogueTitle = "";
        ratingLabelText = "";
        dialogueTitleTextView = null;
        descriptionEditText = null;
        suggestionListView = null;
        suggestionListView = null;
        ratingLabel = null;
        ratingSeekBar = null;
        onApplyListener = null;
    }

    /**
     * Returns a new builder with a fluent API for a {@code TastingComponentDialogFragment}.
     *
     * @param context provides the application context for the used CursorLoader for suggestions.
     * @param sourceColumn the column to get the auto-completion suggestions from.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     */
    @Nonnull
    public static Builder getBuilder(@Nonnull final Context context, @Nonnull final Column sourceColumn) {
        return new Builder(context, sourceColumn);
    }

    @Nonnull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final View view = initView();
        initViewElements(view);

        final Bundle arguments = getArguments();
        if (null != arguments) {
            setInitialValues(arguments);
        }

        final DialogInterface.OnClickListener positiveButtonListener = new ApplyChangesButtonListener();

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(getString(R.string.tasting_component_positive_button), positiveButtonListener)
                .setNegativeButton(getString(R.string.tasting_component_negative_button), (dialog, which) -> dialog.dismiss())
                .create();
    }

    private View initView() {
        final FragmentActivity activity = getActivity();
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.tasting_component_dialog, null);
    }

    private void initViewElements(final View view) {
        initDialogueTitleTextView(view);
        initDescriptionEditText(view);
        suggestionListLabel = (TextView) view.findViewById(R.id.tasting_component_suggestions_label);
        initSuggestionListView(view);
        initRatingLabel(view);
        ratingSeekBar = (SeekBar) view.findViewById(R.id.tasting_component_rating_seek_bar);
    }

    private void initDialogueTitleTextView(final View view) {
        dialogueTitleTextView = (TextView) view.findViewById(R.id.tasting_component_dialog_title);
        dialogueTitleTextView.setText(dialogueTitle);
    }

    private void initDescriptionEditText(final View view) {
        descriptionEditText = (MultiAutoCompleteTextView) view.findViewById(R.id.tasting_component_description_edit_text);
        descriptionEditText.setAdapter(descriptionSuggestionsAdapter);
        descriptionEditText.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        descriptionEditText.setThreshold(1);
        descriptionEditText.addTextChangedListener(new SetProgressTextWatcher());
    }

    private void initSuggestionListView(final View view) {
        suggestionListView = (ExpandableListView) view.findViewById(R.id.tasting_component_suggestion_list);
        if (null != suggestionListAdapter) {
            suggestionListLabel.setVisibility(View.VISIBLE);
            suggestionListView.setAdapter(suggestionListAdapter);
            suggestionListView.setVisibility(View.VISIBLE);

            final OnListItemSelectedListener onListItemSelectedListener = new OnListItemSelectedListener();
            suggestionListView.setOnItemLongClickListener(onListItemSelectedListener);
            suggestionListView.setOnChildClickListener(onListItemSelectedListener);
        }
    }

    private void initRatingLabel(final View view) {
        ratingLabel = (TextView) view.findViewById(R.id.tasting_component_rating_label);
        ratingLabel.setText(ratingLabelText);
    }

    private void setInitialValues(final Bundle arguments) {
        final TastingComponent tastingComponent = arguments.getParcelable(ARG_TASTING_COMPONENT);
        if (null != tastingComponent) {
            final Maybe<String> description = tastingComponent.getDescription();
            if (description.isPresent()) {
                descriptionEditText.setText(description.get());
            }
            ratingSeekBar.setProgress(tastingComponent.getRating().orElse(0));
        }
    }

    @NotThreadSafe
    private final class OnListItemSelectedListener implements ExpandableListView.OnGroupClickListener,
            ExpandableListView.OnChildClickListener, AdapterView.OnItemLongClickListener {
        @Override
        public boolean onGroupClick(final ExpandableListView parent, final View v, final int groupPosition,
                final long id) {
            final Object selectedItem = suggestionListAdapter.getGroup(groupPosition);
            return appendSelectedItemNameToDescriptionEditText(selectedItem);
        }

        @Override
        public boolean onChildClick(final ExpandableListView parent, final View v, final int groupPosition,
                final int childPosition, final long id) {
            final Object selectedItem = suggestionListAdapter.getChild(groupPosition, childPosition);
            return appendSelectedItemNameToDescriptionEditText(selectedItem);
        }

        @Override
        public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position,
                final long id) {
            final int itemType = ExpandableListView.getPackedPositionType(id);
            if (ExpandableListView.PACKED_POSITION_TYPE_GROUP == itemType) {
                final int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                final Object group = suggestionListAdapter.getGroup(groupPosition);
                return appendSelectedItemNameToDescriptionEditText(group);
            }
            return false;
        }

        private boolean appendSelectedItemNameToDescriptionEditText(final Object selectedItem) {
            final String itemName = String.valueOf(selectedItem);
            final String descriptionText = String.valueOf(descriptionEditText.getText());
            if (descriptionText.isEmpty()) {
                descriptionEditText.setText(itemName);
            } else {
                descriptionEditText.setText(descriptionText + ", " + itemName);
            }
            final int position = descriptionEditText.length();
            descriptionEditText.setSelection(position);

            return true;
        }
    }

    private final class ApplyChangesButtonListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(final DialogInterface dialog, final int which) {
            if (null != onApplyListener) {
                final String description = String.valueOf(descriptionEditText.getText());
                final int rating = ratingSeekBar.getProgress();
                onApplyListener.apply(description, rating);
            }
            dialog.dismiss();
        }
    }

    /**
     * Listener to be notified if the values of the dialog should be applied.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("squid:S1609")
    public interface OnApplyListener {
        /**
         * This method is called with the values of the dialog.
         *
         * @param descriptionText the description text which was entered by the user.
         * @param ratingValue the rating value which was determined by the user.
         */
        void apply(@Nonnull CharSequence descriptionText, int ratingValue);
    }

    /**
     * A mutable builder with a fluent API for a {@code TastingComponentDialogFragment}.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    static final class Builder {
        private final Context context;
        private final Column sourceColumn;
        private CharSequence dialogueTitle;
        private CharSequence ratingLabelText;
        private ExpandableListAdapter suggestionListAdapter;
        private TastingComponent sourceTastingComponent;
        private OnApplyListener onApplyListener;

        private Builder(final Context context, final Column sourceColumn) {
            this.context = context;
            this.sourceColumn = sourceColumn;
        }

        public Builder withDialogueTitle(@Nullable final CharSequence dialogueTitle) {
            this.dialogueTitle = dialogueTitle;
            return this;
        }

        public Builder withRatingLabel(@Nullable final CharSequence ratingLabel) {
            this.ratingLabelText = ratingLabel;
            return this;
        }

        public Builder withSuggestionListAdapter(@Nullable final ExpandableListAdapter adapter) {
            suggestionListAdapter = adapter;
            return this;
        }

        public Builder withSourceTastingComponent(@Nullable final TastingComponent sourceTastingComponent) {
            this.sourceTastingComponent = sourceTastingComponent;
            return this;
        }

        public Builder withOnApplyListener(@Nullable final OnApplyListener listener) {
            onApplyListener = listener;
            return this;
        }

        public TastingComponentDialogFragment build() {
            final TastingComponentDialogFragment result = new TastingComponentDialogFragment();

            result.dialogueTitle = dialogueTitle;
            if (!TextUtils.isEmpty(ratingLabelText)) {
                result.ratingLabelText = ratingLabelText.toString();
            }
            result.descriptionSuggestionsAdapter = DbColumnArrayAdapter.getInstance(context, CONTENT_URI_SINGLE_COLUMN,
                    sourceColumn);
            result.suggestionListAdapter = suggestionListAdapter;
            result.onApplyListener = onApplyListener;

            if (null != sourceTastingComponent) {
                final Bundle args = new Bundle();
                args.putParcelable(ARG_TASTING_COMPONENT, sourceTastingComponent);
                result.setArguments(args);
            }

            return result;
        }
    }

    /**
     * This class sets the progress automatically to {@code 1} if a description text was entered. The reason
     * therefore is that as soon as a characteristic of a Beer can be perceived it must have an intensity.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SetProgressTextWatcher extends AbstractTextWatcher {
        @Override
        public void afterTextChanged(final Editable s) {
            if (0 == descriptionEditText.length()) {
                ratingSeekBar.setProgress(0);
            } else if (0 == ratingSeekBar.getProgress()) {
                ratingSeekBar.setProgress(1);
            }
        }
    }

}
