/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.ObjectUtil.areEqual;

/**
 * A mutable builder with a fluent API for an immutable {@link Scent}.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class ScentBuilder {

    private TastingComponent fruit;
    private TastingComponent flower;
    private TastingComponent vegetal;
    private TastingComponent spicy;
    private TastingComponent warmthMinted;
    private TastingComponent biological;

    private ScentBuilder() {
        fruit = ImmutableTastingComponent.empty();
        flower = ImmutableTastingComponent.empty();
        vegetal = ImmutableTastingComponent.empty();
        spicy = ImmutableTastingComponent.empty();
        warmthMinted = ImmutableTastingComponent.empty();
        biological = ImmutableTastingComponent.empty();
    }

    /**
     * Returns an instance of {@code ScentBuilder}.
     *
     * @return the instance.
     */
    @Nonnull
    public static ScentBuilder getInstance() {
        return new ScentBuilder();
    }

    /**
     * Sets the given tasting component for describing the fruit scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder fruit(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            fruit = tastingComponent;
        }
        return this;
    }

    /**
     * Sets the given tasting component for describing the flower scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder flower(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            flower = tastingComponent;
        }
        return this;
    }

    /**
     * Sets the given tasting component for describing the vegetal scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder vegetal(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            vegetal = tastingComponent;
        }
        return this;
    }

    /**
     * Sets the given tasting component for describing the spice scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder spicy(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            spicy = tastingComponent;
        }
        return this;
    }

    /**
     * Sets the given tasting component for describing the warmth-minted scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder warmthMinted(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            warmthMinted = tastingComponent;
        }
        return this;
    }

    /**
     * Sets the given tasting component for describing the biological scent.
     *
     * @param tastingComponent the tasting component.
     * @return this builder to allow Method Chaining.
     */
    public ScentBuilder biological(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            biological = tastingComponent;
        }
        return this;
    }

    /**
     * Returns a new immutable instance of {@link Scent} with the properties provided to this builder.
     *
     * @return the instance.
     */
    @Nonnull
    public Scent build() {
        return new ImmutableScent(this);
    }

    /**
     * Immutable implementation of {@link Scent}.
     *
     * @since 1.0.0
     */
    @Immutable
    static final class ImmutableScent implements Scent {
        /**
         * Creator which creates instances of {@code ImmutableScent} from a Parcel.
         */
        public static final Parcelable.Creator<Scent> CREATOR = new ImmutableScentCreator();

        private static final byte MAX_CONTENT_VALUES = 13;

        private final TastingComponent fruit;
        private final TastingComponent flower;
        private final TastingComponent vegetal;
        private final TastingComponent spicy;
        private final TastingComponent warmthMinted;
        private final TastingComponent biological;

        private ImmutableScent(final ScentBuilder builder) {
            fruit = builder.fruit;
            flower = builder.flower;
            vegetal = builder.vegetal;
            spicy = builder.spicy;
            warmthMinted = builder.warmthMinted;
            biological = builder.biological;
        }

        @Nonnull
        @Override
        public TastingComponent getFruitComponent() {
            return fruit;
        }

        @Nonnull
        @Override
        public TastingComponent getFlowerComponent() {
            return flower;
        }

        @Nonnull
        @Override
        public TastingComponent getVegetalComponent() {
            return vegetal;
        }

        @Nonnull
        @Override
        public TastingComponent getSpicyComponent() {
            return spicy;
        }

        @Nonnull
        @Override
        public TastingComponent getWarmthMintedComponent() {
            return warmthMinted;
        }

        @Nonnull
        @Override
        public TastingComponent getBiologicalComponent() {
            return biological;
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean isEmpty() {
            return fruit.isEmpty()
                    && flower.isEmpty()
                    && vegetal.isEmpty()
                    && spicy.isEmpty()
                    && warmthMinted.isEmpty()
                    && biological.isEmpty();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(final Parcel dest, final int flags) {
            ParcelWrapper.of(dest)
                    .wrap(fruit)
                    .wrap(flower)
                    .wrap(vegetal)
                    .wrap(spicy)
                    .wrap(warmthMinted)
                    .wrap(biological);
        }

        @Nonnull
        @Override
        public ContentValues asContentValues() {
            final ContentValues result = new ContentValues(MAX_CONTENT_VALUES);
            putDescriptionIfSet(TastingEntry.COLUMN_FRUIT_DESCRIPTION, fruit, result);
            putRating(TastingEntry.COLUMN_FRUIT_RATING, fruit, result);
            putDescriptionIfSet(TastingEntry.COLUMN_FLOWER_DESCRIPTION, flower, result);
            putRating(TastingEntry.COLUMN_FLOWER_RATING, flower, result);
            putDescriptionIfSet(TastingEntry.COLUMN_VEGETAL_DESCRIPTION, vegetal, result);
            putRating(TastingEntry.COLUMN_VEGETAL_RATING, vegetal, result);
            putDescriptionIfSet(TastingEntry.COLUMN_SPICY_DESCRIPTION, spicy, result);
            putRating(TastingEntry.COLUMN_SPICY_RATING, spicy, result);
            putDescriptionIfSet(TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION, warmthMinted, result);
            putRating(TastingEntry.COLUMN_WARMTH_MINTED_RATING, warmthMinted, result);
            putDescriptionIfSet(TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION, biological, result);
            putRating(TastingEntry.COLUMN_BIOLOGICAL_RATING, biological, result);
            return result;
        }

        private static void putDescriptionIfSet(final CharSequence key, final TastingComponent component,
                final ContentValues contentValues) {
            final Maybe<String> description = component.getDescription();
            if (description.isPresent()) {
                contentValues.put(key.toString(), description.get());
            }
        }

        private static void putRating(final CharSequence key, final TastingComponent tastingComponent,
                final ContentValues contentValues) {
            contentValues.put(key.toString(), tastingComponent.getRating().orElse(0));
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImmutableScent that = (ImmutableScent) o;

            return areEqual(fruit, that.fruit)
                    && areEqual(flower, that.flower)
                    && areEqual(vegetal, that.vegetal)
                    && areEqual(spicy, that.spicy)
                    && areEqual(warmthMinted, that.warmthMinted)
                    && areEqual(biological, that.biological);
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(fruit, flower, vegetal, spicy, warmthMinted, biological);
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {" +
                    "fruit=" + fruit +
                    ", flower=" + flower +
                    ", vegetal=" + vegetal +
                    ", spicy=" + spicy +
                    ", warmthMinted=" + warmthMinted +
                    ", biological=" + biological +
                    "}";
        }

        @Nonnull
        @Override
        public JSONObject toJson() {
            final ScentJsonConverter converter = ScentJsonConverter.getInstance();
            return converter.toJson(this);
        }
    }

    /**
     * This class creates instances of {@code ImmutableScent} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableScentCreator implements Parcelable.Creator<Scent> {
        @Override
        public Scent createFromParcel(final Parcel source) {
            final ParcelUnwrapper parcelUnwrapper = ParcelUnwrapper.of(source);

            final ScentBuilder scentBuilder = ScentBuilder.getInstance();
            scentBuilder.fruit = parcelUnwrapper.unwrapParcelable();
            scentBuilder.flower = parcelUnwrapper.unwrapParcelable();
            scentBuilder.vegetal = parcelUnwrapper.unwrapParcelable();
            scentBuilder.spicy = parcelUnwrapper.unwrapParcelable();
            scentBuilder.warmthMinted = parcelUnwrapper.unwrapParcelable();
            scentBuilder.biological = parcelUnwrapper.unwrapParcelable();
            return scentBuilder.build();
        }

        @Override
        public Scent[] newArray(final int size) {
            return new Scent[size];
        }
    }

}
