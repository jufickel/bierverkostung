/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.Function;

/**
 * This class converts EBC values to approximately resembling RGB colour values. The values as well as the conversion
 * algorithm is taken from
 * <a href="https://github.com/Gremmel/kleiner-brauhelfer/blob/22c95099d6302f3319a1e9799f1e2199f3547895/source/src/berechnungen.cpp#L968">Kleiner Brauhelfer</a>.
 *
 * @since 1.2.0
 */
@Immutable
@SuppressWarnings("MagicNumber")
final class EbcColourResolver implements Function<Number, Integer> {

    /**
     * An EBC value above this threshold is such dark that it requires a bright text colour to gain enough contrast.
     */
    static final double DARK_EBC_VALUE_THRESHOLD = 30.0D;

    private static final short KNOWN_COLOURS_COUNT = 300;

    private static final List<Integer> SRM_RGB_COLOUR_MAP = new ArrayList<>(KNOWN_COLOURS_COUNT);

    static {
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 210));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 204));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 199));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 193));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 188));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 182));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 177));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 171));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 166));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 160));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 155));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 149));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 144));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 138));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 133));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 127));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 122));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 116));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 111));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 105));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 100));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 94));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 89));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 83));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(250, 250, 78));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(249, 250, 72));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(248, 249, 67));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(247, 248, 61));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(246, 247, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(245, 246, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(244, 245, 45));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(243, 244, 45));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(242, 242, 45));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(241, 240, 46));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(240, 238, 46));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(239, 236, 46));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(238, 234, 46));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(237, 232, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(236, 230, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(235, 228, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(234, 226, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(233, 224, 48));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(232, 222, 48));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(231, 220, 48));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(230, 218, 48));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(229, 216, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(228, 214, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(227, 212, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(226, 210, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(225, 208, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(224, 206, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(223, 204, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(222, 202, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(221, 200, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(220, 198, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(219, 196, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(218, 194, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(217, 192, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(216, 190, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(215, 188, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(214, 186, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(213, 184, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(212, 182, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(211, 180, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(210, 178, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(209, 176, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(208, 174, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(207, 172, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(206, 170, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(205, 168, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(204, 166, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(203, 164, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(202, 162, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(201, 160, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(200, 158, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(200, 156, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(199, 154, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(199, 152, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(198, 150, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(198, 148, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(197, 146, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(197, 144, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(196, 142, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(196, 141, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(195, 140, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(195, 139, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(194, 139, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(194, 138, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(193, 137, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(193, 136, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 136, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 135, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 134, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 133, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 133, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 132, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 131, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 130, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 130, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 129, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 128, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 127, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 127, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 126, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 125, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 124, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 124, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 123, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 122, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 121, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 121, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 120, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 119, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 118, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 118, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 117, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 116, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 115, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 115, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 114, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 113, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 112, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 112, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 111, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 110, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 109, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 109, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(192, 108, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(191, 107, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(190, 106, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(189, 106, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(188, 105, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(187, 104, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(186, 103, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(185, 103, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(184, 102, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(183, 101, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(182, 100, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(181, 100, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(180, 99, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(179, 98, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(178, 97, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(177, 97, 56));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(175, 96, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(174, 95, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(172, 94, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(171, 94, 55));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(169, 93, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(168, 92, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(167, 91, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(165, 91, 54));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(164, 90, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(162, 89, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(161, 88, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(159, 88, 53));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(158, 87, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(157, 86, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(155, 85, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(154, 85, 52));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(152, 84, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(151, 83, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(149, 82, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(148, 82, 51));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(147, 81, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(145, 80, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(144, 79, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(142, 78, 50));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(141, 77, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(139, 76, 49));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(138, 75, 48));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(137, 75, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(135, 74, 47));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(134, 73, 46));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(132, 72, 45));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(131, 72, 45));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(129, 71, 44));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(128, 70, 43));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(127, 69, 43));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(125, 69, 42));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(124, 68, 41));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(122, 67, 41));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(121, 66, 40));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(119, 66, 39));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(118, 65, 39));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(117, 64, 38));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(115, 63, 37));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(114, 63, 37));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(112, 62, 36));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(111, 61, 35));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(109, 60, 34));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(108, 60, 33));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(107, 59, 32));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(105, 58, 31));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(104, 57, 29));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(102, 57, 28));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(101, 56, 27));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(99, 55, 26));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(98, 54, 25));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(97, 54, 24));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(95, 53, 23));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(94, 52, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(92, 51, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(91, 51, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(89, 50, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(88, 49, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(87, 48, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(85, 48, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(84, 47, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(82, 46, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(81, 45, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(79, 45, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(78, 44, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(77, 43, 8));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(75, 42, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(74, 42, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(72, 41, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(71, 40, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(69, 39, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(68, 39, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(67, 38, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(65, 37, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(64, 36, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(62, 36, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(61, 35, 14));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(59, 34, 14));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(58, 33, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(57, 33, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(55, 32, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(54, 31, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(52, 30, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(51, 30, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(49, 29, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(48, 28, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(47, 27, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(45, 27, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(44, 26, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(42, 25, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(41, 24, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(39, 24, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(38, 23, 22));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(37, 22, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(37, 22, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(36, 22, 21));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(36, 21, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(35, 21, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(35, 21, 20));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(34, 20, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(34, 20, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(33, 20, 19));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(33, 19, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(32, 19, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(32, 19, 18));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(31, 18, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(31, 18, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(30, 18, 17));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(30, 17, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(29, 17, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(29, 17, 16));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(28, 16, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(28, 16, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(27, 16, 15));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(27, 15, 14));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(26, 15, 14));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(26, 15, 14));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(25, 14, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(25, 14, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(24, 14, 13));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(24, 13, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(23, 13, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(23, 13, 12));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(22, 12, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(22, 12, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(21, 12, 11));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(21, 11, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(20, 11, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(20, 11, 10));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(19, 10, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(19, 10, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(18, 10, 9));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(18, 9, 8));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(17, 9, 8));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(17, 9, 8));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(16, 8, 7));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(16, 8, 7));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(15, 8, 7));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(15, 7, 6));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(14, 7, 6));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(14, 7, 6));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(13, 6, 5));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(13, 6, 5));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(12, 6, 5));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(12, 5, 4));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(11, 5, 4));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(11, 5, 4));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(10, 4, 3));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(10, 4, 3));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(9, 4, 3));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(9, 3, 2));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(8, 3, 2));
        SRM_RGB_COLOUR_MAP.add(Color.rgb(8, 3, 2));
    }

    private EbcColourResolver() {
        super();
    }

    /**
     * Returns an instance of {@code EbcColourResolver}.
     *
     * @return the instance.
     */
    public static EbcColourResolver getInstance() {
        return new EbcColourResolver();
    }

    @Override
    public Integer apply(final Number ebcValue) {
        final double srmValue = ebcValue.doubleValue() / 1.97;
        int index = (int) Math.round(srmValue * 10);
        if (KNOWN_COLOURS_COUNT < index) {
            index = KNOWN_COLOURS_COUNT;
        }

        return SRM_RGB_COLOUR_MAP.get(index - 1);
    }

}
