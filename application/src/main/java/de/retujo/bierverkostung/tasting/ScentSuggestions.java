/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.tasting.ExpandableSuggestionListAdapter.Group;

/**
 * TODO Javadoc.
 *
 * @since 1.0.0
 */
@Immutable
interface ScentSuggestions {

    /**
     * TODO Javadoc
     *
     * @return the groups.
     */
    @Nonnull
    List<Group> getFruityGroups();

    @Nonnull
    List<Group> getFloweryGroups();

    @Nonnull
    List<Group> getVegetalGroups();

    @Nonnull
    List<Group> getSpicyGroups();

    @Nonnull
    List<Group> getWarmthMintedGroups();

    @Nonnull
    List<Group> getBiologicalGroups();

}
