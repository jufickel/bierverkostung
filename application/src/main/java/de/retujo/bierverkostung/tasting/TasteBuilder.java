/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.common.CommaDelimiterTrimmer;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.bierverkostung.tasting.ImmutableTastingComponent.NULL_RATING;
import static de.retujo.java.util.ObjectUtil.areEqual;

/**
 * A mutable builder with a fluent API for creating an immutable {@link Taste}.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class TasteBuilder {

    private int bitternessRating;
    private int sweetnessRating;
    private int acidityRating;
    private String mouthfeelDescription;
    private int fullBodiedRating;
    private String bodyDescription;
    private TastingComponent aftertaste;

    private TasteBuilder() {
        bitternessRating = NULL_RATING;
        sweetnessRating = NULL_RATING;
        acidityRating = NULL_RATING;
        mouthfeelDescription = null;
        fullBodiedRating = NULL_RATING;
        bodyDescription = null;
        aftertaste = ImmutableTastingComponent.empty();
    }

    @Nonnull
    public static TasteBuilder getInstance() {
        return new TasteBuilder();
    }

    @Nonnull
    public TasteBuilder bitternessRating(final int rating) {
        if (bitternessRating != rating) {
            bitternessRating = rating;
        }
        return this;
    }

    @Nonnull
    public TasteBuilder sweetnessRating(final int rating) {
        if (sweetnessRating != rating) {
            sweetnessRating = rating;
        }
        return this;
    }

    @Nonnull
    public TasteBuilder acidityRating(final int rating) {
        if (acidityRating != rating) {
            acidityRating = rating;
        }
        return this;
    }

    @Nonnull
    public TasteBuilder mouthfeelDescription(@Nullable final CharSequence description) {
        if (!TextUtils.isEmpty(description)) {
            mouthfeelDescription = CommaDelimiterTrimmer.trim(description);
        }
        return this;
    }

    @Nonnull
    public TasteBuilder fullBodiedRating(final int rating) {
        if (fullBodiedRating != rating) {
            fullBodiedRating = rating;
        }
        return this;
    }

    @Nonnull
    public TasteBuilder bodyDescription(@Nullable final CharSequence description) {
        if (!TextUtils.isEmpty(description)) {
            bodyDescription = CommaDelimiterTrimmer.trim(description);
        }
        return this;
    }

    @Nonnull
    public TasteBuilder aftertaste(@Nullable final TastingComponent tastingComponent) {
        if (null != tastingComponent) {
            aftertaste = tastingComponent;
        }
        return this;
    }

    @Nonnull
    public Taste build() {
        return new ImmutableTaste(this);
    }

    /**
     * An immutable implementation of {@link Taste}.
     *
     * @since 1.0.0
     */
    @Immutable
    static final class ImmutableTaste implements Taste {
        /**
         * Creator which creates instances of {@code ImmutableTastingComponent} from a Parcel.
         */
        public static final Parcelable.Creator<Taste> CREATOR = new ImmutableTasteCreator();

        private static final byte MAX_CONTENT_VALUES = 8;

        private final int bitternessRating;
        private final int sweetnessRating;
        private final int acidityRating;
        private final String mouthfeelDescription;
        private final int fullBodiedRating;
        private final String bodyDescription;
        private final TastingComponent aftertaste;

        private ImmutableTaste(final TasteBuilder builder) {
            bitternessRating = builder.bitternessRating;
            sweetnessRating = builder.sweetnessRating;
            acidityRating = builder.acidityRating;
            mouthfeelDescription = builder.mouthfeelDescription;
            fullBodiedRating = builder.fullBodiedRating;
            bodyDescription = builder.bodyDescription;
            aftertaste = builder.aftertaste;
        }

        @Nonnull
        @Override
        public Maybe<Integer> getBitternessRating() {
            if (NULL_RATING != bitternessRating) {
                return Maybe.of(bitternessRating);
            }
            return Maybe.empty();
        }

        @Nonnull
        @Override
        public Maybe<Integer> getSweetnessRating() {
            if (NULL_RATING != sweetnessRating) {
                return Maybe.of(sweetnessRating);
            }
            return Maybe.empty();
        }

        @Nonnull
        @Override
        public Maybe<Integer> getAcidityRating() {
            if (NULL_RATING != acidityRating) {
                return Maybe.of(acidityRating);
            }
            return Maybe.empty();
        }

        @Nonnull
        @Override
        public Maybe<String> getMouthfeelDescription() {
            return Maybe.ofNullable(mouthfeelDescription);
        }

        @Nonnull
        @Override
        public Maybe<Integer> getFullBodiedRating() {
            if (NULL_RATING != fullBodiedRating) {
                return Maybe.of(fullBodiedRating);
            }
            return Maybe.empty();
        }

        @Nonnull
        @Override
        public Maybe<String> getBodyDescription() {
            return Maybe.ofNullable(bodyDescription);
        }

        @Nonnull
        @Override
        public TastingComponent getAftertaste() {
            return aftertaste;
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean isEmpty() {
            return (NULL_RATING == bitternessRating)
                    && (NULL_RATING == sweetnessRating)
                    && (NULL_RATING == acidityRating)
                    && (TextUtils.isEmpty(mouthfeelDescription))
                    && (NULL_RATING == fullBodiedRating)
                    && (TextUtils.isEmpty(bodyDescription))
                    && aftertaste.isEmpty();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(final Parcel dest, final int flags) {
            ParcelWrapper.of(dest)
                    .wrap(bitternessRating)
                    .wrap(sweetnessRating)
                    .wrap(acidityRating)
                    .wrap(mouthfeelDescription)
                    .wrap(fullBodiedRating)
                    .wrap(bodyDescription)
                    .wrap(aftertaste);
        }

        @Nonnull
        @Override
        public ContentValues asContentValues() {
            final ContentValues result = new ContentValues(MAX_CONTENT_VALUES);
            result.put(TastingEntry.COLUMN_BITTERNESS_RATING.toString(), bitternessRating);
            result.put(TastingEntry.COLUMN_SWEETNESS_RATING.toString(), sweetnessRating);
            result.put(TastingEntry.COLUMN_ACIDITY_RATING.toString(), acidityRating);
            putIfNotNull(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION, mouthfeelDescription, result);
            result.put(TastingEntry.COLUMN_FULL_BODIED_RATING.toString(), fullBodiedRating);
            putIfNotNull(TastingEntry.COLUMN_BODY_DESCRIPTION, bodyDescription, result);
            if (null != aftertaste) {
                putIfNotNull(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION, aftertaste.getDescription().orElse(null),
                        result);
                result.put(TastingEntry.COLUMN_AFTERTASTE_RATING.toString(), aftertaste.getRating().orElse(0));
            }
            return result;
        }

        private static void putIfNotNull(final CharSequence key, final String value,
                final ContentValues contentValues) {
            if (null != value) {
                contentValues.put(key.toString(), value);
            }
        }

        @SuppressWarnings("squid:S1067")
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImmutableTaste that = (ImmutableTaste) o;

            return bitternessRating == that.bitternessRating
                    && sweetnessRating == that.sweetnessRating
                    && acidityRating == that.acidityRating
                    && areEqual(mouthfeelDescription, that.mouthfeelDescription)
                    && fullBodiedRating == that.fullBodiedRating
                    && areEqual(bodyDescription, that.bodyDescription)
                    && areEqual(aftertaste, that.aftertaste);
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(bitternessRating, sweetnessRating, acidityRating, mouthfeelDescription,
                    fullBodiedRating, bodyDescription, aftertaste);
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {" +
                    "bitternessRating=" + bitternessRating +
                    ", sweetnessRating=" + sweetnessRating +
                    ", acidityRating=" + acidityRating +
                    ", mouthfeelDescription='" + mouthfeelDescription + '\'' +
                    ", fullBodiedRating=" + fullBodiedRating +
                    ", bodyDescription='" + bodyDescription + '\'' +
                    ", aftertaste=" + aftertaste +
                    "}";
        }

        @Nonnull
        @Override
        public JSONObject toJson() {
            final TasteJsonConverter converter = TasteJsonConverter.getInstance();
            return converter.toJson(this);
        }
    }

    /**
     * This class creates instances of {@code ImmutableTaste} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableTasteCreator implements Parcelable.Creator<Taste> {
        @Override
        public Taste createFromParcel(final Parcel source) {
            final ParcelUnwrapper parcelUnwrapper = ParcelUnwrapper.of(source);
            return TasteBuilder.getInstance()
                    .bitternessRating(parcelUnwrapper.unwrapInt())
                    .sweetnessRating(parcelUnwrapper.unwrapInt())
                    .acidityRating(parcelUnwrapper.unwrapInt())
                    .mouthfeelDescription(parcelUnwrapper.unwrapString())
                    .fullBodiedRating(parcelUnwrapper.unwrapInt())
                    .bodyDescription(parcelUnwrapper.unwrapString())
                    .aftertaste(parcelUnwrapper.unwrapParcelable())
                    .build();
        }

        @Override
        public Taste[] newArray(final int size) {
            return new Taste[size];
        }
    }

}
