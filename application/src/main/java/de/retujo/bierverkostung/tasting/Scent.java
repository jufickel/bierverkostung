/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcelable;

import javax.annotation.Nonnull;

import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.bierverkostung.data.ContentValuesRepresentable;

/**
 * Scent is the characteristic of a beer which can be perceived with the nose. Mostly it is complex this is why it is
 * subdivided into {@code Component}s. Components make it easier to describe the scent of a beer.
 *
 * @since 1.0.0
 */
interface Scent extends ContentValuesRepresentable, Parcelable, Jsonable {

    /**
     * Returns the fruit component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getFruitComponent();

    /**
     * Returns the flower component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getFlowerComponent();

    /**
     * Returns the vegetal component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getVegetalComponent();

    /**
     * Returns the spicy component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getSpicyComponent();

    /**
     * Returns the warmth-minted component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getWarmthMintedComponent();

    /**
     * Returns the biological component of the beer's scent.
     *
     * @return the component.
     */
    @Nonnull
    TastingComponent getBiologicalComponent();

    /**
     * Indicates whether no scent component was specified at all.
     *
     * @return {@code true} if no scent component was specified, {@code false} else.
     */
    boolean isEmpty();

}
