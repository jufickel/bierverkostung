/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.content.SharedPreferences;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.data.BierverkostungContract;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class encapsulates the options for sorting the items of the tastings overview.
 *
 * @since 1.2.0
 */
@NotThreadSafe
final class SortOrderPreferences {

    private static final String PREFERENCES_FILENAME = "sort_options";
    private static final String SELECTED_SORT_ORDER = "sort.order.selected";
    private static final String SELECTED_SORT_CRITERIA = "sort.criteria.selected";

    private final SharedPreferences sharedPreferences;

    private SortOrderPreferences(final SharedPreferences theSharedPreferences) {
        sharedPreferences = theSharedPreferences;
    }

    /**
     * Returns an instance of {@code SortOrderPreferences}.
     *
     * @param context the context to get the shared preferences from.
     * @return the instance.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    @Nonnull
    public static SortOrderPreferences getInstance(@Nonnull final Context context) {
        isNotNull(context, "context");
        return new SortOrderPreferences(context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_PRIVATE));
    }

    /**
     * Returns the ID of the persisted sort order or the ID of the "ascending" sort order if none is persisted.
     *
     * @return the ID.
     */
    public int getPersistedSortOrder() {
        return sharedPreferences.getInt(SELECTED_SORT_ORDER, R.id.sort_options_dialogue_asc);
    }

    /**
     * Returns the ID of the persisted sort criteria or the ID of the tasting date if none is persisted.
     *
     * @return the ID.
     */
    public int getPersistedSortCriteria() {
        return sharedPreferences.getInt(SELECTED_SORT_CRITERIA, R.id.sort_options_dialogue_sort_criteria_date);
    }

    /**
     * Returns the sort order string to be used in database queries for determining the sort order of tasting items.
     * The result is based on the persisted sort order preferences.
     *
     * @return the sort order string.
     */
    @Nonnull
    public String getPersistedSortOrderString() {
        return getSortOrderString(getPersistedSortCriteria(), getPersistedSortOrder());
    }

    /**
     * Returns the sort order string to be used in database queries for determining the sort order of tasting items.
     * The result is based on the specified sort order preferences.
     *
     * @param sortCriteriaId the ID of the sort criteria to use for composing the result.
     * @param sortOrderId the ID of the sort order to use for composing the result.
     * @return the sort order string.
     */
    @Nonnull
    public static String getSortOrderString(final int sortCriteriaId, final int sortOrderId) {
        final String selectedSortCriteria;
        switch(sortCriteriaId) {
            case R.id.sort_options_dialogue_sort_criteria_beer_name:
                selectedSortCriteria = BierverkostungContract.BeerEntry.COLUMN_NAME.getQualifiedName();
                break;
            case R.id.sort_options_dialogue_sort_criteria_brewery_name:
                selectedSortCriteria = BierverkostungContract.BreweryEntry.COLUMN_NAME.getQualifiedName();
                break;
            case R.id.sort_options_dialogue_sort_criteria_rating:
                selectedSortCriteria = TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING.getQualifiedName();
                break;
            default:
                selectedSortCriteria = TastingEntry.COLUMN_DATE.getQualifiedName();
        }

        final String selectedSortOrder;
        if (R.id.sort_options_dialogue_desc == sortOrderId) {
            selectedSortOrder = " DESC";

        } else {
            selectedSortOrder = " ASC";
        }

        return selectedSortCriteria + selectedSortOrder;
    }

    /**
     * Returns a new editor for the sort order preferences.
     *
     * @return the editor.
     * @see SharedPreferences.Editor#edit()
     */
    @Nonnull
    public Editor edit() {
        return new Editor(sharedPreferences.edit());
    }

    /**
     * A mutable editor with a fluent API for changing the shared preferences for the sort order of tasting items.
     *
     * @since 1.2.0
     */
    @NotThreadSafe
    public static final class Editor {
        private final SharedPreferences.Editor sharedPreferencesEditor;

        private Editor(final SharedPreferences.Editor editor) {
            sharedPreferencesEditor = editor;
        }

        /**
         * Makes note of the specified ID for the sort order. The value get written with a call to {@link #apply()}.
         *
         * @param sortOrderId the new ID of the sort criteria.
         * @return the same instance of this editor to allow method chaining.
         */
        public Editor sortOrder(final int sortOrderId) {
            sharedPreferencesEditor.putInt(SELECTED_SORT_ORDER, sortOrderId);
            return this;
        }

        /**
         * Makes note of the specified ID for the sort criteria. The value get written with a call to {@link #apply()}.
         *
         * @param sortCriteriaId the new ID of the sort criteria.
         * @return the same instance of this editor to allow method chaining.
         */
        public Editor sortCriteria(final int sortCriteriaId) {
            sharedPreferencesEditor.putInt(SELECTED_SORT_CRITERIA, sortCriteriaId);
            return this;
        }

        /**
         * Atomically commits the changed preferences in the background.
         *
         * @see SharedPreferences.Editor#apply()
         */
        public void apply() {
            sharedPreferencesEditor.apply();
        }
    }

}
