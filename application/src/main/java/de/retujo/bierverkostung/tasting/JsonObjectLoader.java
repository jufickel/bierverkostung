/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTaskLoader provides the data of a particular JSON resource file as {@link JSONObject}.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class JsonObjectLoader extends AsyncTaskLoader<JSONObject> {

    private static final short BUFFER_SIZE = 1024;

    private final InputStream inputStream;

    private JsonObjectLoader(final Context context, final InputStream theInputStream) {
        super(context);
        inputStream = theInputStream;
    }

    /**
     * Returns a new instance of {@code JsonObjectLoader}.
     *
     * @param context used to retrieve the application context
     * @param resourceId the ID of the JSON resource to be loaded.
     * @return the instance.
     * @throws NullPointerException if {@code context} is {@code null}.
     * @throws android.content.res.Resources.NotFoundException NotFoundException if {@code resourceId} does not exist.
     */
    @Nonnull
    public static JsonObjectLoader getInstance(@Nonnull final Context context, final int resourceId) {
        isNotNull(context, "context");
        final Resources resources = context.getResources();
        final InputStream inputStream = resources.openRawResource(resourceId);
        return new JsonObjectLoader(context, inputStream);
    }

    @Override
    public JSONObject loadInBackground() {
        return tryToLoadJsonInBackground();
    }

    private JSONObject tryToLoadJsonInBackground() {
        try {
            return loadJsonInBackground();
        } catch (final Exception e) {
            throw new JsonResourceException("Failed to load JSON resource!", e);
        }
    }

    private JSONObject loadJsonInBackground() throws IOException, JSONException {
        final Writer writer  = new StringWriter();
        final char[] buffer = new char[BUFFER_SIZE];
        final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        final Reader reader = new BufferedReader(inputStreamReader);
        try {
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            reader.close();
        }

        final String jsonString = writer.toString();

        return new JSONObject(jsonString);
    }

    /**
     * This RuntimeException is thrown if there are problems while loading a JSON resource.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    static class JsonResourceException extends RuntimeException {
        private static final long serialVersionUID = -3963481361889229143L;

        /**
         * Constructs a new {@code JsonResourceException} object.
         *
         * @param message the detail message for this exception.
         * @param cause the cause of this exception.
         */
        public JsonResourceException(final String message, final Throwable cause) {
            super(message, cause);
        }
    }

}
