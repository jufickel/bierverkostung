/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.common.AbstractViewHolder;
import de.retujo.bierverkostung.common.DateUtil;
import de.retujo.java.util.Maybe;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} binds Tasting-related view contents.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class TastingViewHolder extends AbstractViewHolder<Tasting> {

    private final DateUtil dateUtil;
    private final TextView dateTextView;
    private final RatingBar totalImpressionRatingBar;
    private final TextView beerNameTextView;
    private final TextView beerBreweryNameTextView;

    /**
     * Constructs a new {@code TastingViewHolder} object.
     *
     * @param tastingItemView the view inflated in onCreateViewHolder.
     * @param onTastingSelectedListener a listener to be notified when a particular tasting was clicked.
     * @throws NullPointerException if {@code beerNameTextView} is {@code null}.
     */
    public TastingViewHolder(@Nonnull final View tastingItemView,
            @Nullable final View.OnClickListener onTastingSelectedListener) {
        super(tastingItemView);

        tastingItemView.setOnClickListener(onTastingSelectedListener);
        dateUtil = DateUtil.getInstance(tastingItemView.getContext());
        dateTextView = (TextView) tastingItemView.findViewById(R.id.tasting_item_date);
        totalImpressionRatingBar = (RatingBar) tastingItemView.findViewById(R.id.tasting_item_total_impression_rating);
        beerNameTextView = (TextView) tastingItemView.findViewById(R.id.tasting_item_beer_name);
        beerBreweryNameTextView = (TextView) tastingItemView.findViewById(R.id.tasting_item_brewery_name);
    }

    @Override
    protected void handleDomainObjectSet(@Nonnull final Tasting tasting) {
        final String rawDate = tasting.getDate();
        final Maybe<String> formattedDate = dateUtil.getLongString(rawDate);
        dateTextView.setText(formattedDate.orElse(rawDate));

        totalImpressionRatingBar.setProgress(tasting.getTotalImpressionRating());

        final Beer beer = tasting.getBeer();
        beerNameTextView.setText(beer.getName());

        final Maybe<Brewery> breweryMaybe = beer.getBrewery();
        if (breweryMaybe.isPresent()) {
            final Brewery brewery = breweryMaybe.get();
            beerBreweryNameTextView.setText(brewery.getName());
        } else {
            beerBreweryNameTextView.setText(null);
        }
    }

}
