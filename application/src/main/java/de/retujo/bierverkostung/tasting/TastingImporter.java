/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.beer.BeerImporter;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Imports an external Tasting or returns or updates an existing Tasting.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class TastingImporter extends DataEntityImporter<Tasting> {

    private final DataEntityImporter<Beer> beerImporter;

    private TastingImporter(final ContentResolver contentResolver, final DataEntityImporter<Beer> theBeerImporter) {
        super(contentResolver, TastingEntry.TABLE, TastingFactory::newTastingFromCursor);
        beerImporter = isNotNull(theBeerImporter, "beer importer");
    }

    /**
     * Returns an instance of {@code TastingImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing tastings or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static TastingImporter getInstance(final ContentResolver contentResolver) {
        return new TastingImporter(contentResolver, BeerImporter.getInstance(contentResolver));
    }

    @Override
    protected Tasting importSubEntities(@Nonnull final Tasting externalTasting) {
        return TastingBuilder.newInstance(externalTasting)
                .beer(beerImporter.importOrGetExisting(externalTasting.getBeer()))
                .build();
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final Tasting externalTasting) {
        return Selection.where(TastingEntry.COLUMN_DATE).is(externalTasting.getDate())
                .andWhere(TastingEntry.COLUMN_BEER_ID).is(externalTasting.getBeer().getId())
                .andWhere(TastingEntry.COLUMN_LOCATION).is(externalTasting.getLocation())
                .build();
    }

}
