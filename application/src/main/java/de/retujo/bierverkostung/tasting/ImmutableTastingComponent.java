/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.ParcelUnwrapper;
import de.retujo.bierverkostung.data.ParcelWrapper;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.ObjectUtil;

/**
 * An immutable implementation of {@link TastingComponent}.
 *
 * @since 1.0.0
 */
@Immutable
final class ImmutableTastingComponent implements TastingComponent {

    /**
     * Creator which creates instances of {@code ImmutableTastingComponent} from a Parcel.
     */
    public static final Parcelable.Creator<TastingComponent> CREATOR = new ImmutableScentComponentCreator();

    /**
     * This value represents a semantic {@code null} rating.
     */
    static final byte NULL_RATING = 0;

    private final String description;
    private final int rating;

    private ImmutableTastingComponent(final String theDescription, final int theRating) {
        description = theDescription;
        rating = theRating;
    }

    /**
     * Returns a semantic {@code null} instance of {@code ImmutableTastingComponent}.
     *
     * @return the instance.
     * @throws NullPointerException if {@code id} is {@code null}.
     */
    public static ImmutableTastingComponent empty() {
        return of(null, NULL_RATING);
    }

    /**
     * Returns a new instance of {@code ImmutableTastingComponent}.
     *
     * @param description the description of the scent component.
     * @param rating the rating of the scent component.
     * @return the instance.
     * @throws IllegalArgumentException if {@code rating} is lower than {@value #NULL_RATING}.
     */
    public static ImmutableTastingComponent of(@Nullable final CharSequence description, final int rating) {
        if (NULL_RATING > rating) {
            final String msgTemplate = "The rating must not be lower than <{0}>!";
            throw new IllegalArgumentException(MessageFormat.format(msgTemplate, NULL_RATING));
        }

        return new ImmutableTastingComponent(!TextUtils.isEmpty(description) ? description.toString() : null, rating);
    }

    @Nonnull
    @Override
    public Maybe<String> getDescription() {
        return Maybe.ofNullable(description);
    }

    @Nonnull
    @Override
    public Maybe<Integer> getRating() {
        if (NULL_RATING != rating) {
            return Maybe.of(rating);
        }
        return Maybe.empty();
    }

    @Override
    public boolean isEmpty() {
        return null == description && (NULL_RATING == rating);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        ParcelWrapper.of(dest)
                .wrap(description)
                .wrap(rating);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ImmutableTastingComponent that = (ImmutableTastingComponent) o;
        return ObjectUtil.areEqual(description, that.description) && rating == that.rating;
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(description, rating);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + "description='" + description + '\'' + ", rating=" + rating + "}";
    }

    @Nonnull
    @Override
    public JSONObject toJson() {
        final TastingComponentJsonConverter converter = TastingComponentJsonConverter.getInstance();
        return converter.toJson(this);
    }

    /**
     * This class creates instances of {@code ImmutableTastingComponent} from a Parcel.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableScentComponentCreator implements Parcelable.Creator<TastingComponent> {
        @Override
        public TastingComponent createFromParcel(final Parcel source) {
            final ParcelUnwrapper parcelUnwrapper = ParcelUnwrapper.of(source);
            return of(parcelUnwrapper.unwrapString(), parcelUnwrapper.unwrapInt());
        }

        @Override
        public TastingComponent[] newArray(final int size) {
            return new TastingComponent[size];
        }
    }

}
