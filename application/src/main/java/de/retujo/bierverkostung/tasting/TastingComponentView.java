/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;

/**
 * This special LinearLayout shows the content of a {@link TastingComponent}, i. e. an optional description text and
 * an optional rating bar.
 * <p>
 * Saving and restoring instance state was rescued by
 * <a href="https://code.tutsplus.com/tutorials/creating-compound-views-on-android--cms-22889">Creating Compound Views on Android</a>
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class TastingComponentView extends LinearLayout {

    private static final float DEFAULT_DESCRIPTION_TEXT_SIZE = 14.0F;
    private static final String STATE_SUPER_CLASS_KEY = "superState";
    private static final String LABEL_KEY = "label";
    private static final String DESCRIPTION_KEY = "description";
    private static final String RATING_KEY = "rating";

    private final TextView labelTextView;
    private final TextView descriptionTextView;
    private final ProgressBar ratingProgressBar;

    /**
     * Constructs a new {@code TastingComponentView} object.
     *
     * @param context the context the view is running in, through which it can access the current theme, resources, etc.
     */
    public TastingComponentView(@Nonnull final Context context) {
        this(context, null);
    }

    /**
     * Constructs a new {@code TastingComponentView} object.
     *
     * @param context the context the view is running in, through which it can access the current theme, resources, etc.
     * @param attrs the attributes of the XML tag that is inflating the view.
     */
    public TastingComponentView(@Nonnull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.tasting_component_view, this, true);
        labelTextView = (TextView) findViewById(R.id.tasting_component_view_label);
        descriptionTextView = (TextView) findViewById(R.id.tasting_component_view_description);
        ratingProgressBar = (ProgressBar) findViewById(R.id.tasting_component_view_rating);

        if (null != attrs) {
            final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TastingComponentView);
            try {
                bindAttributesToViewParts(typedArray);
            } finally {
                typedArray.recycle();
            }
        }
    }

    private void bindAttributesToViewParts(final TypedArray typedArray) {
        for (int i = 0; i < typedArray.getIndexCount(); i++) {
            final int attr = typedArray.getIndex(i);
            if (R.styleable.TastingComponentView_labelText == attr) {
                setLabelText(typedArray.getString(attr));
            } else if (R.styleable.TastingComponentView_descriptionTextSize == attr) {
                descriptionTextView.setTextSize(typedArray.getFloat(attr, DEFAULT_DESCRIPTION_TEXT_SIZE));
            } else if (R.styleable.TastingComponentView_descriptionText == attr) {
                setDescriptionText(typedArray.getString(attr));
            } else if (R.styleable.TastingComponentView_ratingValue == attr) {
                setRatingValue(typedArray.getInteger(attr, 0));
            }
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle result = new Bundle();
        result.putParcelable(STATE_SUPER_CLASS_KEY, super.onSaveInstanceState());
        result.putString(LABEL_KEY, getLabelText());
        result.putString(DESCRIPTION_KEY, getDescriptionText());
        result.putInt(RATING_KEY, getRatingValue());
        return result;
    }

    @Override
    protected void onRestoreInstanceState(final Parcelable state) {
        if (state instanceof Bundle) {
            final Bundle b = (Bundle) state;
            setLabelText(b.getString(LABEL_KEY));
            setDescriptionText(b.getString(DESCRIPTION_KEY));
            setRatingValue(b.getInt(RATING_KEY));
            super.onRestoreInstanceState(b.getParcelable(STATE_SUPER_CLASS_KEY));
        } else {
            super.onRestoreInstanceState(state);
        }
    }

    @Override
    protected void dispatchSaveInstanceState(final SparseArray<Parcelable> container) {
        // Make sure that the state of the child views are not saved since the state
        // is handled in onSaveInstanceState
        super.dispatchFreezeSelfOnly(container);
    }

    /**
     * Sets the specified text as label for the description.
     *
     * @param labelText the label text or {@code null}.
     */
    public void setLabelText(@Nullable final CharSequence labelText) {
        if (null != labelText) {
            labelTextView.setText(labelText);
        }
    }

    /**
     * Returns the text of the label TextView.
     *
     * @return the text.
     */
    @Nonnull
    public String getLabelText() {
        return String.valueOf(labelTextView.getText());
    }

    /**
     * Sets the specified description text.
     *
     * @param descriptionText the description text or {@code null}.
     */
    public void setDescriptionText(@Nullable final CharSequence descriptionText) {
        if (!TextUtils.isEmpty(descriptionText)) {
            descriptionTextView.setVisibility(VISIBLE);
            descriptionTextView.setText(descriptionText);
        } else {
            descriptionTextView.setVisibility(GONE);
        }
    }

    /**
     * Returns the description text.
     *
     * @return the description text or {@code null}.
     */
    @Nullable
    public String getDescriptionText() {
        final CharSequence result = descriptionTextView.getText();
        if (null != result) {
            return result.toString();
        }
        return null;
    }

    /**
     * Sets the specified rating value.
     *
     * @param value the rating value.
     */
    public void setRatingValue(final int value) {
        ratingProgressBar.setProgress(value);
    }

    /**
     * Returns the rating value.
     *
     * @return the value.
     */
    public int getRatingValue() {
        return ratingProgressBar.getProgress();
    }

}
