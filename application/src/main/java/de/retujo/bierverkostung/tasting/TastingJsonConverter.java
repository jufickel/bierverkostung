/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.beer.BeerFactory;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * Converts a {@link Tasting} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
@AllNonnull
@Immutable
final class TastingJsonConverter extends DataEntityJsonConverter<Tasting> {

    @Immutable
    static final class JsonName {
        static final String DATE = "date";
        static final String BEER = "beer";
        static final String LOCATION = "location";
        static final String OPTICAL_APPEARANCE = "opticalAppearance";
        static final String SCENT = "scent";
        static final String TASTE = "taste";
        static final String FOOD_RECOMMENDATION = "foodRecommendation";
        static final String TOTAL_IMPRESSION_DESCRIPTION = "totalImpressionDescription";
        static final String TOTAL_IMPRESSION_RATING = "totalImpressionRating";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private TastingJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code TastingJsonConverter}.
     *
     * @return the instance.
     */
    public static TastingJsonConverter getInstance() {
        return new TastingJsonConverter();
    }

    @Override
    protected void putEntityValuesTo(final JSONObject targetJsonObject, final Tasting tasting) throws JSONException {
        targetJsonObject.put(JsonName.DATE, tasting.getDate());
        targetJsonObject.put(JsonName.BEER, tasting.getBeer().toJson());

        final Maybe<String> location = tasting.getLocation();
        if (location.isPresent()) {
            targetJsonObject.put(JsonName.LOCATION, location.get());
        }

        targetJsonObject.put(JsonName.OPTICAL_APPEARANCE, tasting.getOpticalAppearance().toJson());
        targetJsonObject.put(JsonName.SCENT, tasting.getScent().toJson());
        targetJsonObject.put(JsonName.TASTE, tasting.getTaste().toJson());

        final Maybe<String> foodRecommendation = tasting.getFoodRecommendation();
        if (foodRecommendation.isPresent()) {
            targetJsonObject.put(JsonName.FOOD_RECOMMENDATION, foodRecommendation.get());
        }

        final Maybe<String> totalImpressionDescription = tasting.getTotalImpressionDescription();
        if (totalImpressionDescription.isPresent()) {
            targetJsonObject.put(JsonName.TOTAL_IMPRESSION_DESCRIPTION, totalImpressionDescription.get());
        }

        targetJsonObject.put(JsonName.TOTAL_IMPRESSION_RATING, tasting.getTotalImpressionRating());
    }

    @Override
    protected Tasting createEntityInstanceFromJson(final JSONObject sourceJsonObject, final EntityCommonData commonData)
            throws JSONException {

        final String date = sourceJsonObject.getString(JsonName.DATE);
        final Beer beer = BeerFactory.newBeer(sourceJsonObject.getJSONObject(JsonName.BEER));
        final TastingBuilder tastingBuilder = TastingBuilder.newInstance(date, beer);
        tastingBuilder.commonData(commonData);

        if (sourceJsonObject.has(JsonName.LOCATION)) {
            tastingBuilder.location(sourceJsonObject.getString(JsonName.LOCATION));
        }

        tastingBuilder.opticalAppearance(getOpticalAppearance(sourceJsonObject));
        tastingBuilder.scent(getScent(sourceJsonObject));
        tastingBuilder.taste(getTaste(sourceJsonObject));

        if (sourceJsonObject.has(JsonName.FOOD_RECOMMENDATION)) {
            tastingBuilder.foodRecommendation(sourceJsonObject.getString(JsonName.FOOD_RECOMMENDATION));
        }

        if (sourceJsonObject.has(JsonName.TOTAL_IMPRESSION_DESCRIPTION)) {
            tastingBuilder.totalImpressionDescription(sourceJsonObject.getString(JsonName.TOTAL_IMPRESSION_DESCRIPTION));
        }

        tastingBuilder.totalImpressionRating(sourceJsonObject.getInt(JsonName.TOTAL_IMPRESSION_RATING));

        return tastingBuilder.build();
    }

    private static OpticalAppearance getOpticalAppearance(final JSONObject tastingJsonObject) throws JSONException {
        final OpticalAppearanceJsonConverter jsonConverter = OpticalAppearanceJsonConverter.getInstance();
        return jsonConverter.fromJson(tastingJsonObject.getJSONObject(JsonName.OPTICAL_APPEARANCE));
    }

    private static Scent getScent(final JSONObject tastingJsonObject) throws JSONException {
        final ScentJsonConverter jsonConverter = ScentJsonConverter.getInstance();
        return jsonConverter.fromJson(tastingJsonObject.getJSONObject(JsonName.SCENT));
    }

    private static Taste getTaste(final JSONObject tastingJsonObject) throws JSONException {
        final TasteJsonConverter jsonConverter = TasteJsonConverter.getInstance();
        return jsonConverter.fromJson(tastingJsonObject.getJSONObject(JsonName.TASTE));
    }

}
