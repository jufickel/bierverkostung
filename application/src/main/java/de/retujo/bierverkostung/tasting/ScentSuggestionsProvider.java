/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.tasting.ExpandableSuggestionListAdapter.Group;
import de.retujo.java.util.Provider;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * TODO Javadoc.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class ScentSuggestionsProvider implements Provider<ScentSuggestions> {

    private final AsyncTaskLoader<JSONObject> jsonResourceLoader;

    private ScentSuggestionsProvider(final AsyncTaskLoader<JSONObject> theJsonResourceLoader) {
        jsonResourceLoader = theJsonResourceLoader;
    }

    /**
     * Returns a new instance of {@code ScentSuggestionsProvider}.
     *
     * @param jsonResourceLoader provides the JSON object to be converted.
     * @return the instance.
     * @throws NullPointerException if {@code jsonResourceLoader} is {@code null}.
     */
    @Nonnull
    public static ScentSuggestionsProvider of(@Nonnull final AsyncTaskLoader<JSONObject> jsonResourceLoader) {
        return new ScentSuggestionsProvider(isNotNull(jsonResourceLoader, "JSON resource loader"));
    }

    @Nonnull
    @Override
    public ScentSuggestions get() {
        final JsonObjectToGroupsFunction jsonObjectToGroups =
                new JsonObjectToGroupsFunction(jsonResourceLoader.loadInBackground());

        return new ScentSuggestionsBuilder()
                .fruity(jsonObjectToGroups.apply("fruity", "citrus", "berry", "pip", "tropical", "artificial", "dried"))
                .flowery(jsonObjectToGroups.apply("flowery", "sweet_blossom", "pungent_blossom"))
                .vegetal(jsonObjectToGroups.apply("vegetal", "fresh", "boiled", "dried", "resinous"))
                .spicy(jsonObjectToGroups.apply("spicy", "pungent_spice", "sweet_spice", "nutty", "herbs"))
                .warmthMinted(jsonObjectToGroups.apply("warmth_minted", "caramelised", "smokey", "roast"))
                .biological(jsonObjectToGroups.apply("biological", "bakery", "animalistic", "lactic", "alcoholic"))
                .build();
    }

    /**
     * This class gets from a specified {@link JSONObject} a list of {@link Group}s.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private static final class JsonObjectToGroupsFunction {
        private final JSONObject jsonObject;

        private JsonObjectToGroupsFunction(final JSONObject theJsonObject) {
            jsonObject = theJsonObject;
        }

        /**
         *
         * @param parentName
         * @param groupNames
         * @return
         * @throws NullPointerException if any argument is {@code null}.
         */
        @Nullable
        public List<Group> apply(@Nonnull final String parentName, @Nonnull final String ... groupNames) {
            try {
                return getGroups(parentName, groupNames);
            } catch (final JSONException e) {
                throw new JsonObjectLoader.JsonResourceException("Failed to get groups from JSON object!", e);
            }
        }

        private List<Group> getGroups(final String parentName, final String ...
                groupNames) throws JSONException {
            final List<Group> result = new ArrayList<>(groupNames.length);
            final JSONObject parentJsonObject = jsonObject.getJSONObject(parentName);
            for (final String groupName : groupNames) {
                final JSONObject groupJsonObject = parentJsonObject.getJSONObject(groupName);
                final int position = result.size();
                result.add(getGroup(groupJsonObject, position));
            }
            return result;
        }

        private static Group getGroup(final JSONObject groupJsonObject, final int position)
                throws JSONException {
            return ExpandableSuggestionListAdapter.GroupBuilder.getInstance()
                    .position(position)
                    .localisedName(groupJsonObject.getString("name"))
                    .children(getCharacteristicsNames(groupJsonObject))
                    .build();
        }

        private static List<String> getCharacteristicsNames(final JSONObject groupJsonObject) throws JSONException {
            final JSONArray characteristics = groupJsonObject.getJSONArray("characteristics");
            final List<String> result = new ArrayList<>(characteristics.length());
            for (int i = 0; i < characteristics.length(); i++) {
                final JSONObject characteristicJsonObject = characteristics.getJSONObject(i);
                result.add(characteristicJsonObject.getString("name"));
            }
            return result;
        }
    }

    /**
     * An immutable implementation of {@link ScentSuggestions}.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ImmutableScentSuggestions implements ScentSuggestions {
        private final List<Group> fruityGroups;
        private final List<Group> floweryGroups;
        private final List<Group> vegetalGroups;
        private final List<Group> spicyGroups;
        private final List<Group> warmthMintedGroups;
        private final List<Group> biologicalGroups;

        private ImmutableScentSuggestions(final ScentSuggestionsBuilder builder) {
            fruityGroups = Collections.unmodifiableList(new ArrayList<>(builder.fruityGroups));
            floweryGroups = Collections.unmodifiableList(new ArrayList<>(builder.floweryGroups));
            vegetalGroups = Collections.unmodifiableList(new ArrayList<>(builder.vegetalGroups));
            spicyGroups = Collections.unmodifiableList(new ArrayList<>(builder.spicyGroups));
            warmthMintedGroups = Collections.unmodifiableList(new ArrayList<>(builder.warmthMintedGroups));
            biologicalGroups = Collections.unmodifiableList(new ArrayList<>(builder.biologicalGroups));
        }

        @Nonnull
        @Override
        public List<Group> getFruityGroups() {
            return fruityGroups;
        }

        @Nonnull
        @Override
        public List<Group> getFloweryGroups() {
            return floweryGroups;
        }

        @Nonnull
        @Override
        public List<Group> getVegetalGroups() {
            return vegetalGroups;
        }

        @Nonnull
        @Override
        public List<Group> getSpicyGroups() {
            return spicyGroups;
        }

        @Nonnull
        @Override
        public List<Group> getWarmthMintedGroups() {
            return warmthMintedGroups;
        }

        @Nonnull
        @Override
        public List<Group> getBiologicalGroups() {
            return biologicalGroups;
        }
    }

    /**
     * A mutable builder with a fluent API for an immutable {@code ScentSuggestions} instance.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private static final class ScentSuggestionsBuilder {
        private List<Group> fruityGroups = Collections.emptyList();
        private List<Group> floweryGroups = Collections.emptyList();
        private List<Group> vegetalGroups = Collections.emptyList();
        private List<Group> spicyGroups = Collections.emptyList();
        private List<Group> warmthMintedGroups = Collections.emptyList();
        private List<Group> biologicalGroups = Collections.emptyList();

        public ScentSuggestionsBuilder fruity(final List<Group> groups) {
            if (null != groups) {
                fruityGroups = groups;
            }
            return this;
        }

        public ScentSuggestionsBuilder flowery(final List<Group> groups) {
            if (null != groups) {
                floweryGroups = groups;
            }
            return this;
        }

        public ScentSuggestionsBuilder vegetal(final List<Group> groups) {
            if (null != groups) {
                vegetalGroups = groups;
            }
            return this;
        }

        public ScentSuggestionsBuilder spicy(final List<Group> groups) {
            if (null != groups) {
                spicyGroups = groups;
            }
            return this;
        }

        public ScentSuggestionsBuilder warmthMinted(final List<Group> groups) {
            if (null != groups) {
                warmthMintedGroups = groups;
            }
            return this;
        }

        public ScentSuggestionsBuilder biological(final List<Group> groups) {
            if (null != groups) {
                biologicalGroups = groups;
            }
            return this;
        }

        public ScentSuggestions build() {
            return new ImmutableScentSuggestions(this);
        }
    }

}
