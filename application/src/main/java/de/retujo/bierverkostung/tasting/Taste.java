/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcelable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.bierverkostung.data.ContentValuesRepresentable;
import de.retujo.java.util.Maybe;

/**
 * This interface represents the taste of a beer. Taste is everything which can be perceived within the mouth like
 * for example sweetness or carbonation.
 *
 * @since 1.0.0
 */
@Immutable
interface Taste extends ContentValuesRepresentable, Parcelable, Jsonable {

    /**
     * Returns the rating of the bitterness.
     *
     * @return the rating or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getBitternessRating();

    /**
     * Returns the rating of the sweetness.
     *
     * @return the rating or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getSweetnessRating();

    /**
     * Returns the rating of the acidity.
     *
     * @return the rating or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getAcidityRating();

    /**
     * Returns the description of the mouthfeel.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getMouthfeelDescription();

    /**
     * Returns the rating of the body fullness.
     *
     * @return the rating or an empty Maybe.
     */
    @Nonnull
    Maybe<Integer> getFullBodiedRating();

    /**
     * Returns the description of the body.
     *
     * @return the description or an empty Maybe.
     */
    @Nonnull
    Maybe<String> getBodyDescription();

    /**
     * Returns a tasting component of the aftertaste.
     *
     * @return the aftertaste component.
     */
    @Nonnull
    TastingComponent getAftertaste();

    /**
     * Indicates whether the taste was defined by the user.
     *
     * @return {@code true} if the user defined at least one taste element, {@code false} if none was defined.
     */
    boolean isEmpty();

}
