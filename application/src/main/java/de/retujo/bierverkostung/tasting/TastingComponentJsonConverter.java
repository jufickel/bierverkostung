/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.exchange.JsonConverter;
import de.retujo.java.util.Maybe;

/**
 * Converts a {@link TastingComponent} to a {@link JSONObject} and vice versa.
 *
 * @since 1.2.0
 */
final class TastingComponentJsonConverter extends JsonConverter<TastingComponent> {

    @Immutable
    private static final class JsonName {
        private static final String DESCRIPTION = "description";
        private static final String RATING = "rating";

        private JsonName() {
            throw new AssertionError();
        }
    }

    private TastingComponentJsonConverter() {
        super();
    }

    /**
     * Returns an instance of {@code TastingComponentJsonConverter}.
     *
     * @return the instance.
     */
    public static TastingComponentJsonConverter getInstance() {
        return new TastingComponentJsonConverter();
    }

    @Override
    protected void putAllValuesTo(@Nonnull final JSONObject targetJsonObject,
            @Nonnull final TastingComponent tastingComponent) throws JSONException {

        final Maybe<String> description = tastingComponent.getDescription();
        if (description.isPresent()) {
            targetJsonObject.put(JsonName.DESCRIPTION, description.get());
        }

        final Maybe<Integer> rating = tastingComponent.getRating();
        if (rating.isPresent()) {
            targetJsonObject.put(JsonName.RATING, rating.get());
        }
    }

    @Nonnull
    @Override
    protected TastingComponent createEntityInstanceFromJson(@Nonnull final JSONObject jsonObject) throws JSONException {
        String description = null;
        if (jsonObject.has(JsonName.DESCRIPTION)) {
            description = jsonObject.getString(JsonName.DESCRIPTION);
        }

        int rating = ImmutableTastingComponent.NULL_RATING;
        if (jsonObject.has(JsonName.RATING)) {
            rating = jsonObject.getInt(JsonName.RATING);
        }

        return ImmutableTastingComponent.of(description, rating);
    }

}
