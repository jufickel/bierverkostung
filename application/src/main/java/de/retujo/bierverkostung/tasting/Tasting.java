/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * This interface represents a beer tasting.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
interface Tasting extends DataEntity, Jsonable {

    /**
     * Returns the date of this tasting in ISO 8601 format.
     *
     * @return the date.
     */
    String getDate();

    /**
     * Returns the location of this tasting.
     *
     * @return the location of an empty Maybe.
     */
    Maybe<String> getLocation();

    /**
     * Returns the beer of this tasting.
     *
     * @return the beer.
     */
    Beer getBeer();

    /**
     * Returns the optical appearance of the tasted beer.
     *
     * @return the optical appearance.
     */
    OpticalAppearance getOpticalAppearance();

    /**
     * Returns the scent (flavour) of the tasted beer.
     *
     * @return the scent.
     */
    Scent getScent();

    /**
     * Returns the taste of the tasted beer.
     *
     * @return the taste.
     */
    Taste getTaste();

    /**
     * Returns the food recommendation for the tasted beer.
     *
     * @return the food recommendation or an empty Maybe.
     */
    Maybe<String> getFoodRecommendation();

    /**
     * Returns the description of the total impression of the beer.
     *
     * @return the description or an empty Maybe.
     */
    Maybe<String> getTotalImpressionDescription();

    /**
     * Returns the rating of the total impression of the beer.
     *
     * @return the rating.
     */
    int getTotalImpressionRating();

}
