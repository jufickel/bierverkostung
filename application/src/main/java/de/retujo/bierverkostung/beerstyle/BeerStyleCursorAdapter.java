/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractCursorAdapter;

/**
 * This CursorAdapter creates and binds {@link BeerStyleViewHolder}s, that hold the name of a Beer Style, to a
 * RecyclerView to efficiently display the data.
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
final class BeerStyleCursorAdapter extends AbstractCursorAdapter<BeerStyleViewHolder> {

    /**
     * Constructs a new {@code BeerStyleCursorAdapter} object.
     *
     * @param context the current context.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    public BeerStyleCursorAdapter(final Context context,
            @Nullable final View.OnClickListener onBeerStyleSelectedListener) {

        super(context, R.layout.beer_style_item, onBeerStyleSelectedListener);
    }

    @Nonnull
    @Override
    protected BeerStyleViewHolder doCreateViewHolder(final View view,
            @Nullable final View.OnClickListener onViewClickListener) {

        return new BeerStyleViewHolder(view, onViewClickListener);
    }

    @Override
    protected void doBindViewHolder(final BeerStyleViewHolder viewHolder, final Cursor cursor, final Context context) {
        final BeerStyle beerStyle = BeerStyleFactory.newBeerStyle(cursor);
        if (null != beerStyle) {
            viewHolder.setDomainObject(beerStyle);
        }
    }

}
