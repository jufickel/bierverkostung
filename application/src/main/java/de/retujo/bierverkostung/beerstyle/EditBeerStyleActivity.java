/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;
import de.retujo.bierverkostung.common.ButtonEnablingTextWatcher;
import de.retujo.bierverkostung.data.InsertHandler;
import de.retujo.bierverkostung.data.UpdateHandler;

/**
 * This activity allows to either add a new beer style or to edit an existing beer style.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class EditBeerStyleActivity extends BaseActivity {


    /**
     * Key to retrieve the newly created beer style from this Activity's result Intent.
     */
    public static final String EDITED_BEER_STYLE = "editedBeerStyle";

    /**
     * Key to set an existing BeerStyle to be edited to the Intent for starting this Activity.
     */
    public static final String EDIT_BEER_STYLE = "beerStyleToBeEdited";

    private TextView nameTextView;
    private Button saveButton;
    private Runnable saveAction;

    /**
     * Constructs a new {@code EditBeerStyleActivity} object.
     */
    public EditBeerStyleActivity() {
        nameTextView = null;
        saveButton = null;
        saveAction = null;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_beer_style);

        initSaveBeerStyleButton();
        initBeerStyleNameTextView();

        final Intent afferentIntent = getIntent();
        final Bundle extras = afferentIntent.getExtras();
        if (isAddNewBeerStyleActivity(extras)) {
            setTitle(getString(R.string.add_beer_style_activity_name));
            saveAction = new SaveNewBeerStyleAction();
        } else {
            setTitle(getString(R.string.edit_beer_style_activity_name));
            final BeerStyle beerStyleToBeChanged = afferentIntent.getParcelableExtra(EDIT_BEER_STYLE);
            nameTextView.setText(beerStyleToBeChanged.getName());
            saveAction = new SaveEditedBeerStyleAction(beerStyleToBeChanged);
        }
    }

    private void initSaveBeerStyleButton() {
        saveButton = findView(R.id.edit_beer_style_save_button);
        saveButton.setEnabled(false);
    }

    private void initBeerStyleNameTextView() {
        nameTextView = findView(R.id.edit_beer_style_name_edit_text);
        nameTextView.addTextChangedListener(new ButtonEnablingTextWatcher(saveButton));
    }

    private static boolean isAddNewBeerStyleActivity(final Bundle bundle) {
        return null == bundle || !bundle.containsKey(EDIT_BEER_STYLE);
    }

    /**
     * Persists the new or changed beer style.
     *
     * @param view is ignored by this method.
     */
    public void saveBeerStyle(final View view) {
        saveAction.run();
        finish();
    }

    private void setResult(final BeerStyle beerStyle) {
        final Intent resultIntent = new Intent();
        resultIntent.putExtra(EDITED_BEER_STYLE, beerStyle);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    /**
     * This class inserts a new BeerStyle into the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveNewBeerStyleAction implements Runnable {
        @Override
        public void run() {
            final InsertHandler<BeerStyle> insertHandler = InsertHandler.async(getContentResolver(),
                    EditBeerStyleActivity.this::setResult);
            insertHandler.startInsert(BeerStyleFactory.newBeerStyle(nameTextView.getText()));
        }
    }

    /**
     * This class updates an existing BeerStyle in the database.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class SaveEditedBeerStyleAction implements Runnable {
        private final BeerStyle beerStyleToBeChanged;

        private SaveEditedBeerStyleAction(final BeerStyle beerStyleToBeChanged) {
            this.beerStyleToBeChanged = beerStyleToBeChanged;
        }

        @Override
        public void run() {
            final UpdateHandler<BeerStyle> updateHandler = UpdateHandler.async(getContentResolver(),
                    EditBeerStyleActivity.this::setResult);
            updateHandler.startUpdate(beerStyleToBeChanged.setName(nameTextView.getText()));
        }
    }

}
