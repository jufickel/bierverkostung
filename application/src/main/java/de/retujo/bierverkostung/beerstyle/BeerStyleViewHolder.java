/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractViewHolder;
import de.retujo.bierverkostung.common.DeleteEntityDialogue;
import de.retujo.bierverkostung.data.DeleteDbEntityTask;
import de.retujo.java.util.Maybe;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} binds Beer Style-related view contents.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class BeerStyleViewHolder extends AbstractViewHolder<BeerStyle> implements PopupMenu.OnMenuItemClickListener {

    private final TextView beerStyleNameTextView;

    /**
     * Constructs a new {@code BeerStyleViewHolder} object.
     *
     * @param beerStyleItemView the view inflated in onCreateViewHolder.
     * @param onBeerStyleSelectedListener a listener to be notified when a particular beer style was clicked.
     * @throws NullPointerException if {@code beerStyleNameTextView} is {@code null}.
     */
    public BeerStyleViewHolder(@Nonnull final View beerStyleItemView,
            @Nullable final View.OnClickListener onBeerStyleSelectedListener) {
        super(beerStyleItemView);
        beerStyleItemView.setOnClickListener(onBeerStyleSelectedListener);
        beerStyleNameTextView = (TextView) beerStyleItemView.findViewById(R.id.beer_style_item_name_text_view);
        final ImageView overflowIcon =
                (ImageView) beerStyleItemView.findViewById(R.id.beer_style_item_context_menu_image_view);
        overflowIcon.setOnClickListener(v -> {
            final PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
            popupMenu.inflate(R.menu.entity_options_menu);
            popupMenu.setOnMenuItemClickListener(BeerStyleViewHolder.this);
            popupMenu.show();
        });
    }

    @Override
    protected void handleDomainObjectSet(@Nonnull final BeerStyle beerStyle) {
        beerStyleNameTextView.setText(beerStyle.getName());
    }

    @Override
    public boolean onMenuItemClick(final MenuItem item) {
        final int itemId = item.getItemId();
        final Context context = getContext();
        if (R.id.options_menu_edit == itemId) {
            final Maybe<BeerStyle> domainObject = getDomainObject();
            if (domainObject.isPresent()) {
                final Intent editBeerStyleIntent = new Intent(context, EditBeerStyleActivity.class);
                editBeerStyleIntent.putExtra(EditBeerStyleActivity.EDIT_BEER_STYLE, domainObject.get());
                context.startActivity(editBeerStyleIntent);
                return true;
            }
        } else if (R.id.options_menu_delete == itemId) {
            final Maybe<BeerStyle> beerStyleMaybe = getDomainObject();
            if (beerStyleMaybe.isPresent()) {
                final BeerStyle beerStyle = beerStyleMaybe.get();
                final DeleteEntityDialogue dialogue = DeleteEntityDialogue.getBuilder(context, beerStyle)
                        .setTitle(R.string.delete_beer_style_dialog_title)
                        .setMessage(MessageFormat.format(context.getString(R.string.delete_beer_style_dialog_message),
                                beerStyle.getName()))
                        .setOnDeleteEntityListener(
                                beerStyleToBeDeleted -> DeleteDbEntityTask.getInstance(context.getContentResolver())
                                        .execute(beerStyleToBeDeleted))
                        .build();
                dialogue.show();
                return true;
            }
        }

        return false;
    }

}
