/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.BaseActivity;

/**
 * This Activity shows a list of all persisted beer styles. The user has the possibility to add a new beer style or to
 * delete beer styles by swiping them off the list.
 * <p>
 * A short click on a beer style item finishes the Activity and sets the selected style as extra in the result Intent.
 * The key to obtain the selected beer style is <code>"{@value #SELECTED_BEER_STYLE}"</code>.
 *
 * @since 1.0.0
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
@NotThreadSafe
public final class SelectBeerStyleActivity extends BaseActivity {

    /**
     * Key to retrieve the selected beer style from this Activity's result Intent.
     */
    public static final String SELECTED_BEER_STYLE = "selectedBeerStyle";

    private static final int NEW_BEER_STYLE_REQUEST_CODE = 153;

    /**
     * Constructs a new {@code SelectBeerStyleActivity} object.
     */
    public SelectBeerStyleActivity() {
        super();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_beer_style);

        final Fragment selectBeerStyleFragment = SelectBeerStyleFragment.newInstance(view -> {
            final BeerStyle beerStyle = (BeerStyle) view.getTag();
            finishWithResult(beerStyle);
        });
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_select_beer_style_container, selectBeerStyleFragment)
                .commit();
    }

    @Override
    public void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // This overwrites the default OnClickListener which was already registered by the Fragment.
        initAddBeerStyleButton();
    }

    private void initAddBeerStyleButton() {
        final FloatingActionButton addBeerStyleButton = findView(R.id.select_beer_style_fab_new_beer_style);
        addBeerStyleButton.setOnClickListener(v -> {
            final Intent addBeerStyleIntent = new Intent(SelectBeerStyleActivity.this, EditBeerStyleActivity.class);
            startActivityForResult(addBeerStyleIntent, NEW_BEER_STYLE_REQUEST_CODE);
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (NEW_BEER_STYLE_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode) {
            final BeerStyle newBeerStyle = data.getParcelableExtra(EditBeerStyleActivity.EDITED_BEER_STYLE);
            finishWithResult(newBeerStyle);
        }
    }

    private void finishWithResult(final BeerStyle selectedBeerStyle) {
        final Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_BEER_STYLE, selectedBeerStyle);
        setResult(Activity.RESULT_OK, returnIntent);

        finish();
    }

}
