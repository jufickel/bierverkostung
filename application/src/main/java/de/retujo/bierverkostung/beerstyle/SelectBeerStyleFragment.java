/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.R;
import de.retujo.bierverkostung.common.AbstractItemSwipeHandler;
import de.retujo.bierverkostung.common.AbstractLoaderCallbacks;
import de.retujo.bierverkostung.common.HideFabOnScrollListener;

/**
 * This Fragment shows a list of all persisted beer styles. The user has the possibility to add a new beer style and to
 * delete or edit beer styles by swiping them.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public class SelectBeerStyleFragment extends Fragment {

    private FloatingActionButton addBeerStyleButton;
    private RecyclerView beerStylesRecyclerView;
    private View.OnClickListener onBeerStyleSelectedListener;

    /**
     * Creates a new {@code SelectBeerStyleFragment} object.
     */
    public SelectBeerStyleFragment() {
        addBeerStyleButton = null;
        beerStylesRecyclerView = null;
    }

    /**
     * @param onBeerStyleSelectedListener a listener to be notified if a beer style item is selected or {@code null}.
     * If no listener is specified nothing will happen if a beer style is selected.
     * @return the instance.
     */
    @Nonnull
    public static SelectBeerStyleFragment newInstance(
            @Nullable final View.OnClickListener onBeerStyleSelectedListener) {
        final SelectBeerStyleFragment result = new SelectBeerStyleFragment();
        result.onBeerStyleSelectedListener = onBeerStyleSelectedListener;
        return result;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.fragment_select_beer_style, null);

        initAddBeerStyleButton(result);
        final BeerStyleCursorAdapter cursorAdapter =
                new BeerStyleCursorAdapter(getContext(), onBeerStyleSelectedListener);
        final AbstractLoaderCallbacks cursorLoaderCallbacks = initBeerStylesLoader(cursorAdapter);
        initBeerStylesRecyclerView(result, cursorAdapter, cursorLoaderCallbacks);

        return result;
    }

    private void initAddBeerStyleButton(final View parentView) {
        addBeerStyleButton = (FloatingActionButton) parentView.findViewById(R.id.select_beer_style_fab_new_beer_style);
        addBeerStyleButton.setOnClickListener(v -> {
            final Intent addBeerStyleIntent = new Intent(getContext(), EditBeerStyleActivity.class);
            startActivity(addBeerStyleIntent);
        });
    }

    private AbstractLoaderCallbacks initBeerStylesLoader(final BeerStyleCursorAdapter cursorAdapter) {
        final BeerStyleLoaderCallbacks result = new BeerStyleLoaderCallbacks(getContext(), cursorAdapter);
        final LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(result.getId(), null, result);
        return result;
    }

    private void initBeerStylesRecyclerView(final View parentView, final BeerStyleCursorAdapter cursorAdapter,
            final AbstractLoaderCallbacks cursorLoaderCallbacks) {
        beerStylesRecyclerView = (RecyclerView) parentView.findViewById(R.id.select_beer_style_recycler_view);
        beerStylesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        beerStylesRecyclerView.setAdapter(cursorAdapter);
        beerStylesRecyclerView.addOnScrollListener(HideFabOnScrollListener.of(addBeerStyleButton));

        final SimpleCallback deleteBeerStyleOnSwipeHandler = new BeerStyleItemSwipeHandler(cursorLoaderCallbacks);
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(deleteBeerStyleOnSwipeHandler);
        itemTouchHelper.attachToRecyclerView(beerStylesRecyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Without the following code the item slot would be empty after starting edit activity but not saving.
        final RecyclerView.Adapter beersRecyclerViewAdapter = beerStylesRecyclerView.getAdapter();
        beersRecyclerViewAdapter.notifyDataSetChanged();
    }

    /**
     * This class deletes a beer style when the user swipes it right. Beforehand the deletion has to be verified
     * by the user; this is done with an AlertDialog. If the beer style item is swiped left an activity for editing
     * is opened.
     *
     * @since 1.0.0
     */
    @NotThreadSafe
    private final class BeerStyleItemSwipeHandler extends AbstractItemSwipeHandler<BeerStyle> {
        /**
         * Constructs a new {@code BeerStyleItemSwipeHandler} object.
         *
         * @param loaderCallbacks are called when a beer style gets deleted.
         * @throws NullPointerException if {@code loaderCallbacks} is {@code null}.
         */
        private BeerStyleItemSwipeHandler(@Nonnull final AbstractLoaderCallbacks loaderCallbacks) {
            super(getContext(), getLoaderManager(), loaderCallbacks);
        }

        @Override
        protected int getDialogTitle() {
            return R.string.delete_beer_style_dialog_title;
        }

        @Nonnull
        @Override
        protected String getDialogMessage(@Nonnull final BeerStyle beerStyle) {
            return MessageFormat.format(getString(R.string.delete_beer_style_dialog_message), beerStyle.getName());
        }
    }

}
