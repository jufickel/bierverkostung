/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.BaseParcelableCreator;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.data.Table;
import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;
import de.retujo.bierverkostung.data.Revision;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Immutable implementation of {@link BeerStyle}.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
final class ImmutableBeerStyle implements BeerStyle {

    /**
     * Creator which creates instances of {@code ImmutableBeerStyle} from a Parcel.
     */
    public static final Parcelable.Creator<BeerStyle> CREATOR = new ImmutableBeerStyleCreator();

    private static final Table TABLE = BeerStyleEntry.TABLE;

    private final EntityCommonData commonData;
    private final String name;

    private ImmutableBeerStyle(final EntityCommonData theCommonData, final CharSequence theName) {
        commonData = isNotNull(theCommonData, "common data");
        name = argumentNotEmpty(theName, "name").toString();
    }

    /**
     * Returns a new instance of {@code ImmutableBeerStyle} with the given common data and name.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param name the name of the beer style.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     */
    public static ImmutableBeerStyle newInstance(final EntityCommonData commonData, final CharSequence name) {
        return new ImmutableBeerStyle(commonData, name);
    }

    @Override
    public UUID getId() {
        return commonData.getId();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BeerStyle setName(final CharSequence newName) {
        if (ObjectUtil.areEqual(newName, name)) {
            return this;
        }
        return newInstance(commonData.incrementRevision(), newName);
    }

    @Override
    public Uri getContentUri() {
        return commonData.getContentUri(TABLE);
    }

    @Override
    public Revision getRevision() {
        return commonData.getRevision();
    }

    @Override
    public ContentValues asContentValues() {
        final ContentValues result = commonData.asContentValues(TABLE);
        result.put(BeerStyleEntry.COLUMN_NAME.toString(), name);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        commonData.writeToParcel(dest, flags);
        dest.writeString(name);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ImmutableBeerStyle that = (ImmutableBeerStyle) o;
        return ObjectUtil.areEqual(commonData, that.commonData) && ObjectUtil.areEqual(name, that.name);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(commonData, name);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + commonData + ", name='" + name + '\'' + "}";
    }

    @Nonnull
    @Override
    public JSONObject toJson() {
        final BeerStyleJsonConverter converter = BeerStyleJsonConverter.getInstance();
        return converter.toJson(this);
    }

    /**
     * This class creates instances of {@code ImmutableBeerStyle} from a Parcel.
     *
     * @since 1.0.0
     */
    @AllNonnull
    @Immutable
    private static final class ImmutableBeerStyleCreator extends BaseParcelableCreator<BeerStyle> {
        @Override
        protected BeerStyle createFromParcel(final Parcel source, final EntityCommonData commonData) {
            return newInstance(commonData, source.readString());
        }

        @Override
        public BeerStyle[] newArray(final int size) {
            return new BeerStyle[0];
        }
    }

}
