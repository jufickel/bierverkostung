/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import de.retujo.bierverkostung.data.DataEntity;
import de.retujo.bierverkostung.exchange.Jsonable;
import de.retujo.java.util.AllNonnull;

/**
 * This interface represents a beer style.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface BeerStyle extends DataEntity, Jsonable {

    /**
     * Returns the name of this beer style.
     *
     * @return the name.
     */
    String getName();

    /**
     * Sets the specified name to a copy of this {@code BeerStyle} object.
     *
     * @param newName the name to be set.
     * @return a copy of this BeerStyle object with {@code newName} set or this BeerStyle object if it already has the
     * same name.
     * @throws NullPointerException if {@code newName} is {@code null}.
     * @throws IllegalArgumentException if {@code newName} is empty.
     */
    BeerStyle setName(CharSequence newName);

}
