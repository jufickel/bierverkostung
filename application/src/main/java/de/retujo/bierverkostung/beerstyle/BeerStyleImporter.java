/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.Selection;
import de.retujo.bierverkostung.exchange.DataEntityImporter;
import de.retujo.java.util.AllNonnull;

/**
 * Imports an external BeerStyle or returns or updates an existing BeerStyle.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class BeerStyleImporter extends DataEntityImporter<BeerStyle> {

    private BeerStyleImporter(final ContentResolver contentResolver) {
        super(contentResolver, BeerStyleEntry.TABLE, BeerStyleFactory::newBeerStyle);
    }

    /**
     * Returns an instance of {@code BeerStyleImporter}.
     *
     * @param contentResolver the ContentResolver for querying existing BeerStyles or inserting new ones.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static BeerStyleImporter getInstance(final ContentResolver contentResolver) {
        return new BeerStyleImporter(contentResolver);
    }

    @Override
    protected Selection selectSameProperties(@Nonnull final BeerStyle externalBeerStyle) {
        return Selection.where(BeerStyleEntry.COLUMN_NAME).is(externalBeerStyle.getName()).build();
    }

}
