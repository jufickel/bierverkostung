/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beerstyle;

import android.database.Cursor;

import org.json.JSONObject;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.CursorReader;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.java.util.AllNonnull;
import de.retujo.bierverkostung.data.Revision;

/**
 * A factory for creating {@link BeerStyle} instances.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
public final class BeerStyleFactory {

    private BeerStyleFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a new immutable instance of {@link BeerStyle} with the given name.
     *
     * @param beerStyleName the name of the beer style.
     * @return the instance.
     * @throws NullPointerException if {@code beerStyleName} is {@code null}.
     * @throws IllegalArgumentException if {@code beerStyleName} is empty.
     */
    public static BeerStyle newBeerStyle(final CharSequence beerStyleName) {
        return ImmutableBeerStyle.newInstance(EntityCommonData.getInstance(), beerStyleName);
    }

    /**
     * Returns a new immutable instance of {@link BeerStyle} with the given name.
     *
     * @param commonData the common data like row ID, UUID and timestamp.
     * @param beerStyleName the name of the beer style.
     * @return the instance.
     * @throws NullPointerException if {@code beerStyleName} is {@code null}.
     * @throws IllegalArgumentException if {@code beerStyleName} is empty.
     */
    public static BeerStyle newBeerStyle(final EntityCommonData commonData, final CharSequence beerStyleName) {
        return ImmutableBeerStyle.newInstance(commonData, beerStyleName);
    }

    /**
     * Returns a new immutable instance of {@link BeerStyle} from the specified cursor.
     *
     * @param cursor the cursor to provide the brewery data.
     * @return the instance or {@code null}.
     * @throws IllegalArgumentException if {@code cursor} did not contain all expected columns.
     */
    @Nullable
    public static BeerStyle newBeerStyle(@Nullable final Cursor cursor) {
        if (null == cursor || 0 == cursor.getCount()) {
            return null;
        }

        final CursorReader cr = CursorReader.of(cursor);

        final String idAsString = cr.getString(BeerStyleEntry.COLUMN_ID);
        if (null != idAsString) {
            final Revision revision = Revision.of(cr.getInt(BeerStyleEntry.COLUMN_REVISION));
            final String name = cr.getString(BeerStyleEntry.COLUMN_NAME);

            return newBeerStyle(EntityCommonData.getInstance(UUID.fromString(idAsString), revision), name);
        }
        return null;
    }

    /**
     * Returns a new immutable instance of {@link BeerStyle} from the specified {@link JSONObject}.
     *
     * @param jsonObject the JSON object to be converted to a BeerStyle.
     * @return the beer style.
     * @throws NullPointerException if {@code jsonObject} is {@code null}.
     * @throws de.retujo.bierverkostung.exchange.JsonConversionException if {@code jsonObject} cannot be converted to
     * a BeerStyle.
     * @since 1.2.0
     */
    public static BeerStyle newBeerStyle(final JSONObject jsonObject) {
        final BeerStyleJsonConverter converter = BeerStyleJsonConverter.getInstance();
        return converter.fromJson(jsonObject);
    }

}
