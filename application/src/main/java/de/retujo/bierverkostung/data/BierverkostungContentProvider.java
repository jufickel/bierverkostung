/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoFileEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.java.util.Logger;
import de.retujo.java.util.Maybe;
import de.retujo.java.util.Provider;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class provides content to the application by communicating with the database. It operates on a low level of
 * abstraction, thus domain logic has to be implemented on a higher abstraction level.
 *
 * @since 1.0.0
 */
public final class BierverkostungContentProvider extends ContentProvider {

    private static final Logger LOGGER = Logger.forSimpleClassName(BierverkostungContentProvider.class);

    private SQLiteOpenHelper dbHelper;

    /**
     * Constructs a new {@code BierverkostungContentProvider} object.
     */
    public BierverkostungContentProvider() {
        dbHelper = null;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new BierverkostungDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@Nonnull final Uri uri,
            final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final BierverkostungUriMatcher uriMatcher = BierverkostungUriMatcher.getInstance();
        final Cursor result;
        switch (uriMatcher.match(uri)) {
            case BEERS:
                result = queryBeers(projection, selection, selectionArgs, sortOrder);
                break;
            case BEER_STYLES:
                result = query(BeerStyleEntry.TABLE, projection, selection, selectionArgs, sortOrder);
                break;
            case BREWERIES:
                result = queryBreweries(projection, selection, selectionArgs, sortOrder);
                break;
            case COUNTRIES:
                result = query(CountryEntry.TABLE, projection, selection, selectionArgs, sortOrder);
                break;
            case SINGLE_COLUMN_VALUES:
                final String tableName = getTableName(uri);
                final String columnName = getFirstColumnName(projection);
                result = queryDistinctColumnValues(DefaultTable.getInstance(uri, tableName), columnName);
                break;
            case BEER_PHOTO_FILES:
                result = query(BeerPhotoFileEntry.TABLE, projection, selection, selectionArgs, sortOrder);
                break;
            case BEER_PHOTOS:
                result = queryBeerPhotos(projection, selection, selectionArgs, sortOrder);
                break;
            case TASTINGS:
                result = queryTastings(projection, selection, selectionArgs, sortOrder);
                break;
            default:
                throw new UnsupportedOperationException(MessageFormat.format("Unknown URI <{0}> for query!", uri));
        }

        final Context context = getContext();
        if (null != result && null != context) {
            final ContentResolver contentResolver = context.getContentResolver();
            result.setNotificationUri(contentResolver, uri);
        }

        return result;
    }

    private Cursor queryBeers(final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final String sqlQuery = QuerySqlBuilder.table(BeerEntry.TABLE)
                .leftOuterJoin(BreweryEntry.TABLE).on(BeerEntry.COLUMN_BREWERY_ID, BreweryEntry.COLUMN_ID)
                .leftOuterJoin(CountryEntry.TABLE).on(BreweryEntry.COLUMN_COUNTRY_ID, CountryEntry.COLUMN_ID)
                .leftOuterJoin(BeerStyleEntry.TABLE).on(BeerEntry.COLUMN_STYLE_ID, BeerStyleEntry.COLUMN_ID)
                .projection(projection)
                .selection(selection)
                .sortOrder(sortOrder)
                .toString();

        return query(sqlQuery, selectionArgs);
    }

    @Nullable
    private Cursor query(final String sqlQuery, @Nullable final String[] selectionArgs) {
        final Cursor result = dbHelper.getReadableDatabase().rawQuery(sqlQuery, selectionArgs);
        if (null != result) {
            result.moveToFirst();
        }
        return result;
    }

    private Cursor queryBeerPhotos(final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final String sqlQuery = QuerySqlBuilder.table(BeerPhotoEntry.TABLE)
                .leftOuterJoin(BeerPhotoFileEntry.TABLE)
                .on(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID, BeerPhotoFileEntry.COLUMN_ID)
                .projection(projection)
                .selection(selection)
                .sortOrder(sortOrder)
                .toString();

        return query(sqlQuery, selectionArgs);
    }

    private Cursor queryBreweries(final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final String sqlQuery = QuerySqlBuilder.table(BreweryEntry.TABLE)
                .leftOuterJoin(CountryEntry.TABLE).on(BreweryEntry.COLUMN_COUNTRY_ID, CountryEntry.COLUMN_ID)
                .projection(projection)
                .selection(selection)
                .sortOrder(sortOrder)
                .toString();

        return query(sqlQuery, selectionArgs);
    }

    private Cursor query(final Table table,
            @Nullable final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final String sqlQuery = QuerySqlBuilder.table(table)
                .projection(projection)
                .selection(selection)
                .sortOrder(sortOrder)
                .toString();

        return query(sqlQuery, selectionArgs);
    }

    private static String getTableName(final Uri contentUri) {
        final List<String> pathSegments = contentUri.getPathSegments();
        isNotNull(pathSegments, "path segments of the Content URI");
        final int minPathSegmentSize = 2;
        final int pathSegmentsSize = pathSegments.size();
        if (minPathSegmentSize > pathSegmentsSize) {
            final String pattern = "The Content URI must have at least <{0}> path segments! (<{1}>)";
            throw new IllegalArgumentException(MessageFormat.format(pattern, minPathSegmentSize, pathSegments));
        }
        // Returns the last but one path segment.
        return pathSegments.get(pathSegmentsSize - minPathSegmentSize);
    }

    private static String getFirstColumnName(final String[] columnNames) {
        isNotNull(columnNames, "column names array");
        if (0 == columnNames.length) {
            throw new IllegalArgumentException("The column names array must not be empty");
        }
        return columnNames[0];
    }

    private Cursor queryDistinctColumnValues(final Table table, final String columnName) {
        final String querySql = QuerySqlBuilder.table(table)
                .projection(table.createStringColumn(columnName))
                .distinct()
                .toString();

        return query(querySql, null);
    }

    private Cursor queryTastings(final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {

        final String querySql = QuerySqlBuilder.table(TastingEntry.TABLE)
                .leftOuterJoin(BeerEntry.TABLE).on(TastingEntry.COLUMN_BEER_ID, BeerEntry.COLUMN_ID)
                .leftOuterJoin(BreweryEntry.TABLE).on(BeerEntry.COLUMN_BREWERY_ID, BreweryEntry.COLUMN_ID)
                .leftOuterJoin(CountryEntry.TABLE).on(BreweryEntry.COLUMN_COUNTRY_ID, CountryEntry.COLUMN_ID)
                .leftOuterJoin(BeerStyleEntry.TABLE).on(BeerEntry.COLUMN_STYLE_ID, BeerStyleEntry.COLUMN_ID)
                .projection(projection)
                .selection(selection)
                .sortOrder(sortOrder)
                .toString();

        return query(querySql, selectionArgs);
    }

    @Override
    @Nullable
    public String getType(@Nonnull final Uri uri) {
        return null;
    }

    @Override
    @Nullable
    public Uri insert(@Nonnull final Uri uri, final ContentValues values) {
        final BierverkostungUriMatcher uriMatcher = BierverkostungUriMatcher.getInstance();
        switch (uriMatcher.match(uri)) {
            case BEER_WITH_ID:
                return insertIntoDb(BeerEntry.TABLE, values);
            case BEER_PHOTO_FILE_WITH_ID:
                return insertIntoDb(BeerPhotoFileEntry.TABLE, values);
            case BEER_PHOTO_WITH_ID:
                return insertIntoDb(BeerPhotoEntry.TABLE, values);
            case BEER_STYLE_WITH_ID:
                return insertIntoDb(BeerStyleEntry.TABLE, values);
            case BREWERY_WITH_ID:
                return insertIntoDb(BreweryEntry.TABLE, values);
            case COUNTRY_WITH_ID:
                return insertIntoDb(CountryEntry.TABLE, values);
            case TASTING_WITH_ID:
                return insertIntoDb(TastingEntry.TABLE, values);
            default:
                throw new UnsupportedOperationException(MessageFormat.format("Unknown URI <{0}> for insertion!", uri));
        }
    }

    private Uri insertIntoDb(final Table table, final ContentValues values) {
        try {
            return tryToInsertIntoDB(table, values);
        } catch (final SQLiteConstraintException e) {
            final Provider<Uri> handler =
                    SQLiteConstraintExceptionHandler.getInstance(table, values, e, dbHelper.getReadableDatabase());
            return handler.get();
        }
    }

    private Uri tryToInsertIntoDB(final Table table, final ContentValues values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final long id = db.insertOrThrow(table.toString(), null, values);
        if (0 < id) {
            final Uri result = table.getContentUri()
                    .buildUpon()
                    .appendPath(values.getAsString(BaseColumns._ID))
                    .build();
            notifyContentResolverAboutChange(result);
            LOGGER.debug("Inserted into table <{0}>: <{1}>", table, values);
            return result;
        } else {
            throw new android.database.SQLException("Failed to insert row into <" + table.getContentUri() + ">!");
        }
    }

    private void notifyContentResolverAboutChange(final Uri uri) {
        final Context context = getContext();
        if (null != context) {
            final ContentResolver contentResolver = context.getContentResolver();
            contentResolver.notifyChange(uri, null);
        }
    }

    @Override
    public int delete(@Nonnull final Uri uri, final String selection, final String[] selectionArgs) {
        final BierverkostungUriMatcher uriMatcher = BierverkostungUriMatcher.getInstance();
        switch (uriMatcher.match(uri)) {
            case BEER_WITH_ID:
                return deleteFromDbIfPossible(uri, BeerEntry.TABLE, TastingEntry.COLUMN_BEER_ID);
            case BEER_PHOTO_WITH_ID:
                return deleteFromDb(uri, BeerPhotoEntry.TABLE);
            case BEER_PHOTOS:
                return deleteFromDb(BeerPhotoEntry.TABLE, selection, selectionArgs);
            case BEER_STYLE_WITH_ID:
                return deleteFromDb(uri, BeerStyleEntry.TABLE);
            case BREWERY_WITH_ID:
                return deleteFromDbIfPossible(uri, BreweryEntry.TABLE, BeerEntry.COLUMN_BREWERY_ID);
            case COUNTRY_WITH_ID:
                return deleteFromDb(uri, CountryEntry.TABLE);
            case BEER_PHOTO_FILE_WITH_ID:
                return deleteFromDbIfPossible(uri, BeerPhotoFileEntry.TABLE, BeerPhotoEntry.COLUMN_PHOTO_FILE_ID);
            case BEER_PHOTO_FILES:
                return deleteFromDb(BeerPhotoFileEntry.TABLE, selection, selectionArgs);
            case TASTING_WITH_ID:
                return deleteFromDb(uri, TastingEntry.TABLE);
            default:
                throw new UnsupportedOperationException("Unknown URI <" + uri + "> for deletion!");
        }
    }

    private int deleteFromDbIfPossible(final Uri uri, final Table deleteFromTable, final Column countColumn) {
        final UriToIdFunction uriToId = UriToIdFunction.getInstance();
        final Maybe<UUID> idMaybe = uriToId.apply(uri);
        if (idMaybe.isPresent()) {
            final long amountOfAfferentReferences = count(countColumn, idMaybe.get());
            if (0 == amountOfAfferentReferences) {
                LOGGER.debug("Deleted entry for URI <{0}> as it had no afferent references.", uri);
                return deleteFromDb(idMaybe.get(), deleteFromTable, uri);
            }
            LOGGER.debug("Did not delete entry for URI <{0}> as it is required by <{1}> other entities!", uri,
                    amountOfAfferentReferences);
        }
        return 0;
    }

    private long count(final Column column, final UUID targetMatchingId) {
        final Selection selection = Selection.where(column).is(targetMatchingId).build();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();

        return DatabaseUtils.queryNumEntries(db, column.getTable().getName(), selection.getSelectionString(),
                selection.getSelectionArgs());
    }

    private int deleteFromDb(final UUID id, final CharSequence tableName, final Uri uri) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int result = db.delete(tableName.toString(), "_id=?", new String[]{String.valueOf(id)});
        if (0 < result) {
            notifyContentResolverAboutChange(uri);
        }
        return result;
    }

    private int deleteFromDb(final Uri uri, final Table table) {
        int result = 0;
        final UriToIdFunction uriToId = UriToIdFunction.getInstance();
        final Maybe<UUID> idMaybe = uriToId.apply(uri);
        if (idMaybe.isPresent()) {
            final Column idColumn = table.getColumnOrThrow(BaseColumns._ID);
            final Selection selection = Selection.where(idColumn).is(idMaybe.get()).build();
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            result = db.delete(table.getName(), selection.getSelectionString(), selection.getSelectionArgs());
        }

        if (0 < result) {
            LOGGER.debug("Deleted <{0}> rows from database for URI <{1}>.", result, uri);
            notifyContentResolverAboutChange(uri);
        }
        return result;
    }

    private int deleteFromDb(final Table table, final String selection, final String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int result = db.delete(table.getName(), selection, selectionArgs);
        if (0 < result) {
            LOGGER.debug("Deleted <{0}> rows from table <{1}> for selection args <{2}>.", result, table,
                    Arrays.toString(selectionArgs));
        }
        return result;
    }

    @Override
    public int update(@Nonnull final Uri uri,
            final ContentValues values,
            final String selection,
            final String[] selectionArgs) {

        final BierverkostungUriMatcher uriMatcher = BierverkostungUriMatcher.getInstance();
        switch (uriMatcher.match(uri)) {
            case BEER_WITH_ID:
                return updateDb(uri, BeerEntry.TABLE, values);
            case BEER_PHOTO_WITH_ID:
                return updateDb(uri, BeerPhotoEntry.TABLE, values);
            case BEER_PHOTO_FILE_WITH_ID:
                return updateDb(uri, BeerPhotoFileEntry.TABLE, values);
            case BEER_STYLE_WITH_ID:
                return updateDb(uri, BeerStyleEntry.TABLE, values);
            case BREWERY_WITH_ID:
                return updateDb(uri, BreweryEntry.TABLE, values);
            case COUNTRY_WITH_ID:
                return updateDb(uri, CountryEntry.TABLE, values);
            case TASTING_WITH_ID:
                return updateDb(uri, TastingEntry.TABLE, values);
            default:
                throw new UnsupportedOperationException(MessageFormat.format("Unknown URI <{0}> for update!", uri));
        }
    }

    private int updateDb(final Uri uri, final Table table, final ContentValues values) {
        final Selection s = Selection.where(table.getColumnOrThrow(BaseColumns._ID))
                .is(values.getAsString(BaseColumns._ID))
                .build();

        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int result = db.update(table.toString(), values, s.getSelectionString(), s.getSelectionArgs());

        if (0 < result) {
            notifyContentResolverAboutChange(uri);
            LOGGER.debug("Updated <{0}>: <{1}>", uri, values);
        }
        return result;
    }

}
