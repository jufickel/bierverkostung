/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import javax.annotation.concurrent.Immutable;

/**
 * An enumeration of all known versions of Bierverkostung's database.
 *
 * @since 1.2.0
 */
@Immutable
final class DbVersion {

    /**
     * The initial database schema.
     *
     * @since 1.1.0
     */
    public static final int V_1 = 1;

    /**
     * The same as {@link #V_1} but with an additional column in Beer table for image names.
     *
     * @since 1.2.0
     */
    public static final int V_2 = 2;

    private DbVersion() {
        throw new AssertionError();
    }

}
