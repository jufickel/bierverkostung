/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class wraps an {@link ArrayAdapter}. Its (String) values are the distinct selection if a certain table column.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class DbColumnArrayAdapter extends BaseAdapter implements Filterable {

    private static final int RESOURCE = android.R.layout.simple_dropdown_item_1line;

    private final ArrayAdapter<String> arrayAdapter;

    private DbColumnArrayAdapter(final ArrayAdapter<String> theArrayAdapter) {
        arrayAdapter = theArrayAdapter;
    }

    /**
     * Returns a new instance of {@code DbColumnArrayAdapter} for loading the distinct values of a table column. Each
     * column cell could contain more than one value, delimited by {@value ColumnValuesLoader#DELIMITER}.
     * Duplicates are consequently removed.
     *
     * @param context provides the application context for the used CursorLoader.
     * @param contentUri the URI for the {@code content://} scheme to determine the content to be retrieved.
     * @param column the column the load the values from.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    @Nonnull
    public static DbColumnArrayAdapter getInstance(@Nonnull final Context context, @Nonnull final Uri contentUri,
            @Nonnull final Column<String> column) {

        return newInstance(context, contentUri, column);
    }

    private static DbColumnArrayAdapter newInstance(@Nonnull final Context context,
            final Uri contentUri, @Nonnull final Column<String> column) {

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, RESOURCE, new ArrayList<>());
        final ColumnValuesLoader columnValuesLoader = new ColumnValuesLoader(context, contentUri, column, arrayAdapter);
        columnValuesLoader.execute();

        return new DbColumnArrayAdapter(arrayAdapter);
    }

    @Override
    public boolean hasStableIds() {
        return arrayAdapter.hasStableIds();
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        return arrayAdapter.getView(position, convertView, parent);
    }

    @Override
    public void registerDataSetObserver(final DataSetObserver observer) {
        arrayAdapter.registerDataSetObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(final DataSetObserver observer) {
        arrayAdapter.unregisterDataSetObserver(observer);
    }

    @Override
    public int getCount() {
        return arrayAdapter.getCount();
    }

    @Override
    public Object getItem(final int position) {
        return arrayAdapter.getItem(position);
    }

    @Override
    public long getItemId(final int position) {
        return arrayAdapter.getItemId(position);
    }

    @Override
    public void notifyDataSetInvalidated() {
        arrayAdapter.notifyDataSetInvalidated();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return arrayAdapter.areAllItemsEnabled();
    }

    @Override
    public boolean isEnabled(final int position) {
        return arrayAdapter.isEnabled(position);
    }

    @Override
    public int getItemViewType(final int position) {
        return arrayAdapter.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return arrayAdapter.getViewTypeCount();
    }

    @Override
    public boolean isEmpty() {
        return arrayAdapter.isEmpty();
    }

    @Override
    public Filter getFilter() {
        return arrayAdapter.getFilter();
    }

    /**
     * Asynchronously fills an ArrayAdapter with distinct values of a single database table column.
     *
     * @since 1.0.0
     */
    @ParametersAreNonnullByDefault
    private static final class ColumnValuesLoader extends AsyncTask<Void, Void, Void> {

        /**
         * This regex is used to split concatenated values of a column cell.
         */
        static final String DELIMITER = ",\\s*";

        private final ContentResolver contentResolver;
        private final Uri contentUri;
        private final Column<?> column;
        private final ArrayAdapter<String> arrayAdapter;

        /**
         * Constructs a new {@code ColumnValuesLoader} object.
         *
         * @param context provides the application context for the used CursorLoader.
         * @param contentUri the URI for the {@code content://} scheme to determine the content to be retrieved.
         * @param column the column the load the values from.
         * @param arrayAdapter the ArrayAdapter to asynchronously filled.
         * @throws NullPointerException if any argument is {@code null}.
         */
        private ColumnValuesLoader(final Context context,
                final Uri contentUri,
                final Column<String> column,
                final ArrayAdapter<String> arrayAdapter) {

            isNotNull(context, "current Context");
            contentResolver = context.getContentResolver();
            this.contentUri = isNotNull(contentUri, "content URI");
            this.column = isNotNull(column, "column");
            this.arrayAdapter = isNotNull(arrayAdapter, "ArrayAdapter");
        }

        @Override
        public Void doInBackground(final Void... params) {
            try (final Cursor cursor = loadColumnData()) {
                if (null != cursor) {
                    addColumnValuesToArrayAdapter(cursor);
                }
            }
            return null;
        }

        private Cursor loadColumnData() {
            return contentResolver.query(contentUri, new String[]{column.toString()}, null, null, null);
        }

        private void addColumnValuesToArrayAdapter(final Cursor cursor) {
            final Set<String> synchronizationSet = new HashSet<>(cursor.getCount());
            while (cursor.moveToNext()) {
                for (final String value : splitAtDelimiter(cursor.getString(0))) {
                    if (synchronizationSet.add(value)) { // avoids duplicates
                        arrayAdapter.add(value);
                    }
                }
            }
        }

        private static String[] splitAtDelimiter(final String rawValue) {
            if (!TextUtils.isEmpty(rawValue)) {
                return rawValue.split(DELIMITER);
            }
            return new String[0];
        }

    }

}
