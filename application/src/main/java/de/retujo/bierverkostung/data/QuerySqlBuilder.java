/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class wraps {@link SQLiteQueryBuilder} and provides a very basic Java DSL for creating SQL query strings.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class QuerySqlBuilder {

    private final Set<Table> tables;
    private final StringBuilder inTables;
    @Nullable private String[] projection;
    @Nullable private String selection;
    @Nullable private String groupBy;
    @Nullable private String having;
    @Nullable private String sortOrder;
    @Nullable private String limit;
    private boolean distinct;

    private QuerySqlBuilder() {
        tables = new LinkedHashSet<>();
        inTables = new StringBuilder();
        projection = null;
        selection = null;
        groupBy = null;
        having = null;
        sortOrder = null;
        limit = null;
        distinct = false;
    }

    /**
     * Returns a new builder for a SQL query.
     *
     * @param table the table to query.
     * @param furtherTables further tables to query.
     * @return the builder.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static QuerySqlBuilder table(@Nonnull final Table table,
            @Nonnull final Table ... furtherTables) {

        isNotNull(table, "table");
        isNotNull(furtherTables, "further table names");

        final QuerySqlBuilder result = new QuerySqlBuilder();
        result.tables.add(table);
        result.inTables.append(table);
        for (final Table furtherTable : furtherTables) {
            result.tables.add(isNotNull(furtherTable, "furtherTable"));
            result.inTables.append(", ").append(furtherTable);
        }

        return result;
    }

    /**
     * Sets the table which should be joined with the query table.
     *
     * @param table the table which should be joined with the query table.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code table} is {@code null}.
     * @throws IllegalArgumentException if {@code table} is empty.
     */
    @Nonnull
    public QuerySqlBuilder leftOuterJoin(@Nonnull final Table table) {
        argumentNotEmpty(table, "table name");
        if (!tables.isEmpty()) {
            tables.add(table);
            inTables.append(" LEFT OUTER JOIN ").append(table);
        }
        return this;
    }

    /**
     * Sets the columns whose content should match for joining data.
     *
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if any argument is empty.
     */
    @Nonnull
    public QuerySqlBuilder on(@Nonnull final Column<?> firstMatchingColumn,
            @Nonnull final Column<?> secondMatchingColumn) {

        argumentNotEmpty(firstMatchingColumn, "first matching column");
        argumentNotEmpty(secondMatchingColumn, "second matching column");
        if (!tables.isEmpty()) {
            inTables.append(" ON ")
                    .append(firstMatchingColumn.getQualifiedName())
                    .append('=')
                    .append(secondMatchingColumn.getQualifiedName());
        }
        return this;
    }

    /**
     * Sets the columns to be returned in the query result. Passing {@code null} will return all columns, which is
     * discouraged to prevent reading data from storage that isn't going to be used.
     *
     * @param column the column to be returned by the query.
     * @param furtherColumns further columns to be returned by the query.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code column} is not {@code null} while {@code furtherColumns} is {@code null}.
     */
    @Nonnull
    public QuerySqlBuilder projection(@Nullable final Column<?> column, @Nonnull final Column<?>... furtherColumns) {
        final String[] p;
        if (null != column) {
            isNotNull(furtherColumns, "further columns");
            p = new String[1 + furtherColumns.length];
            p[0] = getQualifiedNameAsAlias(column);
            for (int i = 0; i < furtherColumns.length; i++) {
                p[i + 1] = getQualifiedNameAsAlias(furtherColumns[i]);
            }
        } else {
            p = null;
        }

        return projection(p);
    }

    /**
     * For the specified Column get the qualified name and rename it as the alias of the column using SQL {@code AS}
     * keyword.
     *
     * @param column the Column to get the name for.
     * @return the aliased qualified name of {@code column}.
     * @throws NullPointerException if column is {@code null}.
     */
    static String getQualifiedNameAsAlias(final Column<?> column) {
        isNotNull(column, "column");
        return column.getQualifiedName() + " AS " + column.getAlias();
    }


    /**
     * Sets the names of the columns to be returned in the query result. Passing {@code null} will return all columns,
     * which is discouraged to prevent reading data from storage that isn't going to be used.
     * <em>Note:</em> Call this method after a "JOIN" to include the columns of the joined table, too.
     *
     * @param columnNames the names of the columns to be returned by the query.
     * @return this builder instance to allow method chaining.
     * @throws NullPointerException if {@code columnNames} is {@code null}.
     */
    @Nonnull
    public QuerySqlBuilder projection(@Nullable final String[] columnNames) {
        if (null == columnNames && !tables.isEmpty()) {
            projection = getAliasedQualifiedNamesOfAllTablesColumns();
        } else {
            projection = columnNames;
        }
        return this;
    }

    private String[] getAliasedQualifiedNamesOfAllTablesColumns() {
        final List<String> result = new ArrayList<>();
        for (final Table table : tables) {
            for (final Column<?> column : table.getColumns()) {
                result.add(getQualifiedNameAsAlias(column));
            }
        }

        return result.toArray(new String[result.size()]);
    }

    /**
     * Sets the filter declaring which rows the query returns, formatted as an SQL {@code WHERE} clause (excluding the
     * {@code WHERE} itself). Passing {@code null} will return all rows for the given URL.
     *
     * @param selection the {@code WHERE} filter to be set or {@code null} if all rows should be returned (default).
     * @return the builder instance to allow method chaining.
     */
    @Nonnull
    public QuerySqlBuilder selection(@Nullable final String selection) {
        this.selection = selection;
        return this;
    }

    /**
     * Sets the filter declaring how to group rows, formatted as an {@code SQL GROUP BY} clause (excluding the GROUP BY
     * itself). Passing {@code null} will cause the rows to not be grouped.
     *
     * @param groupBy the {@code GROUP BY} filter to be set or {@code null} if the rows should not be grouped (default).
     * @return this builder instance to allow method chaining.
     */
    @Nonnull
    public QuerySqlBuilder groupBy(@Nullable final String groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    @Nonnull
    public QuerySqlBuilder having(@Nullable final String having) {
        this.having = having;
        return this;
    }

    /**
     * Defines how to order the rows, formatted as an SQL {@code ORDER BY} clause (excluding the {@code ORDER BY}
     * itself). Passing null will use the default sort order, which may be unordered.
     *
     * @param sortOrder the sort order or {@code null} if default sort order should be applied (default).
     * @return this builder instance to allow method chaining.
     */
    @Nonnull
    public QuerySqlBuilder sortOrder(@Nullable final String sortOrder) {
        this.sortOrder = sortOrder;
        return this;
    }

    @Nonnull
    public QuerySqlBuilder limit(@Nullable final String limit) {
        this.limit = limit;
        return this;
    }

    /**
     * Marks the query to be built as {@code DISTINCT}.
     *
     * @return this builder instance to allow method chaining.
     */
    @Nonnull
    public QuerySqlBuilder distinct() {
        distinct = true;
        return this;
    }

    @Override
    public String toString() {
        final SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
        if (!tables.isEmpty()) {
            sqLiteQueryBuilder.setTables(inTables.toString());
        }
        sqLiteQueryBuilder.setDistinct(distinct);
        return sqLiteQueryBuilder.buildQuery(projection, selection, groupBy, having, sortOrder, limit);
    }

}
