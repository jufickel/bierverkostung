/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.UriMatcher;
import android.net.Uri;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.bierverkostung.data.BierverkostungContract.AUTHORITY;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_BEERS;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_BEER_PHOTOS;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_BEER_STYLES;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_BREWERIES;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_COUNTRIES;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_BEER_PHOTO_FILES;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_SINGLE_COLUMN_VALUES;
import static de.retujo.bierverkostung.data.BierverkostungContract.PATH_TASTINGS;
import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This <em>Singleton</em> is a utility to aid in matching URIs in the content provider.
 *
 * @since 1.0.0
 *
 * @see UriMatcher
 */
@ParametersAreNonnullByDefault
@Immutable
final class BierverkostungUriMatcher {

    private static final BierverkostungUriMatcher INSTANCE = new BierverkostungUriMatcher();

    private final UriMatcher uriMatcher;

    private BierverkostungUriMatcher() {
        uriMatcher = UriMatcherBuilder.newInstance(AUTHORITY)
                .withSubDirectory(PATH_TASTINGS, PATH_SINGLE_COLUMN_VALUES, UriNode.SINGLE_COLUMN_VALUES)
                .withSubDirectory(PATH_BEERS, PATH_SINGLE_COLUMN_VALUES, UriNode.SINGLE_COLUMN_VALUES)
                .withDirectory(PATH_TASTINGS, UriNode.TASTINGS)
                .withSingleItem(PATH_TASTINGS, UriNode.TASTING_WITH_ID)
                .withDirectory(PATH_BEERS, UriNode.BEERS)
                .withSingleItem(PATH_BEERS, UriNode.BEER_WITH_ID)
                .withDirectory(PATH_BEER_STYLES, UriNode.BEER_STYLES)
                .withSingleItem(PATH_BEER_STYLES, UriNode.BEER_STYLE_WITH_ID)
                .withDirectory(PATH_BREWERIES, UriNode.BREWERIES)
                .withSingleItem(PATH_BREWERIES, UriNode.BREWERY_WITH_ID)
                .withDirectory(PATH_COUNTRIES, UriNode.COUNTRIES)
                .withSingleItem(PATH_COUNTRIES, UriNode.COUNTRY_WITH_ID)
                .withDirectory(PATH_BEER_PHOTOS, UriNode.BEER_PHOTOS)
                .withSingleItem(PATH_BEER_PHOTOS, UriNode.BEER_PHOTO_WITH_ID)
                .withDirectory(PATH_BEER_PHOTO_FILES, UriNode.BEER_PHOTO_FILES)
                .withSingleItem(PATH_BEER_PHOTO_FILES, UriNode.BEER_PHOTO_FILE_WITH_ID)
                .build();
    }

    /**
     * Returns the <em>Singleton</em> instance of {@code BierverkostungUriMatcher}.
     *
     * @return the instance.
     */
    @Nonnull
    public static BierverkostungUriMatcher getInstance() {
        return INSTANCE;
    }

    /**
     * Returns the UriNode which matches the specified URI or {@code null} if the URI cannot be matched.
     *
     * @param uri the URI whose path to be matched.
     * @return the UriNode or {@link UriNode#NONE}.
     * @throws NullPointerException if {@code uri} is {@code null}.
     */
    public UriNode match(final Uri uri) {
        isNotNull(uri, "URI");
        final int uriNodeCode = uriMatcher.match(uri);
        return UriNode.forCode(uriNodeCode);
    }

    /**
     * A mutable builder with a fluent API for a {@code UriMatcher}.
     *
     * @since 1.0.0
     */
    @ParametersAreNonnullByDefault
    @NotThreadSafe
    private static final class UriMatcherBuilder {
        private static final char UNIVERSAL_WILDCARD = '*';
        private final UriMatcher uriMatcher;
        private final String authority;

        private UriMatcherBuilder(final String theAuthority) {
            uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
            authority = theAuthority;
        }

        /**
         * Returns a new instance of {@code UriMatcherBuilder}.
         *
         * @param authority the authority to match.
         * @return the instance.
         * @throws NullPointerException if {@code authority} is {@code null}.
         * @throws IllegalArgumentException if {@code authority} is empty.
         */
        @Nonnull
        public static UriMatcherBuilder newInstance(final String authority) {
            argumentNotEmpty(authority, "authority");
            return new UriMatcherBuilder(authority);
        }

        public UriMatcherBuilder withDirectory(final String path, final UriNode uriNode) {
            return addUri(path, uriNode);
        }

        public UriMatcherBuilder withSubDirectory(final String parentPath, final String childPath,
                final UriNode uriNode) {

            return addUri(parentPath + "/" + childPath, uriNode);
        }

        private UriMatcherBuilder addUri(final String path, final UriNode uriNode) {
            argumentNotEmpty(path, "path");
            isNotNull(uriNode, "URI node");
            uriMatcher.addURI(authority, path, uriNode.getCode());
            return this;
        }

        public UriMatcherBuilder withSingleItem(final String path, final UriNode uriNode) {
            return addUri(path + "/" + UNIVERSAL_WILDCARD, uriNode);
        }

        public UriMatcher build() {
            return uriMatcher;
        }
    }

}
