/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentResolver;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This AsyncTask deletes a set of entities from the database one after another.
 *
 * @param <T> the type of the entities to be deleted by this task.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class DeleteDbEntityTask<T extends DataEntity> extends AbstractDeleteTask<DeleteDbEntityTask<T>, T> {

    private final ContentResolver contentResolver;

    private DeleteDbEntityTask(final ContentResolver theContentResolver) {
        contentResolver = theContentResolver;
    }

    /**
     * Returns an instance of {@code DeleteDbEntityTask}.
     *
     * @param contentResolver the ContentResolver to be used for deleting entities from the database.
     * @param <T> the type of the entities to be deleted by the returned task.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> DeleteDbEntityTask<T> getInstance(
            @Nonnull final ContentResolver contentResolver) {

        return new DeleteDbEntityTask<>(isNotNull(contentResolver, "content resolver"));
    }

    @Override
    protected boolean doDelete(final T entityToBeDeleted) {
        return 1 == contentResolver.delete(entityToBeDeleted.getContentUri(), null, null);
    }

}
