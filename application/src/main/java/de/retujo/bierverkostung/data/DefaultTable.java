/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * The default implementation of {@link Table}.
 *
 * @since 1.0.0
 */
@AllNonnull
@NotThreadSafe
final class DefaultTable implements Table {

    private final Uri contentUri;
    private final String tableName;
    private final Map<String, Column<?>> columns;

    private DefaultTable(final Uri theContentUri, final String theTableName) {
        contentUri = theContentUri;
        tableName = theTableName;
        columns = new LinkedHashMap<>();
    }

    /**
     * Returns a new instance of {@code DefaultTable}.
     *
     * @param tableName the name of the table.
     * @return the instance.
     * @throws NullPointerException if {@code tableName} is {@code null}.
     * @throws IllegalArgumentException if {@code tableName} is empty.
     */
    public static DefaultTable getInstance(final Uri baseContentUri, final CharSequence tableName) {
        isNotNull(baseContentUri, "base content URI");
        argumentNotEmpty(tableName, "table name");
        final String tableNameString = tableName.toString();
        final Uri contentUri = baseContentUri.buildUpon()
                .appendPath(tableNameString)
                .build();

        return new DefaultTable(contentUri, tableNameString);
    }

    @Override
    public Column<String> createStringColumn(final CharSequence name) {
        return createColumn(name, ColumnType.TEXT);
    }

    private <T> Column<T> createColumn(final CharSequence name, final ColumnType type) {
        final ImmutableColumn<T> result = ImmutableColumn.newInstance(this, name, type);
        columns.put(result.getSimpleName(), result);
        return result;
    }

    @Override
    public Column<Integer> createIntColumn(final CharSequence name) {
        return createColumn(name, ColumnType.INTEGER);
    }

    @Override
    public Column<Long> createLongColumn(final CharSequence name) {
        return createColumn(name, ColumnType.INTEGER);
    }

    @Override
    public Column<Double> createDoubleColumn(final CharSequence name) {
        return createColumn(name, ColumnType.REAL);
    }

    @Override
    public List<Column> getColumns() {
        return new ArrayList<>(columns.values());
    }

    @Override
    public Maybe<Column> getColumn(final CharSequence columnName) {
        return Maybe.ofNullable(getColumnForName(columnName));
    }

    @Nullable
    private Column<?> getColumnForName(final CharSequence columnName) {
        isNotNull(columnName, "column name to look up");
        Column<?> foundColumn = columns.get(columnName.toString());
        if (null == foundColumn) {
            foundColumn = getColumnForNameFallback(columnName);
        }
        return foundColumn;
    }

    @Nullable
    private Column<?> getColumnForNameFallback(final CharSequence columnName) {
        for (final Column<?> column : columns.values()) {
            if (columnName.equals(column.getAlias()) || columnName.equals(column.getQualifiedName())) {
                return column;
            }
        }
        return null;
    }

    @Override
    public <T> Column<T> getColumnOrThrow(final CharSequence columnName) {
        @SuppressWarnings("unchecked")
        final Column<T> result = (Column<T>) getColumnForName(columnName);
        if (null == result) {
            final String msgPattern = "Table did not contain a column with name <{0}>!";
            throw new NullPointerException(MessageFormat.format(msgPattern, columnName));
        }
        return result;
    }

    @Override
    public Uri getContentUri() {
        return contentUri;
    }

    @Override
    public String getName() {
        return tableName;
    }

    @Override
    public int length() {
        return tableName.length();
    }

    @Override
    public char charAt(final int index) {
        return tableName.charAt(index);
    }

    @Override
    public CharSequence subSequence(final int start, final int end) {
        return tableName.subSequence(start, end);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DefaultTable that = (DefaultTable) o;

        return tableName.equals(that.tableName) && columns.equals(that.columns);
    }

    @Override
    public int hashCode() {
        int result = tableName.hashCode();
        final int prime = 31;
        result = prime * result + columns.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return tableName;
    }

}
