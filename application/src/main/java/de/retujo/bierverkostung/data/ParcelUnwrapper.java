/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class is the counterpart of ParcelWrapper. It is designed to read a byte flag before reading any
 * non-primitive value from the given Parcel. The byte flag is either {@value ParcelWrapper#BYTE_TRUE} if the desired
 * value was written to the Parcel or {@value ParcelWrapper#BYTE_FALSE} else.
 *
 * <em>Note:</em> The methods of this class should only be used if the Parcel was wrapped with {@link ParcelWrapper}.
 *
 * @since 1.0.0
 *
 * @see ParcelWrapper
 */
@NotThreadSafe
public final class ParcelUnwrapper {

    private final Parcel parcel;

    private ParcelUnwrapper(final Parcel theParcel) {
        parcel = theParcel;
    }

    /**
     * Returns a new instance of {@code ParcelUnwrapper}.
     *
     * @param parcel the Parcel to be safely unwrapped.
     * @return the instance.
     */
    public static ParcelUnwrapper of(@Nonnull final Parcel parcel) {
        isNotNull(parcel, "Parcel");
        return new ParcelUnwrapper(parcel);
    }

    /**
     * Reads a {@code int} value from the Parcel at the current position.
     *
     * @return the value.
     */
    public int unwrapInt() {
        return parcel.readInt();
    }

    /**
     * Reads a {@code long} value from the Parcel at the current position.
     *
     * @return the value.
     */
    public long unwrapLong() {
        return parcel.readLong();
    }

    /**
     * Reads a {@code String} value from the Parcel at the current position.
     *
     * @return the value or {@code null}.
     */
    @Nullable
    public String unwrapString() {
        if (isWrapped()) {
            return parcel.readString();
        }
        return null;
    }

    /**
     * Reads a {@code Parcelable} value from the Parcel at the current position.
     *
     * @return the value or {@code null}.
     */
    @Nullable
    public <T extends Parcelable> T unwrapParcelable() {
        T result = null;
        if (isWrapped()) {
            final Class<? extends ParcelUnwrapper> myClass = getClass();
            result =  parcel.readParcelable(myClass.getClassLoader());
        }
        return result;
    }

    private boolean isWrapped() {
        return ParcelWrapper.BYTE_TRUE == parcel.readByte();
    }

}
