/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;

/**
 * This interface defines a database column.
 *
 * @param <T> the Java type of this column.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
public interface Column<T> extends CharSequence {

    /**
     * Returns the table of which this column is a part.
     *
     * @return the table of this column.
     */
    Table getTable();

    /**
     * Returns the type of this column.
     *
     * @return the type.
     */
    ColumnType getType();

    /**
     * Returns the simple name of this column.
     *
     * @return the simple name.
     */
    String getSimpleName();

    /**
     * Returns the qualified name of this column, i. e. {@code <TABLE NAME>.<COLUMN NAME>}.
     *
     * @return the qualified name of this column.
     */
    String getQualifiedName();

    /**
     * Returns the alias name of this column. The alias name is the same as the qualified name with all {@code "."}
     * replaced by {@code "_"}.
     *
     * @return the alias name of this column.
     */
    String getAlias();

    /**
     * Same as {@link #getSimpleName()}.
     */
    @Override
    String toString();

}
