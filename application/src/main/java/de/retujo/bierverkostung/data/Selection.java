/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class provides a little framework for creating a filter declaring which rows to return in the result of a SQL
 * query, formatted as an SQL {@code WHERE} clause (excluding the {@code WHERE} itself).
 *
 * @since 1.2.0
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
public final class Selection {

    private final String selectionString;
    private final String[] selectionArgs;

    private Selection(final String theSelectionString, final String[] theSelectionArgs) {
        selectionString = theSelectionString;
        selectionArgs = theSelectionArgs;
    }

    /**
     * Returns the first step of a builder for creating a {@code Selection} object.
     *
     * @param column the first selection column to be added.
     * @return the next builder step for setting the selection argument for {@code column}.
     * @throws NullPointerException if {@code column} is {@code null}.
     */
    public static IsStep where(final Column<?> column) {
        final SelectionStepBuilder result = new SelectionStepBuilder();
        result.appendQualifiedNameWithWildcard(column);
        return result;
    }

    /**
     * Returns the selection string to be used for querying the database.
     *
     * @return the string.
     */
    public String getSelectionString() {
        return selectionString;
    }

    /**
     * Returns the selection arguments to be used for querying the database.
     *
     * @return the arguments.
     */
    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "selectionString='" + selectionString + '\'' +
                ", selectionArgs=" + Arrays.toString(selectionArgs) +
                "}";
    }

    /**
     * Final step for building the selection.
     */
    public interface BuildStep {

        /**
         * Returns a new {@code Selection} object containing the selection string and the selection arguments.
         *
         * @return the selection object.
         */
        Selection build();

    }

    /**
     * Build step for setting the selection.
     */
    public interface AppendWhereStep extends BuildStep {

        /**
         * Adds the specified column to the selection for reducing the rows of the query result.
         *
         * @param column the column to be added.
         * @return the next builder step.
         * @throws NullPointerException if column is {@code null}.
         */
        IsStep andWhere(Column<?> column);

    }

    /**
     * Build step for setting the selection argument for the beforehand added Column.
     */
    public interface IsStep {

        /**
         * Sets the selection argument for the beforehand added Column.
         *
         * @param selectionArgument the selection argument whose string representation to be set.
         * @return the next builder step.
         * @see String#valueOf(Object)
         */
        AppendWhereStep is(@Nullable Object selectionArgument);

        /**
         * Sets the selection argument for a supposed {@code null} column value.
         *
         * @return the next builder step.
         */
        AppendWhereStep isNull();

    }

    @NotThreadSafe
    private static final class SelectionStepBuilder implements IsStep, AppendWhereStep, BuildStep {

        private final StringBuilder selectionStringBuilder;
        private final List<String> selectionArgs;

        private SelectionStepBuilder() {
            selectionStringBuilder = new StringBuilder();
            selectionArgs = new ArrayList<>();
        }

        @Override
        public SelectionStepBuilder is(@Nullable final Object selectionArgument) {
            if (null != selectionArgument) {
                selectionStringBuilder.append("=?");
                addSelectionArg(selectionArgument);
            } else {
                isNull();
            }
            return this;
        }

        private void addSelectionArg(final Object arg) {
            selectionArgs.add(String.valueOf(isNotNull(arg, "selection argument")));
        }

        @Override
        public AppendWhereStep isNull() {
            selectionStringBuilder.append(" is null");
            return this;
        }

        @Override
        public SelectionStepBuilder andWhere(final Column<?> column) {
            selectionStringBuilder.append(" AND ");
            appendQualifiedNameWithWildcard(column);
            return this;
        }

        private void appendQualifiedNameWithWildcard(final Column<?> column) {
            isNotNull(column, "selection column");
            selectionStringBuilder.append(column.getQualifiedName());
        }

        @Override
        public Selection build() {
            return new Selection(selectionStringBuilder.toString(),
                    selectionArgs.toArray(new String[selectionArgs.size()]));
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " {" +
                    "selectionStringBuilder=" + selectionStringBuilder +
                    ", selectionArgs=" + selectionArgs +
                    "}";
        }

    }

}
