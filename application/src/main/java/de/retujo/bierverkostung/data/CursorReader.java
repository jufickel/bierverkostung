/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class wraps a {@code Cursor} and provides access to its values by {@link Column}s.
 *
 * @since 1.0.0
 */
@NotThreadSafe
public final class CursorReader {

    private static final byte UNKNOWN_COLUMN_INDEX = -1;

    private final Cursor cursor;

    private CursorReader(final Cursor theCursor) {
        cursor = theCursor;
    }

    /**
     * Returns a new instance of {@code CursorReader} which wraps the specified Cursor.
     *
     * @param cursor the Cursor to be wrapped.
     * @return the instance.
     * @throws NullPointerException if {@code cursor} is {@code null}.
     */
    @Nonnull
    public static CursorReader of(@Nonnull final Cursor cursor) {
        isNotNull(cursor, "cursor");

        return new CursorReader(cursor);
    }

    /**
     * Returns the string value of the specified column.
     *
     * @param column the column to get the value for.
     * @return the value or {@code null}.
     * @throws NullPointerException if {@code column} is {@code null}.
     * @throws IllegalArgumentException if the cursor did not contain the column.
     */
    @Nullable
    public String getString(@Nonnull final Column<String> column) {
        final int columnIndex = getColumnIndex(column);
        if (hasColumn(columnIndex) && !isNull(columnIndex)) {
            return cursor.getString(columnIndex);
        }
        return null;
    }

    private static boolean hasColumn(final int columnIndex) {
        return UNKNOWN_COLUMN_INDEX != columnIndex;
    }

    private boolean isNull(final int columnIndex) {
        try {
            return cursor.isNull(columnIndex);
        } catch (final CursorIndexOutOfBoundsException e) {
            return true;
        }
    }

    /**
     * Returns the zero-based index for the specified Column.
     *
     * @param column the column to get the index for.
     * @return the index.
     * @throws NullPointerException if {@code column} is {@code null}.
     */
    public int getColumnIndex(@Nonnull final Column<?> column) {
        checkColumn(column);
        int result = cursor.getColumnIndex(column.getAlias());
        if (-1 == result) {
            result = cursor.getColumnIndex(column.toString());
            if (-1 == result) {
                result = cursor.getColumnIndex(column.getQualifiedName());
            }
        }
        return result;
    }

    private static void checkColumn(final Column<?> column) {
        isNotNull(column, "column");
    }

    /**
     * Returns the int value of the specified column.
     *
     * @param column the column to get the value for.
     * @return the value or {@code 0}.
     * @throws NullPointerException if {@code column} is {@code null}.
     * @throws IllegalArgumentException if the cursor did not contain the column.
     */
    public int getInt(@Nonnull final Column<Integer> column) {
        final int columnIndex = getColumnIndex(column);
        if (!isNull(columnIndex)) {
            return cursor.getInt(columnIndex);
        }
        return 0;
    }

    /**
     * Returns the long value of the specified column.
     *
     * @param column the column to get the value for.
     * @return the value or {@code 0}.
     * @throws NullPointerException if {@code column} is {@code null}.
     * @throws IllegalArgumentException if the cursor did not contain the column.
     */
    public long getLong(@Nonnull final Column<Long> column) {
        final int columnIndex = getColumnIndex(column);
        if (!isNull(columnIndex)) {
            return cursor.getLong(columnIndex);
        }
        return 0L;
    }

    /**
     * Returns the double value of the specified column.
     *
     * @param column the column to get the value for.
     * @return the value or {@code 0.0D}.
     * @throws NullPointerException if {@code column} is {@code null}.
     * @throws IllegalArgumentException if the cursor did not contain the column.
     */
    public double getDouble(@Nonnull final Column<Double> column) {
        final int columnIndex = getColumnIndex(column);
        if (!isNull(columnIndex)) {
            return cursor.getDouble(columnIndex);
        }
        return 0.0D;
    }

}
