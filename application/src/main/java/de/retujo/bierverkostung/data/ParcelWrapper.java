/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class helps to wrap non-primitive values in a safe manner into a Parcel. Safe in this context means that each
 * object is preceded by a "guardian flag" which indicates whether the value was actually wrapped or not. Primitive
 * values are wrapped into the parcel as they are. So for example if a {@code null} String value is passed as an
 * argument, {@link #BYTE_FALSE} is wrapped into the parcel while {@code null} is not.
 *
 * @since 1.0.0
 *
 * @see ParcelUnwrapper
 */
public final class ParcelWrapper {

    /**
     * A semantic boolean {@code false}.
     */
    static final byte BYTE_FALSE = 0;

    /**
     * A semantic boolean {@code true}.
     */
    static final byte BYTE_TRUE = 1;

    private final Parcel parcel;

    private ParcelWrapper(final Parcel theParcel) {
        parcel = theParcel;
    }

    /**
     * Returns a new instance of {@code ParcelWrapper}.
     *
     * @param parcel the parcel to be wrapped.
     * @return the instance.
     * @throws NullPointerException if {@code parcel} is {@code null}.
     */
    public static ParcelWrapper of(@Nonnull final Parcel parcel) {
        isNotNull(parcel, "Parcel");
        return new ParcelWrapper(parcel);
    }

    /**
     * Wraps the given value in the Parcel.
     *
     * @param value the value to be wrapped.
     * @return this ParcelWrapper instance to allow Method Chaining.
     */
    public ParcelWrapper wrap(final int value) {
        parcel.writeInt(value);
        return this;
    }

    /**
     * Wraps the given value in the Parcel.
     *
     * @param value the value to be wrapped.
     * @return this ParcelWrapper instance to allow Method Chaining.
     */
    public ParcelWrapper wrap(final long value) {
        parcel.writeLong(value);
        return this;
    }

    /**
     * Safely wraps the given value in the Parcel. If the value is {@code null} it is not wrapped but its guarding flag
     * is set to {@link #BYTE_FALSE}.
     *
     * @param value the value to be wrapped.
     * @return this ParcelWrapper instance to allow Method Chaining.
     */
    public ParcelWrapper wrap(@Nullable final String value) {
        if (null != value) {
            writeTrueFlag();
            parcel.writeString(value);
        } else {
            writeFalseFlag();
        }
        return this;
    }

    /**
     * Safely wraps the given value in the Parcel. If the value is {@code null} it is not wrapped but its guarding flag
     * is set to {@link #BYTE_FALSE}.
     *
     * @param value the value to be wrapped.
     * @return this ParcelWrapper instance to allow Method Chaining.
     */
    public ParcelWrapper wrap(@Nullable final Parcelable value) {
        if (null != value) {
            writeTrueFlag();
            parcel.writeParcelable(value, 0);
        } else {
            writeFalseFlag();
        }
        return this;
    }

    private void writeTrueFlag() {
        parcel.writeByte(BYTE_TRUE);
    }

    private void writeFalseFlag() {
        parcel.writeByte(BYTE_FALSE);
    }

}
