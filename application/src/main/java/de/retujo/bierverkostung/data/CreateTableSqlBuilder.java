/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;
import static de.retujo.java.util.ObjectUtil.areEqual;

/**
 * This class provides a limited Java DSL for creating database tables.
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
final class CreateTableSqlBuilder {

    private final StringBuilder stringBuilder;
    private final List<ForeignKeyTriple> foreignKeyTriples;

    /**
     * Constructs a new {@code CreateTableSqlBuilder} object.
     */
    private CreateTableSqlBuilder(final String tableName) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("CREATE TABLE ").append(tableName).append(" (");
        foreignKeyTriples = new ArrayList<>();
    }

    /**
     * Returns a new instance of {@code CreateTableSqlBuilder}.
     *
     * @param tableName the name of the table to be created.
     * @return the instance.
     * @throws NullPointerException if {@code tableName} is {@code null}.
     * @throws IllegalArgumentException if {@code tableName} is empty.
     */
    public static CreateTableSqlBuilder newInstance(final CharSequence tableName) {
        argumentNotEmpty(tableName, "table name");
        return new CreateTableSqlBuilder(tableName.toString());
    }

    /**
     * Sets the name of this table's column which contains the primary key.
     *
     * @param primaryKeyColumnName the name of the table's primary key column.
     * @throws NullPointerException if {@code primaryKeyColumnName} is {@code null}.
     * @throws IllegalArgumentException if {@code primaryKeyColumnName} is empty.
     * @return this CreateTableSqlBuilder instance to allow Method Chaining.
     */
    public CreateTableSqlBuilder withPrimaryKeyColumn(final String primaryKeyColumnName) {
        argumentNotEmpty(primaryKeyColumnName, "name of the primary key column");
        stringBuilder.append(primaryKeyColumnName).append(" INTEGER PRIMARY KEY");
        return this;
    }

    /**
     * Sets the name of this table's column which contains the primary key.
     *
     * @param primaryKeyColumn the column of the table's which contains the primary keys.
     * @throws NullPointerException if {@code primaryKeyColumnName} is {@code null}.
     * @throws IllegalArgumentException if {@code primaryKeyColumnName} is empty.
     * @return this CreateTableSqlBuilder instance to allow Method Chaining.
     */
    public CreateTableSqlBuilder withPrimaryKeyColumn(final Column primaryKeyColumn) {
        argumentNotEmpty(primaryKeyColumn, "primary key column");
        stringBuilder.append(primaryKeyColumn).append(" ").append(primaryKeyColumn.getType()).append(" PRIMARY KEY");
        return this;
    }

    /**
     * Creates SQL for creating a new table column.
     *
     * @param column the table column.
     * @param args optional arguments like for example {@code "UNIQUE"} or {@code "NOT NULL"}.
     * @return this CreateTableSqlBuilder instance to allow Method Chaining.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if columnName is empty.
     */
    public CreateTableSqlBuilder withColumn(final Column column, final CharSequence... args) {
        argumentNotEmpty(column, "column");
        isNotNull(args, "optional arguments");
        stringBuilder.append(", ").append(column).append(" ").append(column.getType());
        for (final CharSequence arg : args) {
            argumentNotEmpty(arg, "arg");
            stringBuilder.append(" ").append(arg);
        }
        return this;
    }

    /**
     * Creates SQL for creating a foreign key definition.
     *
     * @param sourceColumn the column which refers to another column.
     * @param targetColumn the column to which is referred to by {@code sourceColumn}.
     * @return this CreateTableSqlBuilder instance to allow Method Chaining.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public CreateTableSqlBuilder withForeignKey(final Column sourceColumn, final Column targetColumn) {
        foreignKeyTriples.add(new ForeignKeyTriple(sourceColumn, targetColumn.getTable(), targetColumn));
        return this;
    }

    /**
     * Returns a fully-fledged SQL string for creating a table.
     *
     * @return the SQL string.
     */
    @Override
    public String toString() {
        addForeignKeys();
        stringBuilder.append(");");
        return stringBuilder.toString();
    }

    private void addForeignKeys() {
        for (final ForeignKeyTriple foreignKeyTriple : foreignKeyTriples) {
            stringBuilder.append(", FOREIGN KEY (").append(foreignKeyTriple.sourceColumnName).append(")")
                    .append(" REFERENCES ").append(foreignKeyTriple.targetTableName)
                    .append("(").append(foreignKeyTriple.targetColumnName).append(")");
        }
    }

    /**
     * This is a data structure to hold the information for building a "foreign key" string. Namely this is the name
     * of the source column, the name of the target table as well as the name of the target column.
     *
     * @since 1.0.0
     */
    @Immutable
    private static final class ForeignKeyTriple {
        private final String sourceColumnName;
        private final String targetTableName;
        private final String targetColumnName;

        /**
         * Constructs a new {@code ForeignKeyTriple} object.
         *
         * @param sourceColumnName the name of the source column.
         * @param targetTableName the name of the target table.
         * @param targetColumnName the name of the target column.
         * @throws NullPointerException if any argument is {@code null}.
         * @throws IllegalArgumentException if any argument is empty.
         */
        private ForeignKeyTriple(final CharSequence sourceColumnName, final CharSequence targetTableName,
                final CharSequence targetColumnName) {
            argumentNotEmpty(sourceColumnName, "name of the source column");
            argumentNotEmpty(targetTableName, "name of the target table");
            argumentNotEmpty(targetColumnName, "name of the target column");
            this.sourceColumnName = sourceColumnName.toString();
            this.targetTableName = targetTableName.toString();
            this.targetColumnName = targetColumnName.toString();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ForeignKeyTriple)) {
                return false;
            }
            final ForeignKeyTriple that = (ForeignKeyTriple) o;

            return areEqual(sourceColumnName, that.sourceColumnName)
                    && areEqual(targetTableName, that.targetTableName)
                    && areEqual(targetColumnName, that.targetColumnName);
        }

        @Override
        public int hashCode() {
            return ObjectUtil.hashCode(sourceColumnName, targetTableName, targetColumnName);
        }
    }

}
