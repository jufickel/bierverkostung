/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.net.Uri;

import java.lang.ref.WeakReference;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.Logger;
import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A helper class to make handling {@link ContentResolver} inserts of {@link DataEntity}s easier. This class offers
 * implementations for inserting the values synchronously or asynchronously.
 *
 * @param <T> the type of the DataEntity to be inserted by this insert handler.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public final class InsertHandler<T extends DataEntity> {

    private static final Logger LOGGER = Logger.forSimpleClassName(InsertHandler.class);

    private final InsertHandlerImplementation<T> actualInsertHandler;

    private InsertHandler(final InsertHandlerImplementation<T> theActualHandler) {
        actualInsertHandler = theActualHandler;
    }

    /**
     * Returns an instance of {@code InsertHandler} which runs asynchronously.
     *
     * @param contentResolver the ContentResolver to be used for inserting the DataEntity into the database.
     * @param onInsertCompleteAction an optional action to be performed after the DataEntity was inserted into the
     * database.
     * @param <T> the type of the DataEntity to be asynchronously inserted into the database by the returned handler
     * object.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> InsertHandler<T> async(final ContentResolver contentResolver,
            @Nullable final Acceptor<T> onInsertCompleteAction) {

        isNotNull(contentResolver, "ContentResolver");
        return new InsertHandler<>(new AsyncInsertHandler<>(contentResolver, onInsertCompleteAction));
    }

    /**
     * Returns an instance of {@code InsertHandler} which runs in the same thread as the caller of the
     * {@link #startInsert(DataEntity)} method.
     *
     * @param contentResolver the ContentResolver to be used for inserting the DataEntity into the database.
     * @param onInsertCompleteAction an optional action to be performed after the DataEntity was inserted into the
     * database.
     * @param <T> the type of the DataEntity to be asynchronously inserted into the database by the returned handler
     * object.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> InsertHandler<T> sync(final ContentResolver contentResolver,
            @Nullable final Acceptor<T> onInsertCompleteAction) {

        isNotNull(contentResolver, "ContentResolver");
        return new InsertHandler<>(new SyncInsertHandler<>(contentResolver, onInsertCompleteAction));
    }

    /**
     * This method begins an asynchronous insert of the specified {@code DataEntity}. When the insert operation is done
     * the optional action of this handler object is called.
     *
     * @param dataEntity the DataEntity to be persisted.
     * @throws NullPointerException if {@code dataEntity} is {@code null}.
     */
    public void startInsert(final T dataEntity) {
        isNotNull(dataEntity, "DataEntity to be inserted");
        LOGGER.debug("Inserting DataEntity into <{0}>.", dataEntity.getContentUri());
        actualInsertHandler.insert(dataEntity);
    }

    private interface InsertHandlerImplementation<T extends DataEntity> {
        void insert(T dataEntity);
    }

    @AllNonnull
    @NotThreadSafe
    private static final class AsyncInsertHandler<T extends DataEntity> extends AsyncQueryHandler
            implements InsertHandlerImplementation<T> {

        private final WeakReference<Acceptor<T>> onInsertCompleteAction;

        private AsyncInsertHandler(final ContentResolver cr,
                @Nullable final Acceptor<T> theOnInsertCompleteAction) {

            super(cr);
            onInsertCompleteAction = new WeakReference<>(theOnInsertCompleteAction);
        }

        @Override
        public void insert(final T dataEntity) {
            startInsert(0, dataEntity, dataEntity.getContentUri(), dataEntity.asContentValues());
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onInsertComplete(final int token, final Object cookie, final Uri uri) {
            final Acceptor<T> action = onInsertCompleteAction.get();
            if (null != action) {
                action.accept(null != uri ? (T) cookie : null);
            }
        }
    }

    @AllNonnull
    @NotThreadSafe
    private static final class SyncInsertHandler<T extends DataEntity> implements InsertHandlerImplementation<T> {
        private final ContentResolver contentResolver;
        private final WeakReference<Acceptor<T>> onInsertCompleteAction;

        private SyncInsertHandler(final ContentResolver theContentResolver,
                @Nullable final Acceptor<T> theOnInsertCompleteAction) {

            contentResolver = theContentResolver;
            onInsertCompleteAction = new WeakReference<>(theOnInsertCompleteAction);
        }

        @Override
        public void insert(final T dataEntity) {
            @Nullable final Uri uri = contentResolver.insert(dataEntity.getContentUri(), dataEntity.asContentValues());
            final Acceptor<T> action = onInsertCompleteAction.get();
            if (null != action) {
                action.accept(null != uri ? dataEntity : null);
            }
        }
    }

}
