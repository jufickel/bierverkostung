/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

/**
 * An enumeration of known SQLite data types which can be used to define columns.
 *
 * @since 1.0.0
 */
enum ColumnType {

    /**
     * The type {@code TEXT}.
     */
    TEXT("TEXT"),

    /**
     * The type {@code INTEGER}
     */
    INTEGER("INTEGER"),

    /**
     * The type {@code REAL}.
     */
    REAL("REAL");

    private final String typeName;

    private ColumnType(final String theTypeName) {
        typeName = theTypeName;
    }

    /**
     * Returns the SQLite identifier of this type like for example {@code "TEXT"}.
     *
     * @return the type identifier.
     */
    @Override
    public String toString() {
        return typeName;
    }

}
