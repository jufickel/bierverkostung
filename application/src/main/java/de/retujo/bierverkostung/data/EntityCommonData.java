/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;

import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class holds data which is common for all {@link DataEntity} objects:
 * <ul>
 *     <li>
 *         The universally unique ID of the entity. This ID will never change.
 *         It is assumed that each DataEntity has the same UUID for its complete lifecycle.
 *         The UUID remains even then the same if the entity is persisted in another database.
 *     </li>
 *     <li>
 *         A {@link Revision} indicating when the entity was created or changed.
 *         The timestamp gets updated with each change of the entity.
 *     </li>
 * </ul>
 */
@AllNonnull
@Immutable
public final class EntityCommonData {

    private final UUID id;
    private final Revision revision;

    private EntityCommonData(final UUID theUuid, final Revision theRevision) {
        id = isNotNull(theUuid, "ID");
        revision = isNotNull(theRevision, "revision");
    }

    /**
     * Returns an instance of {@code EntityCommonData} with a random ID and a revision of zero.
     *
     * @return the instance.
     */
    public static EntityCommonData getInstance() {
        return getInstance(UUID.randomUUID(), Revision.zero());
    }

    /**
     * Returns an instance of {@code EntityCommonData} with the specified ID and a revision of zero.
     *
     * @param id the universally unique ID.
     * @return the instance.
     */
    public static EntityCommonData getInstance(final UUID id) {
        return getInstance(id, Revision.zero());
    }

    /**
     * Returns an instance of {@code EntityCommonData} with the specified properties.
     *
     * @param id the universally unique ID.
     * @param revision the revision.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static EntityCommonData getInstance(final UUID id, final Revision revision) {
        return new EntityCommonData(id, revision);
    }

    /**
     * Returns the universally unique identifier of this common data object.
     *
     * @return the identifier.
     */
    public UUID getId() {
        return id;
    }

    /**
     * Returns the revision of this {@code EntityCommonData} object.
     *
     * @return the revision.
     */
    public Revision getRevision() {
        return revision;
    }

    /**
     * Sets the specified timestamp to a copy of this {@code EntityCommonData} object.
     *
     * @param newRevision the timestamp to be set.
     * @return a new EntityCommonData object with {@code newTimestamp} set or this EntityCommonData object if it
     * already has that timestamp.
     */
    public EntityCommonData setRevision(final Revision newRevision) {
        if (ObjectUtil.areEqual(newRevision, revision)) {
            return this;
        }
        return getInstance(id, newRevision);
    }

    public EntityCommonData incrementRevision() {
        return getInstance(id, revision.increment());
    }

    /**
     * Returns a {@link Uri} which consists of the content-uri of the specified table and the universally unique ID of
     * this {@code EntityCommonData} object appended.
     *
     * @param table the Table which provides the base content-uri.
     * @return the content URI which is based on the content-uri of {@code table}.
     * @throws NullPointerException if {@code table} is {@code null}.
     */
    public Uri getContentUri(final Table table) {
        final Uri baseContentUri = checkTable(table).getContentUri();
        return baseContentUri.buildUpon()
                .appendPath(id.toString())
                .build();
    }

    private static Table checkTable(final Table table) {
        return isNotNull(table, "table");
    }

    /**
     * Returns the properties of this {@code EntityCommonData} object as {@link ContentValues} with keys relative to
     * the specified table.
     *
     * @param table the table for determining the keys of the result.
     * @return the properties of this EntityCommonData object as ContentValues.
     * @throws NullPointerException if {@code table} is {@code null}.
     */
    public ContentValues asContentValues(final Table table) {
        checkTable(table);
        final ContentValues result = new ContentValues(2);
        result.put(getColumnStringOrThrow(table, ExtendedBaseColumns._ID), id.toString());
        result.put(getColumnStringOrThrow(table, ExtendedBaseColumns._REVISION), revision.getRevisionNumber());
        return result;
    }

    private static String getColumnStringOrThrow(final Table table, final String columnName) {
        return table.getColumnOrThrow(columnName).toString();
    }

    /**
     * Writes the properties of this {@code EntityCommonData} object to the specified {@link Parcel}.
     *
     * @param dest the Parcel in which the properties should be written.
     * @param flags Additional flags about how the object should be written. May be {@code 0} or
     * {@link android.os.Parcelable#PARCELABLE_WRITE_RETURN_VALUE}.
     * @throws NullPointerException if {@code dest} is {@code null}.
     * @see android.os.Parcelable#writeToParcel(Parcel, int)
     */
    public void writeToParcel(final Parcel dest, final int flags) {
        isNotNull(dest, "parcel");
        dest.writeString(id.toString());
        dest.writeInt(revision.getRevisionNumber());
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final EntityCommonData that = (EntityCommonData) o;
        return ObjectUtil.areEqual(id, that.id) && ObjectUtil.areEqual(revision, that.revision);
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(id, revision);
    }

    @Override
    public String toString() {
        return "id=" + id + ", revision=" + revision;
    }

}
