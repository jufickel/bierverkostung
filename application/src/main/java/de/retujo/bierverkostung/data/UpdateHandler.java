/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;

import java.lang.ref.WeakReference;

import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A helper class to make handling asynchronous {@link ContentResolver} updates of {@link DataEntity}s easier.
 *
 * @since 1.2.0
 */
@NotThreadSafe
public final class UpdateHandler<T extends DataEntity> {

    private final UpdateHandlerImplementation<T> actualUpdateHandler;

    private UpdateHandler(final UpdateHandlerImplementation<T> theActualUpdateHandler) {
        actualUpdateHandler = theActualUpdateHandler;
    }

    /**
     * Returns an instance of {@code AsyncUpdateHandler}.
     *
     * @param contentResolver the ContentResolver to be used for updating the DataEntity in the database.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> UpdateHandler<T> async(final ContentResolver contentResolver) {
        return async(contentResolver, null);
    }

    /**
     * Returns an instance of {@code AsyncUpdateHandler}.
     *
     * @param contentResolver the ContentResolver to be used for updating the DataEntity in the database.
     * @param onUpdateCompleteAction an optional action to be performed after the DataEntity was updated in the
     * database.
     * @param <T> the type of the DataEntity to be asynchronously updated in the database by the returned handler
     * object.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> UpdateHandler<T> async(final ContentResolver contentResolver,
            @Nullable final Acceptor<T> onUpdateCompleteAction) {

        isNotNull(contentResolver, "ContentResolver");
        return new UpdateHandler<>(new AsyncUpdateHandler<>(contentResolver, onUpdateCompleteAction));
    }

    /**
     * Returns an instance of {@code AsyncUpdateHandler}.
     *
     * @param contentResolver the ContentResolver to be used for updating the DataEntity in the database.
     * @param onUpdateCompleteAction an optional action to be performed after the DataEntity was updated in the
     * database.
     * @param <T> the type of the DataEntity to be asynchronously updated in the database by the returned handler
     * object.
     * @return the instance.
     * @throws NullPointerException if {@code contentResolver} is {@code null}.
     */
    public static <T extends DataEntity> UpdateHandler<T> sync(final ContentResolver contentResolver,
            @Nullable final Acceptor<T> onUpdateCompleteAction) {

        isNotNull(contentResolver, "ContentResolver");
        return new UpdateHandler<>(new SyncUpdateHandler<>(contentResolver, onUpdateCompleteAction));
    }

    /**
     * This method begins an asynchronous update of the specified {@code DataEntity}. When the update operation is done
     * the optional action of this handler object is called.
     *
     * @param dataEntity the DataEntity to be updated.
     * @throws NullPointerException if {@code dataEntity} is {@code null}.
     */
    public void startUpdate(final T dataEntity) {
        isNotNull(dataEntity, "DataEntity to be updated");
        actualUpdateHandler.update(dataEntity);
    }

    private interface UpdateHandlerImplementation<T extends DataEntity> {
        void update(T dataEntity);
    }

    @AllNonnull
    @NotThreadSafe
    private static final class AsyncUpdateHandler<T extends DataEntity> extends AsyncQueryHandler
            implements UpdateHandlerImplementation<T> {

        private final WeakReference<Acceptor<T>> onUpdateCompleteAction;

        private AsyncUpdateHandler(final ContentResolver cr,
                @Nullable final Acceptor<T> theOnUpdateCompleteAction) {

            super(cr);
            onUpdateCompleteAction = new WeakReference<>(theOnUpdateCompleteAction);
        }

        @Override
        public void update(final T dataEntity) {
            startUpdate(0, dataEntity, dataEntity.getContentUri(), dataEntity.asContentValues(), null, null);
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onUpdateComplete(final int token, final Object cookie, final int result) {
            final Acceptor<T> action = onUpdateCompleteAction.get();
            if (null != action) {
                action.accept(0 < result ? (T) cookie : null);
            }
        }
    }

    @AllNonnull
    @NotThreadSafe
    private static final class SyncUpdateHandler<T extends DataEntity> implements UpdateHandlerImplementation<T> {
        private final ContentResolver contentResolver;
        private final WeakReference<Acceptor<T>> onUpdateCompleteAction;

        private SyncUpdateHandler(final ContentResolver theContentResolver,
                @Nullable final Acceptor<T> theOnInsertCompleteAction) {

            contentResolver = theContentResolver;
            onUpdateCompleteAction = new WeakReference<>(theOnInsertCompleteAction);
        }

        @Override
        public void update(final T dataEntity) {
            final int updateResult =
                    contentResolver.update(dataEntity.getContentUri(), dataEntity.asContentValues(), null, null);
            final Acceptor<T> action = onUpdateCompleteAction.get();
            if (null != action) {
                action.accept(0 < updateResult ? dataEntity : null);
            }
        }
    }

}
