/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;

import java.util.List;
import java.util.UUID;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Conditions;
import de.retujo.java.util.Function;
import de.retujo.java.util.Maybe;

/**
 * This Singleton is a function which tries to obtain an ID from a given Uri. It is assumed that the Uri's last path
 * segment is the ID to be obtained. This class circumvents a possible {@code null} return value when calling
 * {@link Uri#getLastPathSegment()}.
 */
@AllNonnull
@Immutable
public final class UriToIdFunction implements Function<Uri, Maybe<UUID>> {

    private static final UriToIdFunction INSTANCE = new UriToIdFunction();

    private UriToIdFunction() {
        super();
    }

    /**
     * Returns the sole instance of {@code UriToIdFunction}.
     *
     * @return the instance.
     */
    public static UriToIdFunction getInstance() {
        return INSTANCE;
    }

    /**
     * Obtains an ID from the given Uri. It is assumed that the Uri's last path segment is the wanted ID.
     *
     * @param uri the URI from which to obtain the ID from.
     * @return the ID or an empty Maybe.
     * @throws NullPointerException if {@code uri} is {@code null}.
     */
    @Override
    public Maybe<UUID> apply(final Uri uri) {
        Conditions.isNotNull(uri, "URI");

        final List<String> pathSegments = uri.getPathSegments();
        final int size = pathSegments.size();
        if (0 < size) {
            return Maybe.of(UUID.fromString(pathSegments.get(size - 1)));
        }
        return Maybe.empty();
    }

}
