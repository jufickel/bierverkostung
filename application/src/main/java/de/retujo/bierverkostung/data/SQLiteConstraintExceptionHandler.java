/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import de.retujo.java.util.Provider;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class handles a SQLiteConstraintException if its cause is the violation of the UNIQUE constraint. The
 * algorithm tries to load and return the already existing DB entry which caused the conflict. This is a workaround
 * for the broken {@code SQLiteDatabase.insertWithOnConflict()}.
 *
 * @since 1.2.0
 */
@ParametersAreNonnullByDefault
final class SQLiteConstraintExceptionHandler implements Provider<Uri> {

    private final Table table;
    private final ContentValues contentValues;
    private final SQLiteConstraintException constraintException;
    private final SQLiteDatabase database;

    private SQLiteConstraintExceptionHandler(final Table theTable,
            final ContentValues theContentValues,
            final SQLiteConstraintException theConstraintException,
            final SQLiteDatabase theDatabase) {

        table = isNotNull(theTable, "table");
        contentValues = isNotNull(theContentValues, "content values");
        constraintException = isNotNull(theConstraintException, "SQLiteConstraintException");
        database = isNotNull(theDatabase, "database");
    }

    /**
     * Returns an instance of {@code SQLiteConstraintExceptionHandler}.
     *
     * @param table the table to insert the value.
     * @param contentValues provide
     * @param constraintException the exception to be handled.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     */
    public static SQLiteConstraintExceptionHandler getInstance(final Table table,
            final ContentValues contentValues,
            final SQLiteConstraintException constraintException,
            final SQLiteDatabase database) {

        return new SQLiteConstraintExceptionHandler(table, contentValues, constraintException, database);
    }

    @Override
    public Uri get() {
        final Matcher matcher = matchExceptionMessage();
        if (isUniqueConstraintViolation(matcher)) {
            final Column<?> idColumn = table.getColumnOrThrow(BaseColumns._ID);
            final Selection selection = getSelection(matcher);
            return getUriWithAppendedId(selection, idColumn);
        }
        throw constraintException;
    }

    private Matcher matchExceptionMessage() {
        final Pattern pattern = Pattern.compile("(column )(.+?)( is not unique \\(code 19\\))|"
                + "(UNIQUE constraint failed: )(.+?)( \\(code 2067\\))");
        return pattern.matcher(constraintException.getMessage());
    }

    private static boolean isUniqueConstraintViolation(final Matcher matcher) {
        return matcher.matches();
    }

    private Selection getSelection(final Matcher matcher) {
        final Column<?> violatingColumn = table.getColumnOrThrow(getNameOfViolatingColumn(matcher));

        return Selection.where(violatingColumn)
                .is(contentValues.get(violatingColumn.getSimpleName()))
                .build();
    }

    @Nullable
    private static String getNameOfViolatingColumn(final Matcher matcher) {
        final byte firstRelevantGroup = 2;
        final byte secondRelevantGroup = 5;

        String result = matcher.group(firstRelevantGroup);
        if (null == result) {
            result = matcher.group(secondRelevantGroup);
        }
        return result;
    }

    private Uri getUriWithAppendedId(final Selection selection, final Column<?> idColumn) {
        final String querySqlString = getQuerySqlString(idColumn, selection.getSelectionString());

        try (final Cursor cursor = database.rawQuery(querySqlString, selection.getSelectionArgs())) {
            cursor.moveToFirst();
            return table.getContentUri()
                    .buildUpon()
                    .appendPath(cursor.getString(cursor.getColumnIndex(idColumn.getAlias())))
                    .build();
        }
    }

    private String getQuerySqlString(final Column<?> idColumn, final String selection) {
        return QuerySqlBuilder.table(table)
                .projection(idColumn)
                .selection(selection)
                .toString();
    }

}
