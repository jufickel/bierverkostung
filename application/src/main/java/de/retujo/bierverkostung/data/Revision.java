/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import java.text.MessageFormat;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.ObjectUtil;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * This class represents a revision for tracking changes of data entities.
 */
@ParametersAreNonnullByDefault
@Immutable
public final class Revision implements Comparable<Revision> {

    private final int revisionNumber;

    private Revision(final int theRevisionNumber) {
        revisionNumber = theRevisionNumber;
    }

    /**
     * Returns an instance of {@code Revision} with a revision number of zero.
     *
     * @return the instance.
     */
    public static Revision zero() {
        return of(0);
    }

    /**
     * Returns an instance of {@code Revision} with the specified revision number.
     *
     * @param revisionNumber the revision number of the returned Revision object, must be at least zero.
     * @return the instance.
     * @throws IllegalArgumentException if {@code revisionNumber} is less than zero.
     */
    public static Revision of(final int revisionNumber) {
        if (0 > revisionNumber) {
            final String msgTemplate = "The revision number must not be less than zero but it was <{0}>!";
            throw new IllegalArgumentException(MessageFormat.format(msgTemplate, revisionNumber));
        }
        return new Revision(revisionNumber);
    }

    /**
     * Indicates whether this revision is less than the specified revision.
     *
     * @param otherRevision the revision to compare with.
     * @return {@code true} if this revision is earlier, {@code false} else.
     * @throws NullPointerException if {@code otherRevision} is {@code null}.
     */
    public boolean isLessThan(final Revision otherRevision) {
        return 0 > compareTo(otherRevision);
    }

    /**
     * Indicates whether this revision is greater than the specified revision.
     *
     * @param otherRevision the revision to compare with.
     * @return {@code true} if this revision is later, {@code false} else.
     * @throws NullPointerException if {@code otherRevision} is {@code null}.
     */
    public boolean isGreaterThan(final Revision otherRevision) {
        return 0 < compareTo(otherRevision);
    }

    /**
     * Returns the revision number of this Revision object.
     *
     * @return the revision number.
     */
    public int getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Returns a new Revision object with a revision number incremented by one.
     *
     * @return the revision with an incremented number.
     */
    public Revision increment() {
        return of(revisionNumber + 1);
    }

    /**
     * Compares the revision number of this Revision object to the specified revision number of the specified
     * Revision object.
     *
     * @param otherRevision the Revision object to be compared to this Revision object.
     * @throws NullPointerException if {@code otherRevision} is {@code null}.
     */
    @Override
    public int compareTo(@Nonnull final Revision otherRevision) {
        return Long.compare(revisionNumber, checkOtherRevisionNumber(otherRevision).revisionNumber);
    }

    private static Revision checkOtherRevisionNumber(final Revision otherRevision) {
        return isNotNull(otherRevision, "Revision to compare with");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Revision revision = (Revision) o;
        return revisionNumber == revision.revisionNumber;
    }

    @Override
    public int hashCode() {
        return ObjectUtil.hashCode(revisionNumber);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" + "revisionNumber=" + revisionNumber + "}";
    }

}
