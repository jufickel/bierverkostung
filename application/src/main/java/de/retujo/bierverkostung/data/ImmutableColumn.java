/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import javax.annotation.concurrent.Immutable;

import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.argumentNotEmpty;
import static de.retujo.java.util.Conditions.isNotNull;

/**
 * An immutable implementation of {@link Column}.
 *
 * @param <T> the Java type of this column.
 *
 * @since 1.0.0
 */
@AllNonnull
@Immutable
final class ImmutableColumn<T> implements Column<T> {

    private final Table table;
    private final String columnName;
    private final ColumnType columnType;

    private ImmutableColumn(final Table theTable, final String theColumnName, final ColumnType theColumnType) {
        table = theTable;
        columnName = theColumnName;
        columnType = theColumnType;
    }

    /**
     * Returns a new instance of {@code ImmutableColumn}.
     *
     * @param table the table which contains the column.
     * @param columnName the name of the column.
     * @param columnType the type of the column.
     * @return the instance.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code columnName} is empty.
     */
    public static <T> ImmutableColumn<T> newInstance(final Table table, final CharSequence columnName,
            final ColumnType columnType) {

        isNotNull(table, "table");
        argumentNotEmpty(columnName, "column name");
        isNotNull(columnType, "column type");
        return new ImmutableColumn<>(table, columnName.toString(), columnType);
    }

    @Override
    public Table getTable() {
        return table;
    }

    @Override
    public ColumnType getType() {
        return columnType;
    }

    @Override
    public String getSimpleName() {
        return columnName;
    }

    @Override
    public String getQualifiedName() {
        return table + "." + columnName;
    }

    @Override
    public String getAlias() {
        return getQualifiedName().replace(".", "_");
    }

    @Override
    public int length() {
        return columnName.length();
    }

    @Override
    public char charAt(final int index) {
        return columnName.charAt(index);
    }

    @Override
    public CharSequence subSequence(final int start, final int end) {
        return columnName.subSequence(start, end);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ImmutableColumn<?> that = (ImmutableColumn<?>) o;

        return table.equals(that.table) && columnName.equals(that.columnName) && columnType != that.columnType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = columnName.hashCode();
        result = prime * result + columnType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getSimpleName();
    }

}
