/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerPhotoFileEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.java.util.Maybe;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * A helper class to manage creation and version management of the application's database.
 *
 * @since 1.0.0
 */
@NotThreadSafe
final class BierverkostungDbHelper extends SQLiteOpenHelper {

    @Immutable
    private static final class Constraint {
        private static final String UNIQUE = "UNIQUE";

        private static final String NOT_NULL = "NOT NULL";

        private Constraint() {
            throw new AssertionError();
        }
    }

    private static final String DATABASE_NAME = "bierverkostung.db";

    /**
     * Constructs a new {@code BierverkostungDbHelper} object.
     *
     * @param context the Context to use to open or create the database.
     * @throws NullPointerException if {@code context} is {@code null}.
     */
    public BierverkostungDbHelper(final Context context) {
        super(isNotNull(context, "context"), DATABASE_NAME, null, DbVersion.V_2);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        createCountriesTable(db);
        createBreweriesTable(db);
        createBeerStylesTable(db);
        createBeersTable(db);
        createTastingsTable(db);
        createBeerPhotoFilesTable(db);
        createBeerPhotosTable(db);
    }

    private static void createCountriesTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(CountryEntry.TABLE)
                .withPrimaryKeyColumn(CountryEntry.COLUMN_ID)
                .withColumn(CountryEntry.COLUMN_REVISION)
                .withColumn(CountryEntry.COLUMN_NAME, Constraint.UNIQUE, Constraint.NOT_NULL)
                .toString();

        db.execSQL(sql);
    }

    private static void createBreweriesTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(BreweryEntry.TABLE)
                .withPrimaryKeyColumn(BreweryEntry.COLUMN_ID)
                .withColumn(BreweryEntry.COLUMN_REVISION)
                .withColumn(BreweryEntry.COLUMN_NAME, Constraint.NOT_NULL)
                .withColumn(BreweryEntry.COLUMN_LOCATION)
                .withColumn(BreweryEntry.COLUMN_COUNTRY_ID)
                .withForeignKey(BreweryEntry.COLUMN_COUNTRY_ID, CountryEntry.COLUMN_ID)
                .toString();

        db.execSQL(sql);
    }

    private static void createBeerStylesTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(BeerStyleEntry.TABLE)
                .withPrimaryKeyColumn(BeerStyleEntry.COLUMN_ID)
                .withColumn(BeerStyleEntry.COLUMN_REVISION)
                .withColumn(BeerStyleEntry.COLUMN_NAME, Constraint.UNIQUE, Constraint.NOT_NULL)
                .toString();

        db.execSQL(sql);
    }

    private static void createBeersTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(BeerEntry.TABLE)
                .withPrimaryKeyColumn(BeerEntry.COLUMN_ID)
                .withColumn(BeerEntry.COLUMN_REVISION)
                .withColumn(BeerEntry.COLUMN_NAME, Constraint.NOT_NULL)
                .withColumn(BeerEntry.COLUMN_BREWERY_ID)
                .withColumn(BeerEntry.COLUMN_STYLE_ID)
                .withColumn(BeerEntry.COLUMN_ORIGINAL_WORT)
                .withColumn(BeerEntry.COLUMN_ALCOHOL)
                .withColumn(BeerEntry.COLUMN_IBU)
                .withColumn(BeerEntry.COLUMN_INGREDIENTS)
                .withColumn(BeerEntry.COLUMN_SPECIFICS)
                .withColumn(BeerEntry.COLUMN_NOTES)
                .withForeignKey(BeerEntry.COLUMN_STYLE_ID, BeerStyleEntry.COLUMN_ID)
                .withForeignKey(BeerEntry.COLUMN_BREWERY_ID, BreweryEntry.COLUMN_ID)
                .toString();

        db.execSQL(sql);
    }

    private static void createTastingsTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(TastingEntry.TABLE)
                .withPrimaryKeyColumn(TastingEntry.COLUMN_ID)
                .withColumn(TastingEntry.COLUMN_REVISION)
                .withColumn(TastingEntry.COLUMN_DATE)
                .withColumn(TastingEntry.COLUMN_LOCATION)
                .withColumn(TastingEntry.COLUMN_BEER_ID)
                .withColumn(TastingEntry.COLUMN_BEER_COLOUR)
                .withColumn(TastingEntry.COLUMN_BEER_COLOUR_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_COLOUR_EBC)
                .withColumn(TastingEntry.COLUMN_CLARITY_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_FOAM_COLOUR)
                .withColumn(TastingEntry.COLUMN_FOAM_STRUCTURE)
                .withColumn(TastingEntry.COLUMN_FOAM_STABILITY)
                .withColumn(TastingEntry.COLUMN_FRUIT_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_FRUIT_RATING)
                .withColumn(TastingEntry.COLUMN_FLOWER_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_FLOWER_RATING)
                .withColumn(TastingEntry.COLUMN_VEGETAL_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_VEGETAL_RATING)
                .withColumn(TastingEntry.COLUMN_SPICY_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_SPICY_RATING)
                .withColumn(TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_WARMTH_MINTED_RATING)
                .withColumn(TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_BIOLOGICAL_RATING)
                .withColumn(TastingEntry.COLUMN_BITTERNESS_RATING)
                .withColumn(TastingEntry.COLUMN_SWEETNESS_RATING)
                .withColumn(TastingEntry.COLUMN_ACIDITY_RATING)
                .withColumn(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_FULL_BODIED_RATING)
                .withColumn(TastingEntry.COLUMN_BODY_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_AFTERTASTE_RATING)
                .withColumn(TastingEntry.COLUMN_FOOD_RECOMMENDATION)
                .withColumn(TastingEntry.COLUMN_TOTAL_IMPRESSION_DESCRIPTION)
                .withColumn(TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING)
                .withForeignKey(TastingEntry.COLUMN_BEER_ID, BeerEntry.COLUMN_ID)
                .toString();

        db.execSQL(sql);
    }

    private static void createBeerPhotoFilesTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(BeerPhotoFileEntry.TABLE)
                .withPrimaryKeyColumn(BeerPhotoFileEntry.COLUMN_ID)
                .withColumn(BeerPhotoFileEntry.COLUMN_REVISION)
                .withColumn(BeerPhotoFileEntry.COLUMN_NAME, Constraint.UNIQUE, Constraint.NOT_NULL)
                .toString();

        db.execSQL(sql);
    }

    private static void createBeerPhotosTable(final SQLiteDatabase db) {
        final String sql = CreateTableSqlBuilder.newInstance(BeerPhotoEntry.TABLE)
                .withPrimaryKeyColumn(BeerPhotoEntry.COLUMN_ID)
                .withColumn(BeerPhotoEntry.COLUMN_REVISION)
                .withColumn(BeerPhotoEntry.COLUMN_BEER_ID, Constraint.NOT_NULL)
                .withColumn(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID, Constraint.NOT_NULL)
                .withForeignKey(BeerPhotoEntry.COLUMN_BEER_ID, BeerEntry.COLUMN_ID)
                .withForeignKey(BeerPhotoEntry.COLUMN_PHOTO_FILE_ID, BeerPhotoFileEntry.COLUMN_ID)
                .toString();

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // If this method grows, introduce strategies or a chain of responsibility for the upgrade steps.
        if (DbVersion.V_1 == oldVersion && DbVersion.V_2 == newVersion) {
            recreateTablesWithChangedSchema(db);
            createBeerPhotoFilesTable(db);
            createBeerPhotosTable(db);
        }
    }

    private static void recreateTablesWithChangedSchema(final SQLiteDatabase db) {
        disableForeignKeyPragma(db);

        // Used to share ID mappings between table migrations.
        final IdMigrationMapping idMigrationMapping = IdMigrationMapping.getInstance();

        // The order is important here to have the new IDs for usage as foreign keys.
        new RecreateCountriesTable(db, idMigrationMapping).run();
        new RecreateBreweriesTable(db, idMigrationMapping).run();
        new RecreateBeerStylesTable(db, idMigrationMapping).run();
        new RecreateBeersTable(db, idMigrationMapping).run();
        new RecreateTastingsTable(db, idMigrationMapping).run();

        enableForeignKeyPragma(db);
    }

    private static void disableForeignKeyPragma(final SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=OFF");
    }

    private static void enableForeignKeyPragma(final SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON");
    }

    @NotThreadSafe
    private abstract static class RecreateTableWithV2Schema implements Runnable {
        private final SQLiteDatabase db;
        private final Table table;
        private final IdMigrationMapping idMigrationMapping;

        protected RecreateTableWithV2Schema(final SQLiteDatabase theDb, final Table theTable,
                final IdMigrationMapping theIdMigrationMapping) {

            db = theDb;
            table = theTable;
            idMigrationMapping = theIdMigrationMapping;
        }

        @Override
        public void run() {
            final String temporaryCopyName = createTemporaryCopyOfOldTable();
            dropTable(table.getName());
            createNewTableWithV2Schema(db);
            insertDataFromTemporaryCopyToNewTableV2(temporaryCopyName);
            dropTable(temporaryCopyName);
        }

        private String createTemporaryCopyOfOldTable() {
            final String result = "tmp_" + table.getName();
            final String sqlPattern = "CREATE TABLE {0} AS SELECT * FROM {1};";
            db.execSQL(MessageFormat.format(sqlPattern, result, table.getName()));
            return result;
        }

        private void dropTable(final String tableName) {
            db.execSQL(MessageFormat.format("DROP TABLE {0};", tableName));
        }

        protected abstract void createNewTableWithV2Schema(SQLiteDatabase db);

        private void insertDataFromTemporaryCopyToNewTableV2(final String temporaryCopyName) {
            final String querySql = MessageFormat.format("SELECT * FROM {0}", temporaryCopyName);
            final Cursor cursor = db.rawQuery(querySql, null);

            if (null == cursor) {
                return;
            }
            try {
                while (cursor.moveToNext()) {
                    insertDataFromTemporaryCopyToNewTableV2(cursor);
                }
            } finally {
                cursor.close();
            }
        }

        private void insertDataFromTemporaryCopyToNewTableV2(final Cursor cursor) {
            final ContentValues contentValues = getContentValues(cursor, idMigrationMapping);
            contentValues.put(ExtendedBaseColumns._REVISION, 0);
            db.insert(table.getName(), null, contentValues);
        }

        protected abstract ContentValues getContentValues(Cursor cursor,
                IdMigrationMapping idMigrationMapping);

        protected String getNewIdAsString(final Cursor cursor) {
            return String.valueOf(idMigrationMapping.putMapping(table,
                    cursor.getInt(cursor.getColumnIndex(ExtendedBaseColumns._ID))));
        }
    }

    @NotThreadSafe
    private static final class IdMigrationMapping {
        private final Map<String, SparseArray<UUID>> mapping;

        private IdMigrationMapping() {
            mapping = new HashMap<>();
        }

        static IdMigrationMapping getInstance() {
            return new IdMigrationMapping();
        }

        UUID putMapping(final Table table, final Integer oldId) {
            SparseArray<UUID> idMapping = mapping.get(table.getName());
            if (null == idMapping) {
                idMapping = new SparseArray<>();
                mapping.put(table.getName(), idMapping);
            }

            final UUID result = UUID.randomUUID();
            idMapping.put(oldId, result);
            return result;
        }

        Maybe<UUID> getNewIdForOldId(final Table table, final Integer oldId) {
            final SparseArray<UUID> idMapping = mapping.get(table.getName());
            if (null != idMapping) {
                return Maybe.ofNullable(idMapping.get(oldId));
            }
            return Maybe.empty();
        }
    }

    @NotThreadSafe
    private static final class RecreateCountriesTable extends RecreateTableWithV2Schema {
        private RecreateCountriesTable(final SQLiteDatabase db, final IdMigrationMapping idMigrationMapping) {
            super(db, CountryEntry.TABLE, idMigrationMapping);
        }

        @Override
        protected void createNewTableWithV2Schema(final SQLiteDatabase db) {
            BierverkostungDbHelper.createCountriesTable(db);
        }

        @Override
        protected ContentValues getContentValues(final Cursor cursor, final IdMigrationMapping idMigrationMapping) {
            final ContentValues result = new ContentValues();
            result.put(ExtendedBaseColumns._ID, getNewIdAsString(cursor));
            result.put(CountryEntry.COLUMN_NAME.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(CountryEntry.COLUMN_NAME.getSimpleName())));
            return result;
        }
    }

    @NotThreadSafe
    private static final class RecreateBreweriesTable extends RecreateTableWithV2Schema {
        private RecreateBreweriesTable(final SQLiteDatabase db, final IdMigrationMapping idMigrationMapping) {
            super(db, BreweryEntry.TABLE, idMigrationMapping);
        }

        @Override
        protected void createNewTableWithV2Schema(final SQLiteDatabase db) {
            BierverkostungDbHelper.createBreweriesTable(db);
        }

        @Override
        protected ContentValues getContentValues(final Cursor cursor, final IdMigrationMapping idMigrationMapping) {
            final ContentValues result = new ContentValues();
            result.put(ExtendedBaseColumns._ID, getNewIdAsString(cursor));
            result.put(BreweryEntry.COLUMN_NAME.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BreweryEntry.COLUMN_NAME.getSimpleName())));
            result.put(BreweryEntry.COLUMN_LOCATION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BreweryEntry.COLUMN_LOCATION.getSimpleName())));

            final String countryIdCol = BreweryEntry.COLUMN_COUNTRY_ID.getSimpleName();
            idMigrationMapping.getNewIdForOldId(CountryEntry.TABLE, cursor.getInt(cursor.getColumnIndex(countryIdCol)))
                    .map(String::valueOf)
                    .ifPresent(newCountryId -> result.put(countryIdCol, newCountryId));

            return result;
        }
    }

    @NotThreadSafe
    private static final class RecreateBeerStylesTable extends RecreateTableWithV2Schema {
        private RecreateBeerStylesTable(final SQLiteDatabase db, final IdMigrationMapping idMigrationMapping) {
            super(db, BeerStyleEntry.TABLE, idMigrationMapping);
        }

        @Override
        protected void createNewTableWithV2Schema(final SQLiteDatabase db) {
            BierverkostungDbHelper.createBeerStylesTable(db);
        }

        @Override
        protected ContentValues getContentValues(final Cursor cursor, final IdMigrationMapping idMigrationMapping) {
            final ContentValues result = new ContentValues();
            result.put(ExtendedBaseColumns._ID, getNewIdAsString(cursor));
            result.put(BeerStyleEntry.COLUMN_NAME.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BeerStyleEntry.COLUMN_NAME.getSimpleName())));
            return result;
        }
    }

    @NotThreadSafe
    private static final class RecreateBeersTable extends RecreateTableWithV2Schema {
        private RecreateBeersTable(final SQLiteDatabase db, final IdMigrationMapping idMigrationMapping) {
            super(db, BeerEntry.TABLE, idMigrationMapping);
        }

        @Override
        protected void createNewTableWithV2Schema(final SQLiteDatabase db) {
            BierverkostungDbHelper.createBeersTable(db);
        }

        @Override
        protected ContentValues getContentValues(final Cursor cursor, final IdMigrationMapping idMigrationMapping) {
            final ContentValues result = new ContentValues();
            result.put(ExtendedBaseColumns._ID, getNewIdAsString(cursor));
            result.put(BeerEntry.COLUMN_NAME.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BeerEntry.COLUMN_NAME.getSimpleName())));

            final String breweryIdCol = BeerEntry.COLUMN_BREWERY_ID.getSimpleName();
            idMigrationMapping.getNewIdForOldId(BreweryEntry.TABLE, cursor.getInt(cursor.getColumnIndex(breweryIdCol)))
                    .map(String::valueOf)
                    .ifPresent(newBreweryId -> result.put(breweryIdCol, newBreweryId));

            final String styleIdCol = BeerEntry.COLUMN_STYLE_ID.getSimpleName();
            idMigrationMapping.getNewIdForOldId(BeerStyleEntry.TABLE, cursor.getInt(cursor.getColumnIndex(styleIdCol)))
                    .map(String::valueOf)
                    .ifPresent(newBreweryId -> result.put(styleIdCol, newBreweryId));

            result.put(BeerEntry.COLUMN_ORIGINAL_WORT.getSimpleName(), String.valueOf(
                    cursor.getDouble(cursor.getColumnIndex(BeerEntry.COLUMN_ORIGINAL_WORT.getSimpleName()))));
            result.put(BeerEntry.COLUMN_ALCOHOL.getSimpleName(),
                    String.valueOf(cursor.getDouble(cursor.getColumnIndex(BeerEntry.COLUMN_ALCOHOL.getSimpleName()))));
            result.put(BeerEntry.COLUMN_IBU.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(BeerEntry.COLUMN_IBU.getSimpleName())));
            result.put(BeerEntry.COLUMN_INGREDIENTS.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BeerEntry.COLUMN_INGREDIENTS.getSimpleName())));
            result.put(BeerEntry.COLUMN_SPECIFICS.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BeerEntry.COLUMN_SPECIFICS.getSimpleName())));
            result.put(BeerEntry.COLUMN_NOTES.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(BeerEntry.COLUMN_NOTES.getSimpleName())));

            return result;
        }
    }

    @NotThreadSafe
    private static final class RecreateTastingsTable extends RecreateTableWithV2Schema {
        private RecreateTastingsTable(final SQLiteDatabase db, final IdMigrationMapping idMigrationMapping) {
            super(db, TastingEntry.TABLE, idMigrationMapping);
        }

        @Override
        protected void createNewTableWithV2Schema(final SQLiteDatabase db) {
            BierverkostungDbHelper.createTastingsTable(db);
        }

        @Override
        protected ContentValues getContentValues(final Cursor cursor, final IdMigrationMapping idMigrationMapping) {
            final ContentValues result = new ContentValues();

            result.put(ExtendedBaseColumns._ID, getNewIdAsString(cursor));
            result.put(TastingEntry.COLUMN_DATE.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_DATE.getSimpleName())));
            result.put(TastingEntry.COLUMN_LOCATION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_LOCATION.getSimpleName())));

            final String beerIdCol = TastingEntry.COLUMN_BEER_ID.getSimpleName();
            idMigrationMapping.getNewIdForOldId(BeerEntry.TABLE, cursor.getInt(cursor.getColumnIndex(beerIdCol)))
                    .map(String::valueOf)
                    .ifPresent(newBreweryId -> result.put(beerIdCol, newBreweryId));

            result.put(TastingEntry.COLUMN_BEER_COLOUR.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_BEER_COLOUR.getSimpleName())));
            result.put(TastingEntry.COLUMN_BEER_COLOUR_DESCRIPTION.getSimpleName(), cursor.getString(
                    cursor.getColumnIndex(TastingEntry.COLUMN_BEER_COLOUR_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_COLOUR_EBC.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_COLOUR_EBC.getSimpleName())));
            result.put(TastingEntry.COLUMN_CLARITY_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_CLARITY_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_FOAM_COLOUR.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_FOAM_COLOUR.getSimpleName())));
            result.put(TastingEntry.COLUMN_FOAM_STRUCTURE.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_FOAM_STRUCTURE.getSimpleName())));
            result.put(TastingEntry.COLUMN_FOAM_STABILITY.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_FOAM_STABILITY.getSimpleName())));
            result.put(TastingEntry.COLUMN_FRUIT_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_FRUIT_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_FRUIT_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_FRUIT_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_FLOWER_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_FLOWER_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_FLOWER_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_FLOWER_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_VEGETAL_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_VEGETAL_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_VEGETAL_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_VEGETAL_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_SPICY_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_SPICY_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_SPICY_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_SPICY_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_WARMTH_MINTED_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_WARMTH_MINTED_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_WARMTH_MINTED_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_BIOLOGICAL_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_BIOLOGICAL_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_BIOLOGICAL_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_BITTERNESS_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex("bitterness_bitterness")));
            result.put(TastingEntry.COLUMN_SWEETNESS_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_SWEETNESS_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_ACIDITY_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_ACIDITY_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_FULL_BODIED_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_FULL_BODIED_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_BODY_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_BODY_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_AFTERTASTE_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_AFTERTASTE_RATING.getSimpleName())));
            result.put(TastingEntry.COLUMN_FOOD_RECOMMENDATION.getSimpleName(),
                    cursor.getString(cursor.getColumnIndex(TastingEntry.COLUMN_FOOD_RECOMMENDATION.getSimpleName())));
            result.put(TastingEntry.COLUMN_TOTAL_IMPRESSION_DESCRIPTION.getSimpleName(), cursor.getString(
                    cursor.getColumnIndex(TastingEntry.COLUMN_TOTAL_IMPRESSION_DESCRIPTION.getSimpleName())));
            result.put(TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING.getSimpleName(),
                    cursor.getInt(cursor.getColumnIndex(TastingEntry.COLUMN_TOTAL_IMPRESSION_RATING.getSimpleName())));

            return result;
        }
    }

}
