/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

/**
 * This is an enumeration of known and valid URI nodes. These are meant to be used for URI matching from within the
 * content provider. The Android tutorial uses plain integers for matching but an enum is more concise.
 *
 * @since 1.0.0
 */
enum UriNode {

    NONE(-1),

    TASTINGS(100),

    TASTING_WITH_ID(101),

    BEERS(200),

    BEER_WITH_ID(201),

    BEER_STYLES(300),

    BEER_STYLE_WITH_ID(301),

    BREWERIES(400),

    BREWERY_WITH_ID(401),

    COUNTRIES(500),

    COUNTRY_WITH_ID(501),

    BEER_PHOTOS(600),

    BEER_PHOTO_WITH_ID(601),

    BEER_PHOTO_FILES(700),

    BEER_PHOTO_FILE_WITH_ID(701),

    SINGLE_COLUMN_VALUES(10000);

    private final int code;

    private UriNode(final int theCode) {
        code = theCode;
    }

    /**
     * Returns the UriNode constant which is associated with the given code.
     *
     * @param code the code to the the associated UriNode for.
     * @return the UriNode with the given code or {@link #NONE}.
     */
    public static UriNode forCode(final int code) {
        if (-1 == code) {
            return UriNode.NONE;
        }
        for (final UriNode uriNode : values()) {
            if (code == uriNode.code) {
                return uriNode;
            }
        }
        return UriNode.NONE;
    }

    /**
     * Returns the code of this UriNode.
     *
     * @return the code.
     */
    public int getCode() {
        return code;
    }

}
