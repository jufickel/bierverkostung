/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.os.AsyncTask;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.annotation.concurrent.NotThreadSafe;

import de.retujo.java.util.Acceptor;
import de.retujo.java.util.AllNonnull;

import static de.retujo.java.util.Conditions.isNotNull;

/**
 * Abstract implementation of an AsyncTask for deleting a set of entities. The actual deletion is delegated to
 * implementations.
 *
 * @param <S> the "self" type of the implementing class.
 * @param <T> the the type of the entities to be deleted by this task.
 *
 * @since 1.2.0
 */
@AllNonnull
@NotThreadSafe
public abstract class AbstractDeleteTask<S extends AbstractDeleteTask<S, T>, T>
        extends AsyncTask<T, Void, Collection<T>> {

    private S myself;
    private Collection<T> entitiesToBeDeleted;
    private Acceptor<Collection<T>> onDeletedListener;
    private Acceptor<Collection<T>> onFailedToDeleteListener;
    private Acceptor<Collection<T>> onCancelledListener;

    /**
     * Constructs a new {@code AbstractDeleteTask} object.
     */
    @SuppressWarnings("unchecked")
    protected AbstractDeleteTask() {
        myself = (S) this;
        entitiesToBeDeleted = Collections.emptyList();
        onDeletedListener = deletedEntities -> {
            // Nothing to do per default
        };
        onFailedToDeleteListener = notDeletedEntities -> {
            // Nothing to do per default
        };
        onCancelledListener = deletedEntities -> {
            // Nothing to do per default
        };
    }

    /**
     * Sets the listener to be notified when all entities were deleted successfully. The specified listener is called
     * at the UI thread.
     *
     * @param listener the listener to be set. Its argument are all deleted entities.
     * @return this delete task instance to allow method chaining.
     * @throws NullPointerException if {@code listener} is {@code null}.
     */
    public S setOnDeletedListener(final Acceptor<Collection<T>> listener) {
        onDeletedListener = isNotNull(listener, "on deleted listener");
        return myself;
    }

    /**
     * Sets the listener to be notified when not all entities could be deleted successfully.
     *
     * @param listener the listener to be set. Its argument are all entities which could not be deleted.
     * @return this delete task instance to allow method chaining.
     * @throws NullPointerException if {@code listener} is {@code null}.
     */
    public S setOnFailedToDeleteListener(final Acceptor<Collection<T>> listener) {
        onFailedToDeleteListener = isNotNull(listener, "on failed to delete listener");
        return myself;
    }

    /**
     * Sets the listener to be notified when deletion was cancelled.
     *
     * @param listener the listener to be set. Its argument are all entities which were successfully deleted until
     * cancellation.
     * @return this delete task instance to allow method chaining.
     * @throws NullPointerException if {@code listener} is {@code null}.
     */
    public S setOnCancelledListener(final Acceptor<Collection<T>> listener) {
        onCancelledListener = isNotNull(listener, "on cancelled listener");
        return myself;
    }

    @Override
    protected Collection<T> doInBackground(final T[] entitiesToBeDeleted) {
        isNotNull(entitiesToBeDeleted, "entities to be deleted");
        this.entitiesToBeDeleted = Arrays.asList(entitiesToBeDeleted);

        final Collection<T> result = new ArrayList<>(entitiesToBeDeleted.length);
        for (int i = 0; i < entitiesToBeDeleted.length; i++) {
            final T entityToBeDeleted = getEntityToBeDeleted(entitiesToBeDeleted, i);
            if (doDelete(entityToBeDeleted)) {
                result.add(entityToBeDeleted);
            }
        }

        return result;

    }

    private static <T> T getEntityToBeDeleted(final T[] entitiesToBeDeleted, final int i) {
        final T entityToBeDeleted = entitiesToBeDeleted[i];
        return isNotNull(entityToBeDeleted, MessageFormat.format("entity to be deleted (index {0})", i));
    }

    /**
     * Performs the actual deletion of the specified entity.
     *
     * @param entityToBeDeleted the entity to be deleted.
     * @return {@code true} if the entity was deleted, {@code false} else.
     */
    protected abstract boolean doDelete(T entityToBeDeleted);

    @Override
    protected void onPostExecute(final Collection<T> deletedEntities) {
        if (allEntitiesDeleted(deletedEntities)) {
            onDeletedListener.accept(deletedEntities);
        } else {
            onFailedToDeleteListener.accept(getNotDeletedEntities(deletedEntities));
        }
    }

    private boolean allEntitiesDeleted(final Collection<T> deletedEntities) {
        return entitiesToBeDeleted.size() == deletedEntities.size();
    }

    private Collection<T> getNotDeletedEntities(final Collection<T> deletedEntities) {
        final Collection<T> result = new ArrayList<>(entitiesToBeDeleted.size() - deletedEntities.size());
        for (final T expectedEntity : entitiesToBeDeleted) {
            if (!deletedEntities.contains(expectedEntity)) {
                result.add(expectedEntity);
            }
        }
        return result;
    }

    @Override
    protected void onCancelled() {
        onCancelledListener.accept(Collections.emptyList());
    }

    @Override
    protected void onCancelled(final Collection<T> deletedEntities) {
        onCancelledListener.accept(deletedEntities);
    }

}
