/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;

import java.util.List;

import de.retujo.java.util.AllNonnull;
import de.retujo.java.util.Maybe;

/**
 * This interface defines a database table.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface Table extends CharSequence {

    /**
     * Creates a new {@code String} column which belongs to this table.
     *
     * @param name the name of the column.
     * @return the new column.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     * @since 1.2.0
     */
    Column<String> createStringColumn(CharSequence name);

    /**
     * Creates a new {@code int} column which belongs to this table.
     *
     * @param name the name of the column.
     * @return the new column.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     * @since 1.2.0
     */
    Column<Integer> createIntColumn(CharSequence name);

    /**
     * Creates a new {@code long} column which belongs to this table.
     *
     * @param name the name of the column.
     * @return the new column.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     * @since 1.2.0
     */
    Column<Long> createLongColumn(CharSequence name);

    /**
     * Creates a new {@code double} column which belongs to this table.
     *
     * @param name the name of the column.
     * @return the new column.
     * @throws NullPointerException if any argument is {@code null}.
     * @throws IllegalArgumentException if {@code name} is empty.
     * @since 1.2.0
     */
    Column<Double> createDoubleColumn(CharSequence name);

    /**
     * Returns the columns of this table which were created by calling the {@code createColumn} methods.
     *
     * @return an copy of the list with the columns. The order is the creation order. If this table does not have
     * columns an empty list is returned.
     */
    List<Column> getColumns();

    /**
     * Returns the column of this table which has the specified name.
     *
     * @param columnName the name of the column to be found.
     * @return the found column.
     * @throws NullPointerException if {@code columnName} is {@code null}.
     * @since 1.2.0
     */
    Maybe<Column> getColumn(CharSequence columnName);

    /**
     * Returns the column of this table which has the specified name. If this table does not contain a column with
     * the given name a NullPointerException is thrown.
     *
     * @param columnName the name of the column to be found.
     * @return the found column.
     * @throws NullPointerException if {@code columnName} is {@code null} or if this table did not contain a column
     * with the specified name.
     * @since 1.2.0
     */
    <T> Column<T> getColumnOrThrow(CharSequence columnName);

    /**
     * Returns the full content URI of this table, i. e. the base content URI with this table's name appended.
     *
     * @return the content URI.
     * @since 1.2.0
     */
    Uri getContentUri();

    /**
     * Returns the name of this column.
     *
     * @return the name.
     * @since 1.2.0
     */
    String getName();

}
