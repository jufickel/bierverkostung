/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

import de.retujo.java.util.AllNonnull;

/**
 * Base implementation of {@link Parcelable.Creator} which by default already reads the properties required by
 * {@link EntityCommonData} from the Parcel.
 *
 * @since 1.2.0
 */
@AllNonnull
public abstract class BaseParcelableCreator<T extends DataEntity> implements Parcelable.Creator<T> {

    /**
     * Constructs a new {@code BaseParcelableCreator} object.
     */
    protected BaseParcelableCreator() {
        super();
    }

    @Override
    public T createFromParcel(final Parcel source) {
        return createFromParcel(source, EntityCommonData.getInstance(readId(source), readRevision(source)));
    }

    private static UUID readId(final Parcel source) {
        return UUID.fromString(source.readString());
    }

    private static Revision readRevision(final Parcel source) {
        return Revision.of(source.readInt());
    }

    /**
     * Creates a new instance of the Parcelable class, instantiating it from the given Parcel whose data had previously
     * been written by {@link Parcelable#writeToParcel Parcelable.writeToParcel()}.
     *
     * @param source The Parcel to read the object's data from.
     * @param commonData common data like ID, UUID and timestamp.
     * @return a new instance of the Parcelable class.
     */
    protected abstract T createFromParcel(Parcel source, EntityCommonData commonData);

}
