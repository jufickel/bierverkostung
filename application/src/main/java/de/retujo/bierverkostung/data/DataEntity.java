/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;
import android.os.Parcelable;

import java.util.UUID;

import de.retujo.java.util.AllNonnull;

/**
 * {@code DataEntity} is the link from database layer to the domain model. An implementation of this class is Parcelable
 * and thus can be used to share domain model objects with Intents.
 *
 * @since 1.0.0
 */
@AllNonnull
public interface DataEntity extends Parcelable, ContentValuesRepresentable {

    /**
     * Returns the universally unique identifier of this data entity. This ID will never change for this entity.
     *
     * @return the identifier.
     * @since 1.2.0
     */
    UUID getId();

    /**
     * Returns the content URI of this row data.
     *
     * @return the content URI.
     */
    Uri getContentUri();

    /**
     * Returns the revision for tracking changes of this entity and comparing data entities with same ID.
     *
     * @return the revision.
     */
    Revision getRevision();

}
