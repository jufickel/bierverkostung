/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;

import javax.annotation.concurrent.Immutable;

/**
 * This class defines to contract so that clients know how to access the beer data.
 *
 * @since 1.0.0
 */
@Immutable
public final class BierverkostungContract {

    /**
     * The authority, which is how your code knows which Content Provider to access.
     */
    public static final String AUTHORITY = "de.retujo.bierverkostung";

    /**
     * The base content URI = {@code "content://" + <authority>}.
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    /**
     * The path for the countries directory.
     */
    public static final String PATH_COUNTRIES = "countries";

    /**
     * The path for the breweries directory.
     */
    public static final String PATH_BREWERIES = "breweries";

    /**
     * The path for the beer styles directory.
     */
    public static final String PATH_BEER_STYLES = "beer_styles";

    /**
     * Define the possible paths for accessing data in this contract. This is the path for the "beers" directory.
     */
    public static final String PATH_BEERS = "beers";

    /**
     * The path for the tastings directory.
     */
    public static final String PATH_TASTINGS = "tastings";

    /**
     * The path for the photos directory.
     *
     * @since 1.2.0
     */
    public static final String PATH_BEER_PHOTO_FILES = "beer_photo_files";

    /**
     * The path for the beer photos directory.
     *
     * @since 1.2.0
     */
    public static final String PATH_BEER_PHOTOS = "beer_photos";

    /**
     * The path for a sub-directory for accessing the values of a single table column.
     */
    public static final String PATH_SINGLE_COLUMN_VALUES = "column_values";

    /**
     * CountryEntry defines the contents of the countries table.
     */
    @Immutable
    public static final class CountryEntry implements ExtendedBaseColumns {
        /**
         * Table which contains the countries.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_COUNTRIES);

        /**
         * Column which contains the unique IDs of the countries.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the country's revision number.
         *
         * @since 1.2.0
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the names of the countries.
         */
        public static final Column<String> COLUMN_NAME = TABLE.createStringColumn("country_name");

        private CountryEntry() {
            throw new AssertionError();
        }
    }

    /**
     * BreweryEntry defines the contents of the breweries table.
     */
    @Immutable
    public static final class BreweryEntry implements ExtendedBaseColumns {
        /**
         * Table which contains the breweries.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_BREWERIES);

        /**
         * Column which contains the breweries' unique ID.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the breweries' revision number.
         *
         * @since 1.2.0
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the breweries' name.
         */
        public static final Column<String> COLUMN_NAME = TABLE.createStringColumn("brewery_name");

        /**
         * Column which contains the breweries' location.
         */
        public static final Column<String> COLUMN_LOCATION = TABLE.createStringColumn("brewery_location");

        /**
         * Column which contains the breweries' country.
         */
        public static final Column<String> COLUMN_COUNTRY_ID = TABLE.createStringColumn("country_id");

        private BreweryEntry() {
            throw new AssertionError();
        }
    }

    /**
     * BeerStyleEntry defines the contents of the breweries table.
     */
    @Immutable
    public static final class BeerStyleEntry implements ExtendedBaseColumns {
        /**
         * Table which contains the beer styles.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_BEER_STYLES);

        /**
         * Column which contains the beer style's unique IDs.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the beer style's revision number.
         *
         * @since 1.2.0
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the beer style's name.
         */
        public static final Column<String> COLUMN_NAME = TABLE.createStringColumn("beer_style_name");

        private BeerStyleEntry() {
            throw new AssertionError();
        }
    }

    /**
     * BeerEntry defines the contents of the beers table.
     */
    @Immutable
    public static final class BeerEntry implements ExtendedBaseColumns {
        /**
         * Name of the table containing the beer data.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_BEERS);

        /**
         * The {@code content://} style URI for an arbitrary column of the "beers" table. It distinctly requests all
         * values of a particular table matching the selection criteria.
         */
        public static final Uri CONTENT_URI_SINGLE_COLUMN = TABLE.getContentUri().buildUpon()
                .appendPath(PATH_SINGLE_COLUMN_VALUES)
                .build();

        /**
         * Column which contains the beer's unique IDs.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the beer's revision number.
         *
         * @since 1.2.0
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the names of the beers.
         */
        public static final Column<String> COLUMN_NAME = TABLE.createStringColumn("beer_name");

        /**
         * Column which contains the IDs of the breweries of the beers.
         */
        public static final Column<String> COLUMN_BREWERY_ID = TABLE.createStringColumn("brewery_id");

        /**
         * Column which contains the IDs of the beer styles of the beers.
         */
        public static final Column<String> COLUMN_STYLE_ID = TABLE.createStringColumn("style_id");

        /**
         * Column which contains the original wort of the beers.
         */
        public static final Column<String> COLUMN_ORIGINAL_WORT = TABLE.createStringColumn("original_wort");

        /**
         * Column which contains the volume alcohol of the beers.
         */
        public static final Column<String> COLUMN_ALCOHOL = TABLE.createStringColumn("alcohol");

        /**
         * Column which contains the IBU of the beers.
         */
        public static final Column<Integer> COLUMN_IBU = TABLE.createIntColumn("ibu");

        /**
         * Column which contains the ingredients of the beers.
         */
        public static final Column<String> COLUMN_INGREDIENTS = TABLE.createStringColumn("ingredients");

        /**
         * Column which contains the specifics of the beers.
         */
        public static final Column<String> COLUMN_SPECIFICS = TABLE.createStringColumn("specifics");

        /**
         * Column which contains the notes of the beers.
         */
        public static final Column<String> COLUMN_NOTES = TABLE.createStringColumn("beer_notes");

        private BeerEntry() {
            throw new AssertionError();
        }
    }

    /**
     * TastingEntry defines the contents of the beers table.
     */
    @Immutable
    public static final class TastingEntry implements ExtendedBaseColumns {
        /**
         * Name of the table containing the tasting data.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_TASTINGS);

        /**
         * The {@code content://} style URI for an arbitrary column of the "tastings" table. It distinctly requests
         * all values of a particular table matching the selection criteria.
         */
        public static final Uri CONTENT_URI_SINGLE_COLUMN = TABLE.getContentUri()
                .buildUpon()
                .appendPath(PATH_SINGLE_COLUMN_VALUES)
                .build();

        /**
         * Column which contains the tasting's unique IDs.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the tasting's revision number.
         *
         * @since 1.2.0
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the date in ISO 8601 when each tasting happened.
         */
        public static final Column<String> COLUMN_DATE = TABLE.createStringColumn("date");

        /**
         * Column which contains the locations where the tastings happened.
         */
        public static final Column<String> COLUMN_LOCATION = TABLE.createStringColumn("location");

        /**
         * Column which contains the ID of the tasted beer.
         */
        public static final Column<String> COLUMN_BEER_ID = TABLE.createStringColumn("beer_id");

        /**
         * Column which contains the perceived beer colour.
         */
        public static final Column<String> COLUMN_BEER_COLOUR = TABLE.createStringColumn("beer_colour");

        /**
         * Column which contains a description of the perceived beer colour.
         */
        public static final Column<String> COLUMN_BEER_COLOUR_DESCRIPTION =
                TABLE.createStringColumn("beer_colour_desc");

        /**
         * Column which contains the perceived beer EBC value.
         */
        public static final Column<Integer> COLUMN_COLOUR_EBC = TABLE.createIntColumn("colour_ebc");

        /**
         * Column which contains the description of the beer's perceived clarity.
         */
        public static final Column<String> COLUMN_CLARITY_DESCRIPTION = TABLE.createStringColumn("clarity");

        /**
         * Column which contains the colour of the beer's foam.
         */
        public static final Column<String> COLUMN_FOAM_COLOUR = TABLE.createStringColumn("foam_colour");

        /**
         * Column which contains the structure of the beer's foam.
         */
        public static final Column<String> COLUMN_FOAM_STRUCTURE = TABLE.createStringColumn("foam_structure");

        /**
         * Column which contains the stability of the beer's foam.
         */
        public static final Column<Integer> COLUMN_FOAM_STABILITY = TABLE.createIntColumn("foam_stability");

        /**
         * Column which contains the description of the beer's perceived fruit flavour (scent).
         */
        public static final Column<String> COLUMN_FRUIT_DESCRIPTION = TABLE.createStringColumn("fruit_desc");

        /**
         * Column which contains the rating of the beer's perceived fruit flavour (scent).
         */
        public static final Column<Integer> COLUMN_FRUIT_RATING = TABLE.createIntColumn("fruit_rating");

        /**
         * Column which contains the description of the beer's perceived flower flavour (scent).
         */
        public static final Column<String> COLUMN_FLOWER_DESCRIPTION = TABLE.createStringColumn("flower_desc");

        /**
         * Column which contains the rating of the beer's perceived flower flavour (scent).
         */
        public static final Column<Integer> COLUMN_FLOWER_RATING = TABLE.createIntColumn("flower_rating");

        /**
         * Column which contains the description of the beer's perceived vegetal flavour (scent).
         */
        public static final Column<String> COLUMN_VEGETAL_DESCRIPTION = TABLE.createStringColumn("vegetal_desc");

        /**
         * Column which contains the rating of the beer's perceived vegetal flavour (scent).
         */
        public static final Column<Integer> COLUMN_VEGETAL_RATING = TABLE.createIntColumn("vegetal_rating");

        /**
         * Column which contains the description of the beer's perceived spicy flavour (scent).
         */
        public static final Column<String> COLUMN_SPICY_DESCRIPTION = TABLE.createStringColumn("spicy_desc");

        /**
         * Column which contains the rating of the beer's perceived spicy flavour (scent).
         */
        public static final Column<Integer> COLUMN_SPICY_RATING = TABLE.createIntColumn("spicy_rating");

        /**
         * Column which contains the description of the beer's perceived warmth-minted flavour (scent).
         */
        public static final Column<String> COLUMN_WARMTH_MINTED_DESCRIPTION =
                TABLE.createStringColumn("warmth_minted_desc");

        /**
         * Column which contains the rating of the beer's perceived warmth-minted flavour (scent).
         */
        public static final Column<Integer> COLUMN_WARMTH_MINTED_RATING = TABLE.createIntColumn("warmth_minted_rating");

        /**
         * Column which contains the description of the beer's perceived biological flavour (scent).
         */
        public static final Column<String> COLUMN_BIOLOGICAL_DESCRIPTION = TABLE.createStringColumn("biological_desc");

        /**
         * Column which contains the rating of the beer's perceived biological flavour (scent).
         */
        public static final Column<Integer> COLUMN_BIOLOGICAL_RATING = TABLE.createIntColumn("biological_rating");

        /**
         * Column which contains the rating for the beer's perceived bitterness (taste).
         */
        public static final Column<Integer> COLUMN_BITTERNESS_RATING = TABLE.createIntColumn("bitterness_rating");

        /**
         * Column which contains the rating for the beer's perceived sweetness (taste).
         */
        public static final Column<Integer> COLUMN_SWEETNESS_RATING = TABLE.createIntColumn("sweetness_rating");

        /**
         * Column which contains the rating for the beer's perceived acidity (taste).
         */
        public static final Column<Integer> COLUMN_ACIDITY_RATING = TABLE.createIntColumn("acidity_rating");

        /**
         * Column which contains the description for the beer's perceived mouthfeel (taste).
         */
        public static final Column<String> COLUMN_MOUTHFEEL_DESCRIPTION = TABLE.createStringColumn("mouth_feel_desc");

        /**
         * Column which contains the rating for the beer's perceived body fullness.
         */
        public static final Column<Integer> COLUMN_FULL_BODIED_RATING = TABLE.createIntColumn("full_bodied_rating");

        /**
         * Column which contains the description for the beer's perceived body (taste).
         */
        public static final Column<String> COLUMN_BODY_DESCRIPTION = TABLE.createStringColumn("body_desc");

        /**
         * Column which contains the description for the beer's perceived aftertaste (taste).
         */
        public static final Column<String> COLUMN_AFTERTASTE_DESCRIPTION = TABLE.createStringColumn("aftertaste_desc");

        /**
         * Column which contains the rating for the beer's perceived aftertaste (taste).
         */
        public static final Column<Integer> COLUMN_AFTERTASTE_RATING = TABLE.createIntColumn("aftertaste_rating");

        /**
         * Column which contains the food recommendation.
         */
        public static final Column<String> COLUMN_FOOD_RECOMMENDATION = TABLE.createStringColumn("food_recommendation");

        /**
         * Column which contains the description for the beer's total impression.
         */
        public static final Column<String> COLUMN_TOTAL_IMPRESSION_DESCRIPTION =
                TABLE.createStringColumn("total_impression_desc");

        /**
         * Column which contains the rating of the total impression of the beer.
         */
        public static final Column<Integer> COLUMN_TOTAL_IMPRESSION_RATING =
                TABLE.createIntColumn("total_impression_rating");

        private TastingEntry() {
            throw new AssertionError();
        }
    }

    /**
     * This class defines the entries of the table which stores file names of beer photos.
     *
     * @since 1.2.0
     */
    @Immutable
    public static final class BeerPhotoFileEntry implements ExtendedBaseColumns {
        /**
         * Name of the table containing the photos data.
         */
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_BEER_PHOTO_FILES);

        /**
         * Column which contains the unique ID of the photo.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the photo's revision number.
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the names of the photos.
         */
        public static final Column<String> COLUMN_NAME = TABLE.createStringColumn("file_name");

        private BeerPhotoFileEntry() {
            throw new AssertionError();
        }
    }

    @Immutable
    public static final class BeerPhotoEntry implements ExtendedBaseColumns {
        public static final Table TABLE = DefaultTable.getInstance(BASE_CONTENT_URI, PATH_BEER_PHOTOS);

        /**
         * Column for the beer photo's unique ID.
         */
        public static final Column<String> COLUMN_ID = TABLE.createStringColumn(_ID);

        /**
         * Column which contains the beer photo's revision number.
         */
        public static final Column<Integer> COLUMN_REVISION = TABLE.createIntColumn(_REVISION);

        /**
         * Column which contains the ID of the associated beer.
         */
        public static final Column<String> COLUMN_BEER_ID = TABLE.createStringColumn("beer_id");

        /**
         * Column which contains the ID of the associated photo.
         */
        public static final Column<String> COLUMN_PHOTO_FILE_ID = TABLE.createStringColumn("photo_file_id");

        private BeerPhotoEntry() {
            throw new AssertionError();
        }
    }

    private BierverkostungContract() {
        throw new AssertionError();
    }

}
