<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright 2017 Juergen Fickel
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
<LinearLayout
    android:id="@+id/activity_show_tasting_scroll_view"
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:custom="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/cardview_dark_background"
    android:orientation="vertical"
    tools:context="de.retujo.bierverkostung.tasting.ShowTastingActivity">

    <include
        layout="@layout/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingBottom="@dimen/vertical_margin_normal"
        android:paddingLeft="@dimen/horizontal_margin_normal"
        android:paddingRight="@dimen/horizontal_margin_normal"
        android:paddingTop="@dimen/vertical_margin_normal">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">


            <!-- General data -->
            <android.support.v7.widget.CardView
                android:id="@+id/show_tasting_metadata_card_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:elevation="100dp"
                android:visibility="visible">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:padding="@dimen/vertical_margin_normal">

                    <TextView
                        android:id="@+id/show_tasting_date"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_marginTop="@dimen/vertical_margin_narrow"
                        tools:text="1. März 2017"/>

                    <TextView
                        android:id="@+id/show_tasting_location"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="@dimen/horizontal_margin_wide"
                        android:layout_marginTop="@dimen/vertical_margin_narrow"
                        android:layout_toEndOf="@id/show_tasting_date"
                        android:visibility="gone"
                        tools:text="Brauhaus Germelshausen"/>

                    <RatingBar
                        android:id="@+id/show_tasting_total_impression_rating"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_date"
                        android:layout_marginTop="@dimen/vertical_margin_narrow"
                        android:isIndicator="true"
                        android:max="3"
                        android:numStars="3"
                        android:rating="1"
                        android:stepSize="1"/>

                    <TextView
                        android:id="@+id/show_tasting_total_impression"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_total_impression_rating"
                        android:visibility="gone"
                        tools:text="So eine super Verkostung. Das Bier erfüllt alle Erwartungen. Lorem ipsum dolor sit amet."/>
                </RelativeLayout>
            </android.support.v7.widget.CardView>

            <!-- Food recommendation -->
            <android.support.v7.widget.CardView
                android:id="@+id/show_tasting_food_recommendation_card_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                android:elevation="100dp"
                android:visibility="gone">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:orientation="vertical"
                    android:padding="@dimen/vertical_margin_normal">

                    <TextView
                        android:id="@+id/show_tasting_header_food_recommendation"
                        style="@style/Base.TextAppearance.AppCompat.Title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/show_tasting_header_food_recommendation"/>

                    <TextView
                        android:id="@+id/show_tasting_food_recommendation"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_narrow"
                        tools:text="At vero eos et accusam et justo duo dolores et ea rebum."/>
                </LinearLayout>
            </android.support.v7.widget.CardView>

            <!-- Beer data -->
            <fragment
                android:id="@+id/show_tasting_beer_details_fragment"
                android:name="de.retujo.bierverkostung.beer.BeerDetailsFragment"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                tools:layout="@layout/fragment_beer_details"/>

            <!-- Brewery data -->
            <fragment
                android:id="@+id/show_tasting_brewery_details_fragment"
                android:name="de.retujo.bierverkostung.brewery.BreweryDetailsFragment"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                tools:layout="@layout/fragment_brewery_details"/>

            <!-- Optical appearance -->
            <android.support.v7.widget.CardView
                android:id="@+id/show_tasting_optical_appearance_card_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                android:elevation="100dp"
                android:visibility="gone">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:padding="@dimen/vertical_margin_normal">

                    <TextView
                        android:id="@+id/show_tasting_header_optical_appearance"
                        style="@style/Base.TextAppearance.AppCompat.Title"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/show_tasting_header_optical_appearance"/>

                    <!-- Colour -->
                    <TextView
                        android:id="@+id/show_tasting_colour_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="230dp"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_header_optical_appearance"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:layout_marginEnd="@dimen/horizontal_margin_wide"
                        android:text="@string/show_tasting_colour_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_colour"
                        style="@style/cardDetailsTextView"
                        android:layout_width="230dp"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_colour_label"
                        android:visibility="gone"
                        tools:text="goldgelb und haselnußbraun"/>

                    <!-- EBC -->
                    <TextView
                        android:id="@+id/show_tasting_ebc_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/show_tasting_header_optical_appearance"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:layout_toEndOf="@id/show_tasting_colour_label"
                        android:text="@string/show_tasting_ebc_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_ebc"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignStart="@id/show_tasting_ebc_label"
                        android:layout_below="@id/show_tasting_ebc_label"
                        android:visibility="gone"
                        tools:text="12"/>

                    <!-- Colour description -->
                    <TextView
                        android:id="@+id/show_tasting_colour_description_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_colour"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_colour_description_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_colour_description"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_colour_description_label"
                        android:visibility="gone"
                        tools:text="leuchtend"/>

                    <!-- Clarity -->
                    <TextView
                        android:id="@+id/show_tasting_clarity_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_colour_description"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_clarity_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_clarity"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_clarity_label"
                        android:visibility="gone"
                        tools:text="kristallklar"/>

                    <!-- Foam colour -->
                    <TextView
                        android:id="@+id/show_tasting_foam_colour_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_clarity"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_foam_colour_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_foam_colour"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_foam_colour_label"
                        android:visibility="gone"
                        tools:text="cremeweiß"/>

                    <!-- Foam structure -->
                    <TextView
                        android:id="@+id/show_tasting_foam_structure_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_foam_colour"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_foam_structure_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_foam_structure"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_foam_structure_label"
                        android:visibility="gone"
                        tools:text="lose strukturiert"/>

                    <!-- Foam stability -->
                    <TextView
                        android:id="@+id/show_tasting_foam_stability_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_foam_structure"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_foam_stability_label"
                        android:visibility="gone"/>

                    <ProgressBar
                        android:id="@+id/show_tasting_foam_stability"
                        style="?android:attr/progressBarStyleHorizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_below="@id/show_tasting_foam_stability_label"
                        android:max="3"
                        android:progress="0"
                        android:visibility="gone"/>
                </RelativeLayout>
            </android.support.v7.widget.CardView>

            <!-- Scent -->
            <android.support.v7.widget.CardView
                android:id="@+id/show_tasting_scent_card_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                android:elevation="100dp"
                android:visibility="gone">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:orientation="vertical"
                    android:padding="@dimen/vertical_margin_normal">

                    <TextView
                        android:id="@+id/show_tasting_header_scent"
                        style="@style/Base.TextAppearance.AppCompat.Title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/show_tasting_header_scent"/>

                    <!-- Fruit scent -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_fruit_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_fruitiness_label"/>

                    <!-- Flower scent -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_flower_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_flowery_label"/>

                    <!-- Vegetal scent -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_vegetal_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_vegetal_label"/>

                    <!-- Spicy scent -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_spicy_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_spicy_label"/>

                    <!-- Warmth-minted -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_warmth_minted_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_warmth_minted_label"/>

                    <!-- Biological -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_biological_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_biological_label"/>
                </LinearLayout>
            </android.support.v7.widget.CardView>

            <!-- Taste -->
            <android.support.v7.widget.CardView
                android:id="@+id/show_tasting_taste_card_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/vertical_margin_normal"
                android:elevation="100dp"
                android:visibility="gone">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:orientation="vertical"
                    android:padding="@dimen/vertical_margin_normal">

                    <TextView
                        android:id="@+id/show_tasting_header_taste"
                        style="@style/Base.TextAppearance.AppCompat.Title"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/show_tasting_header_taste"/>

                    <!-- Bitterness -->
                    <TextView
                        android:id="@+id/show_tasting_bitterness_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_bitterness_label"
                        android:visibility="gone"/>

                    <ProgressBar
                        android:id="@+id/show_tasting_bitterness"
                        style="?android:attr/progressBarStyleHorizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="3"
                        android:progress="0"
                        android:visibility="gone"/>

                    <!-- Sweetness -->
                    <TextView
                        android:id="@+id/show_tasting_sweetness_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_sweetness_label"
                        android:visibility="gone"/>

                    <ProgressBar
                        android:id="@+id/show_tasting_sweetness"
                        style="?android:attr/progressBarStyleHorizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="3"
                        android:progress="0"
                        android:visibility="gone"/>

                    <!-- Acidity -->
                    <TextView
                        android:id="@+id/show_tasting_acidity_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_acidity_label"
                        android:visibility="gone"/>

                    <ProgressBar
                        android:id="@+id/show_tasting_acidity"
                        style="?android:attr/progressBarStyleHorizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="3"
                        android:progress="0"
                        android:visibility="gone"/>

                    <!-- Mouth feel -->
                    <TextView
                        android:id="@+id/show_tasting_mouthfeel_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_mouth_feel_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_mouthfeel"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:visibility="gone"
                        tools:text="stark mousierend"/>

                    <!-- Body-fullness -->
                    <TextView
                        android:id="@+id/show_tasting_body_fullness_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_body_fullness_label"
                        android:visibility="gone"/>

                    <ProgressBar
                        android:id="@+id/show_tasting_body_fullness"
                        style="?android:attr/progressBarStyleHorizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:max="3"
                        android:progress="0"
                        android:visibility="gone"/>

                    <!-- Body description -->
                    <TextView
                        android:id="@+id/show_tasting_body_description_label"
                        style="@style/cardLabelTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/vertical_margin_normal"
                        android:text="@string/show_tasting_body_description_label"
                        android:visibility="gone"/>

                    <TextView
                        android:id="@+id/show_tasting_body_description"
                        style="@style/cardDetailsTextView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:visibility="gone"
                        tools:text="irgendwie sämig"/>

                    <!-- Aftertaste -->
                    <de.retujo.bierverkostung.tasting.TastingComponentView
                        android:id="@+id/show_tasting_aftertaste_component_view"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingEnd="0dp"
                        android:paddingStart="-4dp"
                        android:visibility="gone"
                        custom:descriptionTextSize="14"
                        custom:labelText="@string/show_tasting_aftertaste_label"/>
                </LinearLayout>
            </android.support.v7.widget.CardView>

            <!-- This View is a heck which ensures that the bottom of the last card is shown, too. -->
            <View
                android:layout_width="0dp"
                android:layout_height="@dimen/vertical_margin_normal"
                android:layout_marginTop="@dimen/vertical_margin_normal"/>

        </LinearLayout>

    </ScrollView>

</LinearLayout>
