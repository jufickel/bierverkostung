/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Unit test for {@link CreateTableSqlBuilder}.
 *
 * @since 1.0.0
 */
public final class CreateTableSqlBuilderTest {

    private CreateTableSqlBuilder underTest = null;

    /** */
    @Before
    public void setUp() {
        underTest = CreateTableSqlBuilder.newInstance(BierverkostungContract.TastingEntry.TABLE);
    }

    /** */
    @Ignore
    @Test
    public void createSqlWithPrimaryKeyColumnOnly() {
        System.out.println(underTest.withPrimaryKeyColumn(BierverkostungContract.TastingEntry._ID).toString());
    }

}