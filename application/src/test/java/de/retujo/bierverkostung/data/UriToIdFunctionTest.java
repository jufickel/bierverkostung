/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.UUID;

import de.retujo.java.util.Maybe;

import static com.google.common.truth.Truth.assertThat;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 * Unit test for {@link UriToIdFunction}.
 *
 * @since 1.0.0
 */
public final class UriToIdFunctionTest {

    private UriToIdFunction underTest = null;
    private Uri baseUri = null;

    @Before
    public void setUp() {
        underTest = UriToIdFunction.getInstance();

        baseUri = Mockito.mock(Uri.class);
        Mockito.when(baseUri.getAuthority()).thenReturn("de.retujo.bierverkostung");
        Mockito.when(baseUri.getFragment()).thenReturn("content");
        Mockito.when(baseUri.getPathSegments()).thenReturn(Collections.emptyList());
    }

    @Test
    public void assertImmutability() {
        assertInstancesOf(UriToIdFunction.class, areImmutable());
    }

    @Test
    public void tryToGetIdFromNullUri() {
        try {
            underTest.apply(null);
        } catch (final RuntimeException e) {
            assertThat(e).hasMessage("The <URI> must not be null!");
            assertThat(e.getCause()).isNull();
            assertThat(e).isInstanceOf(NullPointerException.class);
        }
    }

    @Test
    public void getIdFromUriWithoutId() {
        final Maybe<UUID> idMaybe = underTest.apply(baseUri);

        assertThat(idMaybe.isAbsent()).isTrue();
    }

    @Test
    public void getIdFromUriWithId() {
        final UUID uuid = UUID.randomUUID();
        Mockito.when(baseUri.getPathSegments()).thenReturn(Collections.singletonList(uuid.toString()));
        final Maybe<UUID> idMaybe = underTest.apply(baseUri);

        assertThat(idMaybe.isPresent()).isTrue();
        assertThat(idMaybe.get()).isEqualTo(uuid);
    }


}