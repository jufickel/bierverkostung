/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 * Unit test for {@link CommaDelimiterTrimmer}.
 *
 * @since 1.0.0
 */
public final class CommaDelimiterTrimmerTest {

    /** */
    @Test
    public void assertImmutability() {
        assertInstancesOf(CommaDelimiterTrimmer.class, areImmutable());
    }

    /** */
    @Test
    public void trimNullString() {
        final String result = CommaDelimiterTrimmer.trim(null);

        assertThat(result).isNull();
    }

    /** */
    @Test
    public void trimEmptyString() {
        final String result = CommaDelimiterTrimmer.trim("");

        assertThat(result).isEmpty();
    }

    /** */
    @Test
    public void trimStringWithSingleWord() {
        final String input = "Wasser, ";
        final String result = CommaDelimiterTrimmer.trim(input);

        assertThat(result).isEqualTo("Wasser");
    }

    /** */
    @Test
    public void trimStringWithMultipleWords() {
        final String input = "Wasser, Gerstenmalz, Hopfenextrakt, ";
        final String result = CommaDelimiterTrimmer.trim(input);

        assertThat(result).isEqualTo("Wasser, Gerstenmalz, Hopfenextrakt");
    }

    /** */
    @Test
    public void trimStringWithSingleWordWithoutDelimiter() {
        final String input = "Wasser";
        final String result = CommaDelimiterTrimmer.trim(input);

        assertThat(result).isEqualTo(input);
    }

    /** */
    @Test
    public void trimStringWithMultipleWordsWithoutDelimiter() {
        final String input = "Wasser, Gerstenmalz, Hopfenextrakt";
        final String result = CommaDelimiterTrimmer.trim(input);

        assertThat(result).isEqualTo(input);
    }

    /** */
    @Test
    public void trimStringWithManySpacesAtTheEnd() {
        final String input = "Wasser,     ";
        final String result = CommaDelimiterTrimmer.trim(input);

        assertThat(result).isEqualTo("Wasser");
    }

}