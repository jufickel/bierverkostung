/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.exchange;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 * Unit test for {@link FileNameReviser}.
 *
 * @since 1.2.0
 */
public final class FileNameReviserTest {

    private FileNameReviser underTest;

    @Before
    public void setUp() {
        underTest = FileNameReviser.getInstance();
    }

    @Test
    public void assertImmutability() {
        assertInstancesOf(FileNameReviser.class, areImmutable());
    }

    @Test
    public void nullRemainsNull() {
        assertThat(underTest.apply(null)).isNull();
    }

    @Test
    public void spacesAreReplaced() {
        final String given = "This is a file name";
        final String expected = "This_is_a_file_name";

        final String actual = underTest.apply(given);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void numbersArePreserved() {
        final String given = "This is 1 file name";
        final String expected = "This_is_1_file_name";

        final String actual = underTest.apply(given);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void specialCharactersAreReplaced() {
        final String given = "This: is 1 file/name!";
        final String expected = "This__is_1_file_name_";

        final String actual = underTest.apply(given);

        assertThat(actual).isEqualTo(expected);
    }

}