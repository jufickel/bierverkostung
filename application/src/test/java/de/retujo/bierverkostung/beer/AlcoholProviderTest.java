/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import org.junit.Test;

import java.math.BigDecimal;

import static com.google.common.truth.Truth.assertThat;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 * Unit test for {@link AlcoholProvider}.
 *
 * @since 1.0.0
 */
public final class AlcoholProviderTest {

    private static final BigDecimal OFFSET_VALUE = BigDecimal.valueOf(0.02D);

    @Test
    public void assertImmutability() {
        assertInstancesOf(AlcoholProvider.class, areImmutable());
    }

    @Test
    public void calculateAlcohol1() {
        final double originalWort = 11.0D;
        final BigDecimal expectedResult = BigDecimal.valueOf(4.54D);

        final AlcoholProvider alcoholProvider = AlcoholProvider.newInstance(originalWort);
        final BigDecimal result = alcoholProvider.get();

        assertThat(result).isAtLeast(expectedResult.subtract(OFFSET_VALUE));
        assertThat(result).isAtMost(expectedResult.add(OFFSET_VALUE));
    }

    @Test
    public void calculateAlcohol2() {
        final double originalWort = 13.25D;
        final BigDecimal expectedResult = BigDecimal.valueOf(5.57D);

        final AlcoholProvider alcoholProvider = AlcoholProvider.newInstance(originalWort);
        final BigDecimal result = alcoholProvider.get();


        assertThat(result).isAtLeast(expectedResult.subtract(OFFSET_VALUE));
        assertThat(result).isAtMost(expectedResult.add(OFFSET_VALUE));
    }

}