/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import org.junit.Test;

import java.math.BigDecimal;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit test for {@link NumberFormatter}.
 *
 * @since 1.0.0
 */
public final class NumberFormatterTest {

    /** */
    @Test
    public void formatBigDecimalReturnsExpected() {
        final BigDecimal bigDecimal = BigDecimal.valueOf(4.534232D);
        final String expected = "4.53";
        final NumberFormatter underTest = NumberFormatter.newInstance(bigDecimal, 2);

        assertThat(underTest.get()).isEqualTo(expected);
    }

}