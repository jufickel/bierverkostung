/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.junit.Test;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.tasting.TastingBuilder.ImmutableTasting;
import nl.jqno.equalsverifier.EqualsVerifier;

import static org.mutabilitydetector.unittesting.AllowedReason.provided;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 * Unit test for {@link ImmutableTasting}.
 *
 * @since 1.0.0
 */
public final class ImmutableTastingTest {

    /** */
    @Test
    public void assertImmutability() {
        assertInstancesOf(ImmutableTasting.class,
                areImmutable(),
                provided(Beer.class, OpticalAppearance.class, Scent.class, Taste.class).areAlsoImmutable());
    }

    /** */
    @Test
    public void testHashCodeAndEquals() {
        EqualsVerifier.forClass(ImmutableTasting.class)
                .usingGetClass()
                .verify();
    }

}