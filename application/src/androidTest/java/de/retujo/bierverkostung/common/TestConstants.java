/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import de.retujo.bierverkostung.beer.BeerBuilder;
import de.retujo.bierverkostung.beerstyle.BeerStyleFactory;
import de.retujo.bierverkostung.brewery.BreweryFactory;
import de.retujo.bierverkostung.country.CountryFactory;

/**
 * Common constants for unit testing.
 *
 * @since 1.2.0
 */
public final class TestConstants {

    public static final class Country {

        public static final String GERMANY_NAME = "Deutschland";

        public static final String AUSTRIA_NAME = "Österreich";

        public static final String BELGIUM_NAME = "Belgien";

        public static final String ENGLAND_NAME = "England";

        public static final String CZECHIA_NAME = "Tschechien";

        public static final de.retujo.bierverkostung.country.Country GERMANY =
                CountryFactory.newCountry(GERMANY_NAME);

        public static final de.retujo.bierverkostung.country.Country AUSTRIA =
                CountryFactory.newCountry(AUSTRIA_NAME);

        public static final de.retujo.bierverkostung.country.Country BELGIUM =
                CountryFactory.newCountry(BELGIUM_NAME);

        public static final de.retujo.bierverkostung.country.Country ENGLAND =
                CountryFactory.newCountry(ENGLAND_NAME);

        public static final de.retujo.bierverkostung.country.Country CZECHIA =
                CountryFactory.newCountry(CZECHIA_NAME);

        private Country() {
            throw new AssertionError();
        }

    }

    public static final class Brewery {

        public static final String LOVE_CRAFT_NAME = "ACME";

        public static final String LOVE_CRAFT_LOCATION = "Providence";

        public static final de.retujo.bierverkostung.brewery.Brewery ACME =
                BreweryFactory.newBrewery(LOVE_CRAFT_NAME, LOVE_CRAFT_LOCATION, Country.ENGLAND);

        public static final String TITOR_NAME = "Hausbraxatur Titor";

        public static final String TITOR_LOCATION = "Germelshausen";

        public static final de.retujo.bierverkostung.brewery.Brewery TITOR =
                BreweryFactory.newBrewery(TITOR_NAME, TITOR_LOCATION, Country.GERMANY);

        private Brewery() {
            throw new AssertionError();
        }

    }

    public static final class BeerStyle {

        public static final de.retujo.bierverkostung.beerstyle.BeerStyle INDIA_PALE_ALE =
                BeerStyleFactory.newBeerStyle("India Pale Ale");

        public static final de.retujo.bierverkostung.beerstyle.BeerStyle RAUCHBIER =
                BeerStyleFactory.newBeerStyle("Rauchbier");

        private BeerStyle() {
            throw new AssertionError();
        }

    }

    public static final class Beer {

        public static final de.retujo.bierverkostung.beer.Beer GERMELSHAUSENER_PALE_ALE =
                BeerBuilder.newInstance("Germelshausener Pale Ale")
                        .alcohol("6.9")
                        .brewery(Brewery.TITOR)
                        .ibu(72)
                        .ingredients("Wasser, Pilsener Malz, Münchner Malz, Magnum, Cascade, Yellow Sub")
                        .originalWort("18.3")
                        .specifics("Hopfengestopft, unfiltriert")
                        .style(BeerStyle.INDIA_PALE_ALE)
                        .build();

        public static final de.retujo.bierverkostung.beer.Beer RAUCHBIER =
                BeerBuilder.newInstance("Rauchbier")
                        .alcohol("4.3")
                        .brewery(Brewery.ACME)
                        .ibu(27)
                        .ingredients("Wasser, Rauchmalz, Carafa Spezial 2, Magnum")
                        .originalWort("11.9")
                        .style(BeerStyle.RAUCHBIER)
                        .build();

        private Beer() {
            throw new AssertionError();
        }

    }

    private TestConstants() {
        throw new AssertionError();
    }

}
