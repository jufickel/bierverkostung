/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.*in
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.common;

import android.content.ContentValues;

import de.retujo.bierverkostung.data.Column;

/**
 * This class wraps a {@link ContentValues} object to allow access to its values by
 * {@link de.retujo.bierverkostung.data.Column}.
 * 
 * @since 1.1.0
 */
public final class TestContentValues {

    private final ContentValues actualContentValues;

    public TestContentValues(final ContentValues actualContentValues) {
        this.actualContentValues = actualContentValues;
    }

    public int getAsInteger(final Column column) {
        return actualContentValues.getAsInteger(column.toString());
    }

    public String getAsString(final Column column) {
        return actualContentValues.getAsString(column.toString());
    }

}
