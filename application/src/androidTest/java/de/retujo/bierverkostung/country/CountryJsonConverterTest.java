/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.support.test.runner.AndroidJUnit4;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter.DataEntityJsonName;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link CountryJsonConverter}.
 */
@RunWith(AndroidJUnit4.class)
public final class CountryJsonConverterTest {

    private static final String KNOWN_NAME = "Lecker Schmecker";

    private static JSONObject countryJsonObject;
    private static Country country;

    private CountryJsonConverter underTest;

    @BeforeClass
    public static void initTestConstants() throws JSONException {
        final EntityCommonData commonData = EntityCommonData.getInstance();

        countryJsonObject = new JSONObject();
        countryJsonObject.put(DataEntityJsonName.ID, commonData.getId().toString());
        countryJsonObject.put(DataEntityJsonName.REVISION, commonData.getRevision().getRevisionNumber());
        countryJsonObject.put(CountryJsonConverter.JsonName.NAME, KNOWN_NAME);

        country = CountryFactory.newCountry(commonData, KNOWN_NAME);
    }

    @Before
    public void setUp() {
        underTest = CountryJsonConverter.getInstance();
    }

    @Test
    public void convertCountryToJsonObject() {
        final JSONObject actualJsonObject = underTest.toJson(country);

        assertThat(actualJsonObject.toString()).isEqualTo(countryJsonObject.toString());
    }

    @Test
    public void convertJsonObjectToCountry() {
        final Country actualCountry = underTest.fromJson(countryJsonObject);

        assertThat(actualCountry).isEqualTo(country);
    }

}