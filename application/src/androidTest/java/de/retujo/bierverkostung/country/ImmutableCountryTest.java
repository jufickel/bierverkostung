/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.country;

import android.support.test.runner.AndroidJUnit4;

import com.google.common.truth.Truth;

import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.data.EntityCommonData;

/**
 * Unit test for {@link ImmutableCountry}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class ImmutableCountryTest {

    private static final String NAME = "Deutschland";
    private static final EntityCommonData COMMON_DATA = EntityCommonData.getInstance();

    @Test
    public void getNameReturnsExpected() {
        final ImmutableCountry underTest = ImmutableCountry.newInstance(COMMON_DATA, NAME);

        Truth.assertThat(underTest.getName()).isEqualTo(NAME);
    }

}