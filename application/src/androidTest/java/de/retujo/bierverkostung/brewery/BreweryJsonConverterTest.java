/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.brewery;

import android.support.test.runner.AndroidJUnit4;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.brewery.BreweryJsonConverter.JsonName;
import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.country.CountryFactory;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter.DataEntityJsonName;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link BreweryJsonConverter}.
 */
@RunWith(AndroidJUnit4.class)
public final class BreweryJsonConverterTest {

    private static final String KNOWN_NAME = "Wunderbräu";
    private static final String KNOWN_LOCATION = "Germelshausen";
    private static final Country KNOWN_COUNTRY = CountryFactory.newCountry("Petoria");

    private static EntityCommonData commonData;
    private static Brewery brewery;

    private JSONObject breweryJsonObject;
    private BreweryJsonConverter underTest;

    @BeforeClass
    public static void initTestConstants() {
        commonData = EntityCommonData.getInstance();

        brewery = BreweryFactory.newBrewery(commonData, KNOWN_NAME, KNOWN_LOCATION, KNOWN_COUNTRY);
    }

    @Before
    public void setUp() throws JSONException {
        breweryJsonObject = new JSONObject();
        breweryJsonObject.put(DataEntityJsonName.ID, commonData.getId().toString());
        breweryJsonObject.put(DataEntityJsonName.REVISION, commonData.getRevision().getRevisionNumber());
        breweryJsonObject.put(JsonName.NAME, KNOWN_NAME);
        breweryJsonObject.put(JsonName.LOCATION, KNOWN_LOCATION);
        breweryJsonObject.put(JsonName.COUNTRY, KNOWN_COUNTRY.toJson());

        underTest = BreweryJsonConverter.getInstance();
    }

    @Test
    public void convertBreweryToJsonObject() {
        final JSONObject actualJsonObject = underTest.toJson(brewery);

        assertThat(actualJsonObject.toString()).isEqualTo(breweryJsonObject.toString());
    }

    @Test
    public void convertBreweryWithoutCountryToJsonObject() {
        final String countryJsonName = JsonName.COUNTRY;
        breweryJsonObject.remove(countryJsonName);

        final JSONObject actualJsonObject = underTest.toJson(brewery.setCountry(null));

        assertThat(actualJsonObject.length()).isEqualTo(breweryJsonObject.length());
        assertThat(actualJsonObject.has(countryJsonName)).isFalse();
    }

    @Test
    public void convertBreweryWithoutLocationToJsonObject() {
        final String locationJsonName = JsonName.LOCATION;
        breweryJsonObject.remove(locationJsonName);

        final JSONObject actualJsonObject = underTest.toJson(brewery.setLocation(null));

        assertThat(actualJsonObject.length()).isEqualTo(breweryJsonObject.length());
        assertThat(actualJsonObject.has(locationJsonName)).isFalse();
    }

    @Test
    public void convertJsonObjectToBrewery() {
        final Brewery actualBrewery = underTest.fromJson(breweryJsonObject);

        assertThat(actualBrewery).isEqualTo(brewery);
    }

    @Test
    public void convertJsonObjectWithoutCountryToBrewery() {
        final Brewery breweryWithoutCountry = BreweryJsonConverterTest.brewery.setCountry(null);

        final Brewery actualBrewery = underTest.fromJson(breweryWithoutCountry.toJson());

        assertThat(actualBrewery).isEqualTo(breweryWithoutCountry);
    }

    @Test
    public void convertJsonObjectWithoutLocationToBrewery() {
        final Brewery breweryWithoutLocation = BreweryJsonConverterTest.brewery.setLocation(null);

        final Brewery actualBrewery = underTest.fromJson(breweryWithoutLocation.toJson());

        assertThat(actualBrewery).isEqualTo(breweryWithoutLocation);
    }

}