/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.beer.BeerBuilder.ImmutableBeer;

import static junit.framework.Assert.assertEquals;

/**
 * Android unit test for {@link ImmutableBeer}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class ImmutableBeerAndroidTest {

    private static final String BEER_NAME = "Forheimer Pale Ale";
    private static final String INGREDIENTS = "Wasser, Gerstenmalz, Hopfen, Hefe";
    private static final float ALCOHOL = 7.2F;
    private static final float ORIGINAL_WORT = 6.25F;
    private static final int IBU = 72;

    /** */
    @Test
    public void testParcelableImplementation() {
        final Beer beer = BeerBuilder.newInstance(BEER_NAME)
                .ingredients(INGREDIENTS)
                .alcohol(ALCOHOL)
                .originalWort(ORIGINAL_WORT)
                .ibu(IBU)
                .build();

        final Parcel parcel = Parcel.obtain();
        beer.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        final Beer fromParcel = ImmutableBeer.CREATOR.createFromParcel(parcel);

        assertEquals(beer, fromParcel);
    }

}
