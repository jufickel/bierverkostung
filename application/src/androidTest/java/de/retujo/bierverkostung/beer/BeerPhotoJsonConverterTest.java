/*
 * Copyright 2018 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.beer;

import android.support.test.runner.AndroidJUnit4;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import de.retujo.bierverkostung.beer.BeerPhotoJsonConverter.JsonName;
import de.retujo.bierverkostung.data.EntityCommonData;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter.DataEntityJsonName;
import de.retujo.bierverkostung.photo.PhotoFile;
import de.retujo.bierverkostung.photo.PhotoStatus;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link BeerPhotoJsonConverter}.
 */
@RunWith(AndroidJUnit4.class)
public final class BeerPhotoJsonConverterTest {

    private static final UUID KNOWN_BEER_ID = UUID.randomUUID();
    private static final PhotoStatus KNOWN_STATUS = PhotoStatus.EXISTING;

    private static PhotoFile knownPhotoFile;
    private static BeerPhoto beerPhoto;
    private static JSONObject beerPhotoJsonObject;

    private BeerPhotoJsonConverter underTest;

    @BeforeClass
    public static void initTestConstants() throws JSONException {
        knownPhotoFile = BeerFactory.newBeerPhotoFile("not_existing_file.jpg");

        final EntityCommonData commonData = EntityCommonData.getInstance();

        beerPhoto = BeerPhoto.getInstance(commonData, KNOWN_BEER_ID, knownPhotoFile, KNOWN_STATUS);

        beerPhotoJsonObject = new JSONObject();
        beerPhotoJsonObject.put(DataEntityJsonName.ID, commonData.getId().toString());
        beerPhotoJsonObject.put(DataEntityJsonName.REVISION, commonData.getRevision().getRevisionNumber());
        beerPhotoJsonObject.put(JsonName.BEER_ID, KNOWN_BEER_ID.toString());
        beerPhotoJsonObject.put(JsonName.PHOTO_FILE, knownPhotoFile.toJson());
        beerPhotoJsonObject.put(JsonName.STATUS, KNOWN_STATUS.toString());
    }

    @Before
    public void initJsonConverter() {
        underTest = BeerPhotoJsonConverter.getInstance();
    }

    @Test
    public void convertBeerPhotoToJsonObject() {
        final JSONObject actualJsonObject = underTest.toJson(beerPhoto);

        assertThat(actualJsonObject.toString()).isEqualTo(beerPhotoJsonObject.toString());
    }

    @Test
    public void convertJsonObjectToBeerPhoto() {
        final BeerPhoto actualBeerPhoto = underTest.fromJson(beerPhotoJsonObject);

        assertThat(actualBeerPhoto).isEqualTo(beerPhoto);
    }

}
