/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.java.util;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import de.retujo.java.util.Maybe;

/**
 * Asserts for {@link Maybe}.
 *
 * @since 1.1.0
 */
public final class MaybeAsserts {

    private MaybeAsserts() {
        throw new AssertionError();
    }

    public static <T> Matcher<Maybe<T>> contains(final T expectedValue) {
        return new TypeSafeMatcher<Maybe<T>>() {
            @Override
            public void describeTo(final Description description) {
                // Nothing to do
            }

            @Override
            protected boolean matchesSafely(final Maybe<T> item) {
                if (item.isPresent()) {
                    final T actualValue = item.get();
                    return actualValue.equals(expectedValue);
                }
                return false;
            }
        };
    }

    public static Matcher<Maybe<?>> isAbsent() {
        return new TypeSafeMatcher<Maybe<?>>() {
            @Override
            public void describeTo(final Description description) {
                // Nothing to do
            }

            @Override
            protected boolean matchesSafely(final Maybe<?> item) {
                return item.isAbsent();
            }
        };
    }

}
