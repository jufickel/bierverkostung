/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static junit.framework.Assert.assertSame;

/**
 * Unit test for {@link BierverkostungUriMatcher}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class BierverkostungUriMatcherTest {

    private BierverkostungUriMatcher underTest;

    @Before
    public void setUpd() {
        underTest = BierverkostungUriMatcher.getInstance();
    }

    @Test
    public void testMatcherIsSingleton() {
        final BierverkostungUriMatcher blue = BierverkostungUriMatcher.getInstance();
        final BierverkostungUriMatcher green = BierverkostungUriMatcher.getInstance();

        assertSame(blue, green);
    }

    @Test
    public void testMatchTastingsDirectoryUri() {
        final Uri contentUri = BierverkostungContract.TastingEntry.TABLE.getContentUri();

        assertSame(UriNode.TASTINGS, underTest.match(contentUri));
    }

    @Test
    public void testMatchSingleBreweryItem() {
        final Uri uri = BierverkostungContract.BreweryEntry.TABLE.getContentUri()
                .buildUpon()
                .appendPath(UUID.randomUUID().toString())
                .build();

        assertSame(UriNode.BREWERY_WITH_ID, underTest.match(uri));
    }

    @Test
    public void matchUnknownUri() {
        final Uri baseContentUri = BierverkostungContract.BASE_CONTENT_URI;

        assertSame(UriNode.NONE, underTest.match(baseContentUri));
    }

}