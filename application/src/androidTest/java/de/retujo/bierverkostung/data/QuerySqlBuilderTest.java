/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static de.retujo.bierverkostung.data.QuerySqlBuilder.getQualifiedNameAsAlias;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link QuerySqlBuilder}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class QuerySqlBuilderTest {

    private static final String TABLE_NAME_1 = "facts";
    private static final String TABLE_NAME_2 = "alternative_facts";
    private static final String COL_NAME_1 = "foo";
    private static final String COL_NAME_2 = "bar";
    private static final String COL_NAME_3 = "baz";

    private Table table1;
    private Table table2;
    private Column<String> column1;
    private Column<Integer> column2;
    private Column<String> column3;

    @Before
    public void setUp() {
        table1 = DefaultTable.getInstance(BierverkostungContract.BASE_CONTENT_URI, TABLE_NAME_1);
        column1 = table1.createStringColumn(COL_NAME_1);
        column2 = table1.createIntColumn(COL_NAME_2);

        table2 = DefaultTable.getInstance(BierverkostungContract.BASE_CONTENT_URI, TABLE_NAME_2);
        column3 = table2.createStringColumn(COL_NAME_3);
    }

    @Test
    public void queryWithSingleColumnProjectionOnlyCreatesExpected() {
        final String expectedString = "SELECT " + getQualifiedNameAsAlias(column1) +
                " FROM " + TABLE_NAME_1;

        final String actualString = QuerySqlBuilder.table(table1)
                .projection(column1)
                .toString();

        assertThat(actualString).isEqualTo(expectedString);
    }

    @Test
    public void leftOuterJoinCreatesExpected() {
        final Column<String> column4 = table2.createStringColumn(COL_NAME_1);

        final String expectedString = "SELECT " + getQualifiedNameAsAlias(column2) +
                " FROM " + table1 +
                " LEFT OUTER JOIN " + table2 +
                " ON " + column1.getQualifiedName() + "=" + column4.getQualifiedName();

        final String actualString = QuerySqlBuilder.table(table1)
                .leftOuterJoin(table2).on(column1, column4)
                .projection(column2)
                .toString();

        assertThat(actualString).isEqualTo(expectedString);
    }

    @Test
    public void setNullProjectionSetsAllColumnsOfAllTablesWithAliasedQualifiedName() {
        final String expectedString = "SELECT " + getQualifiedNameAsAlias(column1) +
                ", " + getQualifiedNameAsAlias(column2) +
                ", " + getQualifiedNameAsAlias(column3) +
                " FROM " + table1 + ", " + table2;

        final String actualString = QuerySqlBuilder.table(table1, table2)
                .projection(null)
                .toString();

        assertThat(actualString).isEqualTo(expectedString);
    }

    @Test
    public void queryWithOrderByFilterCreatesExpected() {
        final String sortOrder = column2 + " ASC";

        final String expectedString = "SELECT *"
                + " FROM " + table1
                + " ORDER BY " + sortOrder;

        final String actualString = QuerySqlBuilder.table(table1)
                .sortOrder(sortOrder)
                .toString();

        assertThat(actualString).isEqualTo(expectedString);
    }

    @Test
    public void queryWithDistinctFilterCreatesExpected() {
        final String expected = "SELECT DISTINCT " + getQualifiedNameAsAlias(column3) +
                " FROM " + table2;

        final String actualString = QuerySqlBuilder.table(table2)
                .projection(column3)
                .distinct()
                .toString();

        assertThat(actualString).isEqualTo(expected);
    }

}