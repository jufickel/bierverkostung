/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.beer.Beer;
import de.retujo.bierverkostung.beerstyle.BeerStyle;
import de.retujo.bierverkostung.brewery.Brewery;
import de.retujo.bierverkostung.common.TestConstants;
import de.retujo.bierverkostung.country.Country;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BeerStyleEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.BreweryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.CountryEntry;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link BierverkostungContentProvider}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class BierverkostungContentProviderTest {

    private Context appContext = null;
    private SQLiteOpenHelper dbHelper = null;
    private ContentResolver contentResolver = null;

    @Before
    public void setUp() {
        appContext = InstrumentationRegistry.getTargetContext();
        contentResolver = appContext.getContentResolver();
        dbHelper = new BierverkostungDbHelper(appContext);

        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(TastingEntry.TABLE.toString(), null, null);
        database.delete(BeerEntry.TABLE.toString(), null, null);
        database.delete(BreweryEntry.TABLE.toString(), null, null);
        database.delete(CountryEntry.TABLE.toString(), null, null);
        database.delete(BeerStyleEntry.TABLE.toString(), null, null);
    }

    @After
    public void tearDown() {
        dbHelper.close();
    }

    /**
     * This test checks to make sure that the content provider is registered correctly in the
     * AndroidManifest file. If it fails, the AndroidManifest should be checked to see if the
     * <provider/> tag is dded and that the android:authorities attribute is properly specified.
     */
    @Test
    public void testProviderRegistry() {

        /*
         * A ComponentName is an identifier for a specific application component, such as an
         * Activity, ContentProvider, BroadcastReceiver, or a Service.
         *
         * Two pieces of information are required to identify a component: the package (a String)
         * it exists in, and the class (a String) name inside of that package.
         *
         * We will use the ComponentName for our ContentProvider class to ask the system
         * information about the ContentProvider, specifically, the authority under which it is
         * registered.
         */
        final String packageName = appContext.getPackageName();
        final String contentProviderClassName = BierverkostungContentProvider.class.getName();
        final ComponentName componentName = new ComponentName(packageName, contentProviderClassName);

        try {

            /*
             * Get a reference to the package manager. The package manager allows us to access
             * information about packages installed on a particular device. In this case, we're
             * going to use it to get some information about our ContentProvider under test.
             */
            final PackageManager packageManager = appContext.getPackageManager();

            /* The ProviderInfo will contain the authority, which is what we want to test */
            final ProviderInfo providerInfo = packageManager.getProviderInfo(componentName, 0);
            final String actualAuthority = providerInfo.authority;
            final String expectedAuthority = packageName;

            /* Make sure that the registered authority matches the authority from the Contract */
            final String incorrectAuthority = "BierverkostungContentProvider registered with authority: "
                    + actualAuthority + " instead of expected authority: " + expectedAuthority;
            assertEquals(incorrectAuthority, actualAuthority, expectedAuthority);
        } catch (final PackageManager.NameNotFoundException e) {
            final String providerNotRegisteredAtAll = "BierverkostungContentProvider not registered at "
                    + appContext.getPackageName();
            /*
             * This exception is thrown if the ContentProvider hasn't been registered with the
             * manifest at all. If this is the case, you need to double check your
             * AndroidManifest file
             */
            fail(providerNotRegisteredAtAll);
        }
    }

    @Test
    public void insertOneBeerStyleWorksAsExpected() {
        final BeerStyle beerStyle = TestConstants.BeerStyle.INDIA_PALE_ALE;
        final Uri contentUri = beerStyle.getContentUri();

        final Uri uri = contentResolver.insert(contentUri, beerStyle.asContentValues());

        assertThat(uri).isEqualTo(contentUri);
    }

    @Test
    public void insertSameBeerStyleTwiceWorksAsExpected() {
        final BeerStyle beerStyle = TestConstants.BeerStyle.RAUCHBIER;
        final Uri beerStyleContentUri = beerStyle.getContentUri();
        final ContentValues contentValues = beerStyle.asContentValues();

        final Uri uriAfterFirstInsert = contentResolver.insert(beerStyleContentUri, contentValues);
        final Uri uriAfterSecondInsert = contentResolver.insert(beerStyleContentUri, contentValues);

        assertThat(uriAfterFirstInsert).isEqualTo(beerStyleContentUri);
        assertThat(uriAfterSecondInsert).isEqualTo(beerStyleContentUri);
    }

    /**
     * Tests inserting a single row of data via a ContentResolver into Countries table.
     */
    @Test
    public void insertOneCountryWorksAsExpected() {
        final Country country = TestConstants.Country.GERMANY;
        final Uri contentUri = country.getContentUri();

        final Uri uri = contentResolver.insert(contentUri, country.asContentValues());

        assertThat(uri).isEqualTo(contentUri);
    }

    @Test
    public void insertSameCountryTwiceDoesNotConflict() {
        final Country country = TestConstants.Country.CZECHIA;
        final Uri countryContentUri = country.getContentUri();
        final ContentValues contentValues = country.asContentValues();

        final Uri uriAfterFirstInsert = contentResolver.insert(countryContentUri, contentValues);
        final Uri uriAfterSecondInsert = contentResolver.insert(countryContentUri, contentValues);

        assertThat(uriAfterFirstInsert).isEqualTo(countryContentUri);
        assertThat(uriAfterSecondInsert).isEqualTo(countryContentUri);
    }

    @Test
    public void insertOneBreweryWorksAsExpected() {
        final Uri contentUri = TestConstants.Brewery.ACME.getContentUri();
        final ContentValues contentValues = TestConstants.Brewery.ACME.asContentValues();

        final Uri uri = contentResolver.insert(contentUri, contentValues);

        assertThat(uri).isEqualTo(contentUri);
    }

    @Test
    public void insertOneBeerWorksAsExpected() {
        final Beer beer = TestConstants.Beer.RAUCHBIER;
        final Uri contentUri = beer.getContentUri();

        final Uri uri = contentResolver.insert(contentUri, beer.asContentValues());

        assertThat(uri).isEqualTo(contentUri);
    }

    @Test
    public void queryBrewery() {
        final Brewery brewery = TestConstants.Brewery.ACME;
        final String breweryName = brewery.getName();
        final String breweryLocation = TestConstants.Brewery.LOVE_CRAFT_LOCATION;

        final String countryId = brewery.getCountry().map(Country::getId).map(String::valueOf).get();

        final ContentResolver contentResolver = appContext.getContentResolver();
        contentResolver.insert(brewery.getContentUri(), brewery.asContentValues());

        try (final Cursor c = contentResolver.query(BreweryEntry.TABLE.getContentUri(), null, null, null, null)) {
            assertThat(c).isNotNull();
            assertThat(c.moveToFirst()).isTrue();

            final CursorReader cursorReader = CursorReader.of(c);
            assertThat(cursorReader.getString(BreweryEntry.COLUMN_ID)).isEqualTo(brewery.getId().toString());
            assertThat(cursorReader.getString(BreweryEntry.COLUMN_NAME)).isEqualTo(breweryName);
            assertThat(cursorReader.getString(BreweryEntry.COLUMN_LOCATION)).isEqualTo(breweryLocation);
            assertThat(cursorReader.getString(BreweryEntry.COLUMN_COUNTRY_ID)).isEqualTo(countryId);
        }
    }

}
