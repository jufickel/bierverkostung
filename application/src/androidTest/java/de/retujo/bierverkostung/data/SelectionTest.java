/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.data;

import android.support.test.runner.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Unit test for {@link Selection}.
 *
 * @since 1.2.0
 */
@RunWith(AndroidJUnit4.class)
public final class SelectionTest {

    private static final String BAR_SELECTION_ARG = "Bar";

    private static Table table;
    private static Column column;

    @BeforeClass
    public static void setUp() {
        table = DefaultTable.getInstance(BierverkostungContract.BASE_CONTENT_URI, "MyTable");
        column = ImmutableColumn.newInstance(table, "Foo", ColumnType.TEXT);
    }

    @Test
    public void tryToGetBuilderInstanceWithEmptyColumn() {
        try {
            Selection.where(null);
            fail("Expected a NullPointerException");
        } catch (final NullPointerException e) {
            assertThat(e).hasMessage("The <selection column> must not be null!").hasNoCause();
        }
    }

    @Test
    public void selectionStringForNullArgumentIsExpected() {
        final String expected = column.getQualifiedName() + " is null";

        final Selection selection = Selection.where(column).is(null).build();

        assertThat(selection.getSelectionString()).isEqualTo(expected);
    }

    @Test
    public void selectionStringForSingleSelectionIsExpected() {
        final String expected = column.getQualifiedName() + "=?";

        final Selection selection = Selection.where(column).is(BAR_SELECTION_ARG).build();

        assertThat(selection.getSelectionString()).isEqualTo(expected);
    }

    @Test
    public void selectionArgForSingleSelectionIsExpected() {
        final String[] expected = new String[]{BAR_SELECTION_ARG};

        final Selection selection = Selection.where(column).is(BAR_SELECTION_ARG).build();

        assertThat(selection.getSelectionArgs()).isEqualTo(expected);
    }

    @Test
    public void selectionStringForTwoSelectionsIsExpected() {
        final Column<Integer> temperatureColumn = table.createIntColumn("Temperature");
        final int temperatureValueArg = 23;
        final String expected = column.getQualifiedName() + "=?" +
                " AND " + temperatureColumn.getQualifiedName() + "=?";

        final Selection selection = Selection.where(column).is(BAR_SELECTION_ARG)
                .andWhere(temperatureColumn).is(temperatureValueArg)
                .build();

        assertThat(selection.getSelectionString()).isEqualTo(expected);
    }

    @Test
    public void selectionArgsForTwoSelectionsAreExpected() {
        final Column<Integer> temperatureColumn = table.createIntColumn("Temperature");
        final int temperatureValueArg = 23;
        final String[] expected = new String[]{BAR_SELECTION_ARG, String.valueOf(temperatureValueArg)};

        final Selection selection = Selection.where(column).is(BAR_SELECTION_ARG)
                .andWhere(temperatureColumn).is(temperatureValueArg)
                .build();

        assertThat(selection.getSelectionArgs()).isEqualTo(expected);
    }

}