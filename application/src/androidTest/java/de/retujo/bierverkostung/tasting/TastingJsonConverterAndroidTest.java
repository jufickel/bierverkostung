/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import de.retujo.bierverkostung.common.TestConstants;
import de.retujo.bierverkostung.exchange.DataEntityJsonConverter;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link TastingJsonConverter}.
 *
 * @since 1.2.0
 */
public final class TastingJsonConverterAndroidTest {

    private static final UUID TASTING_ID = UUID.randomUUID();
    private static final String DATE = "2017-02-15T174800";
    private static final String TASTING_LOCATION = "Germelshausen";

    private static final OpticalAppearance OPTICAL_APPEARANCE = OpticalAppearanceBuilder.getInstance()
            .beerClarityDescription("crystal clear")
            .beerColour("gold")
            .beerColourDescription("brilliant")
            .ebc(4)
            .foamColour("white")
            .foamStability(2)
            .foamStructureDescription("dense")
            .build();

    private static final Scent SCENT = ScentBuilder.getInstance()
            .fruit(ImmutableTastingComponent.of("tropical fruit", 3))
            .warmthMinted(ImmutableTastingComponent.of("malty", 1))
            .build();

    private static final Taste TASTE = TasteBuilder.getInstance()
            .acidityRating(0)
            .bitternessRating(3)
            .fullBodiedRating(1)
            .sweetnessRating(1)
            .bodyDescription("smooth")
            .aftertaste(ImmutableTastingComponent.of("long", 3))
            .mouthfeelDescription("fizzy")
            .build();

    private static final String FOOD_RECOMMENDATION = "Hamburger, Pizza";
    private static final String TOTAL_IMPRESSION_DESCRIPTION = "This is a very good beer.";
    private static final int TOTAL_IMPRESSION_RATING = 3;

    private static final Tasting TASTING = TastingBuilder.newInstance(DATE, TestConstants.Beer.GERMELSHAUSENER_PALE_ALE)
            .id(TASTING_ID)
            .location(TASTING_LOCATION)
            .opticalAppearance(OPTICAL_APPEARANCE)
            .scent(SCENT)
            .taste(TASTE)
            .foodRecommendation(FOOD_RECOMMENDATION)
            .totalImpressionDescription(TOTAL_IMPRESSION_DESCRIPTION)
            .totalImpressionRating(TOTAL_IMPRESSION_RATING)
            .build();

    private TastingJsonConverter underTest;

    @Before
    public void setUp() {
        underTest = TastingJsonConverter.getInstance();
    }

    @Test
    public void toJsonReturnsExpectedJson() throws JSONException {
        final JSONObject expectedJson = new JSONObject();
        expectedJson.put(DataEntityJsonConverter.DataEntityJsonName.ID, TASTING_ID);
        expectedJson.put(DataEntityJsonConverter.DataEntityJsonName.REVISION, 0);
        expectedJson.put(TastingJsonConverter.JsonName.DATE, DATE);
        expectedJson.put(TastingJsonConverter.JsonName.BEER, TASTING.getBeer().toJson());
        expectedJson.put(TastingJsonConverter.JsonName.LOCATION, TASTING_LOCATION);
        expectedJson.put(TastingJsonConverter.JsonName.OPTICAL_APPEARANCE, OPTICAL_APPEARANCE.toJson());
        expectedJson.put(TastingJsonConverter.JsonName.SCENT, SCENT.toJson());
        expectedJson.put(TastingJsonConverter.JsonName.TASTE, TASTE.toJson());
        expectedJson.put(TastingJsonConverter.JsonName.FOOD_RECOMMENDATION, FOOD_RECOMMENDATION);
        expectedJson.put(TastingJsonConverter.JsonName.TOTAL_IMPRESSION_DESCRIPTION, TOTAL_IMPRESSION_DESCRIPTION);
        expectedJson.put(TastingJsonConverter.JsonName.TOTAL_IMPRESSION_RATING, TOTAL_IMPRESSION_RATING);

        final JSONObject actualJson = underTest.toJson(TASTING);

        assertThat(actualJson.toString()).isEqualTo(expectedJson.toString());
    }

    @Test
    public void fromJsonReturnsExpectedTasting() {
        final JSONObject actualJson = underTest.toJson(TASTING);
        final Tasting deserializedTasting = underTest.fromJson(actualJson);

        assertThat(deserializedTasting).isEqualTo(TASTING);
    }

}