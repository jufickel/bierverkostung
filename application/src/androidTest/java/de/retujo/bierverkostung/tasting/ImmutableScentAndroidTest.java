/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.tasting.ScentBuilder.ImmutableScent;

import static junit.framework.Assert.assertEquals;

/**
 * Android unit test for {@link ImmutableScent}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class ImmutableScentAndroidTest {

    private static final TastingComponent FRUIT = ImmutableTastingComponent.of("Rote Beeren", 1);
    private static final TastingComponent FLOWER = ImmutableTastingComponent.of("Veilchen", 2);
    private static final TastingComponent VEGETAL = ImmutableTastingComponent.empty();
    private static final TastingComponent SPICY = ImmutableTastingComponent.of("Bleen", 0);
    private static final TastingComponent WARMTH_MINTED = ImmutableTastingComponent.of("Lebkuchen, Foo", 3);
    private static final TastingComponent BIOLOGICAL = ImmutableTastingComponent.empty();

    /** */
    @Test
    public void testParcelableImplementation() {
        final Scent scent = ScentBuilder.getInstance()
                .fruit(FRUIT)
                .flower(FLOWER)
                .vegetal(VEGETAL)
                .spicy(SPICY)
                .warmthMinted(WARMTH_MINTED)
                .biological(BIOLOGICAL)
                .build();

        final Parcel parcel = Parcel.obtain();
        scent.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        final Scent fromParcel = ImmutableScent.CREATOR.createFromParcel(parcel);

        assertEquals(scent, fromParcel);
    }

}
