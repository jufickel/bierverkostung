/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

/**
 * Android unit test for {@link ImmutableTastingComponent}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class ImmutableTastingComponentAndroidTest {

    /** */
    @Test
    public void testParcelableImplementationForEmptyComponent() {
        final ImmutableTastingComponent empty = ImmutableTastingComponent.empty();

        final Parcel parcel = Parcel.obtain();
        empty.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        final TastingComponent fromParcel = ImmutableTastingComponent.CREATOR.createFromParcel(parcel);

        assertEquals(empty, fromParcel);
    }

    /** */
    @Test
    public void testParcelableImplementationForComponent() {
        final ImmutableTastingComponent underTest = ImmutableTastingComponent.of("description,", 2);

        final Parcel parcel = Parcel.obtain();
        underTest.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        final TastingComponent fromParcel = ImmutableTastingComponent.CREATOR.createFromParcel(parcel);

        assertEquals(underTest, fromParcel);
    }

}
