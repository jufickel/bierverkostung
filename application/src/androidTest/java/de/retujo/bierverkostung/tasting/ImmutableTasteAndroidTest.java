/*
 * Copyright 2017 Juergen Fickel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.retujo.bierverkostung.tasting;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import de.retujo.bierverkostung.common.TestContentValues;
import de.retujo.bierverkostung.data.BierverkostungContract.TastingEntry;
import de.retujo.bierverkostung.tasting.TasteBuilder.ImmutableTaste;

import static de.retujo.bierverkostung.java.util.MaybeAsserts.contains;
import static de.retujo.bierverkostung.java.util.MaybeAsserts.isAbsent;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Android unit test for {@link ImmutableTaste}.
 *
 * @since 1.0.0
 */
@RunWith(AndroidJUnit4.class)
public final class ImmutableTasteAndroidTest {

    private static final int BITTERNESS_RATING = 2;
    private static final int SWEETNESS_RATING = 1;
    private static final int ACIDITY_RATING = 0;
    private static final String BODY_DESCRIPTION = "weich, voll, usw.";
    private static final int FULL_BODIED_RATING = 3;
    private static final String MOUTHFEEL_DESCRIPTION = null;
    private static final String AFTERTASTE_DESCRIPTION = "fulminant";
    private static final int AFTERTASTE_RATING = 5;
    private static final TastingComponent AFTERTASTE =
            ImmutableTastingComponent.of(AFTERTASTE_DESCRIPTION, AFTERTASTE_RATING);

    /** */
    @Test
    public void testParcelableImplementation() {
        final Taste taste = TasteBuilder.getInstance()
                .bitternessRating(BITTERNESS_RATING)
                .sweetnessRating(SWEETNESS_RATING)
                .acidityRating(ACIDITY_RATING)
                .bodyDescription(BODY_DESCRIPTION)
                .fullBodiedRating(FULL_BODIED_RATING)
                .mouthfeelDescription(MOUTHFEEL_DESCRIPTION)
                .aftertaste(AFTERTASTE)
                .build();

        final Parcel parcel = Parcel.obtain();
        taste.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        final Taste fromParcel = ImmutableTaste.CREATOR.createFromParcel(parcel);

        assertEquals(taste, fromParcel);
    }

    /** */
    @Test
    public void gettersReturnExpectedValues() {
        final int acidityRating = 23;
        final String mouthFeelDescription = "oily";

        final Taste underTest = TasteBuilder.getInstance()
                .bitternessRating(BITTERNESS_RATING)
                .sweetnessRating(SWEETNESS_RATING)
                .acidityRating(acidityRating)
                .bodyDescription(BODY_DESCRIPTION)
                .fullBodiedRating(FULL_BODIED_RATING)
                .mouthfeelDescription(mouthFeelDescription)
                .aftertaste(AFTERTASTE)
                .build();

        assertThat(underTest.getSweetnessRating(), contains(SWEETNESS_RATING));
        assertThat(underTest.getAcidityRating(), contains(acidityRating));
        assertThat(underTest.getBodyDescription(), contains(BODY_DESCRIPTION));
        assertThat(underTest.getFullBodiedRating(), contains(FULL_BODIED_RATING));
        assertThat(underTest.getMouthfeelDescription(), contains(mouthFeelDescription));
        assertEquals(AFTERTASTE, underTest.getAftertaste());
    }

    /** */
    @Test
    public void gettersOfEmptyImmutableTasteReturnExpected() {
        final Taste underTest = TasteBuilder.getInstance().build();

        assertThat(underTest.getBitternessRating(), isAbsent());
        assertThat(underTest.getSweetnessRating(), isAbsent());
        assertThat(underTest.getAcidityRating(), isAbsent());
        assertThat(underTest.getBodyDescription(), isAbsent());
        assertThat(underTest.getFullBodiedRating(), isAbsent());
        assertThat(underTest.getMouthfeelDescription(), isAbsent());
        assertEquals(ImmutableTastingComponent.empty(), underTest.getAftertaste());
    }

    /** */
    @Test
    public void asContentValuesReturnsExpected() {
        final Taste underTest = TasteBuilder.getInstance()
                .bitternessRating(BITTERNESS_RATING)
                .sweetnessRating(SWEETNESS_RATING)
                .acidityRating(ACIDITY_RATING)
                .bodyDescription(BODY_DESCRIPTION)
                .fullBodiedRating(FULL_BODIED_RATING)
                .mouthfeelDescription(MOUTHFEEL_DESCRIPTION)
                .aftertaste(AFTERTASTE)
                .build();

        final TestContentValues contentValues = new TestContentValues(underTest.asContentValues());

        assertEquals(BITTERNESS_RATING, contentValues.getAsInteger(TastingEntry.COLUMN_BITTERNESS_RATING));
        assertEquals(SWEETNESS_RATING, contentValues.getAsInteger(TastingEntry.COLUMN_SWEETNESS_RATING));
        assertEquals(ACIDITY_RATING, contentValues.getAsInteger(TastingEntry.COLUMN_ACIDITY_RATING));
        assertEquals(BODY_DESCRIPTION, contentValues.getAsString(TastingEntry.COLUMN_BODY_DESCRIPTION));
        assertEquals(FULL_BODIED_RATING, contentValues.getAsInteger(TastingEntry.COLUMN_FULL_BODIED_RATING));
        assertEquals(MOUTHFEEL_DESCRIPTION, contentValues.getAsString(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION));
        assertEquals(AFTERTASTE_DESCRIPTION, contentValues.getAsString(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION));
        assertEquals(AFTERTASTE_RATING, contentValues.getAsInteger(TastingEntry.COLUMN_AFTERTASTE_RATING));
    }

    /** */
    @Test
    public void asContentValuesOfEmptyTasteReturnsExpected() {
        final Taste underTest = TasteBuilder.getInstance().build();

        final TestContentValues contentValues = new TestContentValues(underTest.asContentValues());

        assertEquals(0, contentValues.getAsInteger(TastingEntry.COLUMN_BITTERNESS_RATING));
        assertEquals(0, contentValues.getAsInteger(TastingEntry.COLUMN_SWEETNESS_RATING));
        assertEquals(0, contentValues.getAsInteger(TastingEntry.COLUMN_ACIDITY_RATING));
        assertNull(contentValues.getAsString(TastingEntry.COLUMN_BODY_DESCRIPTION));
        assertEquals(0, contentValues.getAsInteger(TastingEntry.COLUMN_FULL_BODIED_RATING));
        assertNull(contentValues.getAsString(TastingEntry.COLUMN_MOUTHFEEL_DESCRIPTION));
        assertNull(contentValues.getAsString(TastingEntry.COLUMN_AFTERTASTE_DESCRIPTION));
        assertEquals(0, contentValues.getAsInteger(TastingEntry.COLUMN_AFTERTASTE_RATING));
    }

}
