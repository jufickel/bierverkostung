# Changelog

## Version 1.2.1

  * Fixed bug which prevented to add photos to a beer for API > 23 ([#74][#74]).
  
[#74]: https://bitbucket.org/jufickel/bierverkostung/issues/74/implement-a-fileprovider-to-show-photos-of

## Version 1.2.0

  * Background colour of EBC values tries to match the same as the actual beer colour ([#55][#55]).
  * Added options for sorting the items of the tastings overview ([#63][#63]).
  * Added functionality to add or remove photos to or from a beer ([#67][#67]).
  * Ask before discarding unsaved changes of beers and tastings ([#69][#69]).
  * Added fine-grained export and import ([#70][#70]). The database schema changed for this one, database migration 
    was necessary.

[#55]: https://bitbucket.org/jufickel/bierverkostung/issues/55/set-background-for-each-ebc-value-item
[#63]: https://bitbucket.org/jufickel/bierverkostung/issues/63/allow-different-sorting-of-tasting
[#67]: https://bitbucket.org/jufickel/bierverkostung/issues/67/image-s-for-beer
[#69]: https://bitbucket.org/jufickel/bierverkostung/issues/69/prevent-accidental-cancellation-of
[#70]: https://bitbucket.org/jufickel/bierverkostung/issues/70/smart-export-and-import


## Version 1.1.0

  * Prevent deletion of a beer which is referenced by at least one tasting ([#45][#45]).
  * Prevent deletion of a brewery which is referenced by at least one beer ([#46][#46]).
  * Fixed bug: When editing any of a tasting's view components the dialogue does not show the already set properties 
    but appears empty.
  * Changed German wording for sweetness.
  * Fixed bug: When editing a tasting, the persisted sweetness is not applied.
  * Show suggestions for beer colour when editing a tasting ([#57][#57]).
  * Added unit tests.
  * Show suggestions for beer clarity when editing a tasting ([#60][#60]).
  * Show suggestions for mouth feel description when editing a tasting ([#62][#62]).
  * Show suggestions for body description/texture when editing a tasting ([#64][#64]).
  * Show suggestions for foam structure when editing a tasting ([#59][#59]).

[#45]: https://bitbucket.org/jufickel/bierverkostung/issues/45/prevent-deletion-of-beer-if-used-in-a
[#46]: https://bitbucket.org/jufickel/bierverkostung/issues/46/prevent-deletion-of-brewery-if-associated
[#57]: https://bitbucket.org/jufickel/bierverkostung/issues/57/tasting-show-suggestions-for-beer-colours
[#60]: https://bitbucket.org/jufickel/bierverkostung/issues/60/tasting-show-suggestions-for-beer-clarity
[#62]: https://bitbucket.org/jufickel/bierverkostung/issues/62/tasting-show-suggestions-for-mouthfeel
[#64]: https://bitbucket.org/jufickel/bierverkostung/issues/64/tasting-show-suggestions-for-body
[#59]: https://bitbucket.org/jufickel/bierverkostung/issues/59/tasting-show-suggestions-for-foam
