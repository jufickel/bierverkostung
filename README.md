# Bierverkostung

## Inhalt / Synopsis

[TOC]

## Was ist das?

Bei „Bierverkosung“ handelt sich um eine *Android*-Anwendung zum Erfassen von persönlchen Bierverkostungen.
Dazu gehört das Anlegen, Ändern und ggf. Löschen von

  * Bieren,
  * Brauereien,
  * Bierstilen und natürlich
  * Verkostungen.

Daneben ist es möglich, die komplette Datenbank zu exportieren und wieder zu importieren.
Dadurch können die Daten einfach auf ein anderes Gerät migriert werden.

Alle Daten befinden sich lokal auf dem Telefon.
Eine Internetverbindung ist deshalb nicht notwendig.


## Bildschirmabzüge

![Verkostungsübersicht](screenshots/Verkostungen_klein.png)
![Verkostung anzeigen 1/3](screenshots/Verkostung_anzeigen-01_klein.png)
![Navigationsansicht](screenshots/Navigation_klein.png)
![Verkostung bearbeiten 2/4](screenshots/Verkostung_bearbeiten-02_klein.png)


## API-Referenz

Minimum-SDK: 18 (Android-Version 4.3)
Ziel-SDK: 25 (Android-Version 7.1.1)


## Installation

Bierverkostung kann auf zwei Arten installiert werden:

  1. direkt mittels [APK-Datei](https://bitbucket.org/jufickel/bierverkostung/downloads/) oder
  2. indirekt über [F-Droid.](https://f-droid.org/repository/browse/?fdfilter=Bierverkostung&fdid=de.retujo.bierverkostung)

In beiden Fällen muß die Installation aus „unsicheren Quellen“ auf dem Handy eingeschaltet werden.
Grund hierfür ist, daß Android nur Programme aus dem Google-Play-Store als „sicher“ einstuft.

F-Droid ist eine Ergänzung zum Google-Play-Store.
Dort sind ausschließlich Open-Source-Programme verfügbar und die Benutzer werden nicht überwacht.
Der Vorteil einer Installation über F-Droid ist, daß *Updates automatisch angezeigt* werden.


## Beitragende

Anwendungs-Logo: [drunken_duck (via openClipart.org)](https://openclipart.org/detail/2214/beer)

---

# Bierverkostung (Beer Tasting)

## What is it?

"Bierverkostung" is an *Android* application for managing personal beer tastings.
Its functionality comprises the addition, changing and, if necessary, the deletion of
 
  * beers,
  * breweries,
  * beer styles and of course
  * tastings.
  
Besides the database can be exported and imported as a whole
This makes it possible to easily migrate the application to another phone - and all without using Blockchain 
Technology. ;-)

All data resides locally on the phone.
An internet connection is thus not required.

The application is fully translated into English.


## API Reference

Minimum SDK: 18 (Android version 4.3)
Target SDK: 25 (Android version 7.1.1)


## Installation

There are two ways to install Bierverkostung:

  1. directly with the [APK file](https://bitbucket.org/jufickel/bierverkostung/downloads/) or
  2. indirectly via [F-Droid.](https://f-droid.org/repository/browse/?fdfilter=Bierverkostung&fdid=de.retujo.bierverkostung)

In both cases the phone must be configured to allow installation from "insecure sources".
This is disabled by default as only apps from the Google Play Store are considered to be secure by Android.

F-Droid is an additional repository for Android apps.
It contains only open source programmes and does not track its users.
The main advantage of installation via F-Droid is that you will *receive updates automatically*.


## Contributors

Application Icon: [drunken_duck (via openClipart.org)](https://openclipart.org/detail/2214/beer)

---

# Lizenz/Licence

Copyright 2017 Juergen Fickel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
